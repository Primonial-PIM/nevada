SELECT REFCON, OPCVM, SICOVAM, to_char(DATENEG, 'YYYY-MM-DD') AS DATENEG, to_char(DATEVAL, 'YYYY-MM-DD') AS DATEVAL, DEVISEPAY AS SETTLEMENT_CCY, -(CASE WHEN (QUANTITE = 0) AND (TYPE = 1) THEN 0 ELSE MONTANT END) AS CONSIDERATION, VERSION
FROM HISTOMVTS
WHERE REFCON = :refcon
UNION ALL
SELECT REFCON, OPCVM, SICOVAM, to_char(DATENEG, 'YYYY-MM-DD') AS DATENEG, to_char(DATEVAL, 'YYYY-MM-DD') AS DATEVAL, DEVISEPAY AS SETTLEMENT_CCY, -(CASE WHEN (QUANTITE = 0) AND (TYPE = 1) THEN 0 ELSE MONTANT END) AS CONSIDERATION, VERSION
FROM audit_mvt
WHERE REFCON = :refcon
ORDER BY VERSION
