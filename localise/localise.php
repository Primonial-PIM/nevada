<?php
/***
 * User: npennin
 * Date: 20/06/12
 * Time: 15:16
 */
date_default_timezone_set("Europe/Paris");
if ((!defined('APPLICATION_ROOT')) || (strlen(APPLICATION_ROOT) <= 0))
  {
//  define('APPLICATION_ROOT', '/var/www/FundStat');
  define('APPLICATION_ROOT', '/var/www/nevada');
  }

if ((!defined('WEB_ROOT')) || (strlen(WEB_ROOT) <= 0))
{
    define('WEB_ROOT', '/var/www');
}

if ((!defined('LOGIN_PASSWORD_FILE')) || (strlen(NEOCAPTURE_ROOT) <= 0))
  {
  define('LOGIN_PASSWORD_FILE', '/var/privatephp/FundStatConnectLive.php');
  }
  
if ((!defined('NEOCAPTURE_TRADEFILES_ROOT')) || (strlen(NEOCAPTURE_TRADEFILES_ROOT) <= 0))
  {
  	define('NEOCAPTURE_TRADEFILES_ROOT', '/mnt/export_opcvm');
  }

  // BNP Paris Trade file constants
  
  if ((!defined('PARIS_TRADEFILE_NAME')) || (strlen(PARIS_TRADEFILE_NAME) <= 0))
  {
  	define('PARIS_TRADEFILE_NAME', 'VTO30029');
  }
  
  if ((!defined('PARIS_TRADEFILE_DIRECTORY')) || (strlen(PARIS_TRADEFILE_DIRECTORY) <= 0))
  {
  	define('PARIS_TRADEFILE_DIRECTORY', '/mnt/export_opcvm/Paris');
  }
  
  if ((!defined('PARIS_TRADEFILE_DELAY_SECONDS')) || (strlen(PARIS_TRADEFILE_DELAY_SECONDS) <= 0))
  {
  	define('PARIS_TRADEFILE_DELAY_SECONDS', 60);
  }
  
  if ((!defined('PARIS_TRADEFILES_ACKNOWLEDGEMENT')) || (strlen(PARIS_TRADEFILES_ACKNOWLEDGEMENT) <= 0))
  {
  	define('PARIS_TRADEFILES_ACKNOWLEDGEMENT', '/mnt/acknowledgements/bp2s');
  }
  
  if ((!defined('PARIS_TRADEFILES_ACKNOWLEDGEMENT_PROCESSED')) || (strlen(PARIS_TRADEFILES_ACKNOWLEDGEMENT_PROCESSED) <= 0))
  {
  	define('PARIS_TRADEFILES_ACKNOWLEDGEMENT_PROCESSED', '/mnt/acknowledgements/bp2s/processed');
  }
  
  if ((!defined('PARIS_TRADEFILES_ACKNOWLEDGEMENT_PATTERN')) || (strlen(PARIS_TRADEFILES_ACKNOWLEDGEMENT_PATTERN) <= 0))
  {
  	define('PARIS_TRADEFILES_ACKNOWLEDGEMENT_PATTERN', '#^[a-zA-Z0-9_\.]+\.VTO5226W\.[a-zA-Z0-9_\.]+$#');
  }
  
  // BNP Luxembourg Trade file constants
  
  if ((!defined('BNP_LUX_TRADEFILE_NAME')) || (strlen(BNP_LUX_TRADEFILE_NAME) <= 0))
  {
  	define('BNP_LUX_TRADEFILE_NAME', 'PBL2403F');
  }
  
  if ((!defined('BNP_LUX_TRADEFILE_DIRECTORY')) || (strlen(BNP_LUX_TRADEFILE_DIRECTORY) <= 0))
  {
  	define('BNP_LUX_TRADEFILE_DIRECTORY', '/mnt/export_opcvm/Lux');
  }
  
  if ((!defined('BNP_LUX_TRADEFILE_DELAY_SECONDS')) || (strlen(BNP_LUX_TRADEFILE_DELAY_SECONDS) <= 0))
  {
  	define('BNP_LUX_TRADEFILE_DELAY_SECONDS', 60);
  }
  
  if ((!defined('LUXEMBOURG_TRADEFILES_ACKNOWLEDGEMENT')) || (strlen(LUXEMBOURG_TRADEFILES_ACKNOWLEDGEMENT) <= 0))
  {
  	define('LUXEMBOURG_TRADEFILES_ACKNOWLEDGEMENT', '/mnt/acknowledgements/bp2s');
  }
  
  if ((!defined('LUXEMBOURG_TRADEFILES_ACKNOWLEDGEMENT_PROCESSED')) || (strlen(LUXEMBOURG_TRADEFILES_ACKNOWLEDGEMENT_PROCESSED) <= 0))
  {
  	define('LUXEMBOURG_TRADEFILES_ACKNOWLEDGEMENT_PROCESSED', '/mnt/acknowledgements/bp2s/processed');
  }
  
  if ((!defined('LUXEMBOURG_TRADEFILES_ACKNOWLEDGEMENT_PATTERN')) || (strlen(LUXEMBOURG_TRADEFILES_ACKNOWLEDGEMENT_PATTERN) <= 0))
  {
  	define('LUXEMBOURG_TRADEFILES_ACKNOWLEDGEMENT_PATTERN', '#^[a-zA-Z0-9_\.]+\.UPLO_ALL001[a-zA-Z0-9_\.]+$#');
  }
  
  
  // BNP Paris CONFIRMATION file constants
  
  
  if ((!defined('PARIS_CONFIRMATIONFILES_LOCALE')) || (strlen(PARIS_CONFIRMATIONFILES_LOCALE) <= 0))
  {
  	define('PARIS_CONFIRMATIONFILES_LOCALE', 'fr_FR'); // Culture locale used to format Numbers and Dates in this file.
  }
  
  if ((!defined('PARIS_CONFIRMATIONFILES')) || (strlen(PARIS_CONFIRMATIONFILES) <= 0))
  {
  	define('PARIS_CONFIRMATIONFILES', '/mnt/confirms/bp2s');
  }
  
  if ((!defined('PARIS_CONFIRMATIONFILES_PROCESSED')) || (strlen(PARIS_CONFIRMATIONFILES_PROCESSED) <= 0))
  {
  	define('PARIS_CONFIRMATIONFILES_PROCESSED', '/mnt/confirms/bp2s/processed');
  }
  
  if ((!defined('PARIS_CONFIRMATIONFILES_PATTERN')) || (strlen(PARIS_CONFIRMATIONFILES_PATTERN) <= 0))
  {
  	define('PARIS_CONFIRMATIONFILES_PATTERN', '#[a-zA-Z0-9_\.]+CNOT_ALL[a-zA-Z0-9_\.]+\.csv$#');
  }
  
  if ((!defined('PARIS_CONFIRMATIONFILES_SEPARATOR')) || (strlen(PARIS_CONFIRMATIONFILES_SEPARATOR) <= 0))
  {
  	define('PARIS_CONFIRMATIONFILES_SEPARATOR', ';');
  }
  
  
  // BNP Paris Subscription / Redemption file constants
  
  
  if ((!defined('PARIS_ORDERFILES_LOCALE')) || (strlen(PARIS_ORDERFILES_LOCALE) <= 0))
  {
  	define('PARIS_ORDERFILES_LOCALE', 'fr_FR'); // Culture locale used to format Numbers and Dates in this file.
  }
  
  if ((!defined('PARIS_ORDERFILES')) || (strlen(PARIS_ORDERFILES) <= 0))
  {
  	define('PARIS_ORDERFILES', '/mnt/SR/Insert_SR');
  }
  
  if ((!defined('PARIS_ORDERFILES_PROCESSED')) || (strlen(PARIS_ORDERFILES_PROCESSED) <= 0))
  {
  	define('PARIS_ORDERFILES_PROCESSED', '/mnt/SR/Insert_SR/processed');
  }
  
  if ((!defined('PARIS_ORDERFILES_PATTERN')) || (strlen(PARIS_ORDERFILES_PATTERN) <= 0))
  {
  	define('PARIS_ORDERFILES_PATTERN', '#[a-zA-Z0-9_\.]+VTO11I6[a-zA-Z0-9_\.]+$#');
  }
  
  if ((!defined('PARIS_ORDERFILES_SEPARATOR')) || (strlen(PARIS_ORDERFILES_SEPARATOR) <= 0))
  {
  	define('PARIS_ORDERFILES_SEPARATOR', ';');
  }

   // Executions file constants

  if ((!defined('EXECUTIONS_FILE_NAME')) || (strlen(EXECUTIONS_FILE_NAME) <= 0))
  {
      define('EXECUTIONS_FILE_NAME', 'GTOAPRX1');
  }

  if ((!defined('EXECUTIONS_FILE_DIRECTORY')) || (strlen(EXECUTIONS_FILE_DIRECTORY) <= 0))
  {
      define('EXECUTIONS_FILE_DIRECTORY', '/mnt/executions');
  }
  
  define('SMTP_ADDRESS','smtp.gmail.com');
  define('SMTP_PORT','465');
  define('SMTP_USERNAME','js343536@gmail.com');
  define('SMTP_PASSWORD','j0hnsm1th');
  define('SMTP_ENCRYPTION','ssl');