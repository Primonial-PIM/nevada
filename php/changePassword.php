<?php
/**
 * User: npennin
 * Date: 14/09/12
 * Time:
 */

require_once('../localise/localise.php');
require_once(APPLICATION_ROOT . '/php/globalInclude.php');

// Validate the parameters

try
{
    $queryParams = json_decode($_POST['formdata'], true);


    if ($queryParams == null) {
        $queryParams = json_decode($_GET['formdata'], true);
    }

}
catch (Exception $e)
{
    // for error, return nothing.

    if (PHP_DEBUG)
    {
        echo "Error : ". $e->getCode(). ": ". $e->getMessage(). " in ". $e.getFile(). " on line ". $e->getLine();
    }

    exit;

}

try{


    $results_array = array();

    try
    {

        $conn = db_connect();

        $conn->set_charset("utf8");

        $username = mysqli_real_escape_string($conn, $queryParams['USERNAME']);
        $password = mysqli_real_escape_string($conn, $queryParams['PASSWORD']);
        $newpassword = mysqli_real_escape_string($conn, $queryParams['NEWPASSWORD']);

        $query = "exec sp_setPassword('" . $username .  "','" . $password . "','" . $newpassword . "','" . $_SERVER['REMOTE_ADDR'] . "','" . $_SERVER['HTTP_USER_AGENT'] . "')";

        if (!($result = $conn->query($query)))
        {
            mysqli_close($conn);
            throw new Exception('Error calling sp_setPassword()');
        }

        for ($count = 0; $row = $result->fetch_assoc(); $count++)
        {
            $results_array[$count] = $row;
            break;
        }

        mysqli_free_result($result);
        mysqli_close($conn);

    } catch (Exception $e)
    {
        if (PHP_DEBUG)
        {
            $results_array[0] = array();
            $results_array[0]['userID'] = -1;
            $results_array[0]['sessionKey'] = "Error in sp_setPassword : " . $e->getCode() . ": " . $e->getMessage() . " in " . $e->getFile() . " on line " . $e->getLine();
        }
        else
        {
            $results_array[0] = array();
            $results_array[0]['userID'] = -1;
            $results_array[0]['sessionKey'] = "Error";
        }
    }

    $rval = json_encode($results_array);

    echo $rval; //json_encode(get_Names($params)); //, JSON_HEX_TAG);

    exit;

}
catch(Exception $e)
{
    // unsuccessful fetch
    if (PHP_DEBUG)
    {
        echo "Error : ". $e->getCode(). ": ". $e->getMessage(). " in ". $e.getFile(). " on line ". $e->getLine();
    }

    exit;
}


?>
