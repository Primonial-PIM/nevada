<?php
header('Content-Type: text/html; charset=utf-8');

require_once('../localise/localise.php');

require_once(APPLICATION_ROOT . '/php/includes/simple_html_dom.php');
require_once(APPLICATION_ROOT . '/php/includes/curl_functions.php');

// Nicholas Pennington, May 2012
// Acknowledgement to Anthony Culligan.
//
// This code will retrieve the HTML for a given URL,
// Strip the content and style sheets from it and then re-constitute
// a new document with all relative links adjusted.
//
// Requires parameters :
//    url
//    contentdiv
//    currentparent
//    currentdir
//    root
//
// Used for Morningstar mashup.


$get = array();

try
  {

//define the url to extract from and the dom selection criteria

  $url = ($_GET['url'] ? $_GET['url'] : $_POST['url']);
  
  $search = ($_GET['search'] ? $_GET['search'] : $_POST['search']);
  $replace = ($_GET['replace'] ? $_GET['replace'] : $_POST['replace']);
  
  $contentDiv = ($_GET['contentdiv'] ? $_GET['contentdiv'] : $_POST['contentdiv']); // #mainContentDiv

//define the root, the current dir and the parent to the current dir

  $currentdirparent = ($_GET['currentdirparent'] ? $_GET['currentdirparent'] : $_POST['currentdirparent']); //"http://www.morningstar.fr/fr/";
  $currentdir = ($_GET['currentdir'] ? $_GET['currentdir'] : $_POST['currentdir']); //"http://www.morningstar.fr/fr/snapshot/";
  $root = ($_GET['root'] ? $_GET['root'] : $_POST['root']); //"http://www.morningstar.fr/";

//

// get the full page and extract the selected clip

  $url_content = curl_download($url);
  // $url_content = http_get($url);

  $mydom = str_get_html($url_content);
  
// check if fund has been found and, if not, try alternative site  
  
  $notfound="following country";
  
  $pos = strpos($url_content, $notfound);

  if ($pos === false) {
  
  } else {
    $url=str_replace ( $search , $replace , $url );
    $currentdir=str_replace ( $search , $replace , $currentdir );
    $currentdirparent=str_replace ( $search , $replace , $currentdirparent );
    $root=str_replace ( $search , $replace , $root );
    
    $url_content = curl_download($url);
    $mydom = str_get_html($url_content);    
  } 

// Extract Body.

  $clipAll = ($mydom->find($contentDiv));
  $scrapeclip = "";

  if (count($clipAll) > 0)
    {
    $scrapeclip = $clipAll[0];
    }

// replace relative html references in clip with absolute references

  $outstring = insert_currentdir($scrapeclip, $currentdir);
  $scrapeclip = $outstring;

  $finalclip = str_replace('../', $currentdirparent, $scrapeclip);
  $scrapeclip = $finalclip;

  $finalclip = str_replace('="/', '="' . $root, $scrapeclip);
  $scrapeclip = $finalclip;

// make sure that all links open a new page

  $finalclip = str_replace('<a', '<a target=\"_blank\"', $scrapeclip);

// extract all the css lines from the full page

  $cssclip = '';

  foreach ($mydom->find("link") as $clip)
    {
    $pos = strpos($clip, "stylesheet");
    if ($pos === false)
      {
      } else
      {
      $cssclip = $cssclip . "\n" . $clip;
      }
    }

// replace relative html references in css section with absolute references

  $finalcss = str_replace('="/', '="' . $root, $cssclip);

  }
catch (Exception $e)
  {
  $finalcss = "";
  $finalclip = $e->getMessage();
  }

// Fix for Morningstar formatting in MS Snapshot / Tab 4

$finalcss = $finalcss .
  '
  <style>
    #managementManagementDiv td a {white-space:nowrap;}
    #managementManagementFundManagerDiv {float:left; width:72% !important;}
  </style>';

// render the page

echo "<!DOCTYPE html>";
echo "<html>";
echo "<head>";

echo $finalcss . "\n";

echo "</head>\n";

if ($contentDiv!='body')
  {
  echo "<body>\n";
  }

echo $finalclip;

if ($contentDiv!='body')
  {
  echo "</body>\n";
  }

echo "</html>";

?>