<?php
/**
 * User: npennin
 */

session_start();

require_once('../localise/localise.php');
require_once(APPLICATION_ROOT . '/php/globalInclude.php');

try
{
    $queryParams = $_POST;

    if ($queryParams == null)
    {
        $queryParams = $_GET;
    }

    $params['SICOVAM'] = ($queryParams['SICOVAM']);
    $params['DATE_FROM'] = ($queryParams['DATE_FROM']);
    $params['DATE_FROM_FORMAT'] = ($queryParams['DATE_FROM_FORMAT']);
    $params['DATE_TO'] = ($queryParams['DATE_TO']);
    $params['DATE_TO_FORMAT'] = ($queryParams['DATE_TO_FORMAT']);
    $params['USERID'] = ($queryParams['USERID']);
    $params['TOKEN'] = ($queryParams['TOKEN']);
    

} catch (Exception $e)
{
    // for error.
    echo "Error : " . $e->getCode() . ": " . $e->getMessage() . " in " . $e . getFile() . " on line " . $e->getLine();
    exit;
}

try
{

    $rval = get_FXQuery($params);
    $rval = json_encode($rval);

    echo $rval; //json_encode(get_Names($params)); //, JSON_HEX_TAG);

    exit;

} catch (Exception $e)
{
// unsuccessful fetch
    echo "Error : " . $e->getCode() . ": " . $e->getMessage() . " in " . $e . getFile() . " on line " . $e->getLine();
    exit;
}

?>
