<?php
require_once('../localise/localise.php');
require_once('reset_functions.php');
require_once('utility_functions.php');
require_once(LOGIN_PASSWORD_FILE);

$params = $_POST;

try
{

	$password=$params['password'];
	$token=$params['token'];
	
	// reset password
	
	$results_array = array();
	
	$sqlsvr_conn = sqlserver_neocapture_connect();
	
	// Create a new stored procedure
	
	$stmt = mssql_init('sp_SetPassword_ResetToken', $sqlsvr_conn);
	
	if ($stmt)
	{
		if (substr(phpversion(), 0, 4) == '5.3.')
		{
			mssql_bind($stmt, '@ResetToken', &$token, SQLVARCHAR, false, false);
			mssql_bind($stmt, '@Password', &$password, SQLVARCHAR, false, false);
	
		} else
		{
			// Pass by reference deprecated in PHP 5.4
	
			mssql_bind($stmt, '@ResetToken', $token, SQLVARCHAR, false, false);
			mssql_bind($stmt, '@Password', $password, SQLVARCHAR, false, false);
		}
	}
	
	$proc_result = mssql_execute($stmt);
	
	// If Statement succeeds and returns some values, i.e. is a resource rather than $proc_result == true...
	
	if (($proc_result !== true) && ($proc_result !== false))
	{
		if (mssql_num_rows($proc_result) > 0)
		{
			while ($row = mssql_fetch_assoc($proc_result))
			{
				$results_array = $row;
			}
		}
	} else {
		$error=mssql_get_last_message();
		$results_array['Status']='Failed';
		$results_array['Message']=$error;
	}
	
	//echo json_encode(array('password'=>$password));
	echo json_encode($results_array);
}
catch (Exception $e)
{
	$error=$e->getMessage();
	$results_array['Status']='Failed';
	$results_array['Message']=$error;
	echo json_encode($results_array);
}
?>