<?php
/**
 * User: npennin
 * Date: 04/07/12
 * Time: 15:26
 * To change this template use File | Settings | File Templates.
 */

session_start();

require_once('../localise/localise.php');
require_once(APPLICATION_ROOT . '/php/globalInclude.php');

try{
	$queryParams = $_POST;

	if ($queryParams == null) {
		$queryParams = $_GET;
	}

}
catch (Exception $e)
{
	// for error.
	$result=array();
    $result['Status']='Error';
    $result['message']=$e->getMessage();
    echo json_encode($result);
    exit;
}


try{

	if ($params['USERID']==null) {$params['USERID']='0';} // Alter to 0 for Venice.
	if ($params['TOKEN']==null) {$params['TOKEN']='';}
	
    $result = set_user($queryParams);
    
    if (isset($result['Status'])){
    	echo json_encode($result);
    } else {
    	$result['Status']='Error';
    	$result['message']='Update failed';
    	echo json_encode($result);
    }

    exit;

}
catch(Exception $e)  {
    // unsuccessful update
    $result=array();
    $result['Status']='Error';
    $result['message']=$e->getMessage();
    echo json_encode($result);
    exit;
}

function set_user($params){
	
	// this is ignored except for new users
	$params['password']='password';
	
	$results_array = array();
	
	$sqlsvr_conn = sqlserver_neocapture_connect();
	
	// Create a new stored procedure
	
	$stmt = mssql_init('spu_tblUsers_Update', $sqlsvr_conn);
	
	if ($stmt)
	{
		if (substr(phpversion(), 0, 4) == '5.3.')
		{
			mssql_bind($stmt, '@UpdateUserID', &$params['user'], SQLINT4, false, false);
			mssql_bind($stmt, '@EmailAddress', &$params['email'], SQLVARCHAR, false, false);
			mssql_bind($stmt, '@IsAdmin', &$params['isadmin'], SQLINT4, false, false);
			mssql_bind($stmt, '@IsLocked', &$params['islocked'], SQLINT4, false, false);
			mssql_bind($stmt, '@UserID', &$params['USERID'], SQLINT4, false, false);
			mssql_bind($stmt, '@Token', &$params['TOKEN'], SQLVARCHAR, false, false);
			
			mssql_bind($stmt, '@UserName', &$params['username'], SQLVARCHAR, false, false);
			mssql_bind($stmt, '@Password', &$params['password'], SQLVARCHAR, false, false);
	
		} else
		{
			// Pass by reference deprecated in PHP 5.4
	
			mssql_bind($stmt, '@UpdateUserID', $params['user'], SQLINT4, false, false);
			mssql_bind($stmt, '@EmailAddress', $params['email'], SQLVARCHAR, false, false);
			mssql_bind($stmt, '@IsAdmin', $params['isadmin'], SQLINT4, false, false);
			mssql_bind($stmt, '@IsLocked', $params['islocked'], SQLINT4, false, false);
			mssql_bind($stmt, '@UserID', $params['USERID'], SQLINT4, false, false);
			mssql_bind($stmt, '@Token', $params['TOKEN'], SQLVARCHAR, false, false);
			
			mssql_bind($stmt, '@UserName', $params['username'], SQLVARCHAR, false, false);
			mssql_bind($stmt, '@Password', $params['password'], SQLVARCHAR, false, false);
		}
	}
	
	$proc_result = mssql_execute($stmt);
	
	// If Statement succeeds and returns some values, i.e. is a resource rather than $proc_result == true...
	
	if (($proc_result !== true) && ($proc_result !== false))
	{
		if (mssql_num_rows($proc_result) > 0)
		{
			while ($row = mssql_fetch_assoc($proc_result))
			{
				$results_array = $row;
			}
		}
	}
	
	return $results_array;
	
}

?>
