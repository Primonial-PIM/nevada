<?php
require_once('../localise/localise.php');
require_once('reset_functions.php');
require_once('utility_functions.php');
require_once(LOGIN_PASSWORD_FILE);

$params = $_POST;

try
{
	
	$username=$params['Email'];
	
	// get a reset token
	
	$results_array = array();
	
	$sqlsvr_conn = sqlserver_neocapture_connect();
	
	// Create a new stored procedure
	
	$stmt = mssql_init('sp_GetResetToken', $sqlsvr_conn);
	
	if ($stmt)
	{
		if (substr(phpversion(), 0, 4) == '5.3.')
		{
			mssql_bind($stmt, '@UserName', &$username, SQLVARCHAR, false, false);
	
		} else
		{
			// Pass by reference deprecated in PHP 5.4
	
			mssql_bind($stmt, '@UserName', $username, SQLVARCHAR, false, false);
		}
	}
	
	$proc_result = mssql_execute($stmt);
	
	// If Statement succeeds and returns some values, i.e. is a resource rather than $proc_result == true...
	
	if (($proc_result !== true) && ($proc_result !== false))
	{
		if (mssql_num_rows($proc_result) > 0)
		{
			while ($row = mssql_fetch_assoc($proc_result))
			{
				$results_array = $row;
			}
		}
	} else {
		$error=mssql_get_last_message();
		$results_array['Status']='Failed';
		$results_array['Message']=$error;
	}
	
	if ($results_array['Status']=='OK'){
		
		$token=$results_array['ResetToken'];
		
		$expiretime=$results_array['ResetTokenExpiryDisplay'];
		
		$to = $params['Email'];
		$headers = 'From: ITSUPPORTAM@primonial.fr';
		$subject = "Rennaisance password reset";
		$body = "Soit cliquer sur ce lien ou collez-le dans votre navigateur,\n\n".
				"http://10.34.1.220/nevada/reset.php?token=".$token."\n\nUtiliser avant ".$expiretime;
		
		// Envoi du mail
		$mailresult=mail($to, $subject, $body, $headers);
			
		if ($mailresult!=1) {
			$results_array['Status']="Error";
			$results_array['Message']=(" Impossible d'envoyer de réinitialisation de mot de passe. <br>Contactez votre administrateur système.");
		}
	}  
	echo json_encode($results_array);
	
}
catch (Exception $e)
{
	$results_array['Status']='Failed';
	$results_array['Message']=$e->getMessage();
	echo json_encode($results_array);
}
?>