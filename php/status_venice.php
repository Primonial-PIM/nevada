<?php
require_once('../localise/localise.php');
error_reporting(0);

try{
	$queryParams = $_POST;

	if ($queryParams == null) {
		$queryParams = $_GET;
	}

	$params['USERID'] = ($queryParams['USERID']);
	$params['TOKEN'] = ($queryParams['TOKEN']);
}
catch (Exception $e)
{
	// for error.
	echo "Error : ". $e->getCode(). ": ". $e->getMessage(). " in ". $e.getFile(). " on line ". $e->getLine();
	exit;
}

try
{

	if ($params['USERID']==null) {$params['USERID']='0';} // Alter to 0 for Venice.
	if ($params['TOKEN']==null) {$params['TOKEN']='';}
	
	$rows=array();
	
	$rows[]=headerRow(array("Process","Script","Period","Last Sucess","Status"));
	
	
	// heading
	
	// 	 	* Positions - capturePositions.php - Custody All Positions. Every 10 minutes from 6am to 9pm
	//      * Luxembourg Confirms  - captureLuxConfirmations.php - every 10 minutes from 6am to 9pm
	//      * UCITS orders - captureUcitsOrders.php - every 10 minutes from 6am to 9pm
	//      * Paris Confirms - captureParisConfirmations.php - every 10 minutes from 6am to 9pm
	//      * Reconciliation - captureReconciliation.php - Fund Administration by Fund. Every 10 minutes from 6am to 9pm

	$rows[]=tableRow(array("Positions","capturePositions.php","Every 10 mins","2013-09-05 17:00","Good"),true);
	$rows[]=tableRow(array("Lux Confirms","capturePositions.php","Every 10 mins","2013-09-05 17:00","Good"),false);
	$rows[]=tableRow(array("UCITS Orders","capturePositions.php","Every 10 mins","2013-09-05 17:00","Good"),true);
	$rows[]=tableRow(array("Paris Confirms","capturePositions.php","Every 10 mins","2013-09-05 17:00","Failed"),false);
	$rows[]=tableRow(array("Reconciliation","capturePositions.php","Every 10 mins","2013-09-05 17:00","Good"),true);
	
	$html=makeTable($rows);
	
	
}
catch (Exception $e)
{
	echo $e;
}

//echo ($html);
echo ('To be implemented');


function headerRow($cells){
	$html="<thead>";
	foreach ($cells as $cell){
		$html.="<th>".$cell."</th>";
	}
	$html.="</thead>";
	return $html;
}

function tableRow($cells,$odd){
	if ($odd===true){
		$html="<tr class='odd'>";
	} else {
		$html="<tr>";
	}
	foreach ($cells as $cell){
		$html.="<td>".$cell."</td>";
	}
	$html.="</tr>";
	return $html;
}

function makeTable($rows){
	$html="<table class='broom_table'>";
	foreach ($rows as $row){
		$html.=$row;
	}
	$html.="</table>";
	return $html;
}

?>