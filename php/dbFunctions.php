<?php
/**
 * User: npennin
 * Date: 26/04/12
 * Time: 15:07
 *
 */

require_once (LOGIN_PASSWORD_FILE);

function db_connect()
  {
  // Check to see if running online or locally.

  // Note : To implement persistent connections in mysqli, prepend the host hane with 'p:'.
  // It does help, but for significant amounts of data, the transfer time seems much more important.
  //

  if (SERVER_NAME == 'www.pam.local.com')
    {
    $result = new mysqli(MYSQL_PAM_HOSTNAME, MYSQL_PAM_USER, MYSQL_PAM_PASSWORD, MYSQL_PAM_DATABASE, MYSQL_PAM_PORT);
    } else
    {
    $result = new mysqli(MYSQL_PAM_HOSTNAME, MYSQL_PAM_USER, MYSQL_PAM_PASSWORD, MYSQL_PAM_DATABASE, MYSQL_PAM_PORT);
    }

  if (!$result)
    {
    throw new Exception('Could not connect to database server');
    } elseif ($result->connect_errno)
    {
    return "Failed to connect to MySQL: (" . $result->connect_errno . ") " . $result->connect_error;
    } else
    {
    return $result;
    }
  }

/*
 *
function sophis_connect()
{

/*((strlen(ORACLE_SOPHIS_CONNECT) > 0) && (ORACLE_SOPHIS_CONNECT <> 'ORACLE_SOPHIS_CONNECT'))
  {
  $connect_string = ORACLE_SOPHIS_CONNECT;
  } else
  {
  $connect_string = '//' . ORACLE_SOPHIS_HOSTNAME . ':' . ORACLE_SOPHIS_PORT . '/' . ORACLE_SOPHIS_SID;
  }

$conn = oci_connect(ORACLE_SOPHIS_USER, ORACLE_SOPHIS_PASSWORD, $connect_string);

if ($conn == false)
  {
  $errTxt = oci_error();

  // try the old way

  $connect_string = '//' . ORACLE_SOPHIS_HOSTNAME . ':' . ORACLE_SOPHIS_PORT . '/' . ORACLE_SOPHIS_SID;
  $conn = oci_connect(ORACLE_SOPHIS_USER, ORACLE_SOPHIS_PASSWORD, $connect_string);

  $errTxt = oci_error();
  }

return $conn;

  }
*/

function sqlserver_connect($pHostname, $pInstance, $pPort, $pUser, $pPassword, $pDatabase)
  {
  // Useful debug option !!!!!
   ini_set('display_errors', 1);

  // Allow an optional first parameter specifying the database to switch to.

  $database_name = $pDatabase;

  // Build the server string <Server>:<port>[\<Instance>]

  $server_string = $pHostname;

  if (strlen($pInstance) > 0)
    {
    $server_string = $server_string . '\\' . $pInstance;
    }

/*
  if ($pPort > 0)
      {
      $server_string = $server_string . ':' . $pPort;
      }
*/

      // Connect
  
  //echo "server_string : ".$server_string." "."user : ".$pUser." "."password : ".$pPassword."<br>";
  
  

  $conn = mssql_connect($server_string, $pUser, $pPassword, true);

  // Change database ?

  if ($conn)
    {
    //echo "got conn<br>";
    if (strlen($database_name) > 0)
      {
      mssql_select_db($database_name, $conn);
      } elseif (strlen($pDatabase) > 0)
      {
      mssql_select_db($pDatabase, $conn);
      }
    } else {
    	//echo "connect failed<br>";
    }

  // return.

  return $conn;

  }

function renaissance_connect($instance,$server)
  {

   $database_name = SQLSERVER_RENAISSANCE_DATABASE;
   
   //echo "instance : ".$instance.' server : '.$server."<br>".PHP_EOL;
    
   return sqlserver_connect($server, $instance, SQLSERVER_RENAISSANCE_PORT, SQLSERVER_RENAISSANCE_USER, SQLSERVER_RENAISSANCE_PASSWORD, $database_name);

  }

function sqlserver_neocapture_connect()
  {
  // Allow an optional first parameter specifying the database to switch to.

  $database_name = '';

  if (func_num_args() > 0)
    {
    $args = func_get_args();
    $database_name = $args[0];
    } elseif ((strlen(SQLSERVER_NEO_DATABASE) > 0) && (SQLSERVER_NEO_DATABASE <> 'SQLSERVER_NEO_DATABASE'))
    {
    $database_name = SQLSERVER_NEO_DATABASE;
    }

  $instanceName = '';
  if ((strlen(SQLSERVER_NEO_INSTANCE) > 0) && (SQLSERVER_NEO_INSTANCE <> 'SQLSERVER_NEO_INSTANCE'))
    {
    $instanceName = SQLSERVER_NEO_INSTANCE;
    }

  return sqlserver_connect(SQLSERVER_NEO_HOSTNAME, $instanceName, SQLSERVER_NEO_PORT, SQLSERVER_NEO_USER, SQLSERVER_NEO_PASSWORD, $database_name);

  }

//

function RefreshToken($params)
  {

  $userID = $params['USERID'];
  $securityToken = $params['TOKEN'];
  $returnArray = array();

  try
    {
    $link = sqlserver_neocapture_connect();

    if ($link)
      { //   select convert(nvarchar,getDate(),23)

      $stmt = mssql_init('spu_RefreshToken', $link);

      if ($stmt)
        {

        if (substr(phpversion(), 0, 4) == '5.3.')
          {

          mssql_bind($stmt, '@UserID', &$userID, SQLINT4, false, false);
          mssql_bind($stmt, '@Token', &$securityToken, SQLVARCHAR, false, false);
          } else
          {
          // Pass by reference deprecated in PHP 5.4

          mssql_bind($stmt, '@UserID', $userID, SQLINT4, false, false);
          mssql_bind($stmt, '@Token', $securityToken, SQLVARCHAR, false, false);
          }

        $proc_result = mssql_execute($stmt);
        }

      // Free statement
      mssql_free_statement($stmt);

      }
    } catch (Exception $e)
    {
    }

  return $returnArray;
  }

// Get Folio Names

function get_fundAccounts($params)
  {
  global $db_instances;
  	
  	try
  	{
  	$results_array = array();
  	
  	foreach ($db_instances as $db_instance){
  			
  		$sqlsvr_conn = renaissance_connect($db_instance['INSTANCE'],$db_instance['SERVER']);
	    //ini_set('display_errors', 1);
	
	    $userID = $params['USERID'];
	    $securityToken = $params['TOKEN'];
	    $knowledgeDate = '1900-01-01';
	
	    // Create a new stored procedure
	
	    $stmt = mssql_init('web_GetFundList', $sqlsvr_conn);
	
	    // Execute
	
	    if ($stmt)
	      {
	      if (substr(phpversion(), 0, 4) == '5.3.')
	        {
	
	        mssql_bind($stmt, '@UserID', &$userID, SQLINT4, false, false);
	        mssql_bind($stmt, '@Token', &$securityToken, SQLVARCHAR, false, false);
	        mssql_bind($stmt, '@KnowledgeDate_Str', &$knowledgeDate, SQLVARCHAR, false, false);
	
	        } else
	        {
	        // Pass by reference deprecated in PHP 5.4
	
	        mssql_bind($stmt, '@UserID', $userID, SQLINT4, false, false);
	        mssql_bind($stmt, '@Token', $securityToken, SQLVARCHAR, false, false);
	        mssql_bind($stmt, '@KnowledgeDate_Str', $knowledgeDate, SQLVARCHAR, false, false);
	        }
	
	      $proc_result = mssql_execute($stmt);
	
	      // If Statement succeeds and returns some values, i.e. is a resource rather than $proc_result == true...
	
	      if (($proc_result !== true) && ($proc_result !== false))
	        {
	        if (mssql_num_rows($proc_result) > 0)
	          {
	          while ($row = mssql_fetch_assoc($proc_result))
	            {
	            $results_array[] = $row;
	            }
	          }
	        }
	
	
	      // Free statement
	      mssql_free_statement($stmt);
	      }
  	}

    } catch (Exception $e)
    {
    }

  return $results_array;

  }

function get_FolioNames($params)
  {
  global $db_instances;
  	
  try
    {
    $results_array = array();
    
    //echo LOGIN_PASSWORD_FILE.PHP_EOL;
    
    foreach ($db_instances as $db_instance){
    	
    	//echo $db_instance['SERVER'].PHP_EOL;
    	//echo $db_instance['INSTANCE'].PHP_EOL;

	    $sqlsvr_conn = renaissance_connect($db_instance['INSTANCE'],$db_instance['SERVER']);
	    //ini_set('display_errors', 1);
	
	    $userID = $params['USERID'];
	    $securityToken = $params['TOKEN'];
	    $knowledgeDate = '1900-01-01';
	
	    // Create a new stored procedure
	
	    $stmt = mssql_init('web_GetFundList', $sqlsvr_conn);
	
	    // Execute
	
	    if ($stmt)
	      {
	      if (substr(phpversion(), 0, 4) == '5.3.')
	        {
	
	        mssql_bind($stmt, '@UserID', &$userID, SQLINT4, false, false);
	        mssql_bind($stmt, '@Token', &$securityToken, SQLVARCHAR, false, false);
	        mssql_bind($stmt, '@KnowledgeDate_Str', &$knowledgeDate, SQLVARCHAR, false, false);
	
	        } else
	        {
	        // Pass by reference deprecated in PHP 5.4
	
	        mssql_bind($stmt, '@UserID', $userID, SQLINT4, false, false);
	        mssql_bind($stmt, '@Token', $securityToken, SQLVARCHAR, false, false);
	        mssql_bind($stmt, '@KnowledgeDate_Str', $knowledgeDate, SQLVARCHAR, false, false);
	        }
	
	      $proc_result = mssql_execute($stmt);
	
	      // If Statement succeeds and returns some values, i.e. is a resource rather than $proc_result == true...
	
	      if (($proc_result !== true) && ($proc_result !== false))
	        {
	        if (mssql_num_rows($proc_result) > 0)
	          {
	          while ($row = mssql_fetch_assoc($proc_result))
	            {
	            $row['INSTANCE']=$db_instance['MATCHING'];
	            $results_array[] = $row;
	            }
	          }
	        }
	
	
	      // Free statement
	      mssql_free_statement($stmt);
	      
	      }
      
    }

    } catch (Exception $e)
    {
    }

  return $results_array;
  }

function get_LastRunDetails($params)
  {
  	/*
  	 *
  	*/
  
  	try
  	{
  		$results_array = array();
  
  		$sqlsvr_conn = sqlserver_neocapture_connect();
  		//ini_set('display_errors', 1);
  
  		$userID = $params['USERID'];
    	$securityToken = $params['TOKEN'];
  		$knowledgeDate = '1900-01-01';
  
  		// Create a new stored procedure
  
  		$stmt = mssql_init('web_GetCaptureSummary', $sqlsvr_conn);
  
  		// Execute
  
  		if ($stmt)
  		{
  			if (substr(phpversion(), 0, 4) == '5.3.')
  			{
  
  				mssql_bind($stmt, '@UserID', &$userID, SQLINT4, false, false);
  				mssql_bind($stmt, '@Token', &$securityToken, SQLVARCHAR, false, false);
  				mssql_bind($stmt, '@KnowledgeDate_Str', &$knowledgeDate, SQLVARCHAR, false, false);
  
  			} else
  			{
  				// Pass by reference deprecated in PHP 5.4
  
  				mssql_bind($stmt, '@UserID', $userID, SQLINT4, false, false);
  				mssql_bind($stmt, '@Token', $securityToken, SQLVARCHAR, false, false);
  				mssql_bind($stmt, '@KnowledgeDate_Str', $knowledgeDate, SQLVARCHAR, false, false);
  			}
  
  			$proc_result = mssql_execute($stmt);
  
  			// If Statement succeeds and returns some values, i.e. is a resource rather than $proc_result == true...
  
  			if (($proc_result !== true) && ($proc_result !== false))
  			{
  				if (mssql_num_rows($proc_result) > 0)
  				{
  					while ($row = mssql_fetch_assoc($proc_result))
  					{
  						$results_array[] = $row;
  					}
  				}
  			}
  
  
  			// Free statement
  			mssql_free_statement($stmt);
  		}
  
  	} catch (Exception $e)
  	{
  	}
  
  	return $results_array;
  }

function get_fundValuations($params)
  {
  
  global $db_instances;

  try
    {
    $results_array = array();
    
    foreach ($db_instances as $db_instance){

	    $sqlsvr_conn = renaissance_connect($db_instance['INSTANCE'],$db_instance['SERVER']);
	    //ini_set('display_errors', 1);
	
	    $userID = $params['USERID'];
	    $securityToken = $params['TOKEN'];
	    $folioID = (int)$params['ID'];
	    $valueDate = '1900-01-01';
	    $knowledgeDate = '1900-01-01';
	
	    // Create a new stored procedure
	
	    $stmt = mssql_init('web_GetFundNAVs', $sqlsvr_conn);
	
	    // Execute
	
	    if ($stmt)
	      {
	      if (substr(phpversion(), 0, 4) == '5.3.')
	        {
	
	        mssql_bind($stmt, '@UserID', &$userID, SQLINT4, false, false);
	        mssql_bind($stmt, '@Token', &$securityToken, SQLVARCHAR, false, false);
	        mssql_bind($stmt, '@FundID', &$folioID, SQLINT4, false, false);
	        mssql_bind($stmt, '@ValueDate_Str', &$valueDate, SQLVARCHAR, false, false);
	        mssql_bind($stmt, '@KnowledgeDate_Str', &$knowledgeDate, SQLVARCHAR, false, false);
	
	        } else
	        {
	        // Pass by reference deprecated in PHP 5.4
	
	        mssql_bind($stmt, '@UserID', $userID, SQLINT4, false, false);
	        mssql_bind($stmt, '@Token', $securityToken, SQLVARCHAR, false, false);
	        mssql_bind($stmt, '@FundID', $folioID, SQLINT4, false, false);
	        mssql_bind($stmt, '@ValueDate_Str', $valueDate, SQLVARCHAR, false, false);
	        mssql_bind($stmt, '@KnowledgeDate_Str', $knowledgeDate, SQLVARCHAR, false, false);
	        }
	
	      $proc_result = mssql_execute($stmt);
	
	      // If Statement succeeds and returns some values, i.e. is a resource rather than $proc_result == true...
	
	      if (($proc_result !== true) && ($proc_result !== false))
	        {
	        if (mssql_num_rows($proc_result) > 0)
	          {
	          while ($row = mssql_fetch_assoc($proc_result))
	            {
	            $row['INSTANCE']=$db_instance['MATCHING'];
	            $results_array[] = $row;
	            }
	          }
	        }
	
	
	      // Free statement
	      mssql_free_statement($stmt);
	      }
      
    }

    } catch (Exception $e)
    {
    }

  return $results_array;

  }

function get_TransactionQuery($params)
  {
  global $db_instances;
  
  try
    {
    $results_array = array();
    
    foreach ($db_instances as $db_instance){
    
	    //echo "dbFunctions.php<br>";
	
	    $sqlsvr_conn = renaissance_connect($db_instance['INSTANCE'],$db_instance['SERVER']);
	    //ini_set('display_errors', 1);
	
	    $userID = $params['USERID'];
	    $securityToken = $params['TOKEN'];
	    $folioID = (int)$params['FOLIO'];
	    $folioSICOVAM = (int)$params['SICOVAM'];
	    $folioDateFrom = (string)$params['DATE_FROM'];
	    $folioDateTo = (string)$params['DATE_TO'];
	    $no_c = (int)$params['NO_C'];
	    $no_e = (int)$params['NO_E'];
	    $knowledgeDate = '1900-01-01';
	
	    // Create a new stored procedure
	
	    $stmt = mssql_init('web_GetTransactions', $sqlsvr_conn);
	
	    /*
	    @UserID int,
	    @Token varchar(100),
	    @FundID [int] = 0,
	    @InstrumentID [int] = 0,
	    @StartDate_Str [varchar](20) = '1900-01-01',
	    @EndDate_Str [varchar](20) = '3000-01-01',
	    @No_C [int] = 0,
	    @No_E [int] = 0,
	    @KnowledgeDate_Str [varchar](20) = '1900-01-01'
	
	     */
	
	    // Execute
	
	    if ($stmt)
	      {
	      if (substr(phpversion(), 0, 4) == '5.3.')
	        {
	
	        mssql_bind($stmt, '@UserID', &$userID, SQLINT4, false, false);
	        mssql_bind($stmt, '@Token', &$securityToken, SQLVARCHAR, false, false);
	        mssql_bind($stmt, '@FundID', &$folioID, SQLINT4, false, false);
	        mssql_bind($stmt, '@InstrumentID', &$folioSICOVAM, SQLINT4, false, false);
	        mssql_bind($stmt, '@StartDate_Str', &$folioDateFrom, SQLVARCHAR, false, false);
	        mssql_bind($stmt, '@EndDate_Str', &$folioDateTo, SQLVARCHAR, false, false);
	        mssql_bind($stmt, '@No_C', &$no_c, SQLINT4, false, false);
	        mssql_bind($stmt, '@No_E', &$no_e, SQLINT4, false, false);
	        mssql_bind($stmt, '@KnowledgeDate_Str', &$knowledgeDate, SQLVARCHAR, false, false);
	
	        } else
	        {
	        // Pass by reference deprecated in PHP 5.4
	
	        mssql_bind($stmt, '@UserID', $userID, SQLINT4, false, false);
	        mssql_bind($stmt, '@Token', $securityToken, SQLVARCHAR, false, false);
	        mssql_bind($stmt, '@FundID', $folioID, SQLINT4, false, false);
	        mssql_bind($stmt, '@InstrumentID', $folioSICOVAM, SQLINT4, false, false);
	        mssql_bind($stmt, '@StartDate_Str', $folioDateFrom, SQLVARCHAR, false, false);
	        mssql_bind($stmt, '@EndDate_Str', $folioDateTo, SQLVARCHAR, false, false);
	        mssql_bind($stmt, '@No_C', $no_c, SQLINT4, false, false);
	        mssql_bind($stmt, '@No_E', $no_e, SQLINT4, false, false);
	        mssql_bind($stmt, '@KnowledgeDate_Str', $knowledgeDate, SQLVARCHAR, false, false);
	        }
	
	      $proc_result = mssql_execute($stmt);

        if (($proc_result !== true) && ($proc_result !== false))
          {
          if (mssql_num_rows($proc_result) > 0)
            {
            while ($row = mssql_fetch_assoc($proc_result))
              {
              $row['INSTANCE']=$db_instance['MATCHING'];
              $results_array[] = $row;
              }
            }
          }
	
	      // Free statement
	      mssql_free_statement($stmt);
	      }
	      
    }

    } catch (Exception $e)
    {
    }

  return $results_array;

  }

function get_TransactionSettlementQuery($params)
  {
  global $db_instances;
  
  
  try
    {
    $results_array = array();
    
    foreach ($db_instances as $db_instance){

	    $sqlsvr_conn = renaissance_connect($db_instance['INSTANCE'],$db_instance['SERVER']);
	    //ini_set('display_errors', 1);
	
	    // Get Folio ID
	    $userID = $params['USERID'];
	    $securityToken = $params['TOKEN'];
	    $folioID = (int)$params['FOLIO'];
	    $folioSICOVAM = (int)$params['SICOVAM'];
	    $folioDateFrom = (string)$params['DATE_FROM'];
	    $folioDateTo = (string)$params['DATE_TO'];
	    $folioDateFx = (string)$params['DATE_FX'];
	    $no_c = (string)$params['NO_C'];
	    $useDateSett = (int)$params['USE_SETTLEMENTDATE'];
	    $knowledgeDate = '1900-01-01';
	
	    // Create a new stored procedure
	
	    $stmt = mssql_init('web_GetTransactionSettlement', $sqlsvr_conn);
	
	    /*
	    CREATE PROCEDURE [dbo].[web_GetTransactionSettlement]
	
	      @UserID int,
	      @Token varchar(100),
	      @FundID [int] = 0,
	      @InstrumentID [int] = 0,
	      @StartDate_Str [varchar](20) = '1900-01-01',
	      @EndDate_Str [varchar](20) = '3000-01-01',
	      @FXDate_Str [varchar](20) = '3000-01-01',
	      @UseValueDate [int] = 0, -- (0 : Use DecisionDate, Else : Use ValueDate)
	      @NoExpenses [int] = 0,
	      @KnowledgeDate_Str [varchar](20) = '1900-01-01'
	         */
	
	    if ($stmt)
	      {
	      if (substr(phpversion(), 0, 4) == '5.3.')
	        {
	
	        mssql_bind($stmt, '@UserID', &$userID, SQLINT4, false, false);
	        mssql_bind($stmt, '@Token', &$securityToken, SQLVARCHAR, false, false);
	        mssql_bind($stmt, '@FundID', &$folioID, SQLINT4, false, false);
	        mssql_bind($stmt, '@InstrumentID', &$folioSICOVAM, SQLINT4, false, false);
	        mssql_bind($stmt, '@StartDate_Str', &$folioDateFrom, SQLVARCHAR, false, false);
	        mssql_bind($stmt, '@EndDate_Str', &$folioDateTo, SQLVARCHAR, false, false);
	        mssql_bind($stmt, '@FXDate_Str', &$folioDateFx, SQLVARCHAR, false, false);
	        mssql_bind($stmt, '@UseSettlementDate', &$useDateSett, SQLINT4, false, false);
	        mssql_bind($stmt, '@NoExpenses', &$no_c, SQLINT4, false, false);
	        mssql_bind($stmt, '@KnowledgeDate_Str', &$knowledgeDate, SQLVARCHAR, false, false);
	
	        } else
	        {
	        // Pass by reference deprecated in PHP 5.4
	
	        mssql_bind($stmt, '@UserID', $userID, SQLINT4, false, false);
	        mssql_bind($stmt, '@Token', $securityToken, SQLVARCHAR, false, false);
	        mssql_bind($stmt, '@FundID', $folioID, SQLINT4, false, false);
	        mssql_bind($stmt, '@InstrumentID', $folioSICOVAM, SQLINT4, false, false);
	        mssql_bind($stmt, '@StartDate_Str', $folioDateFrom, SQLVARCHAR, false, false);
	        mssql_bind($stmt, '@EndDate_Str', $folioDateTo, SQLVARCHAR, false, false);
	        mssql_bind($stmt, '@FXDate_Str', $folioDateFx, SQLVARCHAR, false, false);
	        mssql_bind($stmt, '@UseSettlementDate', $useDateSett, SQLINT4, false, false);
	        mssql_bind($stmt, '@NoExpenses', $no_c, SQLINT4, false, false);
	        mssql_bind($stmt, '@KnowledgeDate_Str', $knowledgeDate, SQLVARCHAR, false, false);
	        }
	
	      $proc_result = mssql_execute($stmt);

        if (($proc_result !== true) && ($proc_result !== false))
          {
          if (mssql_num_rows($proc_result) > 0)
            {
            while ($row = mssql_fetch_assoc($proc_result))
              {
              $row['INSTANCE']=$db_instance['MATCHING'];
              $results_array[] = $row;
              }
            }
          }
	
	      // Free statement
	      mssql_free_statement($stmt);
	      }
    }

    } catch (Exception $e)
    {
    }

  return $results_array;
  }

function get_FXQuery($params)
  {
  	global $db_instances;
  	
  	try
  	{
  	$results_array = array();
  	
  	foreach ($db_instances as $db_instance){
  			
  		$sqlsvr_conn = renaissance_connect($db_instance['INSTANCE'],$db_instance['SERVER']);
  		

	    //ini_set('display_errors', 1);
	
	    // Get Parameters
	
	    $userID = $params['USERID'];
	    $securityToken = $params['TOKEN'];
	    $folioDateFrom = (string)$params['DATE_FROM'];
	    $folioDateTo = (string)$params['DATE_TO'];
	    $knowledgeDate = '1900-01-01';
	
	    // Create a new stored procedure
	
	    /*
	    CREATE PROCEDURE [dbo].[web_GetFXRates]
	      @UserID int,
	      @Token varchar(100),
	      @StartDate_Str [varchar](20) = '1900-01-01',
	      @EndDate_Str [varchar](20)   = '3000-01-01',
	      @KnowledgeDate_Str [varchar](20) = '1900-01-01'
	     */
	
	    $stmt = mssql_init('web_GetFXRates', $sqlsvr_conn);
	
	    if ($stmt)
	      {
	      if (substr(phpversion(), 0, 4) == '5.3.')
	        {
	        mssql_bind($stmt, '@UserID', &$userID, SQLINT4, false, false);
	        mssql_bind($stmt, '@Token', &$securityToken, SQLVARCHAR, false, false);
	        mssql_bind($stmt, '@StartDate_Str', &$folioDateFrom, SQLVARCHAR, false, false);
	        mssql_bind($stmt, '@EndDate_Str', &$folioDateTo, SQLVARCHAR, false, false);
	        mssql_bind($stmt, '@KnowledgeDate_Str', &$knowledgeDate, SQLVARCHAR, false, false);
	        } else
	        {
	        mssql_bind($stmt, '@UserID', $userID, SQLINT4, false, false);
	        mssql_bind($stmt, '@Token', $securityToken, SQLVARCHAR, false, false);
	        mssql_bind($stmt, '@StartDate_Str', $folioDateFrom, SQLVARCHAR, false, false);
	        mssql_bind($stmt, '@EndDate_Str', $folioDateTo, SQLVARCHAR, false, false);
	        mssql_bind($stmt, '@KnowledgeDate_Str', $knowledgeDate, SQLVARCHAR, false, false);
	        }
	      }
	
	    $proc_result = mssql_execute($stmt);
	
	    // If Statement succeeds and returns some values, i.e. is a resource rather than $proc_result == true...
	
	    if (($proc_result !== true) && ($proc_result !== false))
	      {
	      if (mssql_num_rows($proc_result) > 0)
	        {
	        while ($row = mssql_fetch_assoc($proc_result))
	          {
	          $row['INSTANCE']=$db_instance['MATCHING'];
	          $results_array[] = $row;
	          }
	        }
	      }
	
	    // Free statement
	    mssql_free_statement($stmt);
	    
  		}

    } catch (Exception $e)
    {
    }

  return $results_array;

  }

function get_TransactionHistory($params)
  {
  global $db_instances;
  	
  	try
  	{
  	$results_array = array();

    $instance = '';
    if (isset($params['INSTANCE']))
      {
      $instance = $params['INSTANCE'];
      }


    foreach ($db_instances as $db_instance)
      {

      if ($db_instance['MATCHING']==$instance)
        {

        $sqlsvr_conn = renaissance_connect($db_instance['INSTANCE'],$db_instance['SERVER']);
        //ini_set('display_errors', 1);

        $userID = $params['USERID'];
        $securityToken = $params['TOKEN'];
        $TransactionID = (int)$params['TRANSACTIONID'];
        $knowledgeDate = '1900-01-01';

        // Create a new stored procedure

        $stmt = mssql_init('web_GetTransactionHistory', $sqlsvr_conn);

        // Execute

        if ($stmt)
          {
          if (substr(phpversion(), 0, 4) == '5.3.')
            {

            mssql_bind($stmt, '@UserID', &$userID, SQLINT4, false, false);
            mssql_bind($stmt, '@Token', &$securityToken, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@TransactionID', &$TransactionID, SQLINT4, false, false);
            mssql_bind($stmt, '@KnowledgeDate_Str', &$knowledgeDate, SQLVARCHAR, false, false);

            } else
            {
            // Pass by reference deprecated in PHP 5.4

            mssql_bind($stmt, '@UserID', $userID, SQLINT4, false, false);
            mssql_bind($stmt, '@Token', $securityToken, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@TransactionID', $TransactionID, SQLINT4, false, false);
            mssql_bind($stmt, '@KnowledgeDate_Str', $knowledgeDate, SQLVARCHAR, false, false);
            }

          $proc_result = mssql_execute($stmt);

          // If Statement succeeds and returns some values, i.e. is a resource rather than $proc_result == true...

          if (($proc_result !== true) && ($proc_result !== false))
            {
            if (mssql_num_rows($proc_result) > 0)
              {
              while ($row = mssql_fetch_assoc($proc_result))
                {
                $row['INSTANCE']=$db_instance['MATCHING'];
                $results_array[] = $row;
                }
              }
            }


          // Free statement
          mssql_free_statement($stmt);
          } // if ($stmt)

        break;

        } //if ($db_instance

  	  } // foreach

    } catch (Exception $e)
    {
    }

  return $results_array;

  }

function get_TradeFilesQuery($params)
  {

  $userID = $params['USERID'];
  $securityToken = $params['TOKEN'];
  $refcon = (string)$params['REFCON'];
  $folioDateFrom = (string)$params['DATE_FROM']; // YYYY-MM-DD
  $folioDateTo = (string)$params['DATE_TO'];
  $isin = (string)$params['ISIN'];
  $knowledgeDate = '1900-01-01';
  if (isset($params['KNOWLEDGEDATE'])) { $knowledgeDate = substr((string)$params['KNOWLEDGEDATE'], 0, 23); } //  YYYY-MM-DD HH:mm:ss.sss }
  $returnArray = array();

  try
    {
    $link = sqlserver_neocapture_connect();

      /*
       * CREATE PROCEDURE spu_tblTradeFiles_Select
          @UserID int,
          @Token varchar(100),
          @REFCON [nvarchar](50),
          @ISIN [nvarchar](50),
          @DateFrom date,
          @DateTo date,
          @KnowledgeDate datetime
AS
       */

    if ($link)
      { //   select convert(nvarchar,getDate(),23)

      $stmt = mssql_init('spu_tblTradeFiles_Select', $link);

      if ($stmt)
        {

        if (substr(phpversion(), 0, 4) == '5.3.')
          {

          mssql_bind($stmt, '@UserID', &$userID, SQLINT4, false, false);
          mssql_bind($stmt, '@Token', &$securityToken, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@REFCON', &$refcon, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@ISIN', &$isin, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@DateFrom', &$folioDateFrom, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@DateTo', &$folioDateTo, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@KnowledgeDate', &$knowledgeDate, SQLVARCHAR, false, false);

          } else
          {
          // Pass by reference deprecated in PHP 5.4

          mssql_bind($stmt, '@UserID', $userID, SQLINT4, false, false);
          mssql_bind($stmt, '@Token', $securityToken, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@REFCON', $refcon, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@ISIN', $isin, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@DateFrom', $folioDateFrom, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@DateTo', $folioDateTo, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@KnowledgeDate', $knowledgeDate, SQLVARCHAR, false, false);
          }

        $proc_result = mssql_execute($stmt);

        if (($proc_result !== true) && ($proc_result !== false))
          {
          if (mssql_num_rows($proc_result))
            {
            while ($row = mssql_fetch_assoc($proc_result))
              {
              $returnArray[] = $row;
              }

            }
          }

        }

      /*      $queryString =
              "SELECT " .
                "RN,tradefileID,captureID,tradefilename,CONVERT(nvarchar, tradefiledate, 120) AS tradefiledate,status,office,sophis_refcon,instrumentcodetype,instrumentcode,transactiontype,units,value,currency,createoramend,paris_securitiesaccount,lux_account, RIGHT(COALESCE(NULLIF([paris_securitiesaccount], ''), NULLIF([lux_account], '')), 11) AS account,priority,directory,UserEntered,CONVERT(nvarchar, DateEntered, 120) AS DateEntered " .
                "FROM [dbo].[fn_tblTradeFiles_SelectKD] (NULL,'" . $refcon . "','" . $isin . "'," . (strlen($folioDateFrom) <= 0 ? "NULL" : "CONVERT(date, '" . $folioDateFrom . "', 121)") . ")" . (strlen($folioDateTo) <= 0 ? "" : " WHERE  (CONVERT(date, [tradefiledate]) <= CONVERT(date, '" . $folioDateTo . "', 121))");
            $result = mssql_query($queryString, $link);
      */

      // Free statement
      mssql_free_statement($stmt);

      }
    } catch (Exception $e)
    {
    }

  return $returnArray;
  }

function get_ConfirmationsQuery($params)
  {
  $userID = $params['USERID'];
  $securityToken = $params['TOKEN'];
  $folioDateFrom = (string)$params['DATE_FROM']; //  YYYY-MM-DD
  $folioDateTo = (string)$params['DATE_TO']; //  YYYY-MM-DD
  $securitiesAccount = (string)$params['SECURITIES_ACCOUNT'];
  $maxCaptureId = (string)$params['MAX_CAPTURE_ID'];
  $returnArray = array();
  $whereString = '';
  $knowledgeDate = '1900-01-01';
  if (isset($params['KNOWLEDGEDATE'])) { $knowledgeDate = (strlen($params['KNOWLEDGEDATE']) > 0 ? substr((string)$params['KNOWLEDGEDATE'], 0, 23) : '1900-01-01'); } //  YYYY-MM-DD HH:mm:ss.sss }

  //$p_knowledgeDate = ((strlen($$knowledgeDate) > 0) ? "CONVERT(datetime, '" . $$knowledgeDate . "', 121)" : 'NULL');
  //$p_maxCaptureId = ((strlen($maxCaptureId) > 0) ? $maxCaptureId : '0');
  $maxCaptureId = intval(((strlen($maxCaptureId) > 0) ? $maxCaptureId : '0'));


  /*
   *
  CREATE PROCEDURE [dbo].[spu_tblConfirmations_Select]
      @UserID int,
      @Token varchar(100),
      @MaxCaptureID int,
      @SecuritiesAccount [nvarchar](50),
      @DateFrom date,
      @DateTo date,
      @KnowledgeDate datetime
   */
  /*
  // CONVERT(datetime, ISNULL(@neo_DateEntered, '1970-01-01 00:00:00.000'), 121) // ODBC canonical (with milliseconds) yyyy-mm-dd hh:mi:ss.mmm(24h)

  // 'CN','RJ' drop out after 2 days.
  $whereString = "WHERE ((neo_st NOT IN ('CN','RJ')) OR (DateEntered > DATEADD(weekday, -2, GETDATE())))";

  if ((strlen($folioDateFrom) > 0) || (strlen($folioDateTo) > 0))
    {
    if (strlen($folioDateFrom) > 0)
      {
      $whereString = $whereString . " AND (neo_trade_date >= CONVERT(date, '" . $folioDateFrom . "', 121))";

      if (strlen($folioDateTo) > 0)
        {
        $whereString = $whereString . " AND (neo_trade_date <= CONVERT(date, '" . $folioDateTo . "', 121))";
        }
      } else
      {
      $whereString = $whereString . " AND (neo_trade_date <= CONVERT(date, '" . $folioDateTo . "', 121))";
      }
    }
  */

  try
    {
    $link = sqlserver_neocapture_connect();

    if ($link)
      {
      /*
      $queryString =
        "SELECT " .
          "RN,instance,confirmationID,captureID,neo_head,neo_bank,neo_st,neo_status,neo_cat,neo_t,neo_settlement,CONVERT(nvarchar, neo_settlement_date, 120) AS neo_settlement_date,neo_clientreference,neo_bankreference,neo_quantity,neo_quantity_value,neo_unit,neo_isin,neo_securitydescription,neo_settlementamount,neo_settlementcurrency,neo_price,neo_price_value,neo_pricecurrency,neo_blank1,neo_trade,CONVERT(nvarchar, neo_trade_date, 120) AS neo_trade_date, neo_securitiesaccount,RIGHT([neo_securitiesaccount], 11) AS [account],neo_accountdescription,neo_settagentbic,neo_settagentdescr,neo_ctrpbic,neo_tradecounterpartdescr,UserEntered,CONVERT(nvarchar, DateEntered, 120) AS DateEntered " .
          "FROM [dbo].[fn_tblConfirmations_SelectKD] (" . $p_knowledgeDate . "," . $p_maxCaptureId . ",'" . $securitiesAccount . "')" . $whereString;

      $result = mssql_query($queryString, $link);
      */

      $stmt = mssql_init('spu_tblConfirmations_Select', $link);

      if ($stmt)
        {

        if (substr(phpversion(), 0, 4) == '5.3.')
          {

          mssql_bind($stmt, '@UserID', &$userID, SQLINT4, false, false);
          mssql_bind($stmt, '@Token', &$securityToken, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@MaxCaptureID', &$maxCaptureId, SQLINT4, false, false);
          mssql_bind($stmt, '@SecuritiesAccount', &$securitiesAccount, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@DateFrom', &$folioDateFrom, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@DateTo', &$folioDateTo, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@KnowledgeDate', &$knowledgeDate, SQLVARCHAR, false, false);

          } else
          {
          // Pass by reference deprecated in PHP 5.4

          mssql_bind($stmt, '@UserID', $userID, SQLINT4, false, false);
          mssql_bind($stmt, '@Token', $securityToken, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@MaxCaptureID', $maxCaptureId, SQLINT4, false, false);
          mssql_bind($stmt, '@SecuritiesAccount', $securitiesAccount, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@DateFrom', $folioDateFrom, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@DateTo', $folioDateTo, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@KnowledgeDate', $knowledgeDate, SQLVARCHAR, false, false);
          }

        $proc_result = mssql_execute($stmt);

        if (($proc_result !== true) && ($proc_result !== false))
          {
          if (mssql_num_rows($proc_result))
            {
            while ($row = mssql_fetch_assoc($proc_result))
              {
              $returnArray[] = $row;
              }

            }
          }

        }

      mssql_free_statement($stmt);
      }
    } catch (Exception $e)
    {
    }

  return $returnArray;
  }

function get_ConfirmationHistory($params)
  {
  $knowledgedate = substr((string)$params['KNOWLEDGEDATE'], 0, 23); //  YYYY-MM-DD HH:mm:ss.sss
  $neo_bankreference = (string)$params['NEO_BANKREFERENCE'];
  $returnArray = array();

  $p_knowledgedate = ((strlen($knowledgedate) > 5) ? "CONVERT(datetime, '" . $knowledgedate . "', 121)" : 'NULL');

  // 'CN','RJ' drop out after 2 days.

  $whereString = " WHERE ((neo_st NOT IN ('CN','RJ')) OR (DateEntered > DATEADD(weekday, -2, GETDATE())))";


  if (strlen($neo_bankreference) > 0)
    {
    $whereString = $whereString . " AND (NEO_BANKREFERENCE = '" . $neo_bankreference . "')";
    }

  try
    {
    $link = sqlserver_neocapture_connect();

    if ($link)
      {
      $queryString =
        "SELECT " .
          "RN,confirmationID,captureID,instance,neo_head,neo_bank,neo_st,neo_status,neo_cat,neo_t,neo_settlement,CONVERT(nvarchar, neo_settlement_date, 120) AS neo_settlement_date,neo_clientreference,neo_bankreference,neo_quantity,neo_quantity_value,neo_unit,neo_isin,neo_securitydescription,neo_settlementamount,neo_settlementcurrency,neo_price,neo_price_value,neo_pricecurrency,neo_blank1,neo_trade,CONVERT(nvarchar, neo_trade_date, 120) AS neo_trade_date, neo_securitiesaccount,RIGHT([neo_securitiesaccount], 11) AS [account],neo_accountdescription,neo_settagentbic,neo_settagentdescr,neo_ctrpbic,neo_tradecounterpartdescr,UserEntered,CONVERT(nvarchar, DateEntered, 120) AS DateEntered " .
          "FROM [dbo].[fn_getConfirmationHistory] ('" . $neo_bankreference . "'," . $p_knowledgedate . ") " . $whereString;
      $result = mssql_query($queryString, $link);

      if (mssql_num_rows($result))
        {
        while ($row = mssql_fetch_assoc($result))
          {
          $returnArray[] = $row;
          }

        }

      mssql_free_result($result);
      }
    } catch (Exception $e)
    {
    }

  return $returnArray;
  }


function get_Milestones($params)
{
  global $db_instances;

  $userID = $params['USERID'];
  $securityToken = $params['TOKEN'];
  $DateFrom = (string)$params['DATE_FROM']; //  YYYY-MM-DD
  $DateTo = (string)$params['DATE_TO']; //  YYYY-MM-DD

  $results_array = array();
  $knowledgeDate = '1900-01-01';
  if (isset($params['KNOWLEDGEDATE'])) { $knowledgeDate = (strlen($params['KNOWLEDGEDATE']) > 0 ? substr((string)$params['KNOWLEDGEDATE'], 0, 23) : '1900-01-01'); } //  YYYY-MM-DD HH:mm:ss.sss }

  if (strlen($DateFrom) <= 0)
    {
    $DateFrom = date("Y-m-d");
    }

  if (strlen($DateTo) <= 0)
  {
    $DateTo = date("Y-m-d");
  }

  try
    {
    foreach ($db_instances as $db_instance)
      {
      $sqlsvr_conn = renaissance_connect($db_instance['INSTANCE'],$db_instance['SERVER']);
      //ini_set('display_errors', 1);

      // Create a new stored procedure

      $stmt = mssql_init('web_GetMilestones', $sqlsvr_conn);

      // Execute

      if ($stmt)
        {
          if (substr(phpversion(), 0, 4) == '5.3.')
          {

            mssql_bind($stmt, '@UserID', &$userID, SQLINT4, false, false);
            mssql_bind($stmt, '@Token', &$securityToken, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@DATE_FROM', &$DateFrom, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@DATE_TO', &$DateTo, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@KnowledgeDate', &$knowledgeDate, SQLVARCHAR, false, false);

          } else
          {
            // Pass by reference deprecated in PHP 5.4

            mssql_bind($stmt, '@UserID', $userID, SQLINT4, false, false);
            mssql_bind($stmt, '@Token', $securityToken, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@DATE_FROM', $DateFrom, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@DATE_TO', $DateTo, SQLVARCHAR, false, false);
            mssql_bind($stmt, '@KnowledgeDate', $knowledgeDate, SQLVARCHAR, false, false);
          }

          $proc_result = mssql_execute($stmt);

          // If Statement succeeds and returns some values, i.e. is a resource rather than $proc_result == true...

          if (($proc_result !== true) && ($proc_result !== false))
          {
            if (mssql_num_rows($proc_result) > 0)
            {
              while ($row = mssql_fetch_assoc($proc_result))
              {
                $row['INSTANCE'] = $db_instance['INSTANCE'];

                $results_array[] = $row;
              }
            }
          }

          // Free statement
          mssql_free_statement($stmt);
        }

      }

    }
  catch (Exception $e)
    {
    }

  return $results_array;
}



function convertAnsi2UTF8($array)
  {
  foreach ($array as $key => $item)
    {
    if (is_array($item))
      {
      $array[$key] = convertAnsi2UTF8($item);
      } elseif (!is_string($item))
      {
      $array[$key] = $item;
      } else
      {
      $array[$key] = iconv("TIS-620", "UTF-8", $item);
      }
    }
  return $array;
  }

function make_table_string($table,$links,$pagesize,$page,$tablename){
  	$tablestring="<table cellspacing=\"0\" summary=\"table\"
  	class=\"broom_table\" id=\"$tablename\">";
  	if ((is_array($table)) && (count($table)>0)) {
  		$count=0;
  		$odd=true;
  		$pages=1;
  		if ($pagesize>0){
  			if
  			((floor(count($table)/$pagesize))==(count($table)/$pagesize)){
  				$pages=(floor(count($table)/$pagesize));
  			} else {
  				$pages=(floor(count($table)/$pagesize)+1);
  			}
  		}
  		if ($page>$pages){$page=$pages;}
  		if ($page>0){
  			$showstart=($page-1)*$pagesize+1;
  			$showend=$showstart+($pagesize-1);
  		} else {
  			$showstart=0;
  			$showend=count($table)+1;
  		}
  		foreach ($table as $tableitem) {
  			$tableitemvalues=array_values($tableitem);
  			$tableitemkeys=array_keys($tableitem);
  			if ($count==0){
  				$tablestring.="<thead>"."\n";
  				//echo "<tr bgcolor=\"".$color."\">"."\n";
  				$tablestring.="<tr>"."\n";
  				if (count($links)>0){
  					$tablestring.="<th>Action</th>\n";
  				}
  				foreach ($tableitemkeys as $cell){
  					$tablestring.="<th scope=\"col\" id=\"".$cell."\"class=\"sorthead\">".$cell."</th>"."\n";
  				}
  				$tablestring.="</tr>"."\n";
  				$tablestring.="</thead>"."\n";
  				$tablestring.="<tbody>"."\n";
  			}
  			if (($count+1)>=$showstart){
  				if (($count+1)<=$showend){
  					if ($odd){
  						$tablestring.="<tr class=\"odd datarow\" id=\"".$tableitemvalues[0]."\">\n";
  						$odd=false;
  					} else {
  						$tablestring.="<tr class=\"even datarow\" id=\"".$tableitemvalues[0]."\">\n";
  						$odd=true;
  					}
  					if (count($links)>0){
  						$tablestring.="<td class=\"link\">&nbsp".$links[$count]."</td>\n";
  					}
  					foreach ($tableitemvalues as $cell){
  						$tablestring.="<td>".$cell."</td>\n";
  					}
  					$tablestring.="</tr>\n";
  				}
  			}
  			$count++;
  		}
  		$tablestring.="</tbody>\n";
  		$tablestring.="</table>\n";
  
  
  		$tablestring.="<div class=\"twocolwrap\">\n";
  		$tablestring.="<div class=\"leftcol pagination\">\n";
  
  		$startpage=$page-2;
  		if ($startpage<1){$startpage=1;}
  		if ($pages<($startpage+4)){
  			$endpage=$pages;
  		} else {
  			$endpage=$startpage+4;
  		}
  		$tablestring.="<a id=\"First\" href=\"#\" class=\"pager\">First</a>\n";
  
  		for ($i=$startpage; $i<=$endpage; $i++){
  			if ($i==$page){
  				$tablestring.="<a id=\"".$i."\" class=\"selected pager\">".$i."</a>\n";
  			} else {
  				$tablestring.="<a id=\"".$i."\" class=\"pager\">".$i."</a>\n";
  			}
  		}
  		$tablestring.="<a id=\"Last\" href=\"#\" class=\"pager\">Last</a>\n";
  		$tablestring.="</div><!--/end .leftcol-->\n";
  		$tablestring.="<div class=\"rightcoljustified\">\n";
  		$tablestring.="Display&nbsp
        <input name=\"recspp\"
          type=\"text\" value=\"".$pagesize."\" class=\"adhocright recordspp\"/>&nbspof ".$count." Records \n";
  		$tablestring.="</div><!--/end .rightcol-->\n";
  		$tablestring.="</div><!--/end .2colwrap-->\n";
  
  
  	} else {
  		$tablestring="No data to display";
  	}
  	return $tablestring;
  }

?>
