<?php
/**
 * User: npennin
 */

session_start();

require_once('../localise/localise.php');
require_once(APPLICATION_ROOT . '/php/globalInclude.php');

try
  {
  $queryParams = $_POST;

  if ($queryParams == null)
    {
    $queryParams = json_decode($_GET);
    }

  } catch (Exception $e)
  {
  // for error.
  echo "Error : " . $e->getCode() . ": " . $e->getMessage() . " in " . $e . getFile() . " on line " . $e->getLine();
  exit;
  }

try
  {
  $rval = get_ConfirmationsQuery($queryParams);
  $rval = json_encode($rval);

  echo $rval;

  exit;

  } catch (Exception $e)
  {
// unsuccessful fetch
  echo "Error : " . $e->getCode() . ": " . $e->getMessage() . " in " . $e . getFile() . " on line " . $e->getLine();
  exit;
  }

?>
