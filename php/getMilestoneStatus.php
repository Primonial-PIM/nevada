<?php
/**
 * User: npennin
 * Date: 25/06/12
 * Time: 15:29
 */


require_once('../localise/localise.php');
require_once(APPLICATION_ROOT . '/php/globalInclude.php');

$MilestoneCount = 0;

/*  SQL Server, Trade Files */

try
{
  $queryParams = $_POST;

  if ($queryParams == null)
  {
    $queryParams = json_decode($_GET);
  }

  $userID = $queryParams['USERID'];
  $securityToken = $queryParams['TOKEN'];

} catch (Exception $e)
{
  // for error.
  echo "Error : " . $e->getCode() . ": " . $e->getMessage() . " in " . $e . getFile() . " on line " . $e->getLine();
  exit;
}

try
{


  $link = sqlserver_neocapture_connect();

  if ($link)
  {
    $queryString = "SELECT * FROM [dbo].[fn_getMilestoneStatusInstance](" . $userID . ", '" . $securityToken . "')";

    $result = mssql_query($queryString, $link);

    if (mssql_num_rows($result))
    {
      while ($row = mssql_fetch_assoc($result))
      {
        $MilestoneCount = $row['MILESTONE_COUNT'];
      }
    }

    mssql_free_result($result);

  }

} catch (Exception $e)
{
}

$rval = array(
  'VeniceAuditCount' => $MilestoneCount);

$rval = json_encode($rval);

echo $rval; //json_encode(get_Names($params)); //, JSON_HEX_TAG);

exit;

?>