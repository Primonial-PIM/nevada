<?php
/**
 * Created by JetBrains PhpStorm.
 * User: nicholas
 * Date: 22/08/2012
 * Time: 12:32
 * To change this template use File | Settings | File Templates.
 */

require_once('../../localise/localise.php');
require_once(APPLICATION_ROOT . '/php/MPDF55/mpdf.php');


session_start();

if (isset($_POST['formdata']))
{
    $_SESSION['formdata'] = $_POST['formdata'];

    return;
}

if (!isset($_SESSION['formdata']))
{
    return;
}

$queryParams = json_decode($_SESSION['formdata'], true);
$_SESSION['formdata'] = '';

$myHtml = $queryParams['html'];
$titleText = $queryParams['title'];
$headerHTML = $queryParams['headerHTML'];
$footerHTML = $queryParams['footerHTML'];

$mpdf = new mPDF('utf-8',    // mode - default ''
    'A4-L',    // format - A4, for example, default ''
    6,     // font size - default 0
    '',    // default font family
    15,    // margin_left
    15,    // margin right
    16,     // margin top
    16,    // margin bottom
    9,     // margin header
    9,     // margin footer
    'L');  // L - landscape, P - portrait

$mpdf->shrink_tables_to_fit = 2.0;

//$mpdf->defaultheaderfontsize = 10;     /* in pts */
//$mpdf->defaultheaderfontstyle = B;     /* blank, B, I, or BI */
//$mpdf->defaultheaderline = 1;    /* 1 to include line below header/above footer */
//$mpdf->defaultfooterfontsize = 12;     /* in pts */
//$mpdf->defaultfooterfontstyle = B;     /* blank, B, I, or BI */
//$mpdf->defaultfooterline = 1;    /* 1 to include line below header/above footer */

$mpdf->setAutoTopMargin = "stretch";


if (strlen($headerHTML) > 0)
{
    $mpdf->SetHTMLHeader($headerHTML);
    //$mpdf->SetHTMLHeader($headerE,'E');
}
else
{
  $mpdf->SetHeader($titleText.'| | {DATE j-M-Y}');
}

if (strlen($footerHTML) > 0)
{
    $mpdf->SetHTMLFooter($footerHTML);
    //$mpdf->SetHTMLFooter($footerE,'E');
}
else
{
    $mpdf->SetFooter('{DATE j-M-Y H:i:s} | | {PAGENO}');    /* Date format is PHP date format : http://www.php.net/manual/en/function.date.php */
}


$mpdf->WriteHTML($myHtml);

$mpdf->Output();


?>