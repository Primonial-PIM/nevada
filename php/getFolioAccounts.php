<?php
/**
 * User: npennin
 * Date: 04/07/12
 * Time: 15:26
 * To change this template use File | Settings | File Templates.
 */

session_start();

require_once('../localise/localise.php');
require_once(APPLICATION_ROOT . '/php/globalInclude.php');

try{
	$queryParams = $_POST;

	if ($queryParams == null) {
		$queryParams = $_GET;
	}

	$params['USERID'] = ($queryParams['USERID']);
	$params['TOKEN'] = ($queryParams['TOKEN']);
}
catch (Exception $e)
{
	// for error.
	echo "Error : ". $e->getCode(). ": ". $e->getMessage(). " in ". $e.getFile(). " on line ". $e->getLine();
	exit;
}


try{

	if ($params['USERID']==null) {$params['USERID']='0';} // Alter to 0 for Venice.
	if ($params['TOKEN']==null) {$params['TOKEN']='';}
	
    $rVal = get_fundAccounts($params);

    echo json_encode($rVal); //json_encode(get_Names($params)); //, JSON_HEX_TAG);

    exit;

}
catch(Exception $e)  {
    // unsuccessful fetch
    echo "Error : ". $e->getCode(). ": ". $e->getMessage(). " in ". $e.getFile(). " on line ". $e->getLine();
    exit;
}

?>
