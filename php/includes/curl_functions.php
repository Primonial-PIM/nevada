<?php

function urldecode_to_array ($url) {
  $ret_ar = array();
  
  if (($pos = strpos($url, '?')) !== false)         // parse only what is after the ?
    $url = substr($url, $pos + 1);
  if (substr($url, 0, 1) == '&')                    // if leading with an amp, skip it
    $url = substr($url, 1);

  $elems_ar = explode('&', $url);                   // get all variables
  for ($i = 0; $i < count($elems_ar); $i++) {
    list($key, $val) = explode('=', $elems_ar[$i]); // split variable name from value
    $ret_ar[urldecode($key)] = urldecode($val);     // store to indexed array
  }

  return $ret_ar;
}


function scrape_web($url, $selector, $filter){
	$foundclip='';
	try{
		$mydom=str_get_html(curl_download($url));
		if (isset($filter)){
			// filter all matches and return the last one to match 'filter'
			foreach($mydom->find($selector) as $clip){
				$pos = strpos($clip, $filter);
				if ($pos === false) {

				} else {
					$foundclip=$clip;
				}
			}
		} else {
			//return the first match with no filter
			$foundclip=$mydom->find($selector,0);
		}
	}
	catch(Exception $e)  {
		$foundclip='Could not find html';
	}
	return $foundclip;
	//return "Test";
}

function extract_web($url_content, $selector, $filter){
	$foundclip='';
	try{
		$mydom=str_get_html($url_content);
		if (isset($filter)){
			// filter all matches and return all those that match 'filter'
			foreach($mydom->find($selector) as $clip){
				$pos = strpos($clip, $filter);
				if ($pos === false) {

				} else {
					$foundclip=$foundclip."\n".$clip;
				}
			}
		} else {
			//return the all matches with no filter
			foreach($mydom->find($selector) as $clip){
				$foundclip=$foundclip."\n".$clip;
			}
		}
	}
	catch(Exception $e)  {
		$foundclip='Could not find html';
	}
	return $foundclip;
	//return "Test";
}

function insert_currentdir($scrapeclip,$currentdir){
	$outstring='';
	$length=strlen($scrapeclip);
	$char = str_split($scrapeclip);
	for ($i=0; $i<$length; $i++){

		$last6='';
		$next1='';
		$next4='';

		//set last6
		if ($i>4){
			$last6=$char[$i-5].$char[$i-4].$char[$i-3].$char[$i-2].$char[$i-1].$char[$i];
		}

		//set next1
		if ($i<($length-1)){
			$next1=$char[$i+1];
		}

		//set next4
		if ($i<($length-4)){
			$next4=$char[$i+1].$char[$i+2].$char[$i+3].$char[$i+4];
		}

		$outstring=$outstring.$char[$i];

		if ($last6=='href="'){
			if ($next4!='http'){
				if (ctype_alpha($next1)){
					$outstring=$outstring.$currentdir;
				}
			}
		}
	}
	return $outstring;
}

function curl_download($Url){

	// is cURL installed yet?
	if (!function_exists('curl_init')){
		die('Sorry cURL is not installed!');
	}

	// OK cool - then let's create a new cURL resource handle
	$ch = curl_init();

	// Now set some options (most are optional)

	// Set URL to download
	curl_setopt($ch, CURLOPT_URL, $Url);

	// Set a referer
	curl_setopt($ch, CURLOPT_REFERER, "http://www.example.org/yay.htm");

	// User agent
	curl_setopt($ch, CURLOPT_USERAGENT, "MozillaXYZ/1.0");

	// Include header in result? (0 = yes, 1 = no)
	curl_setopt($ch, CURLOPT_HEADER, 0);

	// Should cURL return or print out the data? (true = return, false = print)
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	// Timeout in seconds
	curl_setopt($ch, CURLOPT_TIMEOUT, 20);

  // CURLOPT_FOLLOWLOCATION
  // Not sure if I want to do this?
  //curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

  // Disable SSL checks
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

	// Download the given URL, and return output
	$output = curl_exec($ch);

	// Close the cURL resource, and free system resources
	curl_close($ch);

	return $output;
}
?>