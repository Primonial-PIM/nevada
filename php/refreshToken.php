<?php
/**
 * 
 * User: nicholas
 * Date: 12/03/2014
 * Time: 13:29
 * To change this template use File | Settings | File Templates.
 */

session_start();

require_once('../localise/localise.php');
require_once(APPLICATION_ROOT . '/php/globalInclude.php');

try
  {
  $queryParams = $_POST;

  if ($queryParams == null)
    {
    $queryParams = $_GET;
    }

  $params['USERID'] = ($queryParams['USERID']);
  $params['TOKEN'] = ($queryParams['TOKEN']);

  } catch (Exception $e)
  {
  // for error.
  echo "Error : " . $e->getCode() . ": " . $e->getMessage() . " in " . $e . getFile() . " on line " . $e->getLine();
  exit;
  }

try
  {

  $rval = RefreshToken($params);
  $rval = json_encode($rval);

  echo $rval;

  exit;

  } catch (Exception $e)
  {
  echo "Error : " . $e->getCode() . ": " . $e->getMessage() . " in " . $e . getFile() . " on line " . $e->getLine();
  exit;
  }


?>
