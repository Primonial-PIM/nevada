<?php
/**
 * User: npennin
 * Date: 03/09/12
 * Time: 13:07
 */

require_once('../localise/localise.php');
require_once(APPLICATION_ROOT . '/php/globalInclude.php');

// Validate the parameters

try
{
    $queryParams = $_POST;


    if ($queryParams == null) {
        $queryParams = $_GET;
    }

}
catch (Exception $e)
{
    // for error, return nothing.

    if (PHP_DEBUG)
    {
        echo "Error : ". $e->getCode(). ": ". $e->getMessage(). " in ". $e.getFile(). " on line ". $e->getLine();
    }

    exit;

}

try{


    $results_array = array();

    try
    {

    	$username = $queryParams['USERNAME'];
    	$password = $queryParams['PASSWORD'];

      $results_array = array();

      $sqlsvr_conn = sqlserver_neocapture_connect();
      //ini_set('display_errors', 1);

      // Create a new stored procedure

      $stmt = mssql_init('sp_verifyPassword', $sqlsvr_conn);

      if ($stmt)
        {
        if (substr(phpversion(), 0, 4) == '5.3.')
          {

          mssql_bind($stmt, '@UserName', &$username, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@Password', &$password, SQLVARCHAR, false, false);

          } else
          {
          // Pass by reference deprecated in PHP 5.4

          mssql_bind($stmt, '@UserName', $username, SQLVARCHAR, false, false);
          mssql_bind($stmt, '@Password', $password, SQLVARCHAR, false, false);
          }
        }

      $proc_result = mssql_execute($stmt);

      // If Statement succeeds and returns some values, i.e. is a resource rather than $proc_result == true...

      if (($proc_result !== true) && ($proc_result !== false))
        {
        if (mssql_num_rows($proc_result) > 0)
          {
          while ($row = mssql_fetch_assoc($proc_result))
            {
            $results_array[] = $row;
            }
          }
        }

        /*
    	// DUMMY CODE  ****************************
    	
    	if (strpos($username,"@primonial.fr")>5 && $password=="pw"){
    	//if ($username=="anthony.culligan@primonial.fr" && $password=="pw"){
    		$results_array[0] = array("userID"=>"1","sessionKey"=>"abcdefg","userIsAdmin"=>1,"userIsReadonly"=>0);
    	} else {
    		$results_array[0] = array("userID"=>$username,"sessionKey"=>"fail");
    	}
    	
    	// END DUMMY CODE *************************
		*/
    
    } catch (Exception $e)
    {
       if (PHP_DEBUG)
        {
        $results_array[0] = array();
        $results_array[0]['userID'] = -1;
        $results_array[0]['sessionKey'] = "Error in login_checkUser : " . $e->getCode() . ": " . $e->getMessage() . " in " . $e->getFile() . " on line " . $e->getLine();
        }
        else
        {
        $results_array[0] = array();
        $results_array[0]['userID'] = -1;
        $results_array[0]['sessionKey'] = "Error";
        }
    }

    $rval = json_encode($results_array);

    echo $rval; //json_encode(get_Names($params)); //, JSON_HEX_TAG);

    exit;

}
catch(Exception $e)
  {
    // unsuccessful fetch
    if (PHP_DEBUG)
    {
        echo "Error : ". $e->getCode(). ": ". $e->getMessage(). " in ". $e.getFile(). " on line ". $e->getLine();
    }

    exit;
  }


?>
