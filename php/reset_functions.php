<?php

require_once(APPLICATION_ROOT . '/php/globalInclude.php');

function get_userFromEmail($email){

    try{

        $results_array = array();

        try
        {
        	/*
            $conn = db_connect();

            $conn->set_charset("utf8");

            $dbSalt = PAM_SALT6;
            $dbEmailAddress = mysqli_real_escape_string($conn, $email);

            $query = "exec sp_reset_getUserFromEmail('" . $dbSalt . "', '" . $dbEmailAddress . "')";

            if (!($result = $conn->query($query)))
            {
                mysqli_close($conn);
                throw new Exception('Error calling sp_reset_getUserFromEmail() : ' . $conn->errno . ', ' . $conn->error);
            }

            for ($count = 0; $row = $result->fetch_assoc(); $count++)
            {
                $results_array[$count] = $row;
            }

            mysqli_free_result($result);
            mysqli_close($conn);
            
           */
        	
        	// DUMMY CODE ******************************************
        	 
        	if ($email=="anthony.culligan@primonial.fr"){
        		$results_array[0] = array("USERNAME"=>"1");
        	} else {
        		$results_array[0] = array("USERNAME"=>"");
        	}
        	
        	// END DUMMY CODE **************************************

        } catch (Exception $e)
        {
            return "";
            exit;
        }

        if (count($results_array) > 0)
        {
            return $results_array[0]['USERNAME'];
            exit;
        }

    }
    catch(Exception $e)
    {
        // unsuccessful fetch
        if (PHP_DEBUG)
        {
            echo "Error : ". $e->getCode(). ": ". $e->getMessage(). " in ". $e.getFile(). " on line ". $e->getLine();
        }

        return "";

    }

	return "";
}

function save_token($userID, $token, $expiretime){
    // $expiretime in the format of : date('Y-m-d H:i:s', $phpdate);

    // NOTE : Database will not accept token length < 15 characters.
    // $expiretime can not be more than 1 Day in the future.

    try{

        $results_array = array();

        try
        {
        	
        	/*
            $conn = db_connect();

            $conn->set_charset("utf8");

            $dbSalt = PAM_SALT4;
            $dbUserID = mysqli_real_escape_string($conn, $userID);
            $dbToken = mysqli_real_escape_string($conn, $token);
            $dbExpireTime = mysqli_real_escape_string($conn, $expiretime);

            $query = "exec sp_reset_saveResetToken('" . $dbSalt . "', '" . $dbUserID . "', '" . $dbToken . "', '" . $dbExpireTime . "')";

            if (!($result = $conn->query($query)))
            {
                mysqli_close($conn);
                throw new Exception('Error calling sp_reset_saveResetToken() : ' . $conn->errno . ', ' . $conn->error);
            }

            for ($count = 0; $row = $result->fetch_assoc(); $count++)
            {
                $results_array[$count] = $row;
            }

            mysqli_free_result($result);
            mysqli_close($conn);
            
            */
        	
        	// DUMMY CODE ******************************************
        	
        	$results_array[0]['STATUS'] = 'OK';
        	
        	// END DUMMY CODE **************************************

        } catch (Exception $e)
        {
            return "";
            exit;
        }

        if ((count($results_array) > 0) && ($results_array[0]['STATUS'] == 'OK'))
        {
            return true;
            exit;
        }

    }
    catch(Exception $e)
    {
        // unsuccessful fetch
        if (PHP_DEBUG)
        {
            echo "Error : ". $e->getCode(). ": ". $e->getMessage(). " in ". $e.getFile(). " on line ". $e->getLine();
        }

        return false;

    }

	return false;
}

function valid_token($token){

    /* Returns Email address for valid token, empty string for all else.

      NOTE : This function will only work once for any given token.

      The SQL function, sp_reset_getEmailFromToken() :
          Will return STATUS of 'OK' and the USERNAME and USEREMAIL address if successfull.
          Will return a STATUS of 'expired' if this check has been performed already for the given token.
          Will return a STATUS of 'fail' if this he token is not recognised.
      */

    try{

        $results_array = array();

        try
        {
        	/*
            $conn = db_connect();

            $conn->set_charset("utf8");

            $dbSalt = PAM_SALT3;
            $dbToken = mysqli_real_escape_string($conn, $token);

            $query = "exec sp_reset_getEmailFromToken('" . $dbSalt . "', '" . $dbToken . "')";

            if (!($result = $conn->query($query)))
            {
                mysqli_close($conn);
                throw new Exception('Error calling sp_reset_getEmailFromToken() : ' . $conn->errno . ', ' . $conn->error);
            }

            for ($count = 0; $row = $result->fetch_assoc(); $count++)
            {
                $results_array[$count] = $row;
            }

            mysqli_free_result($result);
            mysqli_close($conn);
            
            */
        	
        	// DUMMY CODE ******************************************
        	
        	
        	if ($token=="df645dff675bf675044a05a6b9e0f25f5561ecc6"){
        		$results_array[0] = array("USEREMAIL"=>"anthony.culligan@primonial.fr","STATUS"=>"OK");
        		
        	} else {
        		$results_array[0] = array("USEREMAIL"=>"","STATUS"=>"OK");
        		
        	}
        	
        	// END DUMMY CODE **************************************

        } catch (Exception $e)
        {
            return "";
            exit;
        }

		//print_r($results_array);
		
        if ((count($results_array) > 0) && ($results_array[0]['STATUS'] == 'OK'))	
        {
            return $results_array[0]['USEREMAIL'];
            exit;
        }

    }
    catch(Exception $e)
    {
        // unsuccessful fetch
        if (PHP_DEBUG)
        {
            echo "Error : ". $e->getCode(). ": ". $e->getMessage(). " in ". $e.getFile(). " on line ". $e->getLine();
        }

        return '';

    }

	return "";
}

function save_new_password($email, $token, $password){
    /*
        NOTE, the SQL function will not accept a password shorter than 8 characters.
    */
    try{

        $results_array = array();

        try
        {
        	/*
            $conn = db_connect();

            $conn->set_charset("utf8");

            $dbSalt = PAM_SALT1;
            $dbEmailAddress = mysqli_real_escape_string($conn, $email);
            $dbPassword = mysqli_real_escape_string($conn, $password);
            $dbToken = mysqli_real_escape_string($conn, $token);


            $query = "exec sp_reset_setNewPassword('" . $dbSalt . "', '" . $dbEmailAddress . "', '" . $dbPassword . "', '" . $dbToken . "','" . $_SERVER['REMOTE_ADDR'] . "','" . $_SERVER['HTTP_USER_AGENT'] . "')";

            if (!($result = $conn->query($query)))
            {
                mysqli_close($conn);
                throw new Exception('Error calling sp_reset_setNewPassword() : ' . $conn->errno . ', ' . $conn->error);
            }

            for ($count = 0; $row = $result->fetch_assoc(); $count++)
            {
                $results_array[$count] = $row;
            }

            mysqli_free_result($result);
            mysqli_close($conn);
            */
        	
        	// DUMMY CODE ******************************************
        	
        	$results_array[0] = array("STATUS"=>"OK");
        
        	// END DUMMY CODE **************************************

        } catch (Exception $e)
        {
            return false;
            exit;
        }

        if ((count($results_array) > 0) && ($results_array[0]['STATUS'] == 'OK'))
        {
            return true;
            exit;
        }

    }
    catch(Exception $e)
    {
        // unsuccessful fetch
        if (PHP_DEBUG)
        {
            echo "Error : ". $e->getCode(). ": ". $e->getMessage(). " in ". $e.getFile(). " on line ". $e->getLine();
        }

        return false;

    }

    return false;

}

?>