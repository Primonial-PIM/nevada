<?php
require_once('../localise/localise.php');
require_once('../php/dbFunctions.php');
error_reporting(0);

try{
	$queryParams = $_POST;

	if ($queryParams == null) {
		$queryParams = $_GET;
	}

	$params['USERID'] = ($queryParams['USERID']);
	$params['TOKEN'] = ($queryParams['TOKEN']);
}
catch (Exception $e)
{
	// for error.
	echo "Error : ". $e->getCode(). ": ". $e->getMessage(). " in ". $e.getFile(). " on line ". $e->getLine();
	exit;
}

try
{

	if ($params['USERID']==null) {$params['USERID']='0';} // Alter to 0 for Venice.
	if ($params['TOKEN']==null) {$params['TOKEN']='';}
	
	$rows=array();
	
	$rows[]=headerRow(array("Process","Script","Last Attempt","Result","Last Success"));
	
	
	// heading
	
	// 	 	* Positions - capturePositions.php - Custody All Positions. Every 10 minutes from 6am to 9pm
	//      * Luxembourg Confirms  - captureLuxConfirmations.php - every 10 minutes from 6am to 9pm
	//      * UCITS orders - captureUcitsOrders.php - every 10 minutes from 6am to 9pm
	//      * Paris Confirms - captureParisConfirmations.php - every 10 minutes from 6am to 9pm
	//      * Reconciliation - captureReconciliation.php - Fund Administration by Fund. Every 10 minutes from 6am to 9pm
	
	$log=get_LastRunDetails($params);
	
	/*
	 * 1 - Capture Positions
	 * 2 - Capture Lux Confirmations
	 * 3 - Capture Paris Confirmations
	 * 4 - Capture Reconciliations
	 * 5 - Capture Trade Files
	 * 6 - Capture UCITS orders
	 * 7 - Capture Morningstar Prices
	 * 8 - Check Confirmations
	 * 8 - Check Subscriptions/Redemptions *** DUPLICATE
	 * 11 - Capture Morningstar Index Returns
	 * 
	 * Check Acknowledgement Lux - no log
	 * Check Acknowledgement Paris - no log
	 * Write TradeFiles Lux - no log
	 * Write Tradefiles Paris - no log
	 * 
	 */

	$rows[]=tableRow(array("Positions","capturePositions.php",getlog(1,"Time",$log),getlog(1,"Result",$log),getlog(1,"Success",$log)),true);
	$rows[]=tableRow(array("Lux Confirms","captureLuxConfirmations.php",getlog(2,"Time",$log),getlog(2,"Result",$log),getlog(2,"Success",$log)),false);
	$rows[]=tableRow(array("UCITS Orders","captureUcitsOrders.php",getlog(6,"Time",$log),getlog(6,"Result",$log),getlog(6,"Success",$log)),true);
	$rows[]=tableRow(array("Paris Confirms","captureParisConfirmations.php",getlog(3,"Time",$log),getlog(3,"Result",$log),getlog(3,"Success",$log)),false);
	$rows[]=tableRow(array("Reconciliation","captureReconciliation.php",getlog(4,"Time",$log),getlog(4,"Result",$log),getlog(4,"Success",$log)),true);
	
	$html=makeTable($rows);
	
	
}
catch (Exception $e)
{
	echo $e;
}

echo ($html);

function getlog($setID,$field,$log){
	foreach ($log as $entry){
		if ($entry['setID']==$setID){
			if ($field=='Time'){
				return $entry['DateEntered'];
				exit;
			}
			if ($field=='Result'){
				if ($entry['result']=="success"){
					return 'success';
				} else {
					return '<span class="statusWarning">'.$entry['result'].'</span>';
				}
				return $entry['result'];
				exit;
			}
			if ($field=='Success'){
				if ($entry['result']=="success"){
					return $entry['DateEntered'];
					exit;
				}
			}
			
		}
	}
	return 'Not Found';
}

function headerRow($cells){
	$html="<thead>";
	foreach ($cells as $cell){
		$html.="<th>".$cell."</th>";
	}
	$html.="</thead>";
	return $html;
}

function tableRow($cells,$odd){
	if ($odd===true){
		$html="<tr class='odd'>";
	} else {
		$html="<tr>";
	}
	foreach ($cells as $cell){
		$html.="<td>".$cell."</td>";
	}
	$html.="</tr>";
	return $html;
}

function makeTable($rows){
	$html="<table class='broom_table'>";
	foreach ($rows as $row){
		$html.=$row;
	}
	$html.="</table>";
	return $html;
}

?>