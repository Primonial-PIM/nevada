<?php
require_once('../localise/localise.php');
//error_reporting(0);
date_default_timezone_set('Europe/Paris');

try{
	$queryParams = $_POST;

	if ($queryParams == null) {
		$queryParams = $_GET;
	}

	$params['USERID'] = ($queryParams['USERID']);
	$params['TOKEN'] = ($queryParams['TOKEN']);
}
catch (Exception $e)
{
	// for error.
	echo "Error : ". $e->getCode(). ": ". $e->getMessage(). " in ". $e.getFile(). " on line ". $e->getLine();
	exit;
}


try
{
	if ($params['USERID']==null) {$params['USERID']='0';} // Alter to 0 for Venice.
	if ($params['TOKEN']==null) {$params['TOKEN']='';}

	$rows=array();
	
	$filestatus=checkFiles();
	
	$rows[]=headerRow(array("Description","Location","Count","Today","Most Recent","Status"));
	
	foreach ($filestatus as $status){
		$rows[]=tableRow($status);
	}
	
	$html=makeTable($rows);
	
	/*
	$rows[]=headerRow(array("Process","Script","Period","Waiting","Done","Status"));
	
	
	// heading
	
	 //* FTP and file tasks
     //* 
     //* Write Paris Trade files for FTP to BNP - writeTradefilesParis.php - every five minutes from 6am to 9pm
     //* Write Lux Trade files for FTP to BNP - writeTradefilesLuxBNP.php - every five minutes from 6am to 9pm
     //* Read Trade files for Nevada monitoring - captureTradeFiles.php - every minute from 6am to 9pm
     //* Paris Acknowledgement Files to update Venice - checkAcknowledgementBNPParis.php - every 5 mins 6am to 9pm
     //* Lux Acknowledgement Files to update Venice - checkAcknowledgementBNPLuxembourg.php - every 5 mins 6am to 9pm
     //* Subs and Redmptions Files to update Venice - checkSubscriptionsBNPParis.php - every 10 mins
     //* Read Paris Confirmations files to update Venice - checkConfirmationsBNPParis.php - every 5 minutes 6am to 9pm 

	$rows[]=tableRow(array("Paris Trade Files","writeTradeFilesParis.php","Every 5 mins","1","5","Good"),true);
	$rows[]=tableRow(array("Lux Trade Files"," writeTradefilesLuxBNP.php","Every 5 mins","1","5","Good"),false);
	$rows[]=tableRow(array("Read Trade Files for Nevada","captureTradeFiles.php","Every minute",$filecount[0],$filecount[1],"Good"),true);
	$rows[]=tableRow(array("Paris Acknowledgements","checkAcknowledgementBNPParis.php","Every 5 mins","1","5","Failed"),false);
	$rows[]=tableRow(array("Lux Acknowledgements","checkAcknowledgementBNPLuxembourg.php","Every 5 mins","1","5","Good"),true);
	$rows[]=tableRow(array("Subscriptions and Redemptions","checkSubscriptionsBNPParis.php","Every 10 mins","1","5","Good"),true);
	$rows[]=tableRow(array("Confirmations","checkConfirmationsBNPParis.php","Every 5 mins","1","5","Good"),true);
	
	$html=makeTable($rows);
	*/
	
}
catch (Exception $e)
{
	echo $e;
}

echo ($html);


function headerRow($cells){
	$html="<thead>";
	foreach ($cells as $cell){
		$html.="<th>".$cell."</th>";
	}
	$html.="</thead>";
	return $html;
}

function tableRow($cells,$odd){
	if ($odd===true){
		$html="<tr class='odd'>";
	} else {
		$html="<tr>";
	}
	foreach ($cells as $cell){
		$html.="<td>".$cell."</td>";
	}
	$html.="</tr>";
	return $html;
}

function makeTable($rows){
	$html="<table class='broom_table'>";
	foreach ($rows as $row){
		$html.=$row;
	}
	$html.="</table>";
	return $html;
}

function checkFiles(){
	
	$filestatus=array(); 
	
	$filestatus[]=array("description"=>"Trade Files for Paris","location"=>PARIS_TRADEFILE_DIRECTORY,"count"=>0,"today"=>0,"last"=>"","status"=>"");
	$filestatus[]=array("description"=>"Trade Files for Lux","location"=>BNP_LUX_TRADEFILE_DIRECTORY,"count"=>0,"today"=>0,"last"=>"","status"=>"");
    $filestatus[]=array("description"=>"Executions Files","location"=>EXECUTIONS_FILE_DIRECTORY,"count"=>0,"today"=>0,"last"=>"","status"=>"");
	$filestatus[]=array("description"=>"Trade Files already Sent","location"=>NEOCAPTURE_TRADEFILES_ROOT."/SENT","count"=>0,"today"=>0,"last"=>"","status"=>"");
	
	$filestatus[]=array("description"=>"Acknowledgement Files","location"=>PARIS_TRADEFILES_ACKNOWLEDGEMENT,"count"=>0,"today"=>0,"last"=>"","status"=>"");
	$filestatus[]=array("description"=>"Acknowledgement Files Processed","location"=>PARIS_TRADEFILES_ACKNOWLEDGEMENT_PROCESSED,"count"=>0,"today"=>0,"last"=>"","status"=>"");
	
	$filestatus[]=array("description"=>"Confirmation Files","location"=>PARIS_CONFIRMATIONFILES,"count"=>0,"today"=>0,"last"=>"","status"=>"");
	$filestatus[]=array("description"=>"Confirmation Files Processed","location"=>PARIS_CONFIRMATIONFILES_PROCESSED,"count"=>0,"today"=>0,"last"=>"","status"=>"");
	
	$filestatus[]=array("description"=>"Subscription/Redemption Files","location"=>PARIS_ORDERFILES,"count"=>0,"today"=>0,"last"=>"","status"=>"");
	$filestatus[]=array("description"=>"Subscription/Redemptions Processed","location"=>PARIS_ORDERFILES_PROCESSED,"count"=>0,"today"=>0,"last"=>"","status"=>"");
	
	$datenow = new DateTime('now');
	$today = $datenow->format('Y-m-d');
		
	foreach ($filestatus as &$status)
	{
		
		$directoryName =$status['location'];
		
		$directoryFiles = scandir($directoryName);
	
		try
		{
			if ($directoryFiles!==false)
				
				$recentdate= new DateTime('1970-01-01');
				
				foreach ($directoryFiles as $thisFile)
				{
					// is this file is a directory, especially . and ..
	
					if (is_dir($directoryName . '/' . $thisFile))
					{
						continue;
					}
					
					if ($thisFile[0]=="."){
						continue;
					}
	
					// Is the file dated today
	
					$fileDate = new DateTime('@' . filemtime($directoryName . '/' . $thisFile));
					
					if ($fileDate>$recentdate){
						$recentdate=$fileDate;
					}
					
					if ($today==$fileDate->format('Y-m-d')){
						$istoday=true;
					} else {
						$istoday=false;
					}
					
					if($istoday){
						$status['today']++;
					}
	
					$status['count']++;
	
					
				}
		} catch (Exception $e) {
			echo $e;
		}
		
		if ($status['count']>0){
			$status['last']=$recentdate->format('Y-m-d h:i:s');
		} else {
			$status['last']="";
		}
	
		If ($status['description']=="Trade Files for Paris" ||
				$status['description']=="Trade Files for Lux" ||
                $status['description']=="Executions Files" ||
				$status['description']=="Acknowledgement Files" ||
				$status['description']=="Confirmation Files" ||
				$status['description']=="Subscription/Redemption Files" )
		{
	
			if ($status['count']>0){
				$since_start=$datenow->diff($recentdate);
				$minutes = $since_start->days * 24 * 60;
				$minutes += $since_start->h * 60;
				$minutes += $since_start->i;
				if ($minutes<10){
					$status['status']="OK";
				} else {
					$status['status']='<span class="statusWarning">Failed</span>';
				}
				
			} else {
				$status['status']="OK";
			}
				
		} else {
			$status['status']="OK";
		}
				
		
		
		
	}
	
	return $filestatus;
}

?>