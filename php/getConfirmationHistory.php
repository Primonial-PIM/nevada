<?php
/**
 * User: npennin
 * Date: 30/07/12
 * Time: 13:12
 * To change this template use File | Settings | File Templates.
 */

session_start();

require_once('../localise/localise.php');
require_once(APPLICATION_ROOT . '/php/globalInclude.php');

try
{
    $queryParams = $_POST;

    if ($queryParams == null)
    {
        $queryParams = $_GET;
    }

    $params['NEO_BANKREFERENCE'] = ($queryParams['NEO_BANKREFERENCE']);
    $params['USERID'] = ($queryParams['USERID']);
    $params['TOKEN'] = ($queryParams['TOKEN']);

} catch (Exception $e)
{
    // for error.
    echo "Error : " . $e->getCode() . ": " . $e->getMessage() . " in " . $e . getFile() . " on line " . $e->getLine();
    exit;
}

try
{

    $rval = get_ConfirmationHistory($params);
    $rval = json_encode($rval);

    echo $rval; //json_encode(get_Names($params)); //, JSON_HEX_TAG);

    exit;

} catch (Exception $e)
{
// unsuccessful fetch
    echo "Error : " . $e->getCode() . ": " . $e->getMessage() . " in " . $e . getFile() . " on line " . $e->getLine();
    exit;
}


?>
