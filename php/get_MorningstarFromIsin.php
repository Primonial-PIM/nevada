<?php
require_once('../localise/localise.php');

require_once(APPLICATION_ROOT . '/php/includes/curl_functions.php');

// Retrieve Morningstar ID from ISIN by searching on google
// Arbitrarily assume that if the ID appears bigger than 20 characters the it has gone wrong.

$url="https://www.google.fr/search?q=morningstar+".strtolower($_POST['ISIN']);
$morningstarCode="";

try
{
	$innerHTML = curl_download($url);
}
catch (Exception $e)
{
	echo $e;
}


$doc = new DOMDocument();
$doc->validateOnParse = true;
$doc->loadHTML($innerHTML);

$xpath = new DOMXPath($doc);

$tags = $xpath->query('//h3/a/@href');

foreach ($tags as $tag) {
	$params=(urldecode_to_array($tag->nodeValue));
	if (array_key_exists('q', $params)){
		$qparams=(urldecode_to_array($params['q']));
		if (array_key_exists('id', $qparams)){
			echo $qparams['id'];
			exit;				
		}
	}	
}
 
echo "";

?>