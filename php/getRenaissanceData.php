<?php
/**
 * User: nicholas
 * Date: 18/12/2012
 * Time: 14:07
 */

session_start();

require_once('../localise/localise.php');
require_once(APPLICATION_ROOT . '/php/globalInclude.php');

$returnArray = array();

try
  {
  $queryString = '';
  $queryParams = $_POST;
  $querytype = '';

  if ($queryParams == null)
    {
    $queryParams = $_GET;
    }

  /*  */

  $userID = (!isset($queryParams['USERID']) ? 0 : $queryParams['USERID']);
  $token = (!isset($queryParams['TOKEN']) ? "''" :("'" . $queryParams['TOKEN'] . "'"));

  $fieldlist = (!isset($queryParams['FIELDLIST']) ? "*" : $queryParams['FIELDLIST']);
  $where = (!isset($queryParams['WHERE']) ? '' : $queryParams['WHERE']);

  $fundID = (!isset($queryParams['FUNDID']) ? 0 : $queryParams['FUNDID']);
  
  $fundparams=explode(":",$fundID);
  $fundID=$fundparams[0];
  if (count($fundparams)>1){
  	$instance=$fundparams[1];
  } else {
  	$instance='';
  }
  
  $instrumentID = (!isset($queryParams['INSTRUMENTID']) ? 0 : $queryParams['INSTRUMENTID']);
  $valueDate = (!isset($queryParams['VALUEDATE']) ? 'Null' : ("CAST('" . $queryParams['VALUEDATE'] . "' AS datetime)"));
  $knowledgedate = (!isset($queryParams['KNOWLEDGEDATE']) ? 'Null' : ("CAST('" . $queryParams['KNOWLEDGEDATE'] . "' AS datetime)"));


  $querytype = (!isset($queryParams['QUERYTYPE']) ? '' : $queryParams['QUERYTYPE']);

  switch (strtolower($querytype))
  {
    case 'valuation':

      $legalEntity = (!isset($queryParams['LEGALENTITY']) ? "''" : ("'" . $queryParams['LEGALENTITY']) . "'");
      $statusGroupFilter = (!isset($queryParams['STATUSGROUPFILTER']) ? "''" : ("'" . $queryParams['STATUSGROUPFILTER']) . "'");
      $statusGroupFilter_Secondary = (!isset($queryParams['STATUSGROUPFILTERSECONDARY']) ? "'EOD'" : ("'" . $queryParams['STATUSGROUPFILTER']) . "'");
      $unitPriceVariant = (!isset($queryParams['UNITPRICEVARIANT']) ? 0 : $queryParams['UNITPRICEVARIANT']);
      $administratorDatesFilter = (!isset($queryParams['ADMINISTRATORDATESFILTER']) ? 0 : $queryParams['ADMINISTRATORDATESFILTER']);
      $disregardWatermarkPoints = (!isset($queryParams['DISREGARDWATERMARKPOINTS']) ? 0 : $queryParams['DISREGARDWATERMARKPOINTS']);
      $aggregateToInstrumentParent = (!isset($queryParams['AGGREGATETOINSTRUMENTPARENT']) ? 0 : $queryParams['AGGREGATETOINSTRUMENTPARENT']);
      $showDetailedTransactions = (!isset($queryParams['SHOWDETAILEDTRANSACTIONS']) ? 0 : $queryParams['SHOWDETAILEDTRANSACTIONS']);
      $detailDate = (!isset($queryParams['DETAILDATE']) ? 'Null' : ("CAST('" . $queryParams['DETAILDATE'] . "' AS datetime)"));

      /*$queryString =
          "SELECT " . (strlen($fieldlist) == 0 ? "*" : $fieldlist) . " FROM webfn_Valuation(" . $userID . "," . $token . "," .
                      $fundID . "," . $legalEntity . "," . $valueDate . "," . $unitPriceVariant . "," . $statusGroupFilter . "," .
                      $administratorDatesFilter . "," . $disregardWatermarkPoints . "," . $aggregateToInstrumentParent . "," .
                      $showDetailedTransactions . "," . $detailDate . "," . $knowledgedate . ")";
      */

      $queryString =
        "SELECT " . (strlen($fieldlist) == 0 ? "*" : $fieldlist) . " FROM webfn_Valuation_Nevada(" . $userID . "," . $token . "," .
        $fundID . "," . $valueDate . "," . $unitPriceVariant . "," . $statusGroupFilter . "," . $statusGroupFilter_Secondary . "," .
        $administratorDatesFilter . "," . $knowledgedate . ")";

      /*
      ALTER FUNCTION [dbo].[webfn_Valuation_Nevada]
        (
        @UserID int,
        @Token varchar(100),
        @FundID int,
        @ValueDate datetime,
        @UnitPriceVariant int,
        @StatusGroupFilterPrimary nvarchar(50),
        @StatusGroupFilterSecondary nvarchar(50),
        @AdministratorDatesFilter int,
        @KnowledgeDate datetime
        )
      */
      break;

    case 'characteristicdata':

      $dataCategoryId = (!isset($queryParams['DATACATEGORYID']) ? 0 : $queryParams['DATACATEGORYID']);
      $dataCharacteristicId = (!isset($queryParams['DATACHARACTERISTICID']) ? 0 : $queryParams['DATACHARACTERISTICID']);


      $queryString =
        "SELECT " . (strlen($fieldlist) == 0 ? "*" : $fieldlist) . " FROM webfn_SelectCharacteristicData(" . $userID . "," . $token . "," .
          $dataCategoryId . "," . $dataCharacteristicId . "," . $fundID . "," . $instrumentID . "," . $knowledgedate . ")";


      break;

    default:


  }

  	if (strlen($where) > 0)
    {
    	$queryString = $queryString . " WHERE " . $where;
    }

    if (strlen($queryString) > 0 )
    {
    	foreach ($db_instances as $db_instance){
    		
    		if ($instance==$db_instance['MATCHING']){
    		 
	    		$conn = renaissance_connect($db_instance['INSTANCE'],$db_instance['SERVER']);
	
	    		$result = mssql_query($queryString, $conn);
	
	    		if (mssql_num_rows($result))
	    		{
	    			while ($row = mssql_fetch_assoc($result))
	    			{
	    				$returnArray[] = $row; // convertAnsi2UTF8($row);
	    			}
	
	    		}

    		mssql_free_result($result);
    		}
    	}
    }


  } catch (Exception $e)
  {
  // for error.
  echo "Error : " . $e->getCode() . ": " . $e->getMessage() . " in " . $e . getFile() . " on line " . $e->getLine();
  exit;
  }

try
  {

  $rval = json_encode($returnArray);

  echo $rval;

  exit;

  } catch (Exception $e)
  {
// unsuccessful fetch
  echo "Error : " . $e->getCode() . ": " . $e->getMessage() . " in " . $e . getFile() . " on line " . $e->getLine();
  exit;
  }

?>
