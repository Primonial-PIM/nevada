<?php
/**
 * User: npennin
 */

session_start();

require_once('../localise/localise.php');
require_once(APPLICATION_ROOT . '/php/globalInclude.php');

try
{
    $queryParams = $_POST;

    if ($queryParams == null)
    {
        $queryParams = $_GET;
    }

    $params['FOLIO'] = ($queryParams['FOLIO']);
    $params['SICOVAM'] = ($queryParams['SICOVAM']);
    $params['DATE_FROM'] = ($queryParams['DATE_FROM']);
    $params['DATE_FROM_FORMAT'] = ($queryParams['DATE_FROM_FORMAT']);
    $params['DATE_TO'] = ($queryParams['DATE_TO']);
    $params['DATE_TO_FORMAT'] = ($queryParams['DATE_TO_FORMAT']);
    $params['DATE_FX'] = ($queryParams['DATE_FX']);
    $params['DATE_FX_FORMAT'] = ($queryParams['DATE_FX_FORMAT']);
    $params['NO_C'] = ($queryParams['NO_C']);
    $params['NO_E'] = ($queryParams['NO_E']);
    $params['USE_SETTLEMENTDATE'] = ($queryParams['USE_SETTLEMENTDATE']);
    $params['USERID'] = ($queryParams['USERID']);
    $params['TOKEN'] = ($queryParams['TOKEN']);

} catch (Exception $e)
{
    // for error.
    echo "Error : " . $e->getCode() . ": " . $e->getMessage() . " in " . $e . getFile() . " on line " . $e->getLine();
    exit;
}

try
{
    if ($params['FOLIO'] === null)
    {
        $params['FOLIO'] = '0';
    }

    $rval = get_TransactionSettlementQuery($params);
    $rval = json_encode($rval);

    echo $rval; //json_encode(get_Names($params)); //, JSON_HEX_TAG);

    exit;

} catch (Exception $e)
{
// unsuccessful fetch
    echo "Error : " . $e->getCode() . ": " . $e->getMessage() . " in " . $e . getFile() . " on line " . $e->getLine();
    exit;
}

?>
