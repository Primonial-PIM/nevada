<?php
/**
 * User: npennin
 */

session_start();

require_once('../localise/localise.php');
require_once(APPLICATION_ROOT . '/php/globalInclude.php');

//ini_set('display_errors', 1);

//if (debug){ echo "debug is on"."<br>";}

try
  {
  $queryParams = $_POST;

  if ($queryParams == null)
    {
    $queryParams = $_GET;
    }

  $params['FOLIO'] = ($queryParams['FOLIO']);
  $params['SICOVAM'] = ($queryParams['SICOVAM']);
  $params['DATE_FROM'] = ($queryParams['DATE_FROM']);
  $params['DATE_FROM_FORMAT'] = ($queryParams['DATE_FROM_FORMAT']);
  $params['DATE_TO'] = ($queryParams['DATE_TO']);
  $params['DATE_TO_FORMAT'] = ($queryParams['DATE_TO_FORMAT']);
  $params['NO_C'] = ($queryParams['NO_C']);
  $params['NO_E'] = ($queryParams['NO_E']);
  $params['USERID'] = ($queryParams['USERID']);
  $params['TOKEN'] = ($queryParams['TOKEN']);

  } catch (Exception $e)
  {
  // for error.
  echo "Error : " . $e->getCode() . ": " . $e->getMessage() . " in " . $e . getFile() . " on line " . $e->getLine();
  exit;
  }

try
  {
  if ($params['FOLIO'] == null)
    {
    $params['FOLIO'] = '0'; // SQL Server - Venice, rather than '1' for Sophis.
    }

    
  $rval = get_TransactionQuery($params); // Venice (Renaissance) Function.
  $rval = json_encode($rval);

  echo $rval; //json_encode(get_Names($params)); //, JSON_HEX_TAG);

  exit;

  } catch (Exception $e)
  {
// unsuccessful fetch
  echo "Error : " . $e->getCode() . ": " . $e->getMessage() . " in " . $e . getFile() . " on line " . $e->getLine();
  exit;
  }

?>
