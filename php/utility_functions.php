<?php

function new_token($userID) {
	$expiretime=strtotime("now")+(3600);
	$token=sha1($userID.convertToSQLDateTime($expiretime));
	return $token;                 	
}

function convertToSQLDateTime($phpdate) {
	$mysqldate=date('Y-m-d H:i:s', $phpdate);
	return $mysqldate;
}

function debug_to_console( $data ) {

	if ( is_array( $data ) )
		$output = "<script>console.log( 'Debug Objects: " . implode( ',', $data) . "' );</script>";
	else
		$output = "<script>console.log( 'Debug Objects: " . $data . "' );</script>";

	echo $output;
}

?>