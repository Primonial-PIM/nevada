<?php
/**
 * User: npennin
 */

session_start();

require_once('../localise/localise.php');
require_once(APPLICATION_ROOT . '/php/globalInclude.php');

try
  {
  $queryParams = $_POST;

  if ($queryParams == null)
    {
    $queryParams = $_GET;
    }

  $params['REFCON'] = ($queryParams['REFCON']);
  $params['DATE_FROM'] = ($queryParams['DATE_FROM']);
  $params['DATE_TO'] = ($queryParams['DATE_TO']);
  $params['ISIN'] = ($queryParams['ISIN']);
  $params['USERID'] = ($queryParams['USERID']);
  $params['TOKEN'] = ($queryParams['TOKEN']);

  } catch (Exception $e)
  {
  // for error.
  echo "Error : " . $e->getCode() . ": " . $e->getMessage() . " in " . $e . getFile() . " on line " . $e->getLine();
  exit;
  }

try
  {

  $rval = get_TradeFilesQuery($params);
  $rval = json_encode($rval);

  echo $rval;

  exit;

  } catch (Exception $e)
  {
// unsuccessful fetch
  echo "Error : " . $e->getCode() . ": " . $e->getMessage() . " in " . $e . getFile() . " on line " . $e->getLine();
  exit;
  }

?>
