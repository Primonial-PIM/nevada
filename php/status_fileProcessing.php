<?php
require_once('../localise/localise.php');
require_once('../php/dbFunctions.php');
error_reporting(0);

try{
	$queryParams = $_POST;

	if ($queryParams == null) {
		$queryParams = $_GET;
	}

	$params['USERID'] = ($queryParams['USERID']);
	$params['TOKEN'] = ($queryParams['TOKEN']);
}
catch (Exception $e)
{
	// for error.
	echo "Error : ". $e->getCode(). ": ". $e->getMessage(). " in ". $e.getFile(). " on line ". $e->getLine();
	exit;
}

try
{
	if ($params['USERID']==null) {$params['USERID']='0';} // Alter to 0 for Venice.
	if ($params['TOKEN']==null) {$params['TOKEN']='';}

	$rows=array();
	
	$rows[]=headerRow(array("Process","Script","Last Attempt","Result","Last Success"));
	
	/*
	 * FTP and file tasks
     * 
     * Write Paris Trade files for FTP to BNP - writeTradefilesParis.php - every five minutes from 6am to 9pm
     * Write Lux Trade files for FTP to BNP - writeTradefilesLuxBNP.php - every five minutes from 6am to 9pm
     * Read Trade files for Nevada monitoring - captureTradeFiles.php - every minute from 6am to 9pm
     * Paris Acknowledgement Files to update Venice - checkAcknowledgementBNPParis.php - every 5 mins 6am to 9pm
     * Lux Acknowledgement Files to update Venice - checkAcknowledgementBNPLuxembourg.php - every 5 mins 6am to 9pm
     * Subs and Redmptions Files to update Venice - checkSubscriptionsBNPParis.php - every 10 mins
     * Read Paris Confirmations files to update Venice - checkConfirmationsBNPParis.php - every 5 minutes 6am to 9pm 
     */
	
	$log=get_LastRunDetails($params);
	
	/*
	 * 1 - Capture Positions
	 * 2 - Capture Lux Confirmations
	 * 3 - Capture Paris Confirmations
	 * 4 - Capture Reconciliations
	 * 5 - Capture Trade Files
	 * 6 - Capture UCITS orders
	 * 7 - Capture Morningstar Prices
	 * 8 - Check Subscriptions/Redemptions 
	 * 9 - Check Confirmations
	 * 10 - Write TradeFiles Lux
	 * 11 - Capture Morningstar Index Returns
	 * 12 - Write TradeFiles Paris
	 * 13 - Check Acknowledgement
	 * 14 - Check Acknowledgement Paris
	 * 
	 */

	$rows[]=tableRow(array("Create Trade Files Paris","writeTradefilesParis.php",getlog(12,"Time",$log),getlog(12,"Result",$log),getlog(12,"Success",$log)),true);
	$rows[]=tableRow(array("Create Trade Files Lux","writeTradefilesLuxBNP.php",getlog(10,"Time",$log),getlog(10,"Result",$log),getlog(10,"Success",$log)),false);
    $rows[]=tableRow(array("Create Executions Files","writeExecutions.php",getlog(15,"Time",$log),getlog(15,"Result",$log),getlog(15,"Success",$log)),false);
	$rows[]=tableRow(array("Nevada monitor Trade Files","captureTradeFiles.php",getlog(5,"Time",$log),getlog(5,"Result",$log),getlog(5,"Success",$log)),true);
	$rows[]=tableRow(array("Read Paris Acknowledgments","checkAcknowledgementBNPParis.php",getlog(14,"Time",$log),getlog(14,"Result",$log),getlog(14,"Success",$log)),false);
	$rows[]=tableRow(array("Read Lux Acknowledgments","checkAcknowledgementBNPLuxembourg.php",getlog(13,"Time",$log),getlog(13,"Result",$log),getlog(13,"Success",$log)),true);
	$rows[]=tableRow(array("Read Subs and Redemptions","checkSubscriptionsBNPParis.php",getlog(8,"Time",$log),getlog(8,"Result",$log),getlog(8,"Success",$log)),true);
	$rows[]=tableRow(array("Read Confirmations","checkConfirmationsBNPParis.php",getlog(9,"Time",$log),getlog(9,"Result",$log),getlog(9,"Success",$log)),true);
	
	$html=makeTable($rows);
	
	
}
catch (Exception $e)
{
	echo $e;
}

echo ($html);

function getlog($setID,$field,$log){
	foreach ($log as $entry){
		if ($entry['setID']==$setID){
			if ($field=='Time'){
				return $entry['DateEntered'];
				exit;
			}
			if ($field=='Result'){
				if ($entry['result']=="success"){
					return 'success';
				} else {
					return '<span class="statusWarning">'.$entry['result'].'</span>';
				}
				return $entry['result'];
				exit;
			}
			if ($field=='Success'){
				if ($entry['result']=="success"){
					return $entry['DateEntered'];
					exit;
				}
			}
			
		}
	}
	return 'Not Found';
}

function headerRow($cells){
	$html="<thead>";
	foreach ($cells as $cell){
		$html.="<th>".$cell."</th>";
	}
	$html.="</thead>";
	return $html;
}

function tableRow($cells,$odd){
	if ($odd===true){
		$html="<tr class='odd'>";
	} else {
		$html="<tr>";
	}
	foreach ($cells as $cell){
		$html.="<td>".$cell."</td>";
	}
	$html.="</tr>";
	return $html;
}

function makeTable($rows){
	$html="<table class='broom_table'>";
	foreach ($rows as $row){
		$html.=$row;
	}
	$html.="</table>";
	return $html;
}

?>