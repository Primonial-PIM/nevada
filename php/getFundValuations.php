<?php
/**
 * Created by JetBrains PhpStorm.
 * User: nicholas
 * Date: 24/08/2012
 * Time: 15:58
 * To change this template use File | Settings | File Templates.
 */

session_start();

require_once('../localise/localise.php');
require_once(APPLICATION_ROOT . '/php/globalInclude.php');

try{
    $queryParams = $_POST;

    if ($queryParams == null) {
        $queryParams = $_GET;
    }

    $params['USERID'] = ($queryParams['USERID']);
    $params['TOKEN'] = ($queryParams['TOKEN']);
    $params['ID']    = ($queryParams['ID']);
}
catch (Exception $e)
{
    // for error.
    echo "Error : ". $e->getCode(). ": ". $e->getMessage(). " in ". $e.getFile(). " on line ". $e->getLine();
    exit;
}

try{
    if ($params['USERID']==null) {$params['USERID']='0';} // Alter to 0 for Venice.
    if ($params['TOKEN']==null) {$params['TOKEN']='';}
    
    $rval = get_fundValuations($params);
    $rval = json_encode($rval);

    echo $rval; //json_encode(get_Names($params)); //, JSON_HEX_TAG);

    exit;

}
catch(Exception $e)  {
// unsuccessful fetch
    echo "Error : ". $e->getCode(). ": ". $e->getMessage(). " in ". $e.getFile(). " on line ". $e->getLine();
    exit;
}

?>
