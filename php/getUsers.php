<?php
/**
 * User: npennin
 * Date: 04/07/12
 * Time: 15:26
 * To change this template use File | Settings | File Templates.
 */

session_start();

require_once('../localise/localise.php');
require_once(APPLICATION_ROOT . '/php/globalInclude.php');

try{
	$params = $_POST;

	if ($params == null) {
		$params = $_GET;
	}
}
catch (Exception $e)
{
	// for error.
	echo "Error : ". $e->getCode(). ": ". $e->getMessage(). " in ". $e.getFile(). " on line ". $e->getLine();
	exit;
}


try{

	if ($params['USERID']==null) {$params['USERID']='0';} // Alter to 0 for Venice.
	if ($params['TOKEN']==null) {$params['TOKEN']='';}
	
    $users = get_users($params);

    $links=array();
    $count=0;
    $linkcount=0;
    $table=array();
    
    foreach($users as $user){
    	if ($params['searchstring']==''){
    		$pos=1;
    	} else {
    		$pos=strpos($user['UserName'],$params['searchstring'],0);
    	}
    	
    	if (!($pos===false)){
        	$links[$linkcount]="<img class=\"imgEdit\" id=\"".$user['UserID']."\" src=\"pictures/Edit-icon.png\" 
        	border=\"0\" height=\"16\" width=\"16\" title=\"Edit\"/> 
        	<img class=\"imgDelete\" id=\"".$user['UserID']."\" src=\"pictures/Delete-icon.png\" 
       	 	border=\"0\" height=\"16\" width=\"16\" title=\"Delete\"/>";
        	$linkcount++;
        	$table[]=$user;
    	}
    }
    
    $tablestring=make_table_string($table,$links,$params['pagesize'], $params['page'],'user');
    $info="<div class=\"info\" style=\"display: none;\">Users (".count($table).")</div>";
    echo "<div>".$tablestring.$info."</div>";

    exit;

}
catch(Exception $e)  {
    // unsuccessful fetch
    echo "Error : ". $e->getCode(). ": ". $e->getMessage(). " in ". $e.getFile(). " on line ". $e->getLine();
    exit;
}

function get_users($params){
	
	$userID=$params['USERID'];
	$token=$params['TOKEN'];
	
	$results_array = array();
	
	$sqlsvr_conn = sqlserver_neocapture_connect();
	//ini_set('display_errors', 1);
	
	// Create a new stored procedure
	
	$stmt = mssql_init('spu_tblUsers_Select', $sqlsvr_conn);
	
	if ($stmt)
	{
		if (substr(phpversion(), 0, 4) == '5.3.')
		{
	
			mssql_bind($stmt, '@UserID', &$userID, SQLINT4, false, false);
			mssql_bind($stmt, '@Token', &$token, SQLVARCHAR, false, false);
	
		} else
		{
			// Pass by reference deprecated in PHP 5.4
	
			mssql_bind($stmt, '@UserID', $userID, SQLINT4, false, false);
			mssql_bind($stmt, '@Token', $token, SQLVARCHAR, false, false);
		}
	}
	
	$proc_result = mssql_execute($stmt);
	
	// If Statement succeeds and returns some values, i.e. is a resource rather than $proc_result == true...
	
	if (($proc_result !== true) && ($proc_result !== false))
	{
		if (mssql_num_rows($proc_result) > 0)
		{
			while ($row = mssql_fetch_assoc($proc_result))
			{
				$results_array[] = $row;
			}
		}
	}
	
	return $results_array;
	
	/*  DUMMY CODE
	 * 
	$results=array();
	$user=array();
	
	$user['RN']=1;
	$user['email']='anthony.culligan@primonial.fr';
	$user['name']='Anthony Culligan';
	$user['status']='Active';
	
	$results[]=$user;
	
	$user['RN']=2;
	$user['email']='nicholas.pennington@primonial.fr';
	$user['name']='Nicholas Pennington';
	$user['status']='Active';
	
	$results[]=$user;
	
	
	return $results;
	
	*/
	
	
}



?>
