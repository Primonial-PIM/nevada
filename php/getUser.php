<?php
/**
 * User: npennin
 * Date: 04/07/12
 * Time: 15:26
 * To change this template use File | Settings | File Templates.
 */

session_start();

require_once('../localise/localise.php');
require_once(APPLICATION_ROOT . '/php/globalInclude.php');

try{
	$queryParams = $_POST;

	if ($queryParams == null) {
		$queryParams = $_GET;
	}

	$params['user'] = ($queryParams['user']);
	$params['USERID'] = ($queryParams['USERID']);
	$params['TOKEN'] = ($queryParams['TOKEN']);
}
catch (Exception $e)
{
	// for error.
	$result=array();
    $result['Status']='Error';
    $result['message']=$e->getMessage();
    echo json_encode($result);
    exit;
}


try{

	if ($params['USERID']==null) {$params['USERID']='2';} // Alter to 0 for Venice.
	if ($params['TOKEN']==null) {$params['TOKEN']='';}
	
    $user = get_user($params);
    
    echo json_encode($user);

    exit;

}
catch(Exception $e)  {
    // unsuccessful fetch
    $result=array();
    $result['Status']='Error';
    $result['message']=$e->getMessage();
    echo json_encode($result);
    exit;
}

function get_user($params){
	
	$user=$params['user'];
	$userID=$params['USERID'];
	$token=$params['TOKEN'];
	
	$results_array = array();
	
	$sqlsvr_conn = sqlserver_neocapture_connect();
	//ini_set('display_errors', 1);
	
	// Create a new stored procedure
	
	$stmt = mssql_init('spu_tblUsers_Select', $sqlsvr_conn);
	
	if ($stmt)
	{
		if (substr(phpversion(), 0, 4) == '5.3.')
		{
			mssql_bind($stmt, '@User', &$user, SQLINT4, false, false);
			mssql_bind($stmt, '@UserID', &$userID, SQLINT4, false, false);
			mssql_bind($stmt, '@Token', &$token, SQLVARCHAR, false, false);
	
		} else
		{
			// Pass by reference deprecated in PHP 5.4
	
			mssql_bind($stmt, '@User', $user, SQLINT4, false, false);
			mssql_bind($stmt, '@UserID', $userID, SQLINT4, false, false);
			mssql_bind($stmt, '@Token', $token, SQLVARCHAR, false, false);
		}
	}
	
	$proc_result = mssql_execute($stmt);
	
	// If Statement succeeds and returns some values, i.e. is a resource rather than $proc_result == true...
	
	if (($proc_result !== true) && ($proc_result !== false))
	{
		if (mssql_num_rows($proc_result) > 0)
		{
			while ($row = mssql_fetch_assoc($proc_result))
			{
				$results_array = $row;
			}
		}
	}
	
	return $results_array;
	
}


?>
