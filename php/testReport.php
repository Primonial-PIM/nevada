<?php
/**
 * Created by JetBrains PhpStorm.
 * User: nicholas
 * Date: 22/08/2012
 * Time: 10:37
 * To change this template use File | Settings | File Templates.
 */

session_start();

if (isset($_POST['formdata']))
{
    $_SESSION['formdata'] = $_POST['formdata'];

    return;
}

if (!isset($_SESSION['formdata']))
{
//    return;
}

require_once('/var/www/MPDF54/mpdf.php');

$queryParams = json_decode($_SESSION['formdata'], true);
unset ($_SESSION['formdata']);
//$myHtml = $queryParams['html'];
$myHtml = '
<table>
<thead>
  <tr>
      <th style="text-align: left;width:80px; border-style: solid;">
      <div class="s">Trade Ref.</div>
      </th>
      <th  style="text-align: left;width:90px; border-style: solid;">
        <div class="s">Other Column.</div>
      </th>
  </tr>
</thead>
<tbody>
  <tr>
  <td style="text-align: left;width:80px;">V1</td>
  <td style="text-align: right;width:90px;">V2</td>
  </tr>
  <tr>
  <td style="text-align: right;">V3</td>
  <td align="right">
    <div style="text-align:right; width: 90px; ">1.0000</div>
  </td>
  </tr></tbody>
</table>
';

$mpdf = new mPDF('utf-8',    // mode - default ''
    'A4-L',    // format - A4, for example, default ''
    6,     // font size - default 0
    '',    // default font family
    15,    // margin_left
    15,    // margin right
    16,     // margin top
    16,    // margin bottom
    9,     // margin header
    9,     // margin footer
    'L');  // L - landscape, P - portrait

$mpdf->shrink_tables_to_fit = 2.0;

$mpdf->WriteHTML($myHtml);


$mpdf->Output();







?>