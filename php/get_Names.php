<?php
/**
 * User: npennin
 * Date: 26/04/12
 * Time: 17:26
 */

session_start();
require_once('../localise/localise.php');
require_once(APPLICATION_ROOT . '/php/globalInclude.php');

$params=array();

// Validate the parameters

try
{
    $queryParams = json_decode($_POST['formdata'], true);


    if ($queryParams == null) {
        $queryParams = json_decode($_GET['formdata']);
    }

    $params['ID'] = ($queryParams['ID']);

}
catch (Exception $e)
{
    // for error, return nothing.

    echo "Error : ". $e->getCode(). ": ". $e->getMessage(). " in ". $e.getFile(). " on line ". $e->getLine();

    exit;

}

if ($params['ID']==null) {$params['ID']="0";}

try{

    $rval = get_Names($params);
    $rval = json_encode($rval);

    echo $rval; //json_encode(get_Names($params)); //, JSON_HEX_TAG);

    exit;

}
catch(Exception $e)  {
    // unsuccessful fetch
    echo "Error : ". $e->getCode(). ": ". $e->getMessage(). " in ". $e.getFile(). " on line ". $e->getLine();
    exit;
}


?>
