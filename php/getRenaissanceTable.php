<?php
/**
 * User: nicholas
 * Date: 18/12/2012
 * Time: 14:07
 */

session_start();

require_once('../localise/localise.php');
require_once(APPLICATION_ROOT . '/php/globalInclude.php');

$returnArray = array();

try
  {
  $queryParams = $_POST;

  if ($queryParams == null)
    {
    $queryParams = $_GET;
    }

  $userID = (!isset($queryParams['USERID']) ? 0 : $queryParams['USERID']);
  $token = (!isset($queryParams['TOKEN']) ? "''" :("'" . $queryParams['TOKEN'] . "'"));

  if (!isset($queryParams['FUNDID']))
    {
    if (!isset($queryParams['INSTANCE']))
      {
      $instance='';
      }
    else
      {
      $instance=$queryParams['INSTANCE'];
      }
    }
  else
    {
    $fundparams=explode(":",$queryParams['FUNDID']);
    $fundID=$fundparams[0];

    if (count($fundparams)>1)
      {
      $instance=$fundparams[1];
      }
    else
      {
      $instance='';
      }
    }

  foreach ($db_instances as $db_instance){

    if ($instance==$db_instance['MATCHING'])
      {

      $conn = renaissance_connect($db_instance['INSTANCE'],$db_instance['SERVER']);

      if ($conn)
        {
        $table = (!isset($queryParams['TABLE']) ? '' : $queryParams['TABLE']);
        $fieldlist = (!isset($queryParams['FIELDLIST']) ? '*' : $queryParams['FIELDLIST']);
        $where = (!isset($queryParams['WHERE']) ? '' : $queryParams['WHERE']);
        $knowledgedate = (!isset($queryParams['KNOWLEDGEDATE']) ? 'Null' : $queryParams['KNOWLEDGEDATE']);

        if (strlen($fieldlist) == 0)
          {
          $fieldlist = "*";
          }

        if ((strlen($knowledgedate) > 30) || (strlen($knowledgedate) < 4))
          {
          $knowledgedate = 'Null';
          }
        else
          {
          $knowledgedate = "CAST('" . $knowledgedate . "' AS datetime)";
          }


        switch (strtolower($table))
        {
          case 'Specific Cases...':


            break;

          default:


            $queryString = "SELECT " . $fieldlist . " FROM webfn_" . $table . "_SelectKD(" . $userID . ", " . $token . "," . $knowledgedate . ")";

            if (strlen($where) > 0)
              {
              $queryString = $queryString . " WHERE " . $where;
              }
            break;

        }

        $result = mssql_query($queryString, $conn);

        if (mssql_num_rows($result))
          {
          while ($row = mssql_fetch_assoc($result))
            {
            $row['INSTANCE'] = $instance;
            $returnArray[] = $row; // convertAnsi2UTF8($row);
            }

          }

        mssql_free_result($result);

        break;

        }

      }

    }

  } catch (Exception $e)
  {
  // for error.
  echo "Error : " . $e->getCode() . ": " . $e->getMessage() . " in " . $e . getFile() . " on line " . $e->getLine();
  exit;
  }

try
  {

  $rval = json_encode($returnArray);

  echo $rval;

  exit;

  } catch (Exception $e)
  {
// unsuccessful fetch
  echo "Error : " . $e->getCode() . ": " . $e->getMessage() . " in " . $e . getFile() . " on line " . $e->getLine();
  exit;
  }

?>
