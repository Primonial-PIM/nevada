<?php
/**
 * Created by PhpStorm.
 * User: nicholas
 * Date: 19/03/2014
 * Time: 14:15
 */

session_start();

require_once('../localise/localise.php');
require_once(APPLICATION_ROOT . '/php/globalInclude.php');

try
{
  $queryParams = $_POST;

  if ($queryParams == null)
  {
    $queryParams = $_GET;
  }

  $params['DATE_FROM'] = (isset($queryParams['DATE_FROM']) ? $queryParams['DATE_FROM'] : '');
  $params['DATE_TO'] = (isset($queryParams['DATE_TO']) ? $queryParams['DATE_TO'] : '');
  $params['USERID'] = ($queryParams['USERID']);
  $params['TOKEN'] = ($queryParams['TOKEN']);

} catch (Exception $e)
{
  // for error.
  echo "Error : " . $e->getCode() . ": " . $e->getMessage() . " in " . $e . getFile() . " on line " . $e->getLine();
  exit;
}

try
{

  $rval = get_Milestones($params);
  $rval = json_encode($rval);

  echo $rval; //json_encode(get_Names($params)); //, JSON_HEX_TAG);

  exit;

} catch (Exception $e)
{
// unsuccessful fetch
  echo "Error : " . $e->getCode() . ": " . $e->getMessage() . " in " . $e . getFile() . " on line " . $e->getLine();
  exit;
}



?>
