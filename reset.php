<?php
require_once('localise/localise.php');
require_once('php/reset_functions.php');
require_once('php/utility_functions.php');
require_once(LOGIN_PASSWORD_FILE);

$params=array();
$params['token'] = htmlspecialchars($_GET['token']);

try
{
	
	include("html/reset.html");
	
}
catch (Exception $e)
{
	echo $e->getMessage();
}
?>