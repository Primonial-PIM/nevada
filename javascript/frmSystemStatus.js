/*
 * User: npennin Date: 18/12/12
 *
 */

var obj = new refreshObj();

function showSystemStatus()
  {
  /*
    Initialise and show the System Status form.
   */
  try
    {
    var dialogID;
    var dialogDiv;
    var thisDialogType;
    var dialogOptions;
    var dialogObject;

    /* Create Dialog */

    thisDialogType = document.dialogArray.dialogType.holdings;
    dialogOptions = {title:"System Status", width:"1000", resize:dialogHoldingsResizeStop, resizeStop:dialogHoldingsResizeStop, stateChanged:dialogHoldingsStateChange, open:dialogHoldingsOpen, close:dialogClose, closeOnEscape:false, captionButtons:{refresh:{visible:false}, print:{visible:true, click:fnSimpleGridPrint}, export:{visible:true, click:writeFromDialogGridToExcel}}};

    dialogObject = getDialog(thisDialogType, dialogOptions);
    dialogID = dialogObject.ID;
    dialogDiv = $('#' + dialogID);
    
    // Show Dialog
    
    $(dialogDiv).dialogExtend("open");
    
    systemStatusShow(dialogID);
    
    /*
     * Neolink Curl tasks
     * 
     * Positions - capturePositions.php - Custody All Positions. Every 10 minutes from 6am to 9pm
     * Luxembourg Confirms  - captureLuxConfirmations.php - every 10 minutes from 6am to 9pm
     * UCITS orders - captureUcitsOrders.php - every 10 minutes from 6am to 9pm
     * Paris Confirms - captureParisConfirmations.php - every 10 minutes from 6am to 9pm
     * Reconciliation - captureReconciliation.php - Fund Administration by Fund. Every 10 minutes from 6am to 9pm
     * 
     * FTP and file tasks
     * 
     * Write Paris Trade files for FTP to BNP - writeTradefilesParis.php - every five minutes from 6am to 9pm
     * Write Lux Trade files for FTP to BNP - writeTradefilesLuxBNP.php - every five minutes from 6am to 9pm
     * Read Trade files for Nevada monitoring - captureTradeFiles.php - every minute from 6am to 9pm
     * Paris Acknowledgement Files to update Venice - checkAcknowledgementBNPParis.php - every 5 mins 6am to 9pm
     * Lux Acknowledgement Files to update Venice - checkAcknowledgementBNPLuxembourg.php - every 5 mins 6am to 9pm
     * Subs and Redmptions Files to update Venice - checkSubscriptionsBNPParis.php - every 10 mins
     * Read Paris Confirmations files to update Venice - checkConfirmationsBNPParis.php - every 5 minutes 6am to 9pm 
     * 
     * Collect prices from Moringstar
     * 
     * Capture fund prices for Venice - captureMorningstarPrices.php - run at 6 am
     * Capture index returns for Venice Attribution - captureMorningstarIndexReturns.php - runs at 7,11,15 and 19 hrs
     * 
     * Critical errors from Venice
     * 
     * Query on Venice table
     * 
     */
    
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }

  }


function systemStatusShow(dialogID)
{

	setHtmlFromFile($('#'+dialogID)[0],'html/systemStatus.html','formDiv',false,false);
	
	$("#tabs").attr("id","tabs_"+dialogID);
	
	$("#tab-0").attr("id","tab-0_"+dialogID);
	$("#tab-1").attr("id","tab-1_"+dialogID);
	$("#tab-2").attr("id","tab-2_"+dialogID);
	$("#tab-3").attr("id","tab-3_"+dialogID);
	$("#tab-4").attr("id","tab-4_"+dialogID);
	
	$(".tab-0_tab").attr("href","#tab-0_"+dialogID);
	$(".tab-1_tab").attr("href","#tab-1_"+dialogID);
	$(".tab-2_tab").attr("href","#tab-2_"+dialogID);
	$(".tab-3_tab").attr("href","#tab-3_"+dialogID);
	$(".tab-4_tab").attr("href","#tab-4_"+dialogID);
	
	$("#tabs_"+dialogID).tabs().css({
		'min-height':'400px',
		'overflow':"auto"
	});
		
	obj.setDialogID(dialogID);
	obj.runNow();
	obj.endInterval();
	obj.startInterval();
}

function refreshObj(){
	
	var dialogID;
	var intervalID;
	
	this.setDialogID = function(val){
		dialogID=val;
	};
	
	this.startInterval = function(){
		intervalID=setInterval(refreshStatus,30000);
	};
	
	this.runNow = function(){
		refreshStatus();
	};
	
	this.endInterval =function(){
		clearInterval(intervalID);
	};
	
	function refreshStatus(){
		
		var formvar = {
			'USERID':document.dakotaUserID,
			'TOKEN':document.dakotaSessionKey
		};
	
		// get status for neolink
		$.ajax({
	
			type : "GET",
			data:formvar,
			url : "php/status_fileProcessing.php",
			success : function(html) {
	
				$('#'+dialogID+' .filesResult').html(html);
	
			}
		});
		
		// get status for neolink
		$.ajax({
	
			type : "GET",
			data:formvar,
			url : "php/status_neolink.php",
			success : function(html) {
	
				$('#'+dialogID+' .curlResult').html(html);
	
			}
		});
		
		
		// get status for files
		$.ajax({
	
			type : "GET",
			data:formvar,
			url : "php/status_files.php",
			success : function(html) {
	
				$('#'+dialogID+' .directoriesResult').html(html);
	
			}
		});
		
		// get status for morningstar
		$.ajax({
	
			type : "GET",
			data:formvar,
			url : "php/status_morningstar.php",
			success : function(html) {
	
				$('#'+dialogID+' .morningstarResult').html(html);
	
			}
		});
		
		// get status for venice
		$.ajax({
	
			type : "GET",
			data:formvar,
			url : "php/status_venice.php",
			success : function(html) {
	
				$('#'+dialogID+' .veniceResult').html(html);
	
			}
		});
	
	}
		
}

