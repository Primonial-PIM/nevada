function showUser(id)
  {
  /*
    Initialise and show the System Status form.
   */
  try
    {
    var dialogID;
    var dialogDiv;
    var thisDialogType;
    var dialogOptions;
    var dialogObject;

    /* Create Dialog */

    thisDialogType = document.dialogArray.dialogType.holdings;
    dialogOptions = {title:"User", width:"1060", height:"auto", zindex:-10000, resize:dialogHoldingsResizeStop, resizeStop:dialogHoldingsResizeStop, stateChanged:dialogHoldingsStateChange, open:dialogHoldingsOpen, close:dialogClose, closeOnEscape:false, captionButtons:{refresh:{visible:false}, print:{visible:true, click:fnSimpleGridPrint}, export:{visible:true, click:writeFromDialogGridToExcel}}};

    dialogObject = getDialog(thisDialogType, dialogOptions);
    dialogID = dialogObject.ID;
    dialogDiv = $('#' + dialogID);
    
    // Show Dialog
    
    $(dialogDiv).dialogExtend("open");
    
    $("#"+dialogID).off("click");
	$("#"+dialogID).off("keyup");
	$("#"+dialogID).off("change");
	
	userPageShow(dialogID,id);
    
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }

  }


function userPageShow(dialogID,currentid){

    setHtmlFromFile($("#"+dialogID)[0], 'html/user.html', 'formDiv', false, false);

    $("#tabs").attr("id","tabs_"+dialogID);

    $("#tab-1").attr("id","tab-1_"+dialogID);
    
    $("#newinstance").attr("id","newinstance_"+dialogID);
    
    $("#tooltip").attr("id","tooltip_"+dialogID);
    
    $(".tab-1_tab").attr("href","#tab-1_"+dialogID);
   

    // jqueryui-ify the controls

    $("#tabs_"+dialogID).tabs().css({
        'min-height': '200px',
        'overflow': 'auto'
    });
    $("#"+dialogID).find(".uibutton").button();
    $("#"+dialogID).find(".radioset").buttonset();
    $("#"+dialogID).find(".cb").button();
    $("#"+dialogID).find(".errorMessage").hide();

    currentid = typeof currentid !== 'undefined' ? currentid : -1;

    if (currentid!=-1){
    //if (false){
        var formvar = {
                'user': currentid,
                'USERID':document.dakotaUserID,
    			'TOKEN':document.dakotaSessionKey
        };

      
        var url = "php/getUser.php";

        $.ajax({type: "POST",
              url: url,
              data: formvar,
              dataType: "json",
              success: function(json){
            	  $('#'+dialogID+' input[name="userid"]').val(json.UserID);
            	  $('#'+dialogID+' input[name="username"]').val(json.UserName);
            	  $('#'+dialogID+' input[name="email"]').val(json.EmailAddress);
            	  $('#'+dialogID+' input[name="isadmin"]').val(json.IsAdmin);
            	  $('#'+dialogID+' input[name="islocked"]').val(json.IsLocked);
              }
        });

        
        var url = "php/getUserPermissions.php";
        
        formvar.pagesize=10;
        formvar.page=1;

        $.ajax({type: "POST",
            url: url,
            data: formvar,
            success: function(html){
                $("#tab-1_"+dialogID+" div.results").html(html);
            }
        });

       

    }

    $("#"+dialogID).on("click", ".submit", function() {
    	
    	// if username is blank set it equal to the email
    	var username=$('#'+dialogID+' input[name="username"]').val();
    	if (username.length==0){
    		username=$('#'+dialogID+' input[name="email"]').val();
    	}
    	$('#'+dialogID+' input[name="username"]').val(username);
    	
        var formvar = {
          'user': $('#'+dialogID+' input[name="userid"]').val(),
          'username':$('#'+dialogID+' input[name="username"]').val(),
          'email':$('#'+dialogID+' input[name="email"]').val(),
          'isadmin':$('#'+dialogID+' input[name="isadmin"]').val(),
          'islocked':$('#'+dialogID+' input[name="islocked"]').val(),
          'USERID':document.dakotaUserID,
		  'TOKEN':document.dakotaSessionKey
                    
        };

        if(userFormValidate(formvar,dialogID)){
            
            var url = "php/setUser.php";

            $.ajax({type: "POST",
                  url: url,
                  data: formvar,
                  dataType: "json",
                  success: function(result){
                	  if (result.Status=='OK'){
                		  alert('User updated');
                		  $('#'+dialogID+' input[name="userid"]').val(result.UserID);
                	  } else {
                		  alert('Error updating user');
                	  }
                  }
            });
        }

    });

    $("#"+dialogID).on("click", ".errorMessage", function() {
        $("#"+dialogID+' .errorMessage').hide();
    });
    
    // instance dropdown
    $("#"+dialogID).on("focus",'#newinstance_'+dialogID, function(){
        var searchstring = $(this).val();
        $('#'+dialogID+' input[name="newinstance"]').val("");
        var dialogpos=$("#"+dialogID).offset();
        var position = $('#'+dialogID+' input[name="newinstance_name"]').offset();
        position.top=position.top-dialogpos.top+23;
        position.left=position.left-dialogpos.left; 
        searchInstances(searchstring,dialogID,position,'instancename');
    });

    $("#"+dialogID).on("click",'.instance', function(){
        var instance = $(this).attr('id');
        $('#newinstance_'+dialogID).val(instance);
        $("#tooltip_"+dialogID).hide();
    });

    $("#"+dialogID).on("focusout",'#newinstance_'+dialogID, function(){
        setTimeout(function() {
            $("#tooltip_"+dialogID).hide();
        }, 300);
    });

    $("#"+dialogID).on("click"," .addinstance", function(){
        var newinstance=$('#'+dialogID+' input[name="newinstance_name"]').val();
        var user=$('#'+dialogID+' input[name="userid"]').val();
        if (newinstance.length>0  && user.length>0){
            var formvar = {
                'user': user,
                'instance': newinstance,
                'USERID':document.dakotaUserID,
      		  	'TOKEN':document.dakotaSessionKey
            };

            var url = "php/setPermission.php";

            $.ajax({type: "POST",
                url: url,
                data: formvar,
                dataType: "json",
                success: function(json){
                	 if (json.Status=='OK'){
                		 var url = "php/getUserPermissions.php";
                    
                		 formvar.pagesize=10;
                		 formvar.page=1;

                		 $.ajax({type: "POST",
                			 url: url,
                			 data: formvar,
                			 success: function(html){
                				 $("#tab-1_"+dialogID+" div.results").html(html);
                			 }
                		 });
                	 } else {
                		 alert('Error adding instance');
                	 }
               }
            });
        } else {
            alert('No instance or user');
        }

    });
/*
    $("#"+dialogID).on("click",'#player .imgEdit', function(){
      var currentid = $(this).attr('id');
      menuSwitch('menuPlayer',currentid);
    });
    */

    $("#"+dialogID).on("click",'.imgDelete', function(){
      var currentid = $(this).attr('id');
      var user=$('#'+dialogID+' input[name="userid"]').val();
      if (currentid.length>0){
        var formvar = {
            'feature': currentid,
            'user': user,
            'USERID':document.dakotaUserID,
  		  	'TOKEN':document.dakotaSessionKey
          };

        var url = "php/deletePermission.php";

        $.ajax({type: "POST",
              url: url,
              data: formvar,
              dataType: "json",
              success: function(json){
            	  if (json.Status=='OK'){
             		 var url = "php/getUserPermissions.php";
                 
             		 formvar.pagesize=10;
             		 formvar.page=1;

             		 $.ajax({type: "POST",
             			 url: url,
             			 data: formvar,
             			 success: function(html){
             				 $("#tab-1_"+dialogID+" div.results").html(html);
             			 }
             		 });
             	 } else {
             		 alert('Error deleting permission');
             	 }
              }
        });
      } else {
        alert('No permission selected');
      }
    });


}

function userFormValidate(formvar,dialogID){
    var errorhtml="<span class=\"ui-icon ui-icon-alert\"style=\"float: left; margin-right: .3em;\"></span><strong>Alert : </strong>";

    $("#"+dialogID+' .errorMessage').hide();

    if (formvar.username.length==0){
        var errormessage="User Name cannot be blank";
        $("#"+dialogID+' .errorMessage .messagetext').html(errorhtml+errormessage);
        $("#"+dialogID+' .errorMessage').show();
        return false;
    }

    if (validateEmail(formvar.email)===false){
        var errormessage="Invalid email address";
        $("#"+dialogID+' .errorMessage .messagetext').html(errorhtml+errormessage);
        $("#"+dialogID+' .errorMessage').show();
        return false;
    }
    
    if (formvar.isadmin!=0  && formvar.isadmin!=1){
        var errormessage="Is Admin must be 0 or 1";
        $("#"+dialogID+' .errorMessage .messagetext').html(errorhtml+errormessage);
        $("#"+dialogID+' .errorMessage').show();
        return false;
    }
    
    if (formvar.islocked!=0  && formvar.islocked!=1){
        var errormessage="Is Locked must be 0 or 1";
        $("#"+dialogID+' .errorMessage .messagetext').html(errorhtml+errormessage);
        $("#"+dialogID+' .errorMessage').show();
        return false;
    }

    return true;

}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function searchInstances(searchstring, dialogID,position,tag){

    posleft=position.left;
    postop=position.top;
    var tPosX = posleft;
    var tPosY = postop;
    $("#tooltip_"+dialogID).css({left: tPosX, top: tPosY, position:'absolute'});

    var formvar = {
            'searchstring': searchstring,
            'tag':tag
    };

    //var formJSON=JSON.encode(formvar);
    var url = "php/search_instances.php";

    $.ajax({type: "POST",
          url: url,
          data: formvar,
          success: function(html){
              $("#tooltip_"+dialogID).show();
              $("#tooltip_"+dialogID).html(html);
          }
    });
}
