/**
 * Created by nicholas on 18/03/2014.
 */

function showMilestones()
  {
  try
    {
    var dialogID = (-1);
    var dialogDiv = undefined;
    var thisDialogType = undefined;
    var leftHeaderDiv = undefined;
    var dialogOptions = undefined;
    var dialogObject = undefined;

    thisDialogType = document.dialogArray.dialogType.Milestones;
    dialogOptions = {title:"Milestones", position:[5, 50], resize: dialogMilestoneResizeStop, resizeStop: dialogMilestoneResizeStop, stateChanged: dialogMilestoneStateChange, open: dialogMilestoneOpen, close: dialogMilestoneClose, closeOnEscape:false, captionButtons:{pin:{visible:false}, refresh:{visible:true, click:refreshMilestonesData}, print:{visible:true, click:fnSimpleGridPrint}, export:{visible:true, click:writeFromDialogGridToExcel}}};

    dialogObject = getDialog(thisDialogType, dialogOptions, undefined , 1000);
    dialogID = dialogObject.ID;
    dialogDiv = $('#' + dialogID);

    /*
    var FormDiv = $('<div id="' + dialogID + '_splitter" class="splitterDiv"><div id="' + dialogID + '_leftHeading"></div><table id="' + dialogID + '_grid" class="gridTable"></table></div>');
    dialogDiv.append(FormDiv);
     */

    var splitterDiv = $('<div id="' + dialogID + '_splitter" class="splitterDiv"><div id="' + dialogID + '_splitterLeft" class="leftPanel"><div id="' + dialogID + '_leftSuper"><div id="' + dialogID + '_leftHeading"></div><div><table id="' + dialogID + '_grid" class="gridTable"></table></div><div><table id="' + dialogID + '_grid2" class="gridTable"></table></div></div></div><div id="' + dialogID + '_splitterRight" class="rightPanel"></div></div>');
    dialogDiv.append(splitterDiv);

    //

    getMilestonesData(dialogID);

    // Show Dialog.

    $(dialogDiv).dialogExtend("open");

    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
    }

  }

function refreshMilestonesData(e)
  {
  if (e)
    {
    try
      {
      var dialogID = this.id;

      getMilestonesData(dialogID);
      }
    catch (e)
      {
      console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
      }
    }
  }

function getMilestonesData(dialogID)
  {

  /* Get Data */

  var cacheObject;
  var cacheKey = MO_MILESTONE_CACHE_PREFIX + dialogID;

  if (cacheKey in document.dataCache)
    {
    cacheObject = document.dataCache[cacheKey];
    cacheObject['inProgress'] = true;
    cacheObject['updateTime'] = (new Date());

    if ('data' in cacheObject)
      {
      cacheObject.data['dataStatus'] = 0;
      }
    else
      {
      cacheObject['data'] = {dataStatus:0, milestoneData:false, milestoneFormObject:false};
      }
    }
  else
    {
    cacheObject = {inProgress:true, filterFlags:{}, updateTime:(new Date()), data:{dataStatus:0, milestoneData:false, milestoneFormObject:false}};
    document.dataCache[cacheKey] = cacheObject;
    }

  getMilestones({DIALOGID:dialogID}, setMilestoneData);
  }

function getMilestones(parameters, customCallback, customParameters)
  {

  var uniqueID;
  var dialogID;
  var date_from;
  var date_to;

  if (!parameters)
    {
    parameters = {};
    }

  uniqueID = 'GetData_' + getUniqueCounter();
  dialogID = (parameters['DIALOGID'] ? parameters['DIALOGID'] : '');
  date_to = new Date;
  date_from = new Date;
  date_from.addBusDays(-10);


  // Establish Query patameters object

  var formvar = {
    'DIALOGID':dialogID,
    'ID':uniqueID,
    'TOKEN':document.dakotaSessionKey,
    'USERID':document.dakotaUserID,
    'DATE_FROM':date_from.format('Y-m-d'),
    'DATE_TO':date_to.format('Y-m-d')
  };

  var cacheObject = document.dataCache.preloadURL;
  if (customCallback)
    {
    cacheObject[uniqueID] = {callback:customCallback, parameters:customParameters};
    }

  // Get data.
  // Venice milestones data query.

  var url = "php/getMilestones.php";

  $.ajax({type:"POST",
    url:url,
    data:formvar,
    success:function (html)
      {
      var rVal = JSON.parse(html);
      var formObject;
      var thisUniqueID = uniqueID;
      var dialogID = '';
      var cacheObject;
      var cachekey;

      var formData = decodeURIComponent(this.data);
      formObject=QueryStringToJSON(formData);

      if (formObject['ID'])
        {
        thisUniqueID = formObject['ID'];
        }

      if (formObject['DIALOGID'])
        {
        dialogID = formObject['DIALOGID'];
        }

      cachekey = MO_MILESTONE_CACHE_PREFIX + dialogID;
      if (document.dataCache[cachekey])
        {
        cacheObject = document.dataCache[cachekey];
        cacheObject.data.milestoneData = rVal;
        cacheObject.data.milestoneFormObject = formObject;
        cacheObject.data.dataStatus = cacheObject.data.dataStatus | 1;
        }

      try
        {
        cacheObject = document.dataCache.preloadURL;
        if (cacheObject[uniqueID])
          {
          var callbackObj = cacheObject[thisUniqueID];

          if (callbackObj && (callbackObj['callback']))
            {
            if (callbackObj['parameters'])
              {
              callbackObj['callback'](formObject, callbackObj['parameters']);
              }
            else
              {
              callbackObj['callback'](formObject);
              }
            }

          // Clean cache.
          delete cacheObject[uniqueID];

          }
        }
      catch (e)
        {
        console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
        }

      }
  });
  }

function setMilestoneData(formObject, customParams)
  {
  var cachekey;
  var cacheObject;
  var dialogID = '';

  try
    {
    if (formObject['DIALOGID'])
      {
      dialogID = formObject['DIALOGID'];

      cachekey = MO_MILESTONE_CACHE_PREFIX + dialogID;
      if (document.dataCache[cachekey])
        {
        cacheObject = document.dataCache[cachekey];

        if (cacheObject.data.dataStatus == 0)
          {
          /* Not finished getting data.... */
          return;
          }

        cacheObject.inProgress = false;

        moFilterMilestoneData(dialogID);
        }
      else
        {
        /* No control object. Abort, Abort.... */
        return;
        }
      }
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
    }
  }


function moFilterMilestoneData(dialogID)
  {
  /*
   Having already retrieved the data, this Function will filter and display the Milestone grid
   */

  var cachekey;
  var cacheObject;
  var dialogDiv = $('#' + dialogID);
  var gridID = dialogID + '_grid';
  var grid = $("#" + gridID);
  var gridData = [];
  var gridDataCount = [];
  var thisGridData;
  var colArray = new Array();
  var thisCol;

  try
    {
    try
      {
      cachekey = MO_MILESTONE_CACHE_PREFIX + dialogID;

      if (document.dataCache[cachekey])
        {
        cacheObject = document.dataCache[cachekey];
        }
      else
        {
        /* No control object. Abort, Abort.... */
        return;
        }
      }
    catch (e)
      {
      console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
      return;
      }

    var veniceMilestones = cacheObject.data.milestoneData;
    var milestoneCounter = 0;
    var thisDateString;
    var thisMilestoneRecord;
    var thisColIndex;


    /*  */

    for (thisColIndex in document.appDefaults.MilestoneGridDefaults.initialColumnOrder)
      {
      colArray.push(jQuery.extend({},document.appDefaults.MilestoneGridDefaults[document.appDefaults.MilestoneGridDefaults.initialColumnOrder[thisColIndex]]));
      }


    /*  */

    var startDate = new Date;
    startDate.addBusDays(-6);
    var endDate = new Date;
    var thisFundKey;
    var KeyMap = new Array(); // Flexgid Data must be numeric indexed array.

    var templateRow = {FundID:0, FundName:''};

    while (endDate >= startDate)
      {
      thisDateString = endDate.format('Y-m-d');
      templateRow[thisDateString] = '';

      thisCol = jQuery.extend({}, document.appDefaults.MilestoneGridDefaults.DEFAULT_VALUE_CELL);
      thisCol['headerText'] = thisDateString;
      thisCol['dataKey'] = thisDateString;
      colArray.push(thisCol);

      endDate.addBusDays(-1);
      }

    veniceMilestones.sort(
      function (a, b)
      {
      // Sort_Date Desc, Name Asc
      if (a.FundID < b.FundID) return -1;
      if (a.FundID > b.FundID) return 1;
      return 0;
      });

    if (veniceMilestones != undefined)
      {
      for (milestoneCounter = 0; milestoneCounter < veniceMilestones.length; milestoneCounter++)
        {
        thisMilestoneRecord = veniceMilestones[milestoneCounter];
        thisDateString = thisMilestoneRecord['MilestoneKnowledgeDate'].substring(0, 10);
        thisFundKey = thisMilestoneRecord['INSTANCE'] + '_' + thisMilestoneRecord['FundID'];

        if (templateRow[thisDateString] != undefined)
          {
          if (KeyMap[thisFundKey] == undefined)
            {
            thisGridData = $.extend({}, templateRow);
            thisGridData['INSTANCE'] = thisMilestoneRecord['INSTANCE'];
            thisGridData['FundID'] = thisMilestoneRecord['FundID'];
            thisGridData['FundName'] = thisMilestoneRecord['FundName'];

            KeyMap[thisFundKey] = Object.keys(KeyMap).length;

            gridData[KeyMap[thisFundKey]] = thisGridData;
            gridDataCount[KeyMap[thisFundKey]] = $.extend({}, templateRow);
            }
          else
            {
            thisGridData = gridData[KeyMap[thisFundKey]];
            }

          if (isNaN(parseInt(gridDataCount[KeyMap[thisFundKey]][thisDateString])))
            {
            thisGridData[thisDateString] = '<span>Date : ' + thisMilestoneRecord['MilestoneDate'] + '</span><br><span>At time : ' + thisMilestoneRecord['MilestoneKnowledgeDate'].substring(11) + '</span><br><span>User : ' + thisMilestoneRecord['UserName'] + '</span>';
            gridDataCount[KeyMap[thisFundKey]][thisDateString] = 0;
            }
          else
            {
            gridDataCount[KeyMap[thisFundKey]][thisDateString] += 1;
            thisGridData[thisDateString] = '<span>Date : ' + thisMilestoneRecord['MilestoneDate'] + '</span><br><span>At time : ' + thisMilestoneRecord['MilestoneKnowledgeDate'].substring(11) + '</span><br><span>User : ' + thisMilestoneRecord['UserName'] + '</span><br><span>+ ' + gridDataCount[KeyMap[thisFundKey]][thisDateString] + ' other dates.</span>';
            }
          }
        }
      }

    // Default Sort

    gridData.sort(
      function (a, b)
      {
      // Sort_Date Desc, Name Asc
      if (a.FundName < b.FundName) return -1;
      if (a.FundName > b.FundName) return 1;
      return 0;
      });

    grid.flexigrid({
      jsArray:gridData,
      colModel:colArray,
      // rowStyleFormatter:undefined,
      // cellStyleFormatter:undefined,
      // sortall:false,
      // gridSortFunction:undefined,
      width:400,
      height:300})

    // Resize Grid, if it exists. Fit to the right splitter panel.

    // var splitterLeft = $("#" + dialogID + '_splitter');
    grid.flexSize({width:(dialogDiv[0].clientWidth), height:(dialogDiv[0].clientHeight)});

    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
    return;
    }
  }

function dialogMilestoneResizeStop(e, data)
  {
  // Exit if dialog has not resised.

  if ((data.size.height < 100) || ((data.originalSize.height == data.size.height) && (data.originalSize.width == data.size.width)))
    {
    return;
    }

  var dialogDiv;
  if ($(e.target).hasClass('dialogDiv'))
    {
    dialogDiv = $(e.target);
    }
  else
    {
    dialogDiv = $(e.target).parents('.dialogDiv');
    }
  var dialogID = dialogDiv.attr('id');

  // Resize Splitter and refresh. // clientHeight

  var initialClientHeight = e.target.clientHeight;
  var initialClientWidth = e.target.clientWidth;

  var moGrid = $("#" + dialogID + '_grid');

  moGrid.flexSize({width: (e.target.clientWidth - 1), height: (e.target.clientHeight - 1)});

}

function dialogMilestoneStateChange(e, data)
  {
  // Call resize code when the dialog is maximised or restored.
  var dialogID = e.target.id;

  switch (data.state)
  {

    case 'maximized':
      document.dialogMaximised[dialogID] = true;

      if (!data.size) data.size = {height: e.target.clientHeight, width: e.target.clientWidth};
      if (!data.originalSize) data.originalSize = {height: 0, width: 0};

      dialogMilestoneResizeStop(e, data);

      break;

    case 'normal':
      document.dialogMaximised[dialogID] = false;

      if (!data.size) data.size = {height: e.target.clientHeight, width: e.target.clientWidth};
      if (!data.originalSize) data.originalSize = {height: 0, width: 0};

      dialogMilestoneResizeStop(e, data);

      data.size = {height: e.target.clientHeight, width: e.target.clientWidth};
      dialogMilestoneResizeStop(e, data);

      break;
  }
  }

function dialogMilestoneOpen(e, data)
  {
  // Resize Dialog on Open. Ensures everything is the correct size.

  var data = {};

  if (!data.size) data.size = {height: e.target.clientHeight, width: e.target.clientWidth};
  if (!data.originalSize) data.originalSize = {height: 0, width: 0};

  dialogMilestoneResizeStop(e, data);
  }

function dialogMilestoneClose(e)
  {
  // Tidy up the data cache on dialog close.

  try
    {
    var dialogID = e.target.id;
    var cacheKey = MO_MILESTONE_CACHE_PREFIX + dialogID;

    if (cacheKey in document.dataCache)
      {
      delete document.dataCache[cacheKey];
      }
    }
  catch (e)
    {
    }
  }