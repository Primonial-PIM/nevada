document.nevadaUserID = 0;
document.nevadaSessionKey = '';


document.dialogArray = new Array();
document.dialogArrayCurrentID = 0;
document.dialogMaximised = [];
if (!window.docUniqueCounter) window.docUniqueCounter = 1

document.is_chrome = navigator.userAgent.indexOf('Chrome') > -1;
document.is_explorer = navigator.userAgent.indexOf('MSIE') > -1;
document.is_firefox = navigator.userAgent.indexOf('Firefox') > -1;
document.is_safari = navigator.userAgent.indexOf("Safari") > -1;
document.is_Opera = navigator.userAgent.indexOf("Presto") > -1;

if ((document.is_chrome) && (document.is_safari))
  {
  document.is_safari = false;
  }

if (Object.freeze)
  {
  document.dialogArray.dialogType =
    Object.freeze({"holdings":4, MoTransactions:5, MoSettlingCash:6, Milestones:7, transactions:8, iframe:16, iframeDisposable:17, info:32, test:1024});
  }
else
  {
  document.dialogArray.dialogType = {"holdings":4, MoTransactions:5, MoSettlingCash:6, Milestones:7, transactions:8, iframe:16, iframeDisposable:17, info:32, test:1024};
  }


document.clickLastDialogID = "";
document.buildingDialogID = "";

document.dataCache = {};
document.dataCache.preloadURL = [];
document.fauxLocalStorage = {};
document.currentContextData = {};
document.pendingUrlCache = [];     // Used to queue pages to cache. Takes object {URL:xx, cacheKey:yy}. See checkPendingUrlCache();

/*  */
var TIMED_UPDATES_INTERVAL = 20000;

/*  */
var DEBUG_FLAG = true;

/* Minimum price difference to trigger a warning on the M/O Transactions Form. */

var MO_TRANSACTION_PRICE_THRESHOLD = 0.0006;

/* Middle Office Transaction Monitor Formatting Constants */

var ALWAYS_HIGHLIGHT_INCOMPLETE_ROWS = true; // If false, incomplete rows are not highlighted if the trade status is one of the `document.appDefaults.cancelledByUs` status'

var MO_TRANSACTION_WARNNG = '#FFCCCC';
var MO_TRANSACTION_INCOMPLETE_ROW = '#FDFFCA'; // Light Yellow
var MO_TRANSACTION_HIGHLIGHT_COLOUR = '#0E13FF'; // Very Blue
var MO_TRANSACTION_SEVEREWARNING = '#E00000'; // Very Red

var MO_TRANSACTION_WARNING_FLAG = 0x01;
var MO_TRANSACTION_HIGHLIGHT_FLAG = 0x02;
var MO_TRANSACTION_SETTLEMENT_CHANGED_FLAG = 0x04;
var MO_TRANSACTION_SEVEREWARNING_FLAG = 0x08;

var MO_TRANSACTION_TOOLTIP_DELAY = 200;
var MO_TRANSACTION_FILTER_UPDATE_DELAY = 1000;

var MO_TRANSACTION_CACHE_PREFIX = 'Mo';
var MO_FUND_BROWSER_CACHE_PREFIX = 'Fb';
var MO_FUND_BROWSER_HIGHLIGHT_COLOUR = '#ffd2d3'; // Pale Pink
var MO_MILESTONE_CACHE_PREFIX = 'Ms';

var CACHE_CATEGORY_PREFIX = 'Category_';

var MO_SETTLING_CASH_TODAY_ROW = '#FDFFCA';

var MO_DATA_DAYS_PADDING = 7; // When getting transaction data etc, how many days data either side of the From and To dates to get.
var MO_DATA_DAYS_UPDATE_MARGIN = 2; // When the view dates change, how near to the data Start/End Dates can you get before loading more data..

/*  */

var SQL_SERVER_QUERY_DATE_FORMAT = ''

/* Data cache is an object with named properties, each of which is expected to be an object containing
 * status values and the underlying data.
 *
 * This is an in-memory cache not localStorage!
 *
 * Standard properties should be :
 *    updateTime
 *    inProgress
 *    data
 *
 * document.dataCache[cacheKey]
 *
 * This cache will need more work....
 * */

// Set some defaults :
// Perhaps take / store defaults in local storage ?

function initialiseDefaults()
  {
  //
  try
    {
    if (!window.console) console = {};
    console.log = console.log || function (message)
      {
      try
        {
        }
      catch (e)
        {
        }
      };
    console.warn = console.warn || function (message)
      {
      try
        {
        }
      catch (e)
        {
        }
      };
    console.error = console.error || function (message)
      {
      try
        {
        }
      catch (e)
        {
        }
      };
    console.info = console.info || function (message)
      {
      try
        {
        }
      catch (e)
        {
        }
      };
    }
  catch (e)
    {
    }

  // Initialise global event handling.
  try
    {
    $(document).on({mouseenter:moTransactionsHoverIn, mouseleave:moTransactionsHoverOut}, ".dialogDiv .gridTransactionPopupClass");
    $(document).on({mouseenter:moSettlingHoverIn, mouseleave:moSettlingHoverOut}, ".dialogDiv .gridSettlingPopupClass");

    $(document).on({mouseover:function ()
      {
      $('div.fsTooltip').show();
      }}, "div.fsTooltip");
    $(document).on({mouseleave:function ()
      {
      $('div.fsTooltip').hide();
      }}, "div.fsTooltip");
    $(document).on({mousemove:function (e)
      {
      document.mouseX = e.pageX;
      document.mouseY = e.pageY;
      }});

    $(document).on({click:fnCheckBoxActivate}, ".checkboxActivate");
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }


  try
    {
    var waitForFinalEvent = (function ()
      {
      var timers = {};
      return function (callback, ms, uniqueId)
        {
        if (!uniqueId)
          {
          uniqueId = "Don't call this twice without a uniqueId";
          }
        if (timers[uniqueId])
          {
          clearTimeout(timers[uniqueId]);
          }
        timers[uniqueId] = setTimeout(callback, ms);
        };
      })();

    $(window).resize(function (e)
    {
    /* Strangely, the splitter resize causes a window resize event, but with e.target == splitter.
     * Check the target is the window to omit spurious 'resizeMaximisedDialogs' calls. (they become continuous when you have a maximised dialog) */
    try
      {
      if (e.target === window)
        {
        waitForFinalEvent(resizeMaximisedDialogs, 500, "dialogResize");
        }
      }
    catch (e)
      {
      console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
      }
    });
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }

  // Initialise global 'constants'.
  try
    {
    if (!document.appDefaults) document.appDefaults = {};

    document.appDefaults.wijmoCulture = "fr"; // http://infinity88.com/jquery-glob/demo/demo.html "fr-FR", "en-GB"

    /*
     54875474	EUR
     55001680	GBP
     55201881	JPY
     54741062	CHF
     55923524	USD
     54613316	AUD
     */

    document.appDefaults.currencyColours =
    {
      ccy_default:'#000000',
      USD:'#006600', // Green
      EUR:'#0000CC', // Blue
      GBP:'#FF0000', // Red
      CHF:'#33FF33', // Light Green
      JPY:'#FF6666', // Light Red
      SEK:'#000000', // Black
      AUD:'#3399FF'  // Light Blue
    };

    document.appDefaults.rowType =
    {data:0, header:1, summary:2, footer:4, spacer:8};

    document.appDefaults.deadSophisTrades = ['', '2', '9', '11', '13', '14', '17', '18', '26', '27', '30', '33', '158', '160', '180', '183', '191', '246', '250'];// Exclude from cash reports
    // cancelledByUs does not include '191' which is 'Rejected by BP2S'
    document.appDefaults.cancelledByUs = ['', '2', '9', '11', '13', '14', '17', '18', '26', '27', '30', '33', '158', '160', '180', '183', '246', '250'];// Exclude from cash reports

    document.appDefaults.gridColours =
    {
      footerBarBackground:'#E0E0E0',
      fundbrowser_characteristic_footerBarBackground:'#F4F4FF',
      fundbrowser_characteristic_footerBarBackgroundSubGroup:'#FAFADC'
    };

    //    document.appDefaults.holdingGridDefaults =
    //    {
    //      initialColumnOrder:['LIBELLE', 'EURO_VALUE', 'FUND_WEIGHT', 'PRICE' , 'HOLDING', 'FX_RATE', 'REA', 'MULTIPLIER', 'SICOVAM', 'INFO', 'TYPE', 'ISIN'],
    //      SICOVAM:{noCellFormatting:true, headerText:"SICOVAM", dataKey:"SICOVAM", visible:true, dataType:"string", width:100},
    //      LIBELLE:{noCellFormatting:true, headerText:"Name", dataKey:"LIBELLE", visible:true, dataType:"string", width:350},
    //      HOLDING:{headerText:"Position", dataKey:"HOLDING", dataType:"number", dataFormatString:"n2", visible:true, width:150},
    //      FUND_WEIGHT:{headerText:"Weight (%)", dataKey:"FUND_WEIGHT", dataType:"number", dataFormatString:"p2", visible:true, width:70},
    //      MULTIPLIER:{headerText:"Multiplier", dataKey:"MULTIPLIER", dataType:"number", dataFormatString:"n6", visible:false, width:100},
    //      PRICE:{headerText:"Price", dataKey:"PRICE", visible:true, dataType:"number", dataFormatString:"n4", width:85},
    //      FX_RATE:{noCellFormatting:true, headerText:"FX to EUR", dataKey:"FX_RATE", visible:true, dataType:"number", dataFormatString:"n4", width:75},
    //      EURO_VALUE:{headerText:"Value (EUR)", dataKey:"EURO_VALUE", visible:true, dataType:"number", width:100, dataFormatString:"n0", footerText:"<b>{0}</b>"},
    //      INFO:{noCellFormatting:true, headerText:"Info", dataKey:"INFO", visible:true, dataType:"string", width:150},
    //      INSTRUMENT_TYPE:{noCellFormatting:true, headerText:"Type", dataKey:"INSTRUMENT_TYPE", visible:false, dataType:"string", width:50},
    //      REFERENCE_PRICE:{noCellFormatting:true, headerText:"Ref. Price", dataKey:"REFERENCE_PRICE", visible:false, dataType:"string", width:100},
    //      REFERENCE:{noCellFormatting:true, headerText:"Reference", dataKey:"REFERENCE", visible:false, dataType:"string", width:100},
    //      UNIT_COUNT:{noCellFormatting:true, headerText:"Shares Issued", dataKey:"UNIT_COUNT", visible:false, dataType:"number", dataFormatString:"n2", width:100},
    //      ISIN:{noCellFormatting:true, headerText:"ISIN", dataKey:"ISIN", visible:true, dataType:"string", width:100},
    //      REA:{noCellFormatting:true, headerText:"REA", dataKey:"REA", visible:true, dataType:"number", dataFormatString:"n4", width:75},
    //      FOLIO:{noCellFormatting:true, headerText:"FOLIO", dataKey:"FOLIO", visible:false, dataType:"number", dataFormatString:"n0", width:75},
    //      QUERYFLAG:{noCellFormatting:true, headerText:"QUERYFLAG", dataKey:"QUERYFLAG", visible:false, dataType:"number", dataFormatString:"n0", width:75},
    //      DEFAULT:{noCellFormatting:true, visible:true}
    //    };

    document.appDefaults.holdingGridDefaults =
    {
      initialColumnOrder:['InstrumentDescription', 'FUND_WEIGHT', 'SignedUnits', 'SignedUnits_Secondary', 'PriceLevel' , 'EURValue', 'InstrumentISIN', 'LocalValue', 'CurrencyOfInstrument', 'CompoundFXRate', 'SignedSettlement', 'InstrumentMultiplier', 'InstrumentContractSize', 'Instrument', 'InstrumentType'],
      //initialCharacteristicColumnOrder:['DataCharacteristic', 'InstrumentDescription', 'characteristicValue', 'characteristicFundWeight', 'Value', 'FUND_WEIGHT', 'PriceLevel' , 'SignedUnits', 'LocalValue', 'CurrencyOfInstrument', 'CompoundFXRate', 'SignedSettlement', 'InstrumentMultiplier', 'InstrumentContractSize', 'InstrumentISIN', 'Instrument', 'InstrumentType'],
      //initialSubCharacteristicColumnOrder:['MainDataCategory', 'DataCharacteristic', 'InstrumentDescription', 'characteristicValue', 'characteristicFundWeight', 'Value', 'FUND_WEIGHT', 'PriceLevel' , 'SignedUnits', 'LocalValue', 'CurrencyOfInstrument', 'CompoundFXRate', 'SignedSettlement', 'InstrumentMultiplier', 'InstrumentContractSize', 'InstrumentISIN', 'Instrument', 'InstrumentType'],
      initialCharacteristicColumnOrder:['DataCharacteristic', 'InstrumentDescription', 'FUND_WEIGHT', 'SignedUnits', 'SignedUnits_Secondary', 'PriceLevel', 'EURValue', 'InstrumentISIN', 'characteristicValue', 'characteristicFundWeight', 'LocalValue', 'CurrencyOfInstrument', 'CompoundFXRate', 'SignedSettlement', 'InstrumentMultiplier', 'InstrumentContractSize', 'Instrument', 'InstrumentType'],
      initialSubCharacteristicColumnOrder:['MainDataCategory', 'DataCharacteristic', 'InstrumentDescription', 'FUND_WEIGHT', 'SignedUnits', 'SignedUnits_Secondary', 'PriceLevel', 'EURValue', 'InstrumentISIN', 'characteristicValue', 'characteristicFundWeight', 'LocalValue', 'CurrencyOfInstrument', 'CompoundFXRate', 'SignedSettlement', 'InstrumentMultiplier', 'InstrumentContractSize', 'Instrument', 'InstrumentType'],
      Instrument:{noCellFormatting:true, headerText:"Instrument", dataKey:"Instrument", visible:true, dataType:"string", width:100},
      InstrumentParentID:{noCellFormatting:true, headerText:"Parent", dataKey:"InstrumentParentID", visible:false, dataType:"string", width:100},
      InstrumentDescription:{noCellFormatting:true, headerText:"Name", dataKey:"InstrumentDescription", visible:true, dataType:"string", width:350},
      MainDataCategory:{noCellFormatting:true, headerText:"Main Category", dataKey:"MainDataCategory", visible:true, dataType:"string", width:150},
      DataCharacteristic:{noCellFormatting:true, headerText:"Characteristic", dataKey:"DataCharacteristic", visible:true, dataType:"string", width:150},
      CurrencyOfInstrument:{noCellFormatting:true, headerText:"Ccy", dataKey:"CurrencyOfInstrument", visible:true, dataType:"string", width:35},
      SignedUnits:{headerText:"Position", dataKey:"SignedUnits", dataType:"number", dataFormatString:"n2", negativeColour:"red", visible:true, width:90},
      SignedUnits_Secondary:{headerText:"Position (EOD)", dataKey:"SignedUnits_Secondary", dataType:"number", dataFormatString:"n2", negativeColour:"red", visible:true, width:90},
      SignedSettlement:{headerText:"Settlement", dataKey:"SignedSettlement", dataType:"number", dataFormatString:"n2", negativeColour:"red", visible:true, width:150},
      FUND_WEIGHT:{headerText:"Weight (%)", dataKey:"FUND_WEIGHT", dataType:"number", dataFormatString:"p2", negativeColour:"red", visible:true, width:70, footerText:"<b>{0}</b>"},
      InstrumentMultiplier:{headerText:"Multiplier", dataKey:"InstrumentMultiplier", dataType:"number", dataFormatString:"n6", visible:false, width:100},
      InstrumentContractSize:{headerText:"Multiplier", dataKey:"InstrumentContractSize", dataType:"number", dataFormatString:"n6", visible:false, width:100},
      PriceLevel:{headerText:"Price", dataKey:"PriceLevel", visible:true, dataType:"number", dataFormatString:"n4", width:85},
      CompoundFXRate:{noCellFormatting:true, headerText:"FX to FUND", dataKey:"CompoundFXRate", visible:true, dataType:"number", dataFormatString:"n4", width:75},
      EURValue:{headerText:"Value (EUR)", dataKey:"EURValue", visible:true, dataType:"number", width:100, dataFormatString:"n0", negativeColour:"red", footerText:"<b>{0}</b>"},
      characteristicValue:{headerText:"Risk Value (Fund)", dataKey:"characteristicValue", visible:true, dataType:"number", width:100, dataFormatString:"n0", negativeColour:"red", footerText:"<b>{0}</b>"},
      characteristicFundWeight:{headerText:"Risk Weight", dataKey:"characteristicFundWeight", dataType:"number", dataFormatString:"p2", negativeColour:"red", visible:true, width:70, footerText:"<b>{0}</b>"},
      ConsiderationValue:{headerText:"Cons. Value", dataKey:"ConsiderationValue", visible:false, dataType:"number", width:100, dataFormatString:"n0", negativeColour:"red"},
      USDValue:{headerText:"Value (USD)", dataKey:"USDValue", visible:false, dataType:"number", width:100, dataFormatString:"n0", negativeColour:"red", footerText:"<b>{0}</b>"},
      LocalValue:{headerText:"Value (Local)", dataKey:"LocalValue", visible:true, dataType:"number", width:100, dataFormatString:"n0", negativeColour:"red"},
      InstrumentType:{noCellFormatting:true, headerText:"Type", dataKey:"InstrumentType", visible:false, dataType:"string", width:50},
      FundUnits:{noCellFormatting:true, headerText:"Shares Issued", dataKey:"FundUnits", visible:false, dataType:"number", dataFormatString:"n2", negativeColour:"red", width:100},
      InstrumentISIN:{noCellFormatting:true, headerText:"ISIN", dataKey:"InstrumentISIN", visible:true, dataType:"string", width:100},
      REA:{noCellFormatting:true, headerText:"REA", dataKey:"REA", visible:true, dataType:"number", dataFormatString:"n4", width:75},
      Fund:{noCellFormatting:true, headerText:"FOLIO", dataKey:"Fund", visible:false, dataType:"number", dataFormatString:"n0", width:75},
      QUERYFLAG:{noCellFormatting:true, headerText:"QUERYFLAG", dataKey:"QUERYFLAG", visible:false, dataType:"number", dataFormatString:"n0", width:75},
      DEFAULT:{noCellFormatting:true, visible:false}
    };

    //    document.appDefaults.transactionsGridDefaults =
    //    {
    //      initialColumnOrder:['REFCON', 'QUANTITE', 'PRICE', 'AMOUNT', 'DATENEG', 'DATEVAL', 'INFOSBACKOFFICE', 'SICOVAM', 'LIBELLE'],
    //      REFCON:{headerText:"Trade Ref.", dataKey:"REFCON", visible:true, dataType:"string", width:100},
    //      OPCVM:{headerText:"Folio ID", dataKey:"OPCVM", visible:true, dataType:"string", width:100},
    //      SICOVAM:{headerText:"SICOVAM", dataKey:"SICOVAM", visible:true, dataType:"string", width:100},
    //      LIBELLE:{headerText:"Name", dataKey:"LIBELLE", visible:true, dataType:"string", width:350},
    //      DATENEG:{headerText:"DATENEG", dataKey:"DATENEG", visible:true, width:150},
    //      DATEVAL:{headerText:"DATEVAL", dataKey:"DATEVAL", visible:true, width:150},
    //      DATECOMPTABLE:{headerText:"DATECOMPT", dataKey:"DATECOMPTABLE", visible:true, width:150},
    //      QUANTITE:{headerText:"Position", dataKey:"QUANTITE", dataType:"number", dataFormatString:"n2", visible:true, width:150, footerText:"<b>{0}</b>"},
    //      PRICE:{headerText:"Price", dataKey:"PRICE", visible:true, dataType:"number", dataFormatString:"n4", width:85},
    //      AMOUNT:{headerText:"Consideration", dataKey:"AMOUNT", dataType:"number", dataFormatString:"n2", visible:true, width:150, footerText:"<b>{0}</b>"},
    //      INFOSBACKOFFICE:{headerText:"Info", dataKey:"INFOSBACKOFFICE", visible:true, dataType:"string", width:500},
    //      DEFAULT:{noCellFormatting:true, visible:true}
    //    };

    document.appDefaults.transactionsGridDefaults =
    {
      initialColumnOrder:['TransactionID', 'IsInEOD', 'TransactionSignedUnits', 'TransactionPrice', 'TransactionSignedSettlement', 'TransactionDecisionDate', 'TransactionValueDate', 'TransactionComment', 'TransactionInstrument', 'InstrumentDescription'],
      TransactionID:{headerText:"Trade Ref.", dataKey:"TransactionID", visible:true, dataType:"string", width:100},
      TransactionFund:{headerText:"Folio ID", dataKey:"TransactionFund", visible:true, dataType:"string", width:100},
      IsInEOD:{headerText:"In EOD", dataKey:"IsInEOD", visible:true, dataType:"string", width:40},
      TransactionInstrument:{headerText:"Instrument", dataKey:"TransactionInstrument", visible:true, dataType:"string", width:100},
      InstrumentDescription:{headerText:"Name", dataKey:"InstrumentDescription", visible:true, dataType:"string", width:350},
      TransactionDecisionDate:{headerText:"Decision", dataKey:"TransactionDecisionDate", visible:true, width:150},
      TransactionValueDate:{headerText:"Value Date", dataKey:"TransactionValueDate", visible:true, width:150},
      TransactionSettlementDate:{headerText:"Settle", dataKey:"TransactionSettlementDate", visible:true, width:150},
      TransactionSignedUnits:{headerText:"Position", dataKey:"TransactionSignedUnits", dataType:"number", dataFormatString:"n2", negativeColour:"red", visible:true, width:90},
      TransactionSignedSettlement:{headerText:"Settlement", dataKey:"TransactionSignedSettlement", dataType:"number", dataFormatString:"n2", negativeColour:"red", visible:true, width:90},
      TransactionPrice:{headerText:"Price", dataKey:"TransactionPrice", visible:true, dataType:"number", dataFormatString:"n4", width:85},
      TransactionComment:{headerText:"Comment", dataKey:"TransactionComment", visible:true, dataType:"string", width:500},
      DEFAULT:{noCellFormatting:true, visible:true}
    };

    document.appDefaults.moTransactionGridDefaults =
    {
      initialColumnOrder:['RN', 'MATCHARRAYINDEX', 'SOPHISMATCH', 'TRADEFILEMATCH', 'CONFIRMATIONMATCH', 'INSTANCE', 'MATCHSTATUSBRIEF', 'MATCHSTATUS', 'SOPHISSTATUS', 'BNPSTATUS', 'REFCON', 'ISIN', 'DEPOSITARY_NAME', 'EURO_VALUE', 'FUND_NAV', 'PERCENTAGE_OF_NAV', 'SOPHISSETTLEMENTCURRENCY', 'SOPHISAMOUNT', 'FILEAMOUNT', 'CONFIRMAMOUNT', 'SOPHISPRICE', 'CONFIRMPRICE', 'SOPHISTRADE', 'FILETRADE', 'CONFIRMTRADE', 'SOPHISSETTLE', 'CONFIRMSETTLE', 'SOPHISSETTLE_MIN', 'SOPHISSETTLE_MAX', 'SOPHISCURRENCY', 'FILECURRENCY', 'CONFIRMCURRENCY', 'NAME', 'PAM_FUND_NAME'],
      alwaysHidden:['RN', 'MATCHARRAYINDEX', 'FORMATTING', 'MATCHSTATUS', 'CCY_COLOUR', 'AMOUNT_DIFFERENCE', 'SOPHISSTATUS_ID'],
      RN:{headerText:"RN", dataKey:"RN", col_class:"rnClass", visible:false, dataType:"number", dataFormatString:"n0", allowMoving:false, width:1, notInColumnList:true},
      MATCHARRAYINDEX:{headerText:"MATCHARRAYINDEX", dataKey:"MATCHARRAYINDEX", col_class:"matchArrayClass", visible:false, dataType:"number", dataFormatString:"n0", allowMoving:false, width:1, notInColumnList:true},
      FORMATTING:{headerText:"FORMATTING", dataKey:"FORMATTING", visible:false, width:1, notInColumnList:true},
      INSTANCE:{headerText:"Instance.", dataKey:"INSTANCE", visible:true, dataType:"string", width:20},
      REFCON:{headerText:"Trade Ref.", dataKey:"REFCON", visible:true, dataType:"string", width:60},
      LIBELLE:{headerText:"Name", dataKey:"LIBELLE", visible:true, dataType:"string", width:350},
      SPACER:{headerText:"", dataKey:"SPACER", visible:true, dataType:"string", width:3, notInColumnList:true},
      NAME:{headerText:"Name", dataKey:"NAME", visible:true, dataType:"string", width:350},
      DEPOSITARY_NAME:{headerText:"Depositary", dataKey:"DEPOSITARY_NAME", visible:true, dataType:"string", width:80},
      SOPHISSTATUS:{headerText:"Venice Status", dataKey:"SOPHISSTATUS", visible:true, dataType:"string", width:160},
      BNPSTATUS:{headerText:"BNP Status", dataKey:"BNPSTATUS", visible:true, dataType:"string", width:120},
      ISIN:{headerText:"ISIN", dataKey:"ISIN", col_class:"gridTransactionPopupClass", visible:true, dataType:"string", width:100, ccy_colour:true},
      SOPHISTRADE:{headerText:"Venice Trade", dataKey:"SOPHISTRADE", visible:true, width:90},
      SOPHISSETTLE:{headerText:"Venice Sett.", dataKey:"SOPHISSETTLE", col_class:"gridTransactionPopupClass gridMovedDatenegClass", visible:true, width:90},
      SOPHISSETTLE_MIN:{headerText:"MIN Venice Sett.", dataKey:"SOPHISSETTLE_MIN", visible:true, width:90},
      SOPHISSETTLE_MAX:{headerText:"MAX Venice Sett.", dataKey:"SOPHISSETTLE_MAX", visible:true, width:90},
      SOPHISAMOUNT:{headerText:"Venice Qty", dataKey:"SOPHISAMOUNT", col_class:"gridTransactionPopupClass sophisMatch", dataType:"number", dataFormatString:"n4", visible:true, width:120},
      AMOUNT_DIFFERENCE:{headerText:"Diff.", dataKey:"AMOUNT_DIFFERENCE", dataType:"number", dataFormatString:"n4", visible:false, width:50},
      SOPHISPRICE:{headerText:"Venice Price", dataKey:"SOPHISPRICE", visible:true, dataType:"number", dataFormatString:"n4", width:85},
      SOPHISCURRENCY:{headerText:"Inst. Ccy", dataKey:"SOPHISCURRENCY", visible:true, dataType:"string", width:40},
      SOPHISSETTLEMENTCURRENCY:{headerText:"Sett. Ccy", dataKey:"SOPHISSETTLEMENTCURRENCY", visible:false, dataType:"string", width:40},
      FILETRADE:{headerText:"File Trade", dataKey:"FILETRADE", visible:true, width:90},
      FILEAMOUNT:{headerText:"File Qty", dataKey:"FILEAMOUNT", col_class:"gridTransactionPopupClass tradefileMatch", dataType:"number", dataFormatString:"n4", visible:true, width:120},
      FILECURRENCY:{headerText:"File Ccy", dataKey:"FILECURRENCY", visible:true, dataType:"string", width:40},
      CONFIRMTRADE:{headerText:"BNP Trade", dataKey:"CONFIRMTRADE", visible:true, width:90},
      CONFIRMSETTLE:{headerText:"BNP Sett.", dataKey:"CONFIRMSETTLE", visible:true, width:90},
      CONFIRMAMOUNT:{headerText:"BNP Qty", dataKey:"CONFIRMAMOUNT", col_class:"gridTransactionPopupClass confirmationMatch", dataType:"number", dataFormatString:"n4", visible:true, width:120},
      CONFIRMPRICE:{headerText:"BNP Price", dataKey:"CONFIRMPRICE", visible:true, dataType:"number", dataFormatString:"n4", width:85},
      CONFIRMCURRENCY:{headerText:"BNP Ccy", dataKey:"CONFIRMCURRENCY", visible:true, dataType:"string", width:40},
      NEO_BANKREFERENCE:{headerText:"BNP Ref", dataKey:"NEO_BANKREFERENCE", visible:false, dataType:"string", width:90},
      SOPHISMATCH:{headerText:"Ven", dataKey:"SOPHISMATCH", col_class:"gridTransactionPopupClass sophisMatch", visible:true, dataType:"boolean", width:30},
      TRADEFILEMATCH:{headerText:"Trd", dataKey:"TRADEFILEMATCH", visible:true, dataType:"boolean", width:30},
      CONFIRMATIONMATCH:{headerText:"BNP", dataKey:"CONFIRMATIONMATCH", col_class:"'gridTransactionPopupClass confirmationMatch'", visible:true, dataType:"boolean", width:30},
      MATCHSTATUS:{headerText:"Long Status", dataKey:"MATCHSTATUS", visible:false, dataType:"string", width:350, notInColumnList:true},
      MATCHSTATUSBRIEF:{headerText:"Status", dataKey:"MATCHSTATUSBRIEF", col_class:"gridTransactionPopupClass", visible:true, dataType:"string", width:60},
      PAM_FOLIO_ID:{headerText:"S. Folio", dataKey:"PAM_FOLIO_ID", visible:false, dataType:"string", width:60},
      BNP_FOLIO_ID:{headerText:"BNP Folio", dataKey:"BNP_FOLIO_ID", visible:false, dataType:"string", width:60},
      PAM_FUND_NAME:{headerText:"Fund Name", dataKey:"PAM_FUND_NAME", visible:true, dataType:"string", width:350},
      BNP_ACCOUNT:{headerText:"BNP Account", dataKey:"BNP_ACCOUNT", visible:false, dataType:"string", width:120},
      NEO_T:{headerText:"t", dataKey:"NEO_T", visible:false, dataType:"string", width:15},
      NEO_BANK:{headerText:"bank", dataKey:"NEO_BANK", visible:false, dataType:"string", width:50},
      NEO_ST:{headerText:"st", dataKey:"NEO_ST", visible:false, dataType:"string", width:20},
      NEO_STATUS:{headerText:"status", dataKey:"NEO_STATUS", visible:false, dataType:"string", width:70},
      CCY_COLOUR:{headerText:"Ccy Colour", dataKey:"CCY_COLOUR", visible:false, dataType:"string", width:80},
      EURO_VALUE:{headerText:"Value EUR", dataKey:"EURO_VALUE", dataType:"number", dataFormatString:"n0", visible:false, width:90 /*, footerText:'{0}' /**/},
      FUND_NAV:{headerText:"NAV EUR", dataKey:"FUND_NAV", dataType:"number", dataFormatString:"n0", visible:false, width:90},
      PERCENTAGE_OF_NAV:{headerText:"% NAV", dataKey:"PERCENTAGE_OF_NAV", dataType:"number", dataFormatString:"p2", visible:true, width:40},
      DEFAULT:{noCellFormatting:true, visible:true}
    };

    // Classes applied selectively to cells of the moTransactionGrid
    document.appDefaults.moTransactionGridClasses =
    {
      HAS_VALUE_CONFIRMPRICE:'gridTransactionPopupClass confirmationPrice',
      MOVED_DATENEG_CELL:'gridTransactionPopupClass gridMovedDatenegClass',
      CELL_HIGHLIGHT_IF_ZERO:'gridTransactionHighlightIfZero'
    };

    document.appDefaults.moSettlingCashGridDefaults =
    {
      initialColumnOrder:['RN', 'CCY_COLOUR', 'REFCON', 'ISIN', 'TYPE', 'BO_STATUS', 'PAM_FUND_NAME', 'NAME', 'DATE_ENTERED', 'DATE_PAID', 'DATE_PAID_MIN', 'DATE_PAID_MAX', 'INSTRUMENT_CCY', 'SETTLEMENT_CCY', 'PRICE', 'FX', 'FX_DATE'],
      alwaysHidden:['RN', 'DATE_PAID_MIN', 'DATE_PAID_MAX', 'CCY_COLOUR'],
      RN:{headerText:"RN", dataKey:"RN", col_class:"rnClass", visible:false, dataType:"number", dataFormatString:"n0", allowMoving:false, width:1, notInColumnList:true},
      REFCON:{headerText:"Trade Ref.", dataKey:"REFCON", visible:true, dataType:"string", width:80},
      ISIN:{headerText:"ISIN", dataKey:"ISIN", col_class:"gridSettlingPopupClass", visible:true, dataType:"string", width:110, mytag:'fred'},
      NAME:{headerText:"Name", dataKey:"NAME", visible:false, dataType:"string", width:350, sortable:true},
      BO_STATUS:{headerText:"Status", dataKey:"BO_STATUS", visible:true, dataType:"string", width:160},
      CCY_COLOUR:{headerText:"Colour", dataKey:"CCY_COLOUR", visible:false, dataType:"string", width:50, notInColumnList:true},
      PAM_FUND_NAME:{headerText:"Fund Name", dataKey:"PAM_FUND_NAME", visible:false, dataType:"string", width:350},
      DATE_ENTERED:{headerText:"Date Entered", dataKey:"DATE_ENTERED", visible:true, width:90},
      DATE_PAID:{headerText:"Date Paid", dataKey:"DATE_PAID", visible:true, width:90},
      DATE_PAID_MIN:{headerText:"Paid (Min)", dataKey:"DATE_PAID_MIN", visible:false, width:90, notInColumnList:true},
      DATE_PAID_MAX:{headerText:"Paid (Max)", dataKey:"DATE_PAID_MAX", visible:false, width:90, notInColumnList:true},
      INSTRUMENT_CCY:{headerText:"Inst. Ccy", dataKey:"INSTRUMENT_CCY", visible:true, width:50},
      SETTLEMENT_CCY:{headerText:"Sett. Ccy", dataKey:"SETTLEMENT_CCY", visible:true, width:50},
      TYPE:{headerText:"Type", dataKey:"TYPE", visible:false, width:50},
      PRICE:{headerText:"Price", dataKey:"PRICE", visible:false, dataType:"number", dataFormatString:"n4", width:90},
      FX:{headerText:"FX Rate", dataKey:"FX_RATE", visible:true, dataType:"number", dataFormatString:"n4", width:90},
      FX_DATE:{headerText:"FX Date", dataKey:"FX_DATE", visible:true, width:90},
      DEFAULT_VALUE_CELL:{visible:true, width:90, dataType:"number", dataFormatString:"n0", valueCell:true},
      DEFAULT:{noCellFormatting:true, visible:false, width:90}
    };

    document.appDefaults.MilestoneGridDefaults =
    {
      initialColumnOrder:['INSTANCE', 'FUNDID', 'FUNDNAME'],
      alwaysHidden:['INSTANCE', 'FUNDID'],
      FUNDID:{headerText:"FundID", dataKey:"FundID", col_class:"rnClass", visible:false, dataType:"number", dataFormatString:"n0", allowMoving:false, width:1, notInColumnList:true},
      INSTANCE:{headerText:"Instance", dataKey:"INSTANCE", visible:false, dataType:"string", width:100, sortable:true},
      FUNDNAME:{headerText:"Fund", dataKey:"FundName", visible:true, dataType:"string", width:250, sortable:true},
      DEFAULT_VALUE_CELL:{visible:true, dataType:"string", width:100, sortable:false},
      DEFAULT:{noCellFormatting:true, visible:true, width:200, sortable:false}
    };

    // Classes applied selectively to cells of the moTransactionGrid
    document.appDefaults.moSettlingCashGridClasses =
    {
      SETTLING_VALUE_CELL:'gridSettlingPopupClass gridSettlingValueClass',
      MOVED_DATENEG_CELL:'gridSettlingPopupClass gridMovedDatenegClass'
    };

    var tempDate = new Date();
    document.appDefaults.todaysDateString = tempDate.getFullYear() + '-' + ("00" + (tempDate.getMonth() + 1)).slice(-2) + '-' + ("00" + tempDate.getDate()).slice(-2);

    document.morningstarCodes = {
      FR0010259937:'F0GBR06FKX',
      FR0010371609:'F000000LL9',
      LU0231483313:'F000000OUF',
      LU0260871040:'FOGBR05JGY',
      FR0010016477:'F000001GU3',
      FR0010737478:'F000002PCE',
      LU0516398475:'F00000JNZJ',
      LU0323456896:'F000005HII',
      LU0281484617:'F000000LQJ',
      FR0010337667:'F0000000TN',
      IE00B1HL8R59:'F000001BMB',
      LU0276015889:'F0000001SA',
      LU0256881128:'F0000000K6',
      LU0278915607:'F000000DUX',
      FR0010035451:'FHUSA04G7M',
      LU0350714233:'F000002OWZ',
      LU0169528188:'F000000AW5',
      FR0010144568:'F0GBR05V2Q',
      LU0370788910:'F00000210Y',
      LU0211118053:'F00000289E',
      LU0106253437:'F0GBR04AE8',
      FR0010007542:'F0GBR04HRN',
      FR0010482042:'F000000KQ0',
      FR0011007251:'F00000MNJW',
      LU0128494944:'F0GBR04JIX',
      LU0417733598:'F000002Q5I',
      LU0357130771:'F00000246I',
      LU0235308482:'F0GBR06MIL',
      FR0010659797:'F0000025EK',
      LU0414047281:'F000002MYS',
      LU0325598752:'F000000LUW',
      LU0102013652:'F0GBR04N9O',
      LU0177222394:'F0GBR061KC',
      FR0010298596:'F0GBR06TFP',
      LU0094219127:'F0GBR050WV',
      LU0555020055:'F00000ME0E',
      LU0208853860:'F0GBR05WL8',
      LU0217389567:'F0GBR0698P',
      GB00B2PDRX95:'F00000MIVR',
      LU0513831684:'F00000JRMW',
      LU0555028207:'F00000ME45',
      LU0546917344:'F00000ME2R',
      LU0424777026:'F000002QOI',
      LU0448666684:'F0000040TU',
      LU0348529875:'F000001AE4',
      LU0102015780:'F0GBR04N2L',
      LU0555024123:'F00000ME2N',
      LU0406674407:'F000002LY9',
      LU0304239170:'F000001WKP',
      LU0503630070:'F00000J4FL',
      LU0304241580:'F00000231J',
      FR0000930455:'F0GBR04RDD',
      BE0948504387:'F000003RV3',
      LU0304236150:'F000001WKR',
      FR0010237503:'F0GBR054A7',
      LU0102014627:'F0GBR04I63',
      LU0055631609:'F0GBR04AR8',
      LU0278456651:'F0000009J2',
      LU0088882138:'F0GBR04DJG',
      LU0128489514:'F0GBR04JH5',
      LU0408846615:'F00000MU1W',
      LU0430495183:'F00000HHAR',
      LU0133805464:'F0GBR05AU0',
      LU0390136579:'F00000269F',
      LU0540001038:'F00000JRAR',
      LU0304234296:'F000001XFV',
      LU0231482349:'F000000OUE',
      LU0135487147:'F0GBR05ATR',
      LU0345780448:'F000000LJQ',
      LU0577863615:'F00000ME3P',
      LU0129463179:'F0GBR055EY',
      GB00B28CMX88:'F000000K9L',
      LU0159065456:'F0GBR06GLW',
      FR0000941312:'F0GBR04R40',
      FR0010251132:'F0GBR06QQW',
      LU0129415286:'F0GBR055EV',
      LU0548367084:'F00000JWDC',
      IE0032722484:'F0GBR052U0',
      LU0318940185:'F000000KDA',
      LU0297941972:'F000000FAK',
      LU0128522157:'F0GBR04ARH',
      LU0316493237:'F000000I3N',
      GB0033874768:'F0GBR04H80',
      FR0007496047:'F0GBR04QGT',
      LU0040507039:'F0GBR04HWE',
      LU0227179875:'F0GBR061JV',
      LU0291300431:'F000000GY7',
      FR0011170786:'F00000NYN6',
      LU0210529573:'F0GBR05VWI',
      LU0217576759:'F0GBR060JU',
      LU0249356808:'F0GBR06WBY',
      LU0282388866:'F000001LO2',
      LU0337098114:'F000001AKI',
      LU0522792919:'F00000NDF8',
      LU0172157280:'0P0000RZ3H',
      BE0948500344:'F000003RV0',
      LU0132180729:'F0GBR065CC',
      LU0128469243:'F0GBR04LYO',
      LU0513834787:'F00000JRMH',
      LU0355583906:'F000001V23',
      LU0432366796:'F000003YH4',
      GB00B2PF4L24:'F00000MIUS',
      LU0272941971:'F0000002EC',
      FR0000295230:'F0GBR04QCK',
      LU0522793131:'F00000M19U',
      LU0309034717:'F000000QSD',
      FR0010937847:'F00000JV3B',
      FR0000444002:'F0GBR04G5D',
      LU0390137205:'F00000269R',
      FR0000978371:'F0GBR04NRL',
      FR0010148981:'F0GBR04F8Y',
      FR0010492728:'F000000RQ5',
      FR0000443954:'F0GBR04G59',
      FR0010429068:'F000000G71',
      LU0347704925:'F000001UFM',
      FR0000983744:'F0GBR04IX1',
      FR0011060300:'F00000MO1X',
      LU0202403266:'FOGBR05JLL',
      FR0011144195:'F00000NOLI',
      LU0106253197:'F0GBR04ADV',
      FR0010032326:'F0GBR065P8',
      LU0350713698:'F000001V21',
      IE0004852103:'F0GBR04SJK',
      GB0033874214:'F0GBR04H7Y',
      LU0304244501:'F00000231V',
      FR0010330902:'F0GBR04N1E',
      FR0010135103:'F0GBR04F90',
      LU0414045822:'F000002MYQ',
      FR0010117085:'F0GBR05PQ0',
      FR0010657122:'F0000025EJ',
      FR0010315770:'F0GBR06TFO',
      FR0000299356:'F0GBR04JY3',
      FR0000170292:'F0GBR04R53',
      LU0055114457:'F0GBR04D14',
      LU0048585144:'F0GBR04RZS',
      LU0081500794:'F0GBR04TBJ',
      FR0010321802:'F0GBR04QCF',
      FR0010188383:'F0GBR067SB',
      LU0243956348:'F0GBR06T9G',
      LU0224508597:'F0GBR064S8',
      DE0009769869:'F0GBR04CNE',
      LU0104884860:'F0GBR04BC7',
      FR0010589044:'F0GBR04EOK',
      FR0010149112:'F0GBR04F8U',
      FR0010376343:'F0GBR04FUQ',
      FR0000989899:'F0GBR04RBR',
      FR0000011892:'F0GBR04ETT',
      LU0165074666:'F0GBR053MP',
      LU0231459107:'F0GBR06X7H',
      LU0346392995:'F000001AU0',
      FR0010554303:'F000001T3K',
      LU0191819951:'F0GBR064OL',
      FR0010326140:'F0GBR06XOC',
      LU0289472085:'F000000QXK',
      GB0033737874:'F0GBR04HGX',
      LU0254836850:'F0000000VT',
      FR0010791145:'F0000045V9',
      LU0164455502:'F0GBR04GUI',
      FR0010791137:'F0000045VA',
      FR0007068085:'F0GBR04MS0',
      FR0010345389:'F0000000FC',
      LU0318934451:'F000000KVC',
      LU0208853274:'F0GBR05S8J',
      FR0007390174:'F0GBR04FLZ',
      FR0000299620:'F0GBR04QPA',
      LU0171301533:'0P0000RZ3G',
      LU0307839646:'F000000GVL',
      LU0318933057:'F000000LQN',
      LU0261946445:'FOGBR05KM5',
      FR0007450002:'F0GBR04P4B',
      FR0000292278:'F0GBR04QCJ',
      LU0225506756:'F0GBR064TZ',
      FR0010149302:'F0GBR04F8J',
      LU0469023948:'F000005LO4',
      LU0348816934:'F0GBR06X07',
      LU0390137031:'F00000269S',
      LU0273147594:'F0000006QD',
      FR0010702209:'F000002HAJ',
      LU0363509208:'F000002FCY',
      LU0171305526:'0P0000VHO0',
      LU0099626896:'F0GBR04N2R',
      LU0555019479:'F00000ME08',
      LU0555027738:'F00000ME3U',
      LU0101987716:'F0GBR04N3B',
      LU0496417709:'F00000H0XP',
      LU0555020998:'F00000ME0Q',
      LU0329592611:'F0000020SI',
      LU0490863262:'F00000HH5C',
      LU0135706553:'F0GBR05WQ3',
      FR0000423147:'F0GBR04QDR',
      LU0177592218:'F0GBR04HGF',
      LU0119211166:'F0GBR04B5T',
      LU0326960662:'F000000KR6',
      FR0007065735:'F0GBR04J1L',
      LU0132208918:'F0GBR06C9L',
      LU0191250173:'F0GBR06Q1Z',
      LU0396318908:'F00000H34R',
      LU0231480137:'F000000OUC',
      LU0413357343:'F000003YJP',
      LU0217138485:'F0GBR04BBY',
      LU0188500879:'F0GBR056KC'
    };

    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }
  }

function fnCheckBoxActivate(e)
  {
  /* toggles 'disabled' status of input items in the next sibling container. */
  var aa = $(this).parents('.formControlDiv').next().find('input');
  var bb = $(this).prop('checked');

  if ($(this).prop('checked'))
    $(this).parents('.formControlDiv').next().find('input').removeAttr('disabled');
  else
    $(this).parents('.formControlDiv').next().find('input').attr('disabled', 'disabled');
  }

function fnPrevNextDatepicker(e)
  {
  /* Utility function to Increment / Decrement a datepicker field using + / - controls. */
  try
    {
    var tag = this.getAttribute("tag");
    var picker;
    var increment = 0;
    var thisDate; // = picker.datepicker("getDate");

    switch (tag)
    {
      case 'prev':
        picker = $(this).parents('.formControlDiv').find('.datePicker:first');
        increment = -1;
        break;

      case 'next':
        picker = $(this).parents('.formControlDiv').find('.datePicker:first');
        increment = 1;
        break;

      case 'prevGroup':
        increment = -1;
        picker = $(this).parents('.formItemGroup').find('.datePicker');
        break;

      case 'nextGroup':
        increment = 1;
        picker = $(this).parents('.formItemGroup').find('.datePicker');
        break;

      case 'prevAll':
        increment = -1;
        picker = $(this).parents('.formItemAll').find('.datePicker');
        break;

      case 'nextAll':
        increment = 1;
        picker = $(this).parents('.formItemAll').find('.datePicker');
        break;

    }

    picker.each(
      function (index, element)
      {
      thisDate = $(this).datepicker("getDate");
      if (thisDate)
        {
        thisDate.addBusDays(increment);
        $(this).datepicker("setDate", thisDate);
        $(this).change();
        }
      }
    );
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }
  }

function fnRecentDatepicker(e)
  {
  try
    {
    e.preventDefault();
    var scopeSelector = this.getAttribute("scope");

    // get associated Date Picker

    var dp = $('.datePicker', $(e.target).parents(scopeSelector)[0]);

    // Only a single context menu is used for the Transactions Grids.

    var menu = $('#bodyDiv .fsContextMenu');
    var menuList = $('#bodyDiv .fsContextMenu ul');
    var todayDate = new Date();
    var tomorrowDate = new Date();
    tomorrowDate.addBusDays(1);
    var yesterdayDate = new Date();
    yesterdayDate.addBusDays(-1);

    // Clear existing entries.

    menuList.empty();

    // Build menu options.

    $('<li><a class="pickToday">Today</a></li>').appendTo(menuList).click(function ()
    {
    dp.datepicker("setDate", todayDate);
    dp.change();
    menu.hide();
    });
    $('<li><a class="pickTomorrow">Tomorrow</a></li>').appendTo(menuList).click(function ()
    {
    dp.datepicker("setDate", tomorrowDate);
    dp.change();
    menu.hide();
    });
    $('<li><a class="pickYesterday">Yesterday</a></li>').appendTo(menuList).click(function ()
    {
    dp.datepicker("setDate", yesterdayDate);
    dp.change();
    menu.hide();
    });

    // position and show window.

    menu.css({top:e.pageY - 10, left:e.pageX - 10, 'z-index':200000});

    menu.show();

    return false;
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }
  }


function fnSimpleGridPrint(e, options)
  {
  /*
   Generic print routine...

   options : {
   flexigrid  : DOM or jQuery flexigrid to print contents of.
   title      : Simple header text.
   headerHTML : HTML fragment to use as page header (takes precedence over '.title').
   footerHTML : HTML fragment to use as page footer.
   appendHTML : HTML to append after other body HTML.
   html       : Page contents HTML. takes precedence over all other sources.
   tableStyle : jQuery CSS styling object applied to flexigrid derived table (if one is derived)
   }

   checks if e.target is a dialogDiv and has a flexiGrid as a child. options.flexigrid takes precedence.

   */
  try
    {
    var thisHTML;

    if (DEBUG_FLAG) console.log('fnSimpleGridPrint()');

    options = options || {};

    if (!options.appendHTML) options.appendHTML = '';
    if (options.html) options.html = options.html + options.appendHTML;

    // output from Dialog table ?

    var dialog;

    if ($(e.target).hasClass('dialogDiv'))
      {
      dialog = e.target;
      }
    else
      {
      dialog = $(e.target).parents('.dialogDiv')[0];
      }

    if ((options.flexigrid) || ((dialog) && ($('.flexigrid', $(dialog)).length > 0)))
      {
      var sourceGrid;

      if (options.flexigrid)
        {
        sourceGrid = $(options.flexigrid)[0];
        }
      else
        {
        sourceGrid = $('.flexigrid', $(dialog))[0];
        }

      var nTable = $(document.createElement("table"));
      if (options.tableStyle)
        {
        nTable.css(options.tableStyle);
        }

      nTable.append($('.hDiv thead:first', $(sourceGrid))[0].outerHTML);
      nTable.append($('.bDiv tbody:first', $(sourceGrid))[0].outerHTML);

      nTable.find('th').filter(function ()
      {
      return $(this).css("display") === 'none';
      }).remove();

      nTable.find('td').filter(function ()
      {
      return $(this).css("display") === 'none';
      }).remove();

      thisHTML = nTable[0].outerHTML;
      }
    else if ($(e.target).attr('printtable').length > 0)
      {
      thisHTML = $($(e.target).attr('printtable'))[0].outerHTML;
      }
    else if (!options.html)
      {
      if (DEBUG_FLAG) console.log('  Nothing found to print, no flexigrid or given HTML.');
      return;
      }

    options = $.extend(true, {html:(thisHTML + options.appendHTML), title:'Primonial Asset Management', headerHTML:'', footerHTML:''}, options, {flexigrid:false});  // Chrome complains if try to stringify jQuery object (flexigrid)

    jQuery.ajax({
      type:"POST",
      async:false,
      url:'php/reports/simpleGridReport.php?antiCache=' + getUniqueCounter(),
      data:{formdata:JSON.stringify(options)}
    });

    window.open("php/reports/simpleGridReport.php?antiCache=" + getUniqueCounter(), "_blank");

    //      if (document.is_safari)
    //          {
    //          console.log("safari");
    //          window.open("php/reports/simpleGridReport.php", "_blank");
    //          }
    //        else if (document.is_chrome)
    //          {
    //          console.log("chrome");
    //          window.open("php/reports/simpleGridReport.php", "_blank");
    //          }
    //        else
    //          {
    //          console.log("not safari");
    //          window.open("php/reports/simpleGridReport.php", "pdfIframe");
    //          }
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }

  }


function checkPendingUrlCache()
  {
  try
    {
    if (!document.pendingUrlCache)
      {
      document.pendingUrlCache = [];
      }

    if (document.pendingUrlCache.length > 0)
      {
      if ($.active <= 0)
        {
        var pendingRequest = document.pendingUrlCache.shift();

        if (typeof pendingRequest == 'object')
          {
          preloadURL(pendingRequest.URL, pendingRequest.cacheKey, 20, false);
          }
        }

      setTimeout("checkPendingUrlCache()", 2000);
      return;
      }
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }

  setTimeout("checkPendingUrlCache()", 5000);
  }

/* Timed Updates */

function timedUpdates(updateFormsOnly)
  {
  var UpdateCacheObject;
  var thisDialog;
  try
    {
    if (DEBUG_FLAG) console.log('timedUpdates(' + updateFormsOnly + ')');

    if ('UpdateCache' in document.dataCache)
      {
      UpdateCacheObject = document.dataCache.UpdateCache;
      }
    else
      {
      document.dataCache.UpdateCache = {};
      document.dataCache.UpdateCache.MoTransactions = {doUpdate:false, lastVeniceAuditCount:-1, lastVeniceHistoCount:-1, lastFileCount:-1, lastNeoCount:-1};
      document.dataCache.UpdateCache.Milestones = {doUpdate:false, lastVeniceAuditCount:-1};
      UpdateCacheObject = document.dataCache.UpdateCache
      }

    var moCacheObject = UpdateCacheObject.MoTransactions;
    var MilestonesCacheObject = UpdateCacheObject.Milestones;

    if (!updateFormsOnly)
      {
      var url;
      var MoTransactionsExist = false;
      var MoSettlingCashExist = false;
      var MilestoneFormsExist = false;

      try /* What forms exist' */
        {

        for (thisDialog in document.dialogArray)
          {
          try
            {
            if (document.dialogArray[thisDialog])
              {
              if (document.dialogArray[thisDialog].inUse == true)
                {
                if (document.dialogArray[thisDialog].dialogType == document.dialogArray.dialogType.MoTransactions)
                  {
                  MoTransactionsExist = true;
                  }
                if (document.dialogArray[thisDialog].dialogType == document.dialogArray.dialogType.MoSettlingCash)
                  {
                  MoSettlingCashExist = true;
                  }
                if (document.dialogArray[thisDialog].dialogType == document.dialogArray.dialogType.Milestones)
                  {
                  MilestoneFormsExist = true;
                  }
                }

              }
            }
          catch (e)
            {
            console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
            }
          } // For (thisDialog...

        }
      catch (e)
        {
        console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
        }

      try /* Update Milestone status' */
        {
        if (MilestoneFormsExist)
          {
          if (DEBUG_FLAG) console.log('  timedUpdates : Check Transactions Status...');

          url = "php/getMilestoneStatus.php";

          var formJSON = {
            'USERID': document.dakotaUserID,
            'TOKEN': document.dakotaSessionKey
          };

          $.ajax({type:"POST",
            url:url,
            data: formJSON,

            complete:function ()
              {
              if (DEBUG_FLAG) console.log('  timedUpdates : ajax : complete()');
              },
            error:function (jqXHR, textStatus, errorThrown)
              {
              console.log('  timedUpdates : ajax : error() : ' + textStatus);
              },

            success:function (html)
              {
              if (DEBUG_FLAG) console.log('  timedUpdates : ajax : Milestones, success()');

              var rVal = JSON.parse(html);
              MilestonesCacheObject = UpdateCacheObject.Milestones;

              if (DEBUG_FLAG) console.log('  timedUpdates : Milestones, Status returned...');

              if (MilestonesCacheObject.lastVeniceAuditCount != rVal.VeniceAuditCount)
                {

                // Update Forms
                if (MilestonesCacheObject.lastVeniceAuditCount >= 0)
                  {
                  // Update NAVs ?
                  if (MilestonesCacheObject.lastVeniceAuditCount != rVal.VeniceAuditCount)
                    {
                    MilestonesCacheObject.doUpdate = true;
                    MilestonesCacheObject.updateMilestones = true;
                    }

                  if (DEBUG_FLAG) console.log('  timedUpdates : Milestones, Update necessary...');
                  }
                else
                  {
                  if (DEBUG_FLAG) console.log('  timedUpdates : Milestones, First check, initialise values ...');
                  }

                if (DEBUG_FLAG)
                  {
                  console.log('    lastVeniceAuditCount : ' + MilestonesCacheObject.lastVeniceAuditCount + ' vs ' + rVal.VeniceAuditCount);
                   }

                MilestonesCacheObject.lastVeniceAuditCount = rVal.VeniceAuditCount;

                timedUpdates(true);
                }
              else
                {
                if (DEBUG_FLAG) console.log('  timedUpdates : Milestones, Nothing to do.');
                }
              }

          });
          }
        else
          {
          if (DEBUG_FLAG) console.log('  timedUpdates ; no milestone forms to check.');
          }
        }
      catch (e)
        {
        console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
        }

      try /* Update Middle Office Transaction status' */
        {

        // If forms that might require updating exist, check the transactions status.

        if (MoTransactionsExist || MoSettlingCashExist)
          {
          if (DEBUG_FLAG) console.log('  timedUpdates : Check Transactions Status...');

          url = "php/getMoTransactionStatus.php";

          var formJSON = {
            'USERID': document.dakotaUserID,
            'TOKEN': document.dakotaSessionKey
          };

          $.ajax({type:"POST",
            url:url,
            data: formJSON,

            complete:function ()
              {
              if (DEBUG_FLAG) console.log('  timedUpdates : ajax : complete()');
              },
            error:function (jqXHR, textStatus, errorThrown)
              {
              console.log('  timedUpdates : ajax : error() : ' + textStatus);
              },

            success:function (html)
              {
              try
                {
                if (DEBUG_FLAG) console.log('  timedUpdates : ajax : Transactions, success()');

                var rVal = JSON.parse(html);
                moCacheObject = UpdateCacheObject.MoTransactions;

                if (DEBUG_FLAG) console.log('  timedUpdates : Transactions, Status returned...');

                if ((moCacheObject.lastVeniceAuditCount != rVal.VeniceAuditCount) ||
                  (moCacheObject.lastVeniceHistoCount != rVal.VeniceHistoCount) ||
                  (moCacheObject.lastFileCount != rVal.FileCount) ||
                  (moCacheObject.lastNeoCount != rVal.NeoCount))
                  {

                  // Update NAVs ?
                  if ((moCacheObject.lastVeniceAuditCount != rVal.VeniceAuditCount) ||
                    (moCacheObject.lastVeniceHistoCount != rVal.VeniceHistoCount))
                    {
                    moCacheObject.updateFundNAVs = true;
                    }

                  // Update Forms
                  if (moCacheObject.lastVeniceAuditCount >= 0)
                    {
                    moCacheObject.doUpdate = true;
                    moCacheObject.updateMoTransactions = true;
                    moCacheObject.updateSettlingCash = true;

                    if (DEBUG_FLAG) console.log('  timedUpdates : Transactions, Update necessary...');
                    }
                  else
                    {
                    if (DEBUG_FLAG) console.log('  timedUpdates : Transactions, First check, initialise values ...');
                    }

                  if (DEBUG_FLAG)
                    {
                    console.log('    lastVeniceAuditCount : ' + moCacheObject.lastVeniceAuditCount + ' vs ' + rVal.VeniceAuditCount);
                    console.log('    lastVeniceHistoCount : ' + moCacheObject.lastVeniceHistoCount + ' vs ' + rVal.VeniceHistoCount);
                    console.log('    lastFileCount        : ' + moCacheObject.lastFileCount + ' vs ' + rVal.FileCount);
                    console.log('    lastNeoCount         : ' + moCacheObject.lastNeoCount + ' vs ' + rVal.NeoCount);
                    }

                  moCacheObject.lastVeniceAuditCount = rVal.VeniceAuditCount;
                  moCacheObject.lastVeniceHistoCount = rVal.VeniceHistoCount;
                  moCacheObject.lastFileCount = rVal.FileCount;
                  moCacheObject.lastNeoCount = rVal.NeoCount;

                  timedUpdates(true);
                  }
                else
                  {
                  if (DEBUG_FLAG) console.log('  timedUpdates : Nothing to do.');
                  }
                }
              catch (e)
                {
                console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
                }
              }
          });

          RefreshTokenExpiry(document.dakotaUserID, document.dakotaSessionKey);
          }
        else
          {
          if (DEBUG_FLAG) console.log('  timedUpdates ; no transactions forms to check.');
          }
        }
      catch (e)
        {
        console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
        }
      }

    try /* Timed updates for Middle Office Transaction matching. */
      {
      var dialogID;

      if (MilestonesCacheObject.doUpdate)
        {
        for (thisDialog in document.dialogArray)
          {
          try
            {
            if (document.dialogArray[thisDialog])
              {
              if (document.dialogArray[thisDialog].inUse == true)
                {
                if ((MilestonesCacheObject.updateMilestones) && (document.dialogArray[thisDialog].dialogType == document.dialogArray.dialogType.Milestones))
                  {
                  dialogID = document.dialogArray[thisDialog].ID;

                  if (DEBUG_FLAG) console.log('  timedUpdates : getMilestonesData(' + dialogID + ')');
                  getMilestonesData(dialogID);
                  }
                }
              }
            }
          catch (e)
            {
            console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
            }
          } // For (thisDialog...
        }

      if (moCacheObject.doUpdate)
        {
        for (thisDialog in document.dialogArray)
          {
          try
            {
            if (document.dialogArray[thisDialog])
              {
              if (document.dialogArray[thisDialog].inUse == true)
                {
                if ((moCacheObject.updateMoTransactions) && (document.dialogArray[thisDialog].dialogType == document.dialogArray.dialogType.MoTransactions))
                  {
                  dialogID = document.dialogArray[thisDialog].ID;

                  if (DEBUG_FLAG) console.log('  timedUpdates : getMoTransactionsData(' + dialogID + ')');
                  getMoTransactionsData(dialogID);
                  }
                if ((moCacheObject.updateSettlingCash) && (document.dialogArray[thisDialog].dialogType == document.dialogArray.dialogType.MoSettlingCash))
                  {
                  dialogID = document.dialogArray[thisDialog].ID;

                  if (DEBUG_FLAG) console.log('  timedUpdates : getMoSettlingCashData(' + dialogID + ')');
                  getMoSettlingCashData(dialogID);
                  }
                }
              }
            }
          catch (e)
            {
            console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
            }
          } // For (thisDialog...
        } // If moCacheObject.doUpdate
      }
    catch (e)
      {
      console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
      }
    finally
      {
      moCacheObject.doUpdate = false;
      moCacheObject.updateMoTransactions = false;
      moCacheObject.updateSettlingCash = false;
      moCacheObject.updateFundNAVs = false;

      MilestonesCacheObject.doUpdate = false;
      MilestonesCacheObject.updateMilestones = false;
      }

    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }
  finally
    {
    if ((updateFormsOnly === undefined) || (updateFormsOnly == false))
      {
      if (DEBUG_FLAG) console.log('  timedUpdates : set next Timeout');
      setTimeout(timedUpdates, TIMED_UPDATES_INTERVAL);
      }
    }
  }

function testClick(e)
  {
  $(".sub-menu", $(e.target).parents(".ui-menu-list")).css({display:"none"})
  }

function test()
  {
  //  window.location.href = 'php/testReport.php';
  //
  //  return;

  //  var oTable = $('.dialogDiv .flexigrid')[0];
  var oTable = $('#fsDialog1_grid');

  var aTest = $(document.createElement("div"));
  aTest.append('<table class="gridTable"></table>');

  var gridArray = $(oTable).flexGet('jsArray');
  var colModel = $(oTable).flexGet('colModel');
  var colArray = new Array();
  var rowStyleFormatter = $(oTable).flexGet('rowStyleFormatter');
  var cellStyleFormatter = $(oTable).flexGet('cellStyleFormatter');

  for (index in colModel)
    {
    switch (colModel[index].dataKey)
    {
      case 'NAME':
      case 'SOPHISSTATUS':
      case 'SOPHISPRICE':
      case 'BNPSTATUS':
      case 'ISIN':
      case 'REFCON':
      case 'SOPHISAMOUNT':
      case 'FILEAMOUNT':
        colArray.push($.extend(true, {}, colModel[index]));
        break;
    }
    }


  aTest.find('table').flexigrid({
      jsArray:gridArray,
      colModel:colArray,
      rowStyleFormatter:rowStyleFormatter,
      cellStyleFormatter:cellStyleFormatter
    }
  );

  ////  generateexcel($('.dialogDiv .gridTable')[0].id);

  var nTable = $(document.createElement("table"));

  nTable.append($('.hDiv thead:first', $(aTest))[0].outerHTML);
  nTable.append($('.bDiv tbody:first', $(aTest))[0].outerHTML);


  nTable.find('th').filter(function ()
  {
  return $(this).css("display") === 'none';
  }).remove();

  nTable.find('td').filter(function ()
  {
  return $(this).css("display") === 'none';
  }).remove();

  var test = {html:nTable[0].outerHTML};

  //window.open('php/test.php?formdata=' + encodeURIComponent(JSON.stringify(test)), '_blank_');

  jQuery.ajax({
    type:"POST",
    url:'test.php',
    data:{formdata:JSON.stringify(test)},
    success:function (response)
      {
      //    win_detail = window.open('', 'name');
      //    win_detail.document.write(response);
      }
  });

  //  window.open('php/test.php', '_blank_');

  window.location.href = 'test.php';
  //  window.open('test.php');

  /*
   var worker = new Worker('javascript/workers/ww_test.js');

   worker.addEventListener('message', function (e)
   {
   alert('Worker said: ' + e.data);
   }, false);

   worker.postMessage('Hello World'); // Send data to our worker.
   */
  /*
   var url = "php/getMoTransactionStatus.php";

   // Get data.
   // Sophis Folio (Funds) data query.

   $.ajax({type:"POST",
   url:url,
   data:{},
   success:function (html)
   {
   try
   {
   var rVal = JSON.parse(html);
   }
   catch (e)
   {
   }
   }
   });
   */
  }

function generateexcel(tableid)
  {
  //var table= document.getElementById(tableid);
  //var html = table.outerHTML;
  //
  ////add more symbols if needed...
  //html = html.replace(/á/g, '&aacute;');
  //html = html.replace(/é/g, '&eacute;');
  //html = html.replace(/í/g, '&iacute;');
  //html = html.replace(/ó/g, '&oacute;');
  //html = html.replace(/ú/g, '&uacute;');
  //html = html.replace(/º/g, '&ordm;');

  //window.open('data:application/vnd.ms-excel;charset=utf8,' + encodeURIComponent(html));

  }

function writeFromDialogGridToExcel (e,thisDialog)
  {
  try
    {

    // output from Dialog table ?

    var dialog;
    var sourceGrid;

    if ($(e.target).hasClass('dialogDiv'))
      {
      dialog = e.target;
      }
    else
      {
      dialog = $(e.target).parents('.dialogDiv')[0];
      }

    sourceGrid = $('.flexigrid .bDiv table', $(dialog));

    if (sourceGrid.length > 0)
      {
      var dd = sourceGrid.flexGet('jsArray');
      sourceGrid.flexSendToExcel();
      }

    }
  catch (e)
    {}
  }




