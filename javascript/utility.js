// JavaScript Document.


function settlingCashGridQuickSort(jFlexGrid, colModel, sortname, sortorder)
  {
  /*
   The clue is in the name.....

   This is a sort function, used for the Settling-Cash form.
   The requirements of this form are that only the active section of the grid is sorted, the
   total lines and summary section must not be changed.

   Note : The generic grid sort function is internal to the flexiGrid code.
   */

  function compare(a, b, sortname, multiplier)
    {
    /*
     a          : Object 1
     b          : Object 2
     sortname   : Property name on which to sort.
     multiplier : 1 - Sort Ascending, -1 - sort Descending.
     */
    var date1;
    var date2;

    if (numericDataType)
      {
      if (parseFloat(a[sortname]) < parseFloat(b[sortname])) return (-1 * multiplier);
      if (parseFloat(a[sortname]) > parseFloat(b[sortname])) return (multiplier);
      }
    else if (dateDataType)
      {
      try
        {
        date1 = Date(nz(a[sortname],''));
        date2 = Date(nz(b[sortname],''));

        if (date1.getTime() < date2.getTime()) return (-1 * multiplier);
        if (date1.getTime() > date2.getTime()) return (multiplier);
        }
      catch (e)
        {
        if (nz(a[sortname],'') < nz(b[sortname],'')) return (-1 * multiplier);
        if (nz(a[sortname],'') > nz(b[sortname],'')) return (multiplier);
        }
      }
    else
      {
      if (nz(a[sortname],'') < nz(b[sortname],'')) return (-1 * multiplier);
      if (nz(a[sortname],'') > nz(b[sortname],'')) return (multiplier);
      }

    return 0;
    }

  function quicksort(sortArray, left, right, sortname, multiplier)
    {
    /*
     Generic QuickSort algorithm, sourced from the internet somewhere.
     Designed to sort a section of an array, delimited by 'left' and 'right'.

     sortArray  : Array to Sort
     left       : Start Index
     right      : End Index
     sortname   : Property name on which to sort.
     multiplier : 1 - Sort Ascending, -1 - sort Descending.

     */

    function swap(sortArray, i, j) { var t=sortArray[i]; sortArray[i]=sortArray[j]; sortArray[j]=t; }

    if (left < right)
      {

      var pivot = sortArray[(left + right) >> 1];
      var left_new = left, right_new = right;

      do {
      while (compare(sortArray[left_new], pivot, sortname, multiplier) < 0)
        {
        left_new++;
        }

      while (compare(pivot, sortArray[right_new], sortname, multiplier) < 0)
        {
        right_new--;
        }

      if (left_new <= right_new)
        swap(sortArray, left_new++, right_new--);
      } while (left_new <= right_new);

      quicksort(sortArray, left, right_new, sortname, multiplier);
      quicksort(sortArray, left_new, right, sortname, multiplier);
      }
    }

  // Sort the array down to the first spacer row, after which will be the summary lines.

  try
    {
    var gridArray = jFlexGrid.flexGet('jsArray');
    var multiplier = 1;
    var thisDataType;
    var numericDataType = false;
    var dateDataType = false;
    var rowTo = -1;

    // find end of sortable range.

    while ((rowTo < gridArray.length) && (gridArray[rowTo+1]['ROW_TYPE'] != document.appDefaults.rowType.spacer) && (gridArray[rowTo+1]['ROW_TYPE'] != document.appDefaults.rowType.footer))
      {
        rowTo++;
      }
    if (rowTo < 0) return;

    // Continue.

    if (!colModel.sortable)
      {
      return;
      }

    thisDataType = colModel.dataType;

    if ((thisDataType == "number") || (thisDataType == "numeric"))
      {
      numericDataType = true;
      }
    else if (thisDataType == "date")
      {
      dateDataType = true;
      }

    if (sortorder == 'desc')
      {
      multiplier = -1;
      }

    quicksort(gridArray, 0, rowTo, sortname, multiplier);

    jFlexGrid.flexReload();

    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }
  }

function getMorningstarCode(ISIN)
  {
  try
    {
    // Check ISIN : Morningstar code cache first.

    var uISIN = ISIN.toUpperCase();

    var thisMSCode = document.morningstarCodes[uISIN];

    // If the ISIN is not in the hard cache, then try a search.

    if (thisMSCode == false)
      {
      var msCodes = getCacheItem('MorningstarCodes');

      if (msCodes === false)
        {
        return false;
        }

      thisMSCode = msCodes[ISIN];
      }

    if (thisMSCode)
      {
      return thisMSCode;
      }

    return false;
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    return false;
    }

  return false;
  }

function setMorningstarCode(ISIN, MsCode)
  {
  try
    {
    var msCodes = getCacheItem('MorningstarCodes');

    if (msCodes === false)
      {
      msCodes = {};
      }

    msCodes[ISIN] = MsCode;

    var cacheExpiryDate = new Date();
    cacheExpiryDate.setHours(23);
    cacheExpiryDate.setMinutes(59);

    var minsToExpiry = (cacheExpiryDate - (new Date())) / 60000;

    setCacheItem('MorningstarCodes', msCodes, minsToExpiry);

    return true;
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    return false;
    }

  return false;
  }

function getCachedURL(URL)
  {
  try
    {
    var savedURL = getCacheItem(URL);

    if (savedURL === false)
      {
      return false;
      }

    return savedURL;
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    return false;
    }

  return false;
  }

function setCachedURL(URL, HTML)
  {
  try
    {
    var cacheExpiryDate = new Date();
    cacheExpiryDate.setHours(23);
    cacheExpiryDate.setMinutes(59);

    var minsToExpiry = (cacheExpiryDate - (new Date())) / 60000;

    setCacheItem(URL, HTML, minsToExpiry);

    return true;
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    return false;
    }

  return false;
  }

function getCacheItem(cacheKey, ignoreDataExpiry)
  {
  try
    {
    if (!ignoreDataExpiry)
      {
      ignoreDataExpiry = false;
      }

    //var datum = localStorage.getItem(cacheKey);
    var datum;

    // replicate local storage with memory.
    try
      {
      datum = document.fauxLocalStorage[cacheKey];
      }
    catch (e)
      {
      console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
      datum = false;
      }

    if (!datum)
      {
      return false;
      }

    // var cacheObject = JSON.parse(datum);
    var cacheObject = datum;  // If using local memory, then no need to stringify data.

    if ((cacheObject['expiryDate']) && (!ignoreDataExpiry))
      {
      var nowDate = new Date();
      var expiryDate = new Date(cacheObject['expiryDate']);
      var dateDifference = (expiryDate - nowDate);

      if (dateDifference < 0)
        {
        return false;
        }
      else
        {
        return cacheObject['data'];
        }
      }
    else
      {
      return cacheObject['data'];
      }
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    return false;
    }

  return false;
  }

function setCacheItem(cacheKey, cacheItem, staleDataThresholdInMinutes)
  {
  try
    {
    if (!staleDataThresholdInMinutes)
      {
      staleDataThresholdInMinutes = (24 * 60);
      }

    var thisExpiryDate = new Date();
    thisExpiryDate.setMinutes(thisExpiryDate.getMinutes() + staleDataThresholdInMinutes);
    //    thisExpiryDate.addMinutes(staleDataThresholdInMinutes);

    var cacheObject = {updateTime:(new Date()), expiryDate:thisExpiryDate, data:cacheItem};

    //localStorage.setItem(cacheKey, JSON.stringify(cacheObject));
    document.fauxLocalStorage[cacheKey] = cacheObject;  // If using local memory, then no need to stringify data.

    return true;
    }
  catch (e)
    {
    //    if (e == QUOTA_EXCEEDED_ERR)
    //      {
    //      }

    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    return false;
    }

  return false;
  }

function getMemoryCacheItem(cacheKey, ignoreDataExpiry)
  {
  try
    {
    if (!ignoreDataExpiry)
      {
      ignoreDataExpiry = false;
      }

    var datum;

    // replicate local storage with memory.
    try
      {
      datum = document.fauxLocalStorage[cacheKey];
      }
    catch (e)
      {
      console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
      datum = false;
      }

    if (!datum)
      {
      return false;
      }

    var cacheObject = datum;  // If using local memory, then no need to stringify data.

    if ((cacheObject['expiryDate']) && (!ignoreDataExpiry))
      {
      var nowDate = new Date();
      var expiryDate = new Date(cacheObject['expiryDate']);
      var dateDifference = (expiryDate - nowDate);

      if (dateDifference < 0)
        {
        return false;
        }
      else
        {
        return cacheObject['data'];
        }
      }
    else
      {
      return cacheObject['data'];
      }
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    return false;
    }

  return false;
  }

function setMemoryCacheItem(cacheKey, cacheItem, staleDataThresholdInMinutes)
  {
  try
    {
    if (!staleDataThresholdInMinutes)
      {
      staleDataThresholdInMinutes = (24 * 60);
      }

    var thisExpiryDate = new Date();
    thisExpiryDate.setMinutes(thisExpiryDate.getMinutes() + staleDataThresholdInMinutes);
    //    thisExpiryDate.addMinutes(staleDataThresholdInMinutes);

    var cacheObject = {updateTime:(new Date()), expiryDate:thisExpiryDate, data:cacheItem};

    document.fauxLocalStorage[cacheKey] = cacheObject;  // If using local memory, then no need to stringify data.

    return true;
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    return false;
    }

  return false;
  }

function getUniqueCounter()
  {
  return window.docUniqueCounter++;
  }

function setHtmlFromFile(targetObject, SourceURL, SourceID, AsyncRequest, getOuterHTML, customCallback, customCallbackParameter)
  {

  //e.g.  setHtmlFromFile(document.getElementById("divCharts1"), "tabPages/tabsEC2.html", "tabPageChart1", function(targetDiv){});
  try
    {
    if (targetObject)
      {
      targetObject["setHtmlFromFileComplete"] = false;

      jQuery.ajax({type:"GET",
        url:SourceURL,
        data:{},
        async:AsyncRequest,
        success:function (html)
          {
          try
            {
            var dHTML = $(html);
            var thisSection;

            thisSection = dHTML.filter("#" + SourceID + ":first");
            if (thisSection.length == 0)
              {
              thisSection = dHTML.find("#" + SourceID + ":first");
              }

            if (thisSection.length > 0)
              {
              if (getOuterHTML)
                {
                $(targetObject).html(thisSection[0].outerHTML);
                }
              else
                {
                $(targetObject).html(thisSection[0].innerHTML);
                }

              targetObject["setHtmlFromFileComplete"] = true;

              if (customCallback) customCallback(targetObject, customCallbackParameter);
              }

            }
          catch (e)
            {
            console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
            }
          }
      });
      }
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }

  }

//
/**
 * @preserve IntegraXor Web SCADA - JavaScript Number Formatter
 * http://www.integraxor.com/
 * author: KPL, KHL
 * (c)2011 ecava
 * Dual licensed under the MIT or GPL Version 2 licenses.
 *
 * http://code.google.com/p/javascript-number-formatter/source/browse/format.js#
 * http://www.integraxor.com/developer/js/number/format-sample.htm
 */

  ////////////////////////////////////////////////////////////////////////////////
  // param: Mask & Value
  ////////////////////////////////////////////////////////////////////////////////

window['format'] = function (m, v)
  {
  if (!m || isNaN(+v))
    {
    return v; //return as it is.
    }
  //convert any string to number according to formation sign.
  var v = m.charAt(0) == '-' ? -v : +v;
  var isNegative = v < 0 ? v = -v : 0; //process only abs(), and turn on flag.

  //search for separator for grp & decimal, anything not digit, not +/- sign, not #.
  var result = m.match(/[^\d\-\+#]/g);
  var Decimal = (result && result[result.length - 1]) || '.'; //treat the right most symbol as decimal
  var Group = (result && result[1] && result[0]) || ',';  //treat the left most symbol as group separator

  //split the decimal for the format string if any.
  var m = m.split(Decimal);
  //Fix the decimal first, toFixed will auto fill trailing zero.
  v = v.toFixed(m[1] && m[1].length);
  v = +(v) + ''; //convert number to string to trim off *all* trailing decimal zero(es)

  //fill back any trailing zero according to format
  var pos_trail_zero = m[1] && m[1].lastIndexOf('0'); //look for last zero in format
  var part = v.split('.');
  //integer will get !part[1]
  if (!part[1] || part[1] && part[1].length <= pos_trail_zero)
    {
    v = (+v).toFixed(pos_trail_zero + 1);
    }
  var szSep = m[0].split(Group); //look for separator
  m[0] = szSep.join(''); //join back without separator for counting the pos of any leading 0.

  var pos_lead_zero = m[0] && m[0].indexOf('0');
  if (pos_lead_zero > -1)
    {
    while (part[0].length < (m[0].length - pos_lead_zero))
      {
      part[0] = '0' + part[0];
      }
    }
  else if (+part[0] == 0)
    {
    part[0] = '';
    }

  v = v.split('.');
  v[0] = part[0];

  //process the first group separator from decimal (.) only, the rest ignore.
  //get the length of the last slice of split result.
  var pos_separator = ( szSep[1] && szSep[ szSep.length - 1].length);
  if (pos_separator)
    {
    var integer = v[0];
    var str = '';
    var offset = integer.length % pos_separator;
    for (var i = 0, l = integer.length; i < l; i++)
      {

      str += integer.charAt(i); //ie6 only support charAt for sz.
      //-pos_separator so that won't trail separator on full length
      if (!((i - offset + 1) % pos_separator) && i < l - pos_separator)
        {
        str += Group;
        }
      }
    v[0] = str;
    }

  v[1] = (m[1] && v[1]) ? Decimal + v[1] : "";
  return (isNegative ? '-' : '') + v[0] + v[1]; //put back any negation and combine integer and fraction.
  };


// IE <= 8 fix for Array.prototype.indexOf function

if (!Array.prototype.indexOf)
  {
  Array.prototype.indexOf = function (needle)
    {
    for (var i = 0; i < this.length; i++)
      {
      if (this[i] === needle)
        {
        return i;
        }
      }
    return -1;
    };
  }

// Commas to Number.

Number.prototype.addCommas = function ()
  {
  var isNegative = (this < 0 ? 1 : 0);
  var intPart = Math.round(Math.abs(this)).toString();
  var decimalPart = (Math.abs(this) - Math.round(Math.abs(this))).toString();
  // Remove the "0." if it exists
  if (decimalPart.length > 2)
    {
    decimalPart = decimalPart.substring(2);
    }
  else
    {
    // Otherwise remove it altogether
    decimalPart = '';
    }
  // Work through the digits three at a time
  var i = intPart.length - 3;
  while (i > 0)
    {
    intPart = intPart.substring(0, i) + ',' + intPart.substring(i);
    i = i - 3;
    }
  return (isNegative ? '-' : '') + intPart + decimalPart;
  };

String.prototype.startsWith = function (str)
  {
  return (this.match("^" + str) == str);
  }

String.prototype.endsWith = function (str)
  {
  return (this.match(str + "$") == str);
  }

String.prototype.isNumber = function ()
  {
  return !isNaN(parseFloat(this)) && isFinite(this);
  }

//
function getCcyFromSophisCode(SophisCode)
  {
  if (isNaN(parseFloat(SophisCode)))
    {
     // Copes with Venice currency codes. e.g. 'EUR'
     return SophisCode;
    }

  var rVal = '';
  var SCode = parseInt(SophisCode);

  try
    {
    rVal = String.fromCharCode(Math.floor(SCode / (256 * 256)) % 256, Math.floor(SCode / 256) % 256, SCode % 256);
    }
  catch (e)
    {
    console.log(e.message);
    }

  return rVal;
  }
// 

function getDirPath(URL)
  {
  // Returns Path including trailing '/'

  if (URL == null) URL = document.URL;

  return unescape(URL.substring(0, (URL.lastIndexOf("/")) + 1));
  }

function nz(pValue, pDefault)
  {
  if ((pValue == undefined) || (pValue == null))
    {
    if ((pDefault == undefined) || (pDefault == null))
      {
      return "";
      }
    else
      {
      return pDefault;
      }
    }

  return pValue;
  }

//
// http://jacwright.com/projects/javascript/date_format/
//
// Simulates PHP's date function
//

Date.prototype.format = function (format)
  {
  var returnStr = '';
  var replace = Date.replaceChars;
  for (var i = 0; i < format.length; i++)
    {
    var curChar = format.charAt(i);
    if (i - 1 >= 0 && format.charAt(i - 1) == "\\")
      {
      returnStr += curChar;
      }
    else if (replace[curChar])
      {
      returnStr += replace[curChar].call(this);
      }
    else if (curChar != "\\")
      {
      returnStr += curChar;
      }
    }
  return returnStr;
  };

Date.replaceChars = {
  shortMonths:['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
  longMonths:['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
  shortDays:['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
  longDays:['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],

  // Day
  d:function ()
    {
    return (this.getDate() < 10 ? '0' : '') + this.getDate();
    },
  D:function ()
    {
    return Date.replaceChars.shortDays[this.getDay()];
    },
  j:function ()
    {
    return this.getDate();
    },
  l:function ()
    {
    return Date.replaceChars.longDays[this.getDay()];
    },
  N:function ()
    {
    return this.getDay() + 1;
    },
  S:function ()
    {
    return (this.getDate() % 10 == 1 && this.getDate() != 11 ? 'st' : (this.getDate() % 10 == 2 && this.getDate() != 12 ? 'nd' : (this.getDate() % 10 == 3 && this.getDate() != 13 ? 'rd' : 'th')));
    },
  w:function ()
    {
    return this.getDay();
    },
  z:function ()
    {
    var d = new Date(this.getFullYear(), 0, 1);
    return Math.ceil((this - d) / 86400000);
    }, // Fixed now
  // Week
  W:function ()
    {
    var d = new Date(this.getFullYear(), 0, 1);
    return Math.ceil((((this - d) / 86400000) + d.getDay() + 1) / 7);
    }, // Fixed now
  // Month
  F:function ()
    {
    return Date.replaceChars.longMonths[this.getMonth()];
    },
  m:function ()
    {
    return (this.getMonth() < 9 ? '0' : '') + (this.getMonth() + 1);
    },
  M:function ()
    {
    return Date.replaceChars.shortMonths[this.getMonth()];
    },
  n:function ()
    {
    return this.getMonth() + 1;
    },
  t:function ()
    {
    var d = new Date();
    return new Date(d.getFullYear(), d.getMonth(), 0).getDate()
    }, // Fixed now, gets #days of date
  // Year
  L:function ()
    {
    var year = this.getFullYear();
    return (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0));
    }, // Fixed now
  o:function ()
    {
    var d = new Date(this.valueOf());
    d.setDate(d.getDate() - ((this.getDay() + 6) % 7) + 3);
    return d.getFullYear();
    }, //Fixed now
  Y:function ()
    {
    return this.getFullYear();
    },
  y:function ()
    {
    return ('' + this.getFullYear()).substr(2);
    },
  // Time
  a:function ()
    {
    return this.getHours() < 12 ? 'am' : 'pm';
    },
  A:function ()
    {
    return this.getHours() < 12 ? 'AM' : 'PM';
    },
  B:function ()
    {
    return Math.floor((((this.getUTCHours() + 1) % 24) + this.getUTCMinutes() / 60 + this.getUTCSeconds() / 3600) * 1000 / 24);
    }, // Fixed now
  g:function ()
    {
    return this.getHours() % 12 || 12;
    },
  G:function ()
    {
    return this.getHours();
    },
  h:function ()
    {
    return ((this.getHours() % 12 || 12) < 10 ? '0' : '') + (this.getHours() % 12 || 12);
    },
  H:function ()
    {
    return (this.getHours() < 10 ? '0' : '') + this.getHours();
    },
  i:function ()
    {
    return (this.getMinutes() < 10 ? '0' : '') + this.getMinutes();
    },
  s:function ()
    {
    return (this.getSeconds() < 10 ? '0' : '') + this.getSeconds();
    },
  u:function ()
    {
    var m = this.getMilliseconds();
    return (m < 10 ? '00' : (m < 100 ?
      '0' : '')) + m;
    },
  // Timezone
  e:function ()
    {
    return "Not Yet Supported";
    },
  I:function ()
    {
    return "Not Yet Supported";
    },
  O:function ()
    {
    return (-this.getTimezoneOffset() < 0 ? '-' : '+') + (Math.abs(this.getTimezoneOffset() / 60) < 10 ? '0' : '') + (Math.abs(this.getTimezoneOffset() / 60)) + '00';
    },
  P:function ()
    {
    return (-this.getTimezoneOffset() < 0 ? '-' : '+') + (Math.abs(this.getTimezoneOffset() / 60) < 10 ? '0' : '') + (Math.abs(this.getTimezoneOffset() / 60)) + ':00';
    }, // Fixed now
  T:function ()
    {
    var m = this.getMonth();
    this.setMonth(0);
    var result = this.toTimeString().replace(/^.+ \(?([^\)]+)\)?$/, '$1');
    this.setMonth(m);
    return result;
    },
  Z:function ()
    {
    return -this.getTimezoneOffset() * 60;
    },
  // Full Date/Time
  c:function ()
    {
    return this.format("Y-m-d\\TH:i:sP");
    }, // Fixed now
  r:function ()
    {
    return this.toString();
    },
  U:function ()
    {
    return this.getTime() / 1000;
    }
};

Number.prototype.mod = function (n)
  {
  return ((this % n) + n) % n;
  }

Date.prototype.addBusDays = function (dd)
  {
  var wks = Math.floor(dd / 5);
  var dys = dd.mod(5);
  var dy = this.getDay();
  if (dy === 6 && dys > -1)
    {
    if (dys === 0)
      {
      dys -= 2;
      dy += 2;
      }
    dys++;
    dy -= 6;
    }
  if (dy === 0 && dys < 1)
    {
    if (dys === 0)
      {
      dys += 2;
      dy -= 2;
      }
    dys--;
    dy += 6;
    }
  if (dy + dys > 5) dys += 2;
  if (dy + dys < 1) dys -= 2;
  this.setDate(this.getDate() + wks * 7 + dys);
  }

function BusinessDaysBetweenDates(startDate, endDate)
  {
  // Acknowledge : http://partialclass.blogspot.fr/2011/07/calculating-working-days-between-two.html

  // Validate input
  if (endDate < startDate)
    return 0;

  // Calculate days between dates
  var millisecondsPerDay = 86400 * 1000; // Day in milliseconds
  startDate.setHours(0, 0, 0, 1);  // Start just after midnight
  endDate.setHours(23, 59, 59, 999);  // End just before midnight
  var diff = endDate - startDate;  // Milliseconds between datetime objects
  var days = Math.ceil(diff / millisecondsPerDay);

  // Subtract two weekend days for every week in between
  var weeks = Math.floor(days / 7);
  var days = days - (weeks * 2);

  // Handle special cases
  var startDay = startDate.getDay();
  var endDay = endDate.getDay();

  // Remove weekend not previously removed.
  if (startDay - endDay > 1)
    days = days - 2;

  // Remove start day if span starts on Sunday but ends before Saturday
  if (startDay == 0 && endDay != 6)
    days = days - 1

  // Remove end day if span ends on Saturday but starts after Sunday
  if (endDay == 6 && startDay != 0)
    days = days - 1

  return days;
  }


function getQueryVariable(variable)
{
var query = window.location.search.substring(1);
var vars = query.split("&");
for (var i=0;i<vars.length;i++) {
var pair = vars[i].split("=");
if(pair[0] == variable){return pair[1];}
}
return(false);
}

function validEmail(emailAddress)
{
var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
if (reg.test(emailAddress) == false)
  {
  return false;
  }
else
  {
  return true;
  }
}

function QueryStringToJSON(qString) {            
    var pairs = qString.split('&');
    
    var result = {};
    pairs.forEach(function(pair) {
        pair = pair.split('=');
        result[pair[0]] = decodeURIComponent(pair[1] || '');
    });

    return JSON.parse(JSON.stringify(result));
}

function showResetPassword(resetToken) {

	try {

		var thisDialogType = document.dialogArray.dialogType.info;
		var dialogOptions = {
			title : "Réinitialiser mot de passe",
			modal : true,
			closeOnEscape : false,
			captionButtons : {
				refresh : {
					visible : false
				},
				print : {
					visible : false
				},
				maximize : {
					visible : false
				},
				minimize : {
					visible : false
				}
			},
			height : 'auto',
			width : 'auto',
			resizable : false,
			position : 'center'
		};

		var dialogObject = getDialog(thisDialogType, dialogOptions);
		var dialogID = dialogObject.ID;
		var dialogDiv = $('#' + dialogID);

		// Initialise Window.

		setHtmlFromFile(dialogDiv[0], 'html/changePassword.html', 'loginDiv', false, false, false, dialogID);

		// Show Dialog.
		dialogDiv.dialogExtend("open");

		document.UserName = '';

		$(".uibutton").button();
		$('.hideable').hide();
		dialogDiv.find(".Email").val($("#resetEmail").html());
		dialogDiv.find(".Email").attr('disabled', true);
		dialogDiv.find(".Email").hide();

		$(".submitbutton").click(function() {
			var password1 = $(".PasswordNew1").val();
			var password2 = $(".PasswordNew2").val();
			var token = $("#resetToken").html();

			if (password1 != password2) {
				$(".errordiv").hide();
				$(".differentpasswords").show();
				return;
			}

			if (password1.length < 6) {
				$(".errordiv").hide();
				$(".passwordlength").show();
				return;
			}

			var formvar = {
				'password' : password1,
				'token' : token
			};

			var url = "php/forgot_password_reset.php";

			jQuery.ajax({
				type : "POST",
				url : url,
				data : formvar,
				dataType: "json",
				async : false,
				success : function(json) {

					if (json.Status=='OK')
					{
						alert('Votre mot de passe a été réinitialisé');
					}
					else
					{
						alert(json.Message);
					}

					window.location = "index.html";

				}
			});

		});

		$(".errordiv").click(function() {
			$(this).hide();
		});

	} catch (e) {
		console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber+ ')');
	}
	
}
