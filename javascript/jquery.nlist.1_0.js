/*
 * jQuery nList 1.0
 *
 * Copyright (c) 2012 Nicholas Pennington.
 *
 * Licensed under MIT
 *   http:// www.opensource.org/licenses/mit-license.php
 *
 * Depends:
 *   jQuery 1.7.2
 *   jQuery UI Dialog 1.8.22
 *
 */
(function ($)
  {
  $.addList = function (targetDiv, params)
    {
    if (targetDiv.list)
      {
      $(targetDiv).empty();
      delete targetDiv.list;
      delete targetDiv.options;

      return $(targetDiv).nList(params);

      } //return if already exist

    params = $.extend(
      {
        selectionMode:'single', // 'multiple' , 'none'
        selected:false,
        oddClass:'',
        evenClass:'',
        listHoverClass:"ui-state-hover",
        listSelectedClass:"ui-state-active"
      },
      params);

    $(targetDiv)
      .addClass("ui-widget jquery-nlist") /* Removed 'ui-widget-content ui-corner-all' NPP 24 May 2013 */
      .show();

    var list =
    {

      setItems:function (data)
        {
        if (!data)
          {
          return false;
          }

        if (data.length == 0) // No Data Rows.
          {
          $('ul, li', targetDiv).unbind();
          $(targetDiv).empty();

          return false;
          }

        var ul = $(list.ul);
        list.data = data;

        $.each(data, function (thisIndex, listItem)
        {
        var li = document.createElement('li');
        $(li)
          .addClass("jquery-nlist-item ui-corner-all " + ((thisIndex % 2) ? params.oddClass : params.evenClass))
          .attr({index:thisIndex})
          .html(listItem['label']);

        ul.append(li);
        }); // end each

        $('li', targetDiv)
          .mouseover(function(){ $(this).addClass(params.listHoverClass); })
          .mouseout(function(){ $(this).removeClass(params.listHoverClass); })
          .click(function(e)
          {
          e.preventDefault();

          if (DEBUG_FLAG) console.log("nList li-Click(), index = " + $(this).attr("index"));

          if (params.selectionMode != 'multiple') $("li", list.ul).removeClass("listSelectedClass");
          if (params.selectionMode != 'none') $(this).addClass("listSelectedClass");
          if ($.isFunction(params.selected)) params.selected(e, {item:list.data[$(this).attr("index")], element:this});
          $(this).parent().focus();
          })
        }

    }; // list = ....

    // Init list

    list.ul = document.createElement('ul');

    $(list.ul).addClass("jquery-nlist-ul");

    $(targetDiv).append(list.ul);

    //

    targetDiv.list = list;
    targetDiv.params = params;

    if (params.jsArray)
      {
      list.setItems(params.jsArray);
      }

    return targetDiv;
    }

  $.fn.nList = function (p)
    {
    return this.each(function ()
    {
    $.addList(this, p);
    });
    }

  $.fn.listExist = function ()
    {
    if (this.length > 0)
      {
      if (this[0].list)
        {
        return true;
        }
      }
    return false;
    }

  $.fn.listSetItems = function (data)
    { // function to add data to list
    return this.each(function ()
    {
    if (this.list) this.list.setItems(data);
    });
    };  // end listSetItems


  })(jQuery);
