/**
 * User: npennin
 * Date: 03/05/12
 * Time: 11:29
 *
 */

function dialogTransactionsOpen(e)
  {
  // Resize Dialog on Open. Ensures everything is the correct size.

  var data = {};

  if (!data.size) data.size = {height: e.target.clientHeight, width: e.target.clientWidth};
  if (!data.originalSize) data.originalSize = {height: 0, width: 0};

  dialogTransactionsResizeStop(e, data);

  }

function dialogTransactionsStateChange(e, data)
  {
  // Call resize code when the dialog is maximised or restored.

  switch (data.state)
  {

    case 'maximized':
    case 'normal':

      if (!data.size) data.size = {height: e.target.clientHeight, width: e.target.clientWidth};
      if (!data.originalSize) data.originalSize = {height: 0, width: 0};

      dialogTransactionsResizeStop(e, data);

      break;

  }

  }

function dialogTransactionsResizeStop(e, data)
  {

  // Exit if dialog has not resised.

  if ((data.size.height < 100) || ((data.originalSize.height == data.size.height) && (data.originalSize.width == data.size.width)))
    {
    return;
    }

  // Resize Splitter and refresh. // clientHeight

  var initialClientHeight = e.target.clientHeight;
  var initialClientWidth = e.target.clientWidth;
  var dialogID = e.target.id;
  var grid = $("#" + dialogID + '_grid');

  grid.flexSize({width: (e.target.clientWidth), height: (e.target.clientHeight)});

  // Do it again if scroll bars screwed it up..

  if ((initialClientHeight != e.target.clientHeight) || (initialClientWidth != e.target.clientWidth))
    {
    grid.flexSize({width: (e.target.clientWidth), height: (e.target.clientHeight)});
    }

  }

/* Middle Office Transactions callback functions */

function dialogMiddleOfficeTransactionsStateChange(e, data)
  {
  // Call resize code when the dialog is maximised or restored.
  var dialogID = e.target.id;

  switch (data.state)
  {

    case 'maximized':
      document.dialogMaximised[dialogID] = true;

      if (!data.size) data.size = {height: e.target.clientHeight, width: e.target.clientWidth};
      if (!data.originalSize) data.originalSize = {height: 0, width: 0};

      dialogMiddleOfficeTransactionsResizeStop(e, data);

      break;

    case 'normal':
      document.dialogMaximised[dialogID] = false;

      if (!data.size) data.size = {height: e.target.clientHeight, width: e.target.clientWidth};
      if (!data.originalSize) data.originalSize = {height: 0, width: 0};

      dialogMiddleOfficeTransactionsResizeStop(e, data);

      break;
  }
  }

function dialogMiddleOfficeTransactionsOpen(e)
  {
  // Resize Dialog on Open. Ensures everything is the correct size.

  var data = {};

  if (!data.size) data.size = {height: e.target.clientHeight, width: e.target.clientWidth};
  if (!data.originalSize) data.originalSize = {height: 0, width: 0};

  dialogMiddleOfficeTransactionsResizeStop(e, data);

  }

function dialogMiddleOfficeTransactionsClose(e)
  {
  // Tidy up the data cache on dialog close.

  try
    {
    var dialogID = e.target.id;
    var cacheKey = MO_TRANSACTION_CACHE_PREFIX + dialogID;

    if (cacheKey in document.dataCache)
      {
      delete document.dataCache[cacheKey];
      }
    }
  catch (e)
    {
    }
  }

function dialogMiddleOfficeTransactionsResizeStop(e, data)
  {
  // Exit if dialog has not resised.

  if ((data.size.height < 100) || ((data.originalSize.height == data.size.height) && (data.originalSize.width == data.size.width)))
    {
    return;
    }

  var dialogDiv;
  if ($(e.target).hasClass('dialogDiv'))
    {
    dialogDiv = $(e.target);
    }
  else
    {
    dialogDiv = $(e.target).parents('.dialogDiv');
    }
  var dialogID = dialogDiv.attr('id');

  // Resize Splitter and refresh. // clientHeight

  var initialClientHeight = e.target.clientHeight;
  var initialClientWidth = e.target.clientWidth;

  // Resize Grid, if it exists. Fit to the right splitter panel.

  if ($("#" + dialogID + ' .moTransactionsFilter').height() == null)
    {
    return;
    }
  else
    {
    splitterMiddleOfficeTransactionsSized(dialogID, initialClientHeight, initialClientWidth);

    if ((initialClientHeight != e.target.clientHeight) || initialClientWidth != e.target.clientWidth)
      {
      splitterMiddleOfficeTransactionsSized(dialogID, e.target.clientHeight, e.target.clientWidth);
      }
    }

  }

function splitterMiddleOfficeTransactionsSized(dialogID, initialClientHeight, initialClientWidth)
  {
  // Resize the Grid control if the splitter div is moved.

  try
    {

    var splitterLeft = $("#" + dialogID + '_splitterLeft');
    var moGrid = $("#" + dialogID + '_grid');
    var grid2 = $("#" + dialogID + '_grid2');
    var topGap;
    var grid2Height = (document.dialogArray[dialogID]['GRID2_HEIGHT'] ? document.dialogArray[dialogID]['GRID2_HEIGHT'] : 100);

    topGap = $("#" + dialogID + ' .moTransactionsFilter').height();

    if (topGap == null)
      {
      return;
      }

    if (grid2.length > 0)
      {
      topGap += grid2Height;
      }

    // topGap = Math.max(0, (gridOffset.top - headingOffset.top) - 45);

    if ((splitterLeft[0]) && (moGrid[0]))
      {
      moGrid.flexSize({width: (initialClientWidth), height: (initialClientHeight - topGap)});

      }


    if ((splitterLeft[0]) && (grid2[0]))
      {
      if (grid2.flexExist())
        {
        grid2.flexSize({width: (initialClientWidth), height: (grid2Height)});
        }
      }


    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }
  }

function resizeMaximisedDialogs()
  {
  /* Function to resize maximised dialogs. Used when the browser window is resized. */
  var dialogIndex;

  try
    {
    for (dialogIndex in document.dialogArray)
      {
      var thisDialogObject = document.dialogArray[dialogIndex];
      var thisDialog;

      try
        {
        if (thisDialogObject.inUse)
          {
          dialogID = thisDialogObject.ID;
          thisDialog = $('#' + dialogID);

          if (thisDialog.dialogExtend("state") == 'maximized')
            {
            thisDialog.dialogExtend("maximize");
            }
          }
        }
      catch (e)
        {
        console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
        }
      }
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }
  }

/*  */

function getDialog(thisDialogType, dialogOptions, dialogHeight, dialogWidth, dialogTop, dialogLeft)
  {
  // Returns an initialised dialogExtend box.
  // dialog cache aware, it will clear the contents of an existing dialog before returning it.

  if (!getDialog['top'])
    {
    getDialog.top = 80;
    }
  if (!getDialog['left'])
    {
    getDialog.left = 50;
    }

  var dialogID = (-1);
  var dialogDiv = undefined;
  var defaultDialogHeight = (dialogHeight ? dialogHeight : 450);
  var defaultDialogWidth = (dialogWidth ? dialogWidth : 800);
  var defaultDialogProperties = {autoOpen: false, height: defaultDialogHeight, width: defaultDialogWidth, stack: true, zIndex: 1};

  // Handle dialogClose event handler. Ensure that dialogCloseCache() is always called.
  // Adjust resize hendlers as necessary.

  if (!dialogOptions)
    {
    dialogOptions = {};
    }

  if (dialogOptions.close)
    {
    // Prepend dialogCloseCache to existing close event handler.
    var orgClose = dialogOptions.close;
    jQuery.extend(dialogOptions, {close: function (e)
      {
      dialogCloseCache(e);
      orgClose(e);
      }});
    }
  else
    {
    // Set default close event handler.
    jQuery.extend(dialogOptions, {close: dialogCloseCache});
    }

  if (dialogOptions.dragStart)
    {
    // Prepend dialogCloseCache to existing close event handler.
    var orgDragStart = dialogOptions.dragStart;
    jQuery.extend(dialogOptions, {dragStart: function (e)
      {
      dialogDragStart(e);
      orgDragStart(e);
      }});
    }
  else
    {
    // Set default close event handler.
    jQuery.extend(dialogOptions, {dragStart: dialogDragStart});
    }

  if (dialogOptions.dragStop)
    {
    // Prepend dialogCloseCache to existing close event handler.
    var orgDragStop = dialogOptions.dragStop;
    jQuery.extend(dialogOptions, {dragStop: function (e)
      {
      dialogDragStop(e);
      orgDragStop(e);
      }});
    }
  else
    {
    // Set default close event handler.
    jQuery.extend(dialogOptions, {dragStop: dialogDragStop});
    }

  if (dialogOptions.resizeStart)
    {
    // Prepend dialogResizeStart to existing resizeStart event handler.
    var orgResizeStart = dialogOptions.resizeStart;
    jQuery.extend(dialogOptions, {resizeStart: function (e, data)
      {
      dialogResizeStart(e, data);
      orgResizeStart(e, data);
      }});
    }
  else
    {
    // Set default close event handler.
    jQuery.extend(dialogOptions, {resizeStart: dialogResizeStart});
    }

  if (dialogOptions.resizeStop)
    {
    // Prepend dialogResizeStop to existing resizeStop event handler.
    var orgResizeStop = dialogOptions.resizeStop;
    jQuery.extend(dialogOptions, {resizeStop: function (e, data)
      {
      dialogResizeStop(e, data);
      orgResizeStop(e, data);
      }});
    }
  else
    {
    // Set default close event handler.
    jQuery.extend(dialogOptions, {resizeStop: dialogResizeStop});
    }

  // Check for unused dialog box in the cache

  for (thisDialog in document.dialogArray)
    {
    if (document.dialogArray[thisDialog])
      {
      if ((document.dialogArray[thisDialog].inUse == false) && (document.dialogArray[thisDialog].dialogType == thisDialogType))
        {
        dialogID = document.dialogArray[thisDialog].ID;

        break;
        }
      }
    }

  if (dialogID < 0)
    {
    // New Dialog box :

    // Set default position for a 'New dialog box

    if (!dialogOptions.position)
      {
      jQuery.extend(dialogOptions, {position: [getDialog.left, getDialog.top]});
      getDialog.left += 30;
      getDialog.top += 40

      if (getDialog.top > 400)
        {
        getDialog.top = 80;
        }
      if (getDialog.left > 600)
        {
        getDialog.left = 50;
        }
      }

    // Allocate new Dialog ID

    document.dialogArrayCurrentID = document.dialogArrayCurrentID + 1;
    dialogID = "fsDialog" + document.dialogArrayCurrentID;
    document.dialogArray[dialogID] = {ID: (dialogID), inUse: true, data: {}, dialogType: thisDialogType, dialogOptions: dialogOptions};

    // Build <div> for Dialog

    dialogDiv = $('<div id="' + dialogID + '" class="dialogDiv"></div>');
    $('#bodyDiv').append(dialogDiv);

    var combinedOptions = jQuery.extend(defaultDialogProperties, dialogOptions);
    dialogDiv.dialogExtend(combinedOptions);

    $('#' + dialogID).click(dialogClickCache);
    }
  else
    {
    // Re-use dialog box

    document.dialogArray[dialogID].inUse = true;
    document.dialogArray[dialogID].dialogOptions = dialogOptions;

    dialogDiv = $('#' + dialogID);

    // Reset Dialog Size

    $(dialogDiv).dialogExtend("option", jQuery.extend(defaultDialogProperties, dialogOptions));

    if (thisDialogType != document.dialogArray.dialogType.iframe)
      {
      $(dialogDiv).empty();
      }

    }

  return document.dialogArray[dialogID];
  }

function dialogDragStart(e)
  {
  var thisDialog = e.target;
  var thisDialogID = thisDialog.id;
  try
    {
    if ((document.dialogArray[thisDialogID]) && (true))
      {
      switch (document.dialogArray[thisDialogID].dialogType)
      {
        // These charts don't seem to like being reused.
        case document.dialogArray.dialogType.iframeDisposable:
          //case document.dialogArray.dialogType.iframe:

          try
            {
            $('#' + thisDialog.id + ' iframe').hide();
            }
          catch (e)
            {
            }
          break;
      }
      }
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }
  }

function dialogDragStop(e)
  {
  var thisDialog = e.target;
  var thisDialogID = thisDialog.id;
  try
    {
    if ((document.dialogArray[thisDialogID]) && (true))
      {
      switch (document.dialogArray[thisDialogID].dialogType)
      {
        // These charts don't seem to like being reused.
        case document.dialogArray.dialogType.iframeDisposable:
          //case document.dialogArray.dialogType.iframe:

          try
            {
            $('#' + thisDialog.id + ' iframe').show();
            }
          catch (e)
            {
            }
          break;
      }
      }
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }
  }

function dialogCloseCache(e)
  {
  // Change dialog cache state when dialog is 'closed'.
  //

  try
    {
    if (!e)
      {
      return;
      }

    var thisDialog = e.target;
    var thisDialogID = thisDialog.id;

    try
      {
      if (document.dialogArray[thisDialogID])
        {
        switch (document.dialogArray[thisDialogID].dialogType)
        {
          // These charts don't seem to like being reused.
          case document.dialogArray.dialogType.iframeDisposable:
          case document.dialogArray.dialogType.iframe:

            try
              {
              $('#' + thisDialog.id).dialogExtend("destroy").remove();
              delete document.dialogArray[thisDialogID];
              }
            catch (e)
              {
              }
            break;

          default:
            document.dialogArray[thisDialogID].inUse = false;
            document.dialogArray[thisDialogID].data = {};

            break;
        }
        }
      }
    catch (e)
      {
      console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
      }

    try
      {
      if (document['dataCache'])
        {
        var cachekey = MO_TRANSACTION_CACHE_PREFIX + thisDialogID;

        if (document.dataCache[cachekey])
          {
          delete document.dataCache[cachekey];
          }
        }

      }
    catch (e)
      {
      console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
      }


    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }

  }

function dialogClickCache(e)
  {
  // Find the enclosing .dialogDiv and dialogExtend('moveToTop');

  var dialogID = e.target.id;

  if (e.target.className.indexOf("dialogDiv") < 0)
    {
    // Parent
    dialogID = $(e.target).parents('.dialogDiv')[0].id;
    }

  if (document.clickLastDialogID !== dialogID)
    {
    document.clickLastDialogID = dialogID;
    $("#" + dialogID).dialogExtend("moveToTop");
    }
  }

function dialogResizeStart(e, data)
  {
  // Hide iframes that are mot in the current dialog.
  $('.dialogDiv iframe').not('#' + e.target.id + ' iframe').hide();
  }

function dialogResizeStop(e, data)
  {
  $('.dialogDiv iframe').show();
  }

function MenuSelect(e, data)
  {
  // Cope with Menu selection.

  var menuId = $(data).parent("li")[0].id;

  //

  var dialogID = (-1);
  var dialogDiv = undefined;
  var thisDialogType = undefined;
  var dialogOptions = undefined;
  var dialogObject = undefined;

  switch (menuId)
  {

    case  'menuStatus':

      showSystemStatus();

      break;

    case  'menuFundsBrowser':

      showFundBrowser();

      break;

    case  'menuMoTransactions':

      showMoTransactions();

      break;

    case 'menuMoSettlement':

      showMoSettlingCash();

      break;

    case 'menuMilestones':

      showMilestones();

      break;

    case  'menuFundsSearch':


      break;

    case  'economicCalendar':

      getEconomicCalendar();

      break;

    case 'alexCartoon':

      getAlexCartoon();

      break;

    case 'users':

      showUsers();

      break;

    case 'adduser':

      showUser();

      break;

    default:

      break;

  }

  RefreshTokenExpiry(document.dakotaUserID, document.dakotaSessionKey);

  }

/* Generalised utility Code. */

function moveOrSizeGridColumnEvent(e, args, gridID, defaultColumsObject)
  {
  // Generalised code to cope with Moving and Sizing of grid columns, given the generalised approach in this project.

  // TODO Does not really work correctly for flexGrid columns. They are not returned in the display order.

  try
    {
    // var thisCol = args.column;
    var realGridCols = $("#" + gridID)[0].p.colModel;
    var newColArray = new Array();

    if ('drag' in args)
    // Drag Event
      {
      for (newColIndex in realGridCols)
        {
        for (oldColIndex in defaultColumsObject)
          {
          if (defaultColumsObject[oldColIndex]['dataKey'] == realGridCols[newColIndex]['dataKey'])
            {
            newColArray.push(defaultColumsObject[oldColIndex]);
            defaultColumsObject[oldColIndex] = {dataKey: '_null_'}; // Wipe old line in case of Cols with duplicate DataKeys.
            break;
            }
          }
        }

      // Copy items to the original array in order.
      // I think that since I am dealing with a parameter value for the columnsArray, just setting the newColArray to it will not work.
      for (newColIndex in newColArray)
        {
        defaultColumsObject[newColIndex] = newColArray[newColIndex];
        }
      }
    else
    // Size event
      {
      defaultColumsObject[args.column.dataIndex].width = args.column.width;
      }
    }
  catch (e)
    {
    console.log(e.message);
    }
  }


/* Data Utility functions  */

function getRenaissanceTable(pUserID, pToken, pInstance, pParameters, pTableName, pFieldList, pWhere, pKnowledgeDate, pAsync, pForceRefresh)
  {
  try
    {
    var cacheTableKey = pTableName + ':' + pInstance;

    if ((!document.dataCache[cacheTableKey]) || (pForceRefresh) || (document.dataCache[cacheTableKey]['userid'] != pUserID ) || (document.dataCache[cacheTableKey]['fieldList'] != pFieldList ) || (document.dataCache[cacheTableKey]['where'] != pWhere) || (document.dataCache[cacheTableKey]['knowledgeDate'] != pKnowledgeDate ))
      {
      document.dataCache[cacheTableKey] = {data: [], inProgress: true, userid: pUserID, instance: pInstance, tableName: pTableName, fieldList: pFieldList, where: pWhere, knowledgeDate: pKnowledgeDate };

      // Prepare parameters object for data fetch.

      var formJSON = $.extend({}, pParameters, {
        'USERID': pUserID,
        'TOKEN': pToken,
        'TABLE': nz(pTableName, ''),
        'FIELDLIST': nz(pFieldList, ''),
        'WHERE': nz(pWhere, ''),
        'KNOWLEDGEDATE': pKnowledgeDate
      });

      var url = "php/getRenaissanceTable.php";

      $.ajax({type: "POST",
        url: url,
        data: formJSON,
        async: pAsync,
        cacheTableKey: cacheTableKey,
        tableName: pTableName,
        success: function (html)
          {
          var rVal = JSON.parse(html);
          var cacheObject = document.dataCache[this['cacheTableKey']];

          cacheObject['updateTime'] = new Date();
          cacheObject['data'] = rVal;
          cacheObject['inProgress'] = false;

          if (cacheObject['callbacks'])
            {
            if (cacheObject['callbacks'].length > 0)
              {
              for (var thisCallback in cacheObject['callbacks'])
                {
                try
                  {
                  if (thisCallback)
                    {
                    cacheObject['callbacks'][thisCallback].callback(cacheObject, cacheObject['callbacks'][thisCallback].parameters);
                    }
                  }
                catch (e)
                  {
                  console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
                  }
                }

              // Clear callback array.
              cacheObject['callbacks'] = [];
              }
            }

          return cacheObject['data'];
          }
      });

      }
    else
      {
      return document.dataCache[cacheTableKey]['data'];
      }

    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }

  try
    {
    return document.dataCache[cacheTableKey]['data'];
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }

  }

// RefreshTokenExpiry(document.dakotaUserID, document.dakotaSessionKey)
function RefreshTokenExpiry(pUserID, pToken)
  {
  try
    {

    var formJSON = {
      'USERID': pUserID,
      'TOKEN': pToken
    };

    var url = "php/refreshToken.php";

    $.ajax({type: "POST",
      url: url,
      data: formJSON,
      async: true
    });

    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }


  }