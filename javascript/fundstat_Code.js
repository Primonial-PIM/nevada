/*
 * User: npennin Date: 03/05/12 Time: 11:44
 * 
 */

function getFundValuationArray(customCallback, customParameters)
  {
  try
    {
    if (!document.dataCache.fundValuationArray)
      {
      document.dataCache.fundValuationArray = {inProgress:false};
      }
    }
  catch (e)
    {
    document.dataCache.fundValuationArray = {inProgress:false};
    console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
    }

  var cacheObject = document.dataCache.fundValuationArray;

  cacheObject['inProgress'] = true;


  if ((!cacheObject['callbacks']) || (customCallback == undefined))
    {
    cacheObject['callbacks'] = [];
    }

  if (customCallback)
    {
    cacheObject['callbacks'].push({callback:customCallback, parameters:customParameters});
    }

  var url = "php/getFundValuations.php";
  
  var formvar = {
		  'USERID':document.dakotaUserID,
		  'TOKEN':document.dakotaSessionKey,
      'ID':0
  };

  // Get data.
  // Venice Folio (Funds) data query.

  $.ajax(
   {type:"POST",
    url:url,
    data:formvar,
    success:function (html)
      {
      try
        {
        var rVal = JSON.parse(html);
        var listData = new Array();
        cacheObject = document.dataCache.fundValuationArray;

        for (var index in rVal)
          {
          listData[rVal[index]['FOLIO']+":"+rVal[index]['INSTANCE']] = rVal[index];
          }

        cacheObject['updateTime'] = new Date();
        cacheObject['data'] = listData;
        cacheObject['inProgress'] = false;

        if (cacheObject['callbacks'])
          {
          if (cacheObject['callbacks'].length > 0)
            {
            for (var thisCallback in cacheObject['callbacks'])
              {
              try
                {
                cacheObject['callbacks'][thisCallback].callback(cacheObject['callbacks'][thisCallback].parameters);
                }
              catch (e)
                {
                console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
                }
              }

            // Clear callback array.
            cacheObject['callbacks'] = [];
            }
          }
        }
      catch (e)
        {
        document.dataCache.fundValuationArray = false;
        console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
        }
      }
  });

  }

function getFolioListArray(customCallback, customParameters)
  {
  try
    {
    if (!document.dataCache.folioListArray)
      {
      document.dataCache.folioListArray = {inProgress:false};
      }

    if (!document.dataCache.folioAccountsArray)
      {
      document.dataCache.folioAccountsArray = {inProgress:false};
      }
    }
  catch (e)
    {
    document.dataCache.folioListArray = {inProgress:false};
    console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
    }

  var cacheObject = document.dataCache.folioListArray;

  cacheObject['inProgress'] = true;
  document.dataCache.folioAccountsArray['inProgress'] = true;

  if ((!cacheObject['callbacks']) || (customCallback == undefined))
    {
    cacheObject['callbacks'] = [];
    }

  if (customCallback)
    {
    cacheObject['callbacks'].push({callback:customCallback, parameters:customParameters});
    }

  var url = "php/getFolioNames.php";
  
  var formvar = {
		  'USERID':document.dakotaUserID,
		  'TOKEN':document.dakotaSessionKey
  };
  

  // Get data.
  // Venice Folio (Funds) data query.

  $.ajax({type:"POST",
    url:url,
    data:formvar,
    success:function (html)
      {
      try
        {
        /* Enable Menus once the Folio list is loaded */

        //$('#MenuDiv').show();
        //$('#LoadingDiv').hide();

        /* Process List. */

        var rVal = JSON.parse(html);
        var listData = new Array();
        var folioToplevelLookup = new Array();
        var folioNameLookup = new Array();
        var tempArray;

        listData.push({label:'.All Portfolios', value:0});
        folioNameLookup['0'] = 'All portfolios';

        for (var i = 0; i < rVal.length; i++)
          {
          // Capture top-level Folio List array

          // Only Add Fund Name to List Array if it is not 'Closed'
          if (rVal[i]['FundClosed'] == 0)
            {
            listData.push({label:rVal[i]['FundCode'], value:rVal[i]['FUNDID']+":"+rVal[i]['INSTANCE']});
            }

          // Add Fund Name to lookup arrays regardless of 'closed' state.

          folioNameLookup[rVal[i]['FUNDID']+":"+rVal[i]['INSTANCE']] = rVal[i]['FundCode'];

          // Capture folio lookup array

          folioToplevelLookup[rVal[i]['FUNDID']+":"+rVal[i]['INSTANCE']] = rVal[i]['FUNDID']+":"+rVal[i]['INSTANCE'];
          }

        cacheObject['data'] = listData;
        cacheObject['folioToplevelLookup'] = folioToplevelLookup;
        cacheObject['folioNameLookup'] = folioNameLookup;
        cacheObject['updateTime'] = new Date();
        cacheObject['inProgress'] = false;

        // Replacement for getFolioAccountsArray()

        var accountsData = new Array();
        var accountsDataReverse = new Array();
        var shortAccount;

        for (var i = 0; i < rVal.length; i++)
          {
          shortAccount = rVal[i]['ACCOUNT'].substr(rVal[i]['ACCOUNT'].length - 11, 11);
          accountsData[folioToplevelLookup[rVal[i]['FUNDID']+":"+rVal[i]['INSTANCE']]] = {folio:rVal[i]['FUNDID']+":"+rVal[i]['INSTANCE'], sicovam:rVal[i]['SICOVAM'], account:shortAccount, account_full:rVal[i]['ACCOUNT'], instance:rVal[i]['INSTANCE'], fundid:rVal[i]['FUNDID']};

          if (shortAccount != '')
            {
            accountsDataReverse[shortAccount] = {folio:rVal[i]['FUNDID']+":"+rVal[i]['INSTANCE'], sicovam:rVal[i]['SICOVAM'], account:shortAccount, account_full:rVal[i]['ACCOUNT'], instance:rVal[i]['INSTANCE'], fundid:rVal[i]['FUNDID']};
            }
          }

        document.dataCache.folioAccountsArray['accountsData'] = accountsData;
        document.dataCache.folioAccountsArray['accountsDataReverse'] = accountsDataReverse;
        document.dataCache.folioAccountsArray['updateTime'] = new Date();
        document.dataCache.folioAccountsArray['inProgress'] = false;

        if (cacheObject['callbacks'])
          {
          if (cacheObject['callbacks'].length > 0)
            {
            for (var thisCallback in cacheObject['callbacks'])
              {
              try
                {
                cacheObject['callbacks'][thisCallback].callback(cacheObject['callbacks'][thisCallback].parameters);
                }
              catch (e)
                {
                console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
                }
              }

            // Clear callback array.
            cacheObject['callbacks'] = [];
            }
          }
        }
      catch (e)
        {
        document.dataCache.folioListArray = false;
        console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
        }
      }
  });

  }


function getFolioAggregatedHoldings(folioID, customCallback, customParameters)
  {
  if (!folioID) folioID = 0;

  var dialogID = customParameters; /* For this function, (so far) customParameters = dialogID */
  var navDate = $('#' + dialogID + ' .navDate').val();

  // Generate cache key
  var cacheKey = 'FoAgHold_' + folioID + '_' + navDate;
  var cacheObject;

  // Create cache object if it does not exist
  if (!document.dataCache[cacheKey])
    {
    document.dataCache[cacheKey] = {inProgress:false};
    }
  else
    {
    // if the cache object does exist and is not 'inProgress' and there is a callback object
    // Then just call the callback. If there is no callback, then assume this is a cache refresh.

    cacheObject = document.dataCache[cacheKey];
    var nowDate = new Date();
    var Datedifference = (nowDate - cacheObject['updateTime']);

    if ((cacheObject['inProgress'] === false) && ((cacheObject['updateTime']) && (Datedifference < 60000)))
      {
      if (customCallback)
        {
        customCallback(customParameters);

        RefreshTokenExpiry(document.dakotaUserID, document.dakotaSessionKey);

        return;
        }
      }
    }

  cacheObject = document.dataCache[cacheKey];

  cacheObject['inProgress'] = true;
  cacheObject['ID'] = folioID;

  if (!cacheObject['callbacks'])
    {
    cacheObject['callbacks'] = [];
    }

  if (customCallback)
    {
    cacheObject['callbacks'].push({callback:customCallback, parameters:customParameters});
    }


  // Establish Query parameters object

//  var formvar = {
//    'ID':folioID
//  };

  var formvar = {
    'QUERYTYPE':'valuation',
    'USERID':document.dakotaUserID,
    'TOKEN':document.dakotaSessionKey,
    'FUNDID':folioID,
    'VALUEDATE':navDate,
    'FIELDS':''
  };

  // Prepare parameters object for data fetch.

  // Get data.
  // Venice Folio (Funds) data query.

//  var url = "php/getFolioAggregatedHoldings.php";
  var url = "php/getRenaissanceData.php";

  $.ajax({type:"POST",
    url:url,
    data:formvar,
    success:function (html)
      {
      var rVal = JSON.parse(html);

      // Calculate additional data for the Holdings information.
      try
        {
        var fund_val = 0;
        var euro_val = 0;
        var index;

        for (index in rVal)
          {
          euro_val += parseFloat(nz(rVal[index]['EURValue'], 0));
          fund_val += parseFloat(nz(rVal[index]['Value'], 0));
          }

        if (fund_val < 1) fund_val = 1;

        for (index in rVal)
          {
          rVal[index]['FUND_WEIGHT'] = rVal[index]['EURValue'] / euro_val;
          }
        }
      catch (e)
        {
        console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
        }


      cacheObject['updateTime'] = new Date();
      cacheObject['data'] = rVal;
      cacheObject['inProgress'] = false;

      if (cacheObject['callbacks'])
        {
        if (cacheObject['callbacks'].length > 0)
          {
          for (var thisCallback in cacheObject['callbacks'])
            {
            try
              {
              if (thisCallback)
                {
                cacheObject['callbacks'][thisCallback].callback(cacheObject['callbacks'][thisCallback].parameters);
                }
              }
            catch (e)
              {
              console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
              }
            }

          // Clear callback array.
          cacheObject['callbacks'] = [];
          }
        }

      }
  });

  }

function rtClickShowColumnsContext(e, rowTag)
  {
  try
    {
    document.currentContextData = rowTag;

    var colArray = document.appDefaults.holdingGridColumns;
    var index;

    // Only a single context menu is used for the Transactions Grids.

    var menu = $('#bodyDiv .fsContextMenu');
    var menuList = $('#bodyDiv .fsContextMenu ul');

    // Clear existing entries.

    menuList.empty();
    var menuText;

    var realGridCols = $("#" + rowTag.DIALOGID + '_grid')[0].p.colModel;
    var visibleCols = {};
    var allCols = [];

    for (index = 0; index < realGridCols.length; index++)
      {
      if (realGridCols[index].visible)
        {
        visibleCols[realGridCols[index].dataKey] = true;
        }
      allCols.push({colIndex:index, headerText:realGridCols[index].headerText, dataKey:realGridCols[index].dataKey, visible:realGridCols[index].visible});
      }

    allCols.sort(
      function (a, b)
      {
      if (a.headerText < b.headerText) return -1;
      if (a.headerText > b.headerText) return 1;
      return 0;
      });

    if (allCols.length > 0)
      {
      for (index in allCols)
        {
        menuText = allCols[index].headerText;
        if (menuText.length > 0)
          {
          if (visibleCols[allCols[index].dataKey])
            {
            menuList.append('<li id="fsContextMenu_' + allCols[index].colIndex + '"><a id="fsContextMenu_' + allCols[index].colIndex + '" style="margin-left:20px; position:relative;"><span class="ui-icon ui-icon-check" style="position:absolute; top: 0; left: -20px"></span>' + menuText + '</a></li>');
            }
          else
            {
            menuList.append('<li id="fsContextMenu_' + allCols[index].colIndex + '"><a id="fsContextMenu_' + allCols[index].colIndex + '" style="margin-left:20px; position:relative;">' + menuText + '</a></li>');
            }

          $('#fsContextMenu_' + allCols[index].colIndex).click(function (e)
          {
          transactionsHeaderContextClick(e, rowTag.DIALOGID + '_grid', realGridCols);
          });
          }
        }
      }
    else
      {
      menuList.append('<li><a class="">There do not appear to be any columns.</a></li>');
      }

    menu.css({top:e.pageY - 10, left:e.pageX - 10, 'z-index':20000});

    menu.show();

    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
    }

  return false;
  }

function transactionsHeaderContextClick(e, gridID, realGridCols)
  {
  try
    {
    $('#bodyDiv .fsContextMenu').hide();

    var index = e.target.id.substr(14, e.target.id.length - 1);

    realGridCols[index].visible = !realGridCols[index].visible;

    // document.appDefaults.transactionsGridColumns[index] =
	// realGridCols[index].visible;
    $("#" + gridID).flexToggleCol(realGridCols[index].cid, realGridCols[index].visible);

    // transactionsSaveColumnInfo(dialogID);
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
    }
  }

function transactionsContextClick(e, menuID, rowTag)
  {
  /*
	 * Process click events on the portfolio context menu
	 * 
	 * All this function does is to retrieve the Mosningstar ID for the given
	 * ISIN and then call the transactionsContextProcess() function.
	 */


  $('#bodyDiv .fsContextMenu').hide();

  switch (menuID)
  {
    default:
      // For the moment, get Morningstar ID for all menu items.

      var cachedMsCode = getMorningstarCode(rowTag.ISIN);

      if (cachedMsCode === false)
        {
        $.ajax({type:"POST",
          url:"php/get_MorningstarFromIsin.php",
          data:{ISIN:rowTag.ISIN},
          success:function (html)
            {
            var derivedID = html;
            try
              {
              if (html.length > 0)
                {
                setMorningstarCode(rowTag.ISIN, derivedID);
                }
              }
            catch (e)
              {
              console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
              }

            transactionsContextProcess(e, menuID, derivedID, rowTag);
            }
        });
        }
      else
        {
        transactionsContextProcess(e, menuID, cachedMsCode, rowTag);
        }

      break;
  }

  }

function transactionsContextProcess(e, menuID, derivedID, rowTag)
  {
  /*
	 * Process click events on the portfolio context menu
	 * 
	 * Having sourced the Morningstar ID, not show the appropriate data.
	 */

  var MsURL;
  var siteHTML;
  var thisDialogType;
  var dialogOptions;
  var dialogObject;
  var dialogDiv;
  var headerText = "";
  var instrumentName = nz(rowTag['DESCRIPTION'], nz(rowTag['LIBELLE'], ''));
  var bodyText = "<p><strong>" + instrumentName + "</strong></p>";

  // Define source URLs:

  var MsPrintSnapshotURL = "http://www.morningstar.fr/fr/funds/snapshot/p_snapshot.aspx?id=" + derivedID;
  var MsTabSnapshotUrl = "http://www.morningstar.fr/fr/funds/snapshot/snapshot.aspx?id=" + derivedID + '&tab='; // Add
																												// Tab
																												// below...

  // Show 'Sorry' Dialog if there is no MS Code.

  if (derivedID.length == 0)
    {
    thisDialogType = document.dialogArray.dialogType.info;
    dialogOptions = {title:instrumentName, closeOnEscape:true, height:'auto', width:'auto'};
    dialogObject = getDialog(thisDialogType, dialogOptions);
    dialogDiv = $('#' + dialogObject.ID);

    $(dialogDiv).append('<p>Sorry there does not seem to be a MorningStar code for this instrument.</p><br/><p>' + instrumentName + '</p><p>ISIN : ' + rowTag['ISIN'] + '</p>');
    $(dialogDiv).dialogExtend("open");

    return;
    }

  // OK, now choose what to do...

  switch (menuID)
  {
    case 1:
      // showMsChart

      MsURL = 'http://tools.morningstar.fr/fr/interactivechart/html/default.aspx?embedded=true&SecurityTokenList=' + derivedID;

      thisDialogType = document.dialogArray.dialogType.iframeDisposable;  // Seems to fail if it shares Dialog with the other iframes
      dialogOptions = {title:instrumentName, closeOnEscape:false, contentUrl:MsURL, height:'525', width:'775', captionButtons:{pin:{visible:false}, refresh:{visible:false}}};

      dialogObject = getDialog(thisDialogType, dialogOptions);
      dialogDiv = $('#' + dialogObject.ID);

      $(dialogDiv).dialogExtend("open");
      $(dialogDiv).dialogExtend("refresh");

      $(dialogDiv).activity({segments:12, width:5.5, space:6, length:13, color:'#252525', speed:1.5});
      
      // $('#' + dialogObject.ID + ' iframe').unbind('load').load(function ()
      $('#' + dialogObject.ID + ' iframe').load(function ()
      {
		  $(this).css("overflow-y","hidden");
		  $(this).css("overflow-x","hidden");
		  $(dialogDiv).activity(false);
      });

      break;

    case 2:
      // showMsSummary

      var requiredDiv = "#overviewQuickstatsBenchmarkDiv"; // "#overviewQuickstatsDiv";

      getMsSnapshotDiv(MsPrintSnapshotURL, 'msPrintSnapshot', requiredDiv, derivedID, rowTag, "<style>body {width:300px;}</style>", bodyText);

      break;

    case 3:
      // showMsPerformance

      var requiredDiv = "#perfColumn"; // "#overviewPerformanceDiv";

      getMsSnapshotDiv(MsPrintSnapshotURL, 'msPrintSnapshot', requiredDiv, derivedID, rowTag, "<style>body {width:275px;}</style>", bodyText);

      break;

    case 4:
      // showMsHoldings

      var requiredDiv = "#portfolioTopHoldingsDiv"; // "#portfolioTopHoldingsDiv";
													// /// Tab 3

      getMsSnapshotDiv(MsPrintSnapshotURL, 'msPrintSnapshot', requiredDiv, derivedID, rowTag, "<style>body {width:400px;}</style>", bodyText);

      break;

    case 5:
      // showMsContactDetails

      var requiredDiv = "#managementManagementDiv"; // "#managementManagementDiv";
													// // Tab 4

      getMsSnapshotDiv(MsPrintSnapshotURL, 'msPrintSnapshot', requiredDiv, derivedID, rowTag, "<style>body {width:400px;}</style>", bodyText);

      break;

    case 6:
      // showMsDocuments

      var requiredDiv = "#managementFeesDiv"; // Tab 12

      // getMsSnapshotDiv(MsTabSnapshotUrl + '12', 'msTabSnapshot12',
		// requiredDiv, derivedID, rowTag, headerText, bodyText);

      // And now for something completely different.....

      preloadURL(MsTabSnapshotUrl + '12', 'msTabSnapshot12_' + derivedID, 20, false, false, showMSDocumentsDiv, {cacheKey:'msTabSnapshot12_' + derivedID, requiredDiv:requiredDiv, derivedID:derivedID, rowTag:rowTag, headerText:headerText, bodyText:bodyText});

      break;

    case 7:
      // overviewPortfolioDiv

      var requiredDiv = "#overviewPortfolioDiv"; // Tab 12

      getMsSnapshotDiv(MsPrintSnapshotURL, 'msPrintSnapshot', requiredDiv, derivedID, rowTag, "<style>body {width:560px;}</style>", bodyText);

      break;
  }

  }

function showMSDocumentsDiv(htmlCacheKey, parameters)
  {
  // Populate a new Dialog box with the chosen content taken from a given URL.
  //
  // URL : URL to take data from. The returned document is sanitised in the
	// PHP.
  // cachekey : localCache key to get the data from.
  // requiredDiv : String or Array of strings representing the Dom objects to
	// be included in the div.
  // derivedID : At present, the Morningstar ID. could be used for the unique
	// ID for other sites.
  // rowTag : information object passed from the context menu. contains ISIN
	// and LIBELLE properties (amongst others).
  //

  // Build base Dialog.

  var thisDialogType = document.dialogArray.dialogType.info;
  var dialogOptions = {title:parameters['derivedID'], closeOnEscape:false, height:'auto', width:'auto', captionButtons:{pin:{visible:false}, refresh:{visible:false}}};

  var dialogObject = getDialog(thisDialogType, dialogOptions, 200, 200);
  var dialogDiv = $('#' + dialogObject.ID);
  var dialogID = dialogObject.ID;

  var pageHTML = getCacheItem(htmlCacheKey);

  var myDiv = $(pageHTML).find('#managementFeesDiv');
  dialogDiv.append(parameters.bodyText);
  dialogDiv.append(myDiv);

  var pdfLinks = dialogDiv.find('td a');
  pdfLinks.click(function (e)
  {
  e.preventDefault();
  try
    {
    showMsPdf(e, parameters);
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
    }
  return false;
  });

  $(dialogDiv).dialogExtend("open");
  }

function showMsPdf(e, parameters)
  {
  var activeElement = $(e.target);
  var linkElement;
  var gg = this;

  linkElement = activeElement;
  if (activeElement.attr("tagName") != 'A') linkElement = activeElement.parent('a');
  var href = linkElement.attr('href');

  var params = href.split('?')[1].split('&');
  var index;
  var documentID = '';

  for (index in params)
    {
    if (params[index].substr(0, 11).toLowerCase() == 'documentid=')
      {
      documentID = params[index].split('=')[1];
      break;
      }
    }

  var thisDialogType = document.dialogArray.dialogType.iframeDisposable;
  var dialogOptions = {title:parameters.rowTag['LIBELLE'], closeOnEscape:false, contentUrl:'about:blank', captionButtons:{pin:{visible:false}, refresh:{visible:false}}};

  var dialogObject = getDialog(thisDialogType, dialogOptions, 600, 800);
  var dialogID = dialogObject.ID;
  var dialogDiv = $('#' + dialogObject.ID);
  var iframeDiv = $('#' + dialogObject.ID + ' iframe');

  $(dialogDiv).dialogExtend("open");

  $(dialogDiv).activity({segments:12, width:5.5, space:6, length:13, color:'#252525', speed:1.5});

  $(iframeDiv).load(function ()
  {
  iframeDiv.css({visibility:'hidden'});

  setTimeout(function ()
  {
  $(dialogDiv).activity(false);
  iframeDiv.css({visibility:'visible'});
  }, 3000)
  });


  var newIframe;
  if (iframeDiv.length > 0)
    {
    newIframe = iframeDiv[0];
    }
  else
    {
    dialogDiv[0].innerHTML = "Error, iframe has not been created. (?)";
    return;
    }

  dialogDiv.addClass('fsFitAfterIframe');

  // pageHTML will contain the whole page, with adjusted links

  var iframeContent = '<html><head></head><body marginwidth="0" marginheight="0"><embed width="100%" height="100%" name="plugin" src="http://doc.morningstar.com/Document/' + documentID + '.msdoc/?clientid=euretailsite&amp;key=9ab7c1c01e51bcec" type="application/pdf"></body></html>';

  newIframe.contentWindow.document.open('text/html', 'replace');
  newIframe.contentWindow.document.write(iframeContent);
  newIframe.contentWindow.document.close();

  }

function preloadMsSnapshotPageFromISIN(ISIN_Code, snapshot_URL, cacheKeyBase)
  {
  /*
	 * any '?' in the snapshot_URL is replaced with the retrieved Morningstar
	 * Code.
	 */

  try
    {
    var cachedMsCode = getMorningstarCode(ISIN_Code);
    var modifiedURL;

    if (cachedMsCode === false)
      {
      $.ajax({type:"POST",
        url:"php/get_MorningstarFromIsin.php",
        data:{ISIN:ISIN_Code},
        success:function (html)
          {
          var derivedID = html;

          if (derivedID.length > 0)
            {
            setMorningstarCode(ISIN_Code, derivedID);
            modifiedURL = snapshot_URL.replace('<MSCODE>', derivedID);

            document.pendingUrlCache.push({URL:modifiedURL, cacheKey:(cacheKeyBase + '_' + derivedID)});
            // preloadURL(modifiedURL, cacheKeyBase + '_' + derivedID, false);
            }
          }
      });
      }
    else
      {
      modifiedURL = snapshot_URL.replace('<MSCODE>', cachedMsCode);

      // Is cache stale ?

      document.pendingUrlCache.push({URL:modifiedURL, cacheKey:(cacheKeyBase + '_' + cachedMsCode)});

      // preloadURL(modifiedURL, cacheKeyBase + '_' + cachedMsCode, false);
      }
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
    }
  }

function preloadURL(URL, cacheKey, cacheExpiryMins, alwaysRefresh, unstrippedHTML, customCallback, customParameters)
  {
  //
  var MsURL = URL;
  var MsCurrentdirparent;
  var MsRoot;
  var MsCurrentdir; // = MsURL.split('?')[0]; MsCurrentdir =
					// MsURL.split('#')[0];
  var getPlainHTML = (nz(unstrippedHTML, false) === false ? false : true );

  // Duck out if you do not always have to refresh the cache and the cache
	// contains something valid.

  if ((alwaysRefresh === false) && (!(getCacheItem(cacheKey, false) === false)))
    {
    if (customCallback)
      {
      if (customParameters)
        {
        customCallback(cacheKey, customParameters);
        }
      else
        {
        customCallback(cacheKey);
        }
      }

    return;
    }

  //

  try
    {
    if (!document.dataCache.preloadURL)
      {
      document.dataCache.preloadURL = [];
      }
    }
  catch (e)
    {
    document.dataCache.preloadURL = [];
    console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
    }
  var cacheObject = document.dataCache.preloadURL;

  if (customCallback)
    {
    cacheObject[cacheKey] = {callback:customCallback, parameters:customParameters};
    }

  //

  try
    {

    var a = document.createElement('a');
    a.href = MsURL;

    MsRoot = 'http://' + a.hostname + '/';
    MsCurrentdir = MsRoot;
    MsCurrentdirparent = MsRoot;

    var parts = a.pathname.split('/');
    if (parts.length > 0)
      {
      while ((parts.length > 0) && (parts[0].length == 0)) parts.shift();
      while ((parts.length > 0) && (parts[parts.length - 1].length == 0)) parts.pop();
      }

    if (parts.length > 0) parts.pop(); // lose the last element, should be the
										// document name.

    // OK add CurrentDir path

    if (parts.length > 0) MsCurrentdir += parts.join('/') + '/';

    // pop last item to get Parent directory

    if (parts.length > 0) parts.pop(); // lose the last element to get 'parent'
										// path

    if (parts.length > 0) MsCurrentdirparent += parts.join('/') + '/';

    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
    }

  try
    {
    // Pass the cachekey as part of the data so that in the success function I
	// can be sure that I am saving to the
    // correct key.

    $.ajax({type:"POST",
      url:(getPlainHTML ? "php/get_URL.php" : "php/MsModifiedPage.php"),
      data:{url:MsURL, contentdiv:"body", 
    	  currentdirparent:MsCurrentdirparent, 
    	  currentdir:MsCurrentdir, root:MsRoot, 
    	  targetCacheKey:cacheKey, 
    	  cacheLife:cacheExpiryMins, 
          search:"www.morningstar.fr/fr", 
          replace:"www.morningstar.co.uk/uk"},
      success:function (html)
        {
        // Retrieve Cache key from data.

        var dataArray = decodeURIComponent(this.data).split('&');
        var targetCacheKey = cacheKey; // default if not in the dataArray
        var targetCacheLife = 20;

        var index;
        for (index in dataArray)
          {
          if (dataArray[index].substr(0, 15) == 'targetCacheKey=')
            {
            targetCacheKey = dataArray[index].split('=')[1];
            }
          if (dataArray[index].substr(0, 10) == 'cacheLife=')
            {
            targetCacheLife = parseInt(dataArray[index].split('=')[1]);
            }
          }

        // Save html to cache.
        if (html.length > 200)
          {
          setCacheItem(targetCacheKey, html, 20);
          }

        // Call callbacks (if any).
        try
          {
          var cacheObject = document.dataCache.preloadURL;
          var callbackObj = cacheObject[targetCacheKey];

          if (callbackObj && (callbackObj['callback']))
            {
            if (callbackObj['parameters'])
              {
              callbackObj['callback'](targetCacheKey, callbackObj['parameters']);
              }
            else
              {
              callbackObj['callback'](targetCacheKey);
              }
            }

          // Clean cache.
          delete cacheObject[targetCacheKey];
          }
        catch (e)
          {
          console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
          }
        }
    });
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
    }
  }

function getMsSnapshotDiv(URL, cachekey, requiredDiv, derivedID, rowTag, headerText, bodyText)
  {
  // Populate a new Dialog box with the chosen content taken from a given URL.
  //
  // URL : URL to take data from. The returned document is sanitised in the
	// PHP to make it usable in an iframe.
  // cachekey : localCache key to save the page in. The given key should be
	// for this general site, the actual key
  // used will have the 'derivedID' appended to it.
  // requiredDiv : String or Array of strings representing the Dom objects to
	// be included in the div.
  // derivedID : At present, the Morningstar ID. could be used for the unique
	// ID for other sites.
  // rowTag : information object passed from the context menu. contains ISIN
	// and LIBELLE properties (amongst others).
  //

  // Build base Dialog.

  var thisDialogType = document.dialogArray.dialogType.iframe;
  var dialogOptions = {title:derivedID, closeOnEscape:false, contentUrl:'about:blank', height:'auto', width:'auto', captionButtons:{pin:{visible:false}, refresh:{visible:false}}};

  var dialogObject = getDialog(thisDialogType, dialogOptions, 200, 200);
  var dialogDiv = $('#' + dialogObject.ID);

  $(dialogDiv).dialogExtend("open");

  $(dialogDiv).activity({segments:12, width:5.5, space:6, length:13, color:'#252525', speed:1.5});

  var dialogID = dialogObject.ID;

  // Setup URL, check the cache.

  var MsURL = URL;
  var pageHTML = getCacheItem(cachekey + '_' + derivedID);

  // OK, Strip parameters from URL
  // Formulate Root, Current Dir & Parent Dir from the given URL.

  var MsCurrentdirparent;
  var MsRoot;
  var MsCurrentdir; // = MsURL.split('?')[0]; MsCurrentdir =
					// MsURL.split('#')[0];

  try
    {

    var a = document.createElement('a');
    a.href = MsURL;

    MsRoot = 'http://' + a.hostname + '/';
    MsCurrentdir = MsRoot;
    MsCurrentdirparent = MsRoot;

    var parts = a.pathname.split('/');
    if (parts.length > 0)
      {
      while ((parts.length > 0) && (parts[0].length == 0)) parts.shift();
      while ((parts.length > 0) && (parts[parts.length - 1].length == 0)) parts.pop();
      }

    if (parts.length > 0) parts.pop(); // lose the last element, should be the
										// document name.

    // OK add CurrentDir path

    if (parts.length > 0) MsCurrentdir += parts.join('/') + '/';

    // pop last item to get Parent directory

    if (parts.length > 0) parts.pop(); // lose the last element to get 'parent'
										// path

    if (parts.length > 0) MsCurrentdirparent += parts.join('/') + '/';

    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
    }

  // Capture the whole body. This makes the cache more useful.

  try
    {

    var MsContentDiv = "body";

    // Get the page if it was not cached.

    if (pageHTML === false)
      {
      $.ajax({type:"POST",
        url:"php/MsModifiedPage.php",
        data:{url:MsURL, 
        contentdiv:MsContentDiv, 
        currentdirparent:MsCurrentdirparent, 
        currentdir:MsCurrentdir, 
        root:MsRoot, 
        search:"www.morningstar.fr/fr", 
        replace:"www.morningstar.co.uk/uk"},
        success:function (html)
          {
          if (html.length > 200)
            {
            setCacheItem(cachekey + '_' + derivedID, html, 10);
            }

          setMsSnapshotDiv(html, dialogID, requiredDiv, derivedID, rowTag, headerText, bodyText);
          }
      });

      }
    else
      {
      setMsSnapshotDiv(pageHTML, dialogID, requiredDiv, derivedID, rowTag, headerText, bodyText);
      }
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
    }

  $(dialogDiv).activity(false);

  }

function setMsSnapshotDiv(pageHTML, dialogID, requiredDiv, derivedID, rowTag, pHeaderTextSnippet, pBodyTextSnippet)
  {
  /*
	 * Sets up the Morningstar snapshot dialog
	 * 
	 */

  // Get handles to the prepared Dialog and iFrame.

  var dialogDiv = $('#' + dialogID);
  var iframeDiv = $('#' + dialogID + ' iframe');

  dialogDiv.addClass('fsFitAfterIframe');

  if (dialogDiv.length == 0)
    {
    return;
    }

  // pageHTML will contain the whole page, with adjusted links

  iframeDiv.unbind('load').load(function ()
  {
  this.style.height = ($(this).contents().height() + 5) + 'px';
  this.style.width = ($(this).contents().width() + 5) + 'px';

  var dialogDiv = $('#' + dialogID);

  try
    {
    if (dialogDiv.hasClass('fsFitAfterIframe')) // Assures a once-only
												// operation.
      {
      dialogDiv.removeClass('fsFitAfterIframe');
      }

    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
    }
  });

  // Get DOM handle to iframe, or error message if it does not exist.

  var newIframe;
  if (iframeDiv.length > 0)
    {
    newIframe = iframeDiv[0];
    }
  else
    {
    dialogDiv[0].innerHTML = "Error, iframe has not been created. (?)";
    return;
    }

  // Cut and Shut time.

  var headerEnd = pageHTML.indexOf('</head>');
  var bodyStart = pageHTML.indexOf('<body') + 6;
  var bodyEnd = pageHTML.indexOf('</body>');
  var bodyText = pageHTML.slice(bodyStart, bodyEnd);

  var myDiv;
  var divHTML = '';
  var requiredDivType = typeof requiredDiv;
  var counter;

  if (requiredDivType == 'object')
    {
    try
      {
      for (thisDiv in requiredDiv)
        {
        myDiv = $(bodyText).find(thisDiv);
        if (myDiv.length > 0)
          {
          for (counter = 0; counter < myDiv.length; counter++)
            {
            divHTML += '<div>' + myDiv[counter].outerHTML + '</div>';
            }
          }
        }
      }
    catch (e)
      {
      console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
      }
    }
  else
    {
    myDiv = $(bodyText).find(requiredDiv);

    if (myDiv.length > 0)
      {
      for (counter = 0; counter < myDiv.length; counter++)
        {
        divHTML += '<div>' + myDiv[counter].outerHTML + '</div>';
        }
      }
    }

  // pHeaderTextSnippet, pBodyTextSnippet

  var iframeContent = pageHTML.slice(0, headerEnd) + pHeaderTextSnippet + '</head><body>' + pBodyTextSnippet + (divHTML.length > 0 ? divHTML : 'Sorry, this information is not available.') + pageHTML.substring(bodyEnd);

  newIframe.contentWindow.document.open('text/html', 'replace');
  newIframe.contentWindow.document.write(iframeContent);
  newIframe.contentWindow.document.close();

  }





/* Economic Calendar */

function getEconomicCalendar()
  {
  var MsURL = 'http://www.dailyfx.com/francais/calendrier_economique/?tz=%2B2.0&sort=date&week=today&eur=true&usd=true&jpy=true&gbp=true&chf=true&aud=true&cad=true&nzd=true&cny=true&high=true&medium=true&low=true';

  var thisDialogType = document.dialogArray.dialogType.iframe;
  var dialogOptions = {title:'Economic Calendar', closeOnEscape:false, contentUrl:'about:blank', height:'auto', width:'auto', captionButtons:{pin:{visible:false}, refresh:{visible:false}}};

  var dialogObject = getDialog(thisDialogType, dialogOptions, 500, 800);
  var dialogID = dialogObject.ID;
  var dialogDiv = $('#' + dialogObject.ID);

  $(dialogDiv).dialogExtend("open");
  $(dialogDiv).activity({segments:12, width:5.5, space:6, length:13, color:'#252525', speed:1.5});

  var pageHTML = getCacheItem('economicCalendar_fr');

  try
    {

    var a = document.createElement('a');
    a.href = MsURL;

    MsRoot = 'http://' + a.hostname + '/';
    MsCurrentdir = MsRoot;
    MsCurrentdirparent = MsRoot;

    var parts = a.pathname.split('/');
    if (parts.length > 0)
      {
      while ((parts.length > 0) && (parts[0].length == 0)) parts.shift();
      while ((parts.length > 0) && (parts[parts.length - 1].length == 0)) parts.pop();
      }

    if (parts.length > 0) parts.pop(); // lose the last element, should be the
										// document name.

    // OK add CurrentDir path

    if (parts.length > 0) MsCurrentdir += parts.join('/') + '/';

    // pop last item to get Parent directory

    if (parts.length > 0) parts.pop(); // lose the last element to get 'parent'
										// path

    if (parts.length > 0) MsCurrentdirparent += parts.join('/') + '/';

    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
    }

  // Capture the whole body. This makes the cache more useful.

  try
    {
    var MsContentDiv = "body";

    // Get the page if it was not cached.

    if (pageHTML === false)
      {
      $.ajax({type:"POST",
        url:"php/MsModifiedPage.php",
        data:{url:MsURL, contentdiv:MsContentDiv, 
        	currentdirparent:MsCurrentdirparent, 
        	currentdir:MsCurrentdir, 
        	root:MsRoot, 
            search:"www.morningstar.fr/fr", 
            replace:"www.morningstar.co.uk/uk"},
        success:function (html)
          {
          if (html.length > 200)
            {
            setCacheItem('economicCalendar_fr', html, 5);
            }

          showEconomicCalendar(html, dialogID);
          }
      });

      }
    else
      {
      showEconomicCalendar(pageHTML, dialogID);
      }
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
    }

  $(dialogDiv).activity(false);

  }

function showEconomicCalendar(pageHTML, dialogID)
  {
  var requiredDiv = "#e-cal-table";
  var pHeaderTextSnippet = '<style type="text/css"> #e-cal-table tr.e-cal-row td { cursor:pointer; } #e-cal-table tr.empty td { cursor: default; } #e-cal-table tr.clckd { background-color:#f4f4f9;  border-bottom: hidden; }  #e-cal-table div.arrow { background:transparent url(http://dailyfx.com/design/fxcm/img/calendar/btn_commentary.gif) no-repeat scroll 0px 0px; width:15px; height:15px; display:block;}  #e-cal-table div.arrow-active { background-position:-15px 0px;}  #e-cal-table div.up { background-position:-15px -15px;}  #e-cal-table tr.clckd { background-position:-15px -15px;}</style>';
  var pBodyTextSnippet = "";

  var dialogDiv = $('#' + dialogID);
  var iframeDiv = $('#' + dialogID + ' iframe');

  dialogDiv.addClass('fsFitAfterIframe');

  if (dialogDiv.length == 0)
    {
    return;
    }

  // pageHTML will contain the whole page, with adjusted links

  iframeDiv.load(function ()
  {
  this.style.height = ($(this).contents().height() + 20) + 'px';
  this.style.width = ($(this).contents().width() + 20) + 'px';

  var dialogDiv = $('#' + dialogID);

  try
    {
    if (dialogDiv.hasClass('fsFitAfterIframe')) // Assures a once-only
												// operation.
      {
      dialogDiv.removeClass('fsFitAfterIframe');

      var thisHeight = $(dialogDiv).height();
      if (thisHeight > 600)
        {
        $(dialogDiv).dialogExtend("option", {"height": 600, "width": $(dialogDiv).width() + 20});
// $(dialogDiv).dialogExtend("option", "height", 600);
// $(dialogDiv).dialogExtend("option", "width", $(dialogDiv).width() + 20);
        }
      }
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
    }
  });

  // Get DOM handle to iframe, or error message if it does not exist.

  var newIframe;
  if (iframeDiv.length > 0)
    {
    newIframe = iframeDiv[0];
    }
  else
    {
    dialogDiv[0].innerHTML = "Error, iframe has not been created. (?)";
    return;
    }

  // Cut and Shut time.

  var headerEnd = pageHTML.indexOf('</head>');
  var bodyStart = pageHTML.indexOf('<body') + 6;
  var bodyEnd = pageHTML.indexOf('</body>');
  var bodyText = pageHTML.slice(bodyStart, bodyEnd);

  var myDiv;
  var divHTML = '';
  var requiredDivType = typeof requiredDiv;
  var counter;
  var thisDiv;

  if (requiredDivType == 'object')
    {
    try
      {
      for (thisDiv in requiredDiv)
        {
        myDiv = $(bodyText).find(thisDiv);
        if (myDiv.length > 0)
          {
          for (counter = 0; counter < myDiv.length; counter++)
            {
            divHTML += '<div>' + myDiv[counter].outerHTML + '</div>';
            }
          }
        }
      }
    catch (e)
      {
      console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
      }
    }
  else
    {
    myDiv = $(bodyText).find(requiredDiv);

    if (myDiv.length > 0)
      {
      for (counter = 0; counter < myDiv.length; counter++)
        {
        divHTML += '<div>' + myDiv[counter].outerHTML + '</div>';
        }
      }
    }

  // pHeaderTextSnippet, pBodyTextSnippet

  var iframeContent = pageHTML.slice(0, headerEnd) + pHeaderTextSnippet + '</head><body>' + pBodyTextSnippet + '<div id="content-article" class="grid_9 e-cal-page">' + (divHTML.length > 0 ? divHTML : 'Sorry, this information is not available.') + '</div>' + pageHTML.substring(bodyEnd);

  newIframe.contentWindow.document.open('text/html', 'replace');
  newIframe.contentWindow.document.write(iframeContent);
  newIframe.contentWindow.document.close();


  }

/* ALEX */

function getAlexCartoon()
  {
  var MsURL = 'http://www.telegraph.co.uk/finance/alex/';

  var thisDialogType = document.dialogArray.dialogType.iframe;
  var dialogOptions = {title:'Alex', closeOnEscape:false, contentUrl:'about:blank', height:'auto', width:'auto', captionButtons:{pin:{visible:false}, refresh:{visible:false}}};

  var dialogObject = getDialog(thisDialogType, dialogOptions, 300, 600);
  var dialogID = dialogObject.ID;
  var dialogDiv = $('#' + dialogObject.ID);

  $(dialogDiv).dialogExtend("open");
  $(dialogDiv).activity({segments:12, width:5.5, space:6, length:13, color:'#252525', speed:1.5});

  var pageHTML = getCacheItem('alexcartoon_en');

  try
    {

    var a = document.createElement('a');
    a.href = MsURL;

    MsRoot = 'http://' + a.hostname + '/';
    MsCurrentdir = MsRoot;
    MsCurrentdirparent = MsRoot;

    var parts = a.pathname.split('/');
    if (parts.length > 0)
      {
      while ((parts.length > 0) && (parts[0].length == 0)) parts.shift();
      while ((parts.length > 0) && (parts[parts.length - 1].length == 0)) parts.pop();
      }

    if (parts.length > 0) parts.pop(); // lose the last element, should be the
										// document name.

    // OK add CurrentDir path

    if (parts.length > 0) MsCurrentdir += parts.join('/') + '/';

    // pop last item to get Parent directory

    if (parts.length > 0) parts.pop(); // lose the last element to get 'parent'
										// path

    if (parts.length > 0) MsCurrentdirparent += parts.join('/') + '/';

    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
    }

  // Capture the whole body. This makes the cache more useful.

  try
    {
    var MsContentDiv = ".tmglSlideshow img";

    // Get the page if it was not cached.

    if (pageHTML === false)
      {
      $.ajax({type:"POST",
        url:"php/MsModifiedPage.php",
        data:{url:MsURL, contentdiv:MsContentDiv, 
        	currentdirparent:MsCurrentdirparent, 
        	currentdir:MsCurrentdir, 
        	root:MsRoot, 
            search:"www.morningstar.fr/fr", 
            replace:"www.morningstar.co.uk/uk"},
        success:function (html)
          {
          if (html.length > 200)
            {
            setCacheItem('alexcartoon_en', html, 60);
            }

          showAlexCartoon(html, dialogID);
          }
      });

      }
    else
      {
      showAlexCartoon(pageHTML, dialogID);
      }
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
    }

  $(dialogDiv).activity(false);

  }

function showAlexCartoon(pageHTML, dialogID)
  {
  var requiredDiv = "img";
  var pHeaderTextSnippet = '';
  var pBodyTextSnippet = '';

  var dialogDiv = $('#' + dialogID);
  var iframeDiv = $('#' + dialogID + ' iframe');

  dialogDiv.addClass('fsFitAfterIframe');

  if (dialogDiv.length == 0)
    {
    return;
    }

  iframeDiv.load(function ()
  {
  this.style.height = ($(this).contents().height() + 20) + 'px';
  this.style.width = ($(this).contents().width() + 20) + 'px';

  var dialogDiv = $('#' + dialogID);

  try
    {
    if (dialogDiv.hasClass('fsFitAfterIframe')) // Assures a once-only
												// operation.
      {
      dialogDiv.removeClass('fsFitAfterIframe');

      var thisHeight = $(dialogDiv).height();
      if (thisHeight > 300)
        {
        $(dialogDiv).dialogExtend("option", {"height": 300, "width": $(dialogDiv).width() + 20});
// $(dialogDiv).dialogExtend("option", "height", 300);
// $(dialogDiv).dialogExtend("option", "width", $(dialogDiv).width() + 20);
        }
      }
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
    }
  });

  // Get DOM handle to iframe, or error message if it does not exist.

  var newIframe;
  if (iframeDiv.length > 0)
    {
    newIframe = iframeDiv[0];
    }
  else
    {
    dialogDiv[0].innerHTML = "Error, iframe has not been created. (?)";
    return;
    }

  // Cut and Shut time.

  var headerEnd = pageHTML.indexOf('</head>');
  var bodyStart = pageHTML.indexOf('<body') + 6;
  var bodyEnd = pageHTML.indexOf('</body>');
  var bodyText = pageHTML.slice(bodyStart, bodyEnd);

  var myDiv;
  var divHTML = '';
  var requiredDivType = typeof requiredDiv;
  var counter;
  var thisDiv;

  if (requiredDivType == 'object')
    {
    try
      {
      for (thisDiv in requiredDiv)
        {
        myDiv = $(bodyText).find(thisDiv);
        if (myDiv.length > 0)
          {
          for (counter = 0; counter < myDiv.length; counter++)
            {
            divHTML += '<div>' + myDiv[counter].outerHTML + '</div>';
            }
          }
        }
      }
    catch (e)
      {
      console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
      }
    }
  else
    {
    // For the Alex cartoon, we got only the <img> tag.

    divHTML += '<div>' + bodyText + '</div>';

    }

  // pHeaderTextSnippet, pBodyTextSnippet

  var iframeContent = pageHTML.slice(0, headerEnd) + pHeaderTextSnippet + '</head><body>' + pBodyTextSnippet + '<div id="content-article" class="grid_9 e-cal-page">' + (divHTML.length > 0 ? divHTML : 'Sorry, this information is not available.') + '</div>' + pageHTML.substring(bodyEnd);

  newIframe.contentWindow.document.open('text/html', 'replace');
  newIframe.contentWindow.document.write(iframeContent);
  newIframe.contentWindow.document.close();

  }


