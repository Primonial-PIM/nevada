

/* Middle Office, Settling cash */

function showMoSettlingCash()
  {
  try
    {
    var dialogID = (-1);
    var dialogDiv = undefined;
    var thisDialogType = undefined;
    var leftHeaderDiv = undefined;
    var dialogOptions = undefined;
    var dialogObject = undefined;

    thisDialogType = document.dialogArray.dialogType.MoSettlingCash;
    dialogOptions = {title:"Settling Cash", position:[5, 50], resizeStop:dialogMiddleOfficeTransactionsResizeStop, stateChanged:dialogMiddleOfficeTransactionsStateChange, open:dialogMiddleOfficeTransactionsOpen, close:dialogMiddleOfficeTransactionsClose, closeOnEscape:false, captionButtons:{pin:{visible:false}, refresh:{visible:true, click:refreshMoSettlingCashData}, print:{visible:true, click:fnSimpleGridPrint}, export:{visible:true, click:writeFromDialogGridToExcel}}};

    dialogObject = getDialog(thisDialogType, dialogOptions, window.innerHeight-60, window.innerWidth-15);
    dialogObject['GRID2_HEIGHT'] = 160;
    dialogID = dialogObject.ID;
    dialogDiv = $('#' + dialogID);

    var splitterDiv = $('<div id="' + dialogID + '_splitter" class="splitterDiv"><div id="' + dialogID + '_splitterLeft" class="leftPanel"><div id="' + dialogID + '_leftSuper"><div id="' + dialogID + '_leftHeading"></div><div><table id="' + dialogID + '_grid" class="gridTable"></table></div><div><table id="' + dialogID + '_grid2" class="gridTable"></table></div></div></div><div id="' + dialogID + '_splitterRight" class="rightPanel"></div></div>');
    dialogDiv.append(splitterDiv);

    // Initialise Splitter.

    leftHeaderDiv = splitterDiv.find('#' + dialogID + '_leftHeading');

    setHtmlFromFile(leftHeaderDiv[0], 'html/moSettlingCash.html?dummy=' + getUniqueCounter().toString(), 'moTransactionsFilter', false, false, setMoSettCashFilterEvents, dialogID);

    // Initialise Transactions list.

    getMoSettlingCashData(dialogID);

    // Show Dialog.

    $(dialogDiv).dialogExtend("open");

    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
    }

  }

function setMoSettCashFilterEvents(targetObject, dialogID)
  {
  /* Clear Fund list options and populate */
  try
    {
    var fundCombo = $('#' + dialogID + ' .moFundCombo');
    var referenceStartDate = new Date;
    // referenceStartDate.addBusDays(-5);
    referenceStartDate.setHours(0, 0, 0, 0);
    var referenceStartDateString = referenceStartDate.getFullYear() + '-' + ("00" + (referenceStartDate.getMonth() + 1)).slice(-2) + '-' + ("00" + referenceStartDate.getDate()).slice(-2);

    var referenceEndDate = new Date;
    referenceEndDate.addBusDays(5);
    referenceEndDate.setHours(0, 0, 0, 0);
    var referenceEndDateString = referenceEndDate.getFullYear() + '-' + ("00" + (referenceEndDate.getMonth() + 1)).slice(-2) + '-' + ("00" + referenceEndDate.getDate()).slice(-2);

    fundCombo.empty();
    fundCombo.append("<option value='1'>All Funds</option>");

    var fundList = document.dataCache.folioListArray.data;

    for (thisIndex in fundList)
      {
      if (fundList[thisIndex]['value'] != '1')
        {
        fundCombo.append("<option value='" + fundList[thisIndex]['value'] + "'>" + fundList[thisIndex]['label'] + "</option>");
        }
      }
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
    }

  try
    {
    /* UI functionality */
    var pickers = $('#' + dialogID + ' .datePicker');
    pickers.datepicker({ dateFormat:"yy-mm-dd", gotoCurrent:true });

    // default values
    $('#' + dialogID + ' .tradeDateFrom').datepicker("setDate", referenceStartDateString);
    $('#' + dialogID + ' .tradeDateTo').datepicker("setDate", referenceEndDateString);

    /* Events */

    $('#' + dialogID + ' .jQueryUiCombobox').change(moSettledCashFilterChanged);
    $('#' + dialogID + ' .checkboxCurrencyColours').change(moSettledCashFilterChanged);
    $('#' + dialogID + ' .formItemDiv a').click(fnPrevNextDatepicker);
    $('#' + dialogID + ' .buttonSelectRecentDate').click(fnRecentDatepicker);
    // $('#' + dialogID + ' .moUpdateButton').click(moSettledCashFilterChanged);
    pickers.change(moSettledCashFilterChanged);
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
    }

  }


function moSettledCashFilterChanged()
  {

  try
    {

    var dialogID = $(this).parents('.dialogDiv').attr('id');
    var cachekey = MO_TRANSACTION_CACHE_PREFIX + dialogID;
    var cacheObject = undefined;

    if (document.dataCache[cachekey])
      {
      cacheObject = document.dataCache[cachekey];

      if ((cacheObject.inProgress == true))
        {
        return;
        }

      if (cacheObject.data.dataStatus < 5) // <- Note !!!!!
        {
        getMoSettlingCashData(dialogID);
        return;
        }

      if (cacheObject.data.transactionsFormObject == false)
        {
        getMoSettlingCashData(dialogID);
        return;
        }
      }
    else
      {
      // Refresh all data.
      getMoSettlingCashData(dialogID);
      return;
      }

    /* Reload Data, or just re-process existing data? */
    /* OK, if the data dates are reasonable, then just re-process */

    var thisFormObject = cacheObject.data.transactionsFormObject;
    var dataDateFrom = new Date(thisFormObject['DATE_FROM']);
    var dataDateTo = new Date(thisFormObject['DATE_TO']);

    var referenceStartDate = new Date();
    referenceStartDate.setHours(0, 0, 0, 0);

    var moDateFrom = $('#' + dialogID + ' .tradeDateFrom').val();
    var filterDateFrom = new Date(((moDateFrom) && (moDateFrom.length >= 8)) ? moDateFrom : referenceStartDate.getTime());
    var moDateTo = $('#' + dialogID + ' .tradeDateTo').val();
    var filterDateTo = new Date(((moDateTo) && (moDateTo.length >= 8)) ? moDateTo : '01/01/2900');
    var useDateval = ($('#' + dialogID + ' .moDateCombo').val() == 'DateVal' ? 1 : 0);


    if ((useDateval == parseInt(thisFormObject['USE_SETTLEMENTDATE'])) && (filterDateFrom.getTime() >= dataDateFrom.getTime()) && (filterDateTo.getTime() <= dataDateTo.getTime()))
      {
      moFilterSettlingCashData(dialogID);
      return;
      }

    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
    }
  finally
    {
    RefreshTokenExpiry(document.dakotaUserID, document.dakotaSessionKey);
    }

  getMoSettlingCashData(dialogID);
  }

function getMoSettlingCashData(dialogID)
  {
  var today = new Date();
  today.setHours(0, 0, 0, 0);

  var startDate;
  var endDate = '3000-01-01';
  var useDateval = 0;

  var referenceStartDate = new Date;
  referenceStartDate.addBusDays(-10);

  var referenceEndDate = new Date;
  referenceEndDate.addBusDays(10);

  /* Filter parameters */

  // Date Selection

  try
    {

    var moDateType = $('#' + dialogID + ' .moDateCombo').val();
    var moDateFrom = $('#' + dialogID + ' .tradeDateFrom').val();
    var moDateTo = $('#' + dialogID + ' .tradeDateTo').val();

    if ((moDateFrom) && (moDateFrom.length >= 8))  // Date From
      {
      try
        {
        referenceStartDate = new Date(moDateFrom);
        referenceStartDate.setDate(referenceStartDate.getDate() - 10);
        }
      catch (e)
        {
        console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');

        referenceStartDate = new Date;
        referenceStartDate.addBusDays(-10);
        }
      }
    referenceStartDate.setHours(0, 0, 0, 0);
    startDate = referenceStartDate.getFullYear() + '-' + ("00" + (referenceStartDate.getMonth() + 1)).slice(-2) + '-' + ("00" + referenceStartDate.getDate()).slice(-2);

    if ((moDateTo) && (moDateTo.length > 0))  // Date To
      {
      try
        {
        referenceEndDate = new Date(moDateTo);
        referenceEndDate.setDate(referenceEndDate.getDate() + 10);
        }
      catch (e)
        {
        console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');

        referenceEndDate = new Date;
        referenceEndDate.addBusDays(10);
        }
      }

    referenceEndDate.setHours(23, 59, 59, 999);
    endDate = referenceEndDate.getFullYear() + '-' + ("00" + (referenceEndDate.getMonth() + 1)).slice(-2) + '-' + ("00" + referenceEndDate.getDate()).slice(-2);

    if (moDateType == 'DateVal')
      {
      useDateval = 1;
      }
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
    }

  /* Get Data */

  var cacheObject;
  var cacheKey = MO_TRANSACTION_CACHE_PREFIX + dialogID;

  if (cacheKey in document.dataCache)
    {
    cacheObject = document.dataCache[cacheKey];
    cacheObject['inProgress'] = true;
    cacheObject['updateTime'] = (new Date());

    if ('data' in cacheObject)
      {
      cacheObject.data['dataStatus'] = 0;
      }
    else
      {
      cacheObject['data'] = {dataStatus:0, transactionsData:false, transactionsFormObject:false, tradefileData:false, tradefileFormObject:false, fxData:false, fxFormObject:false, fxLookup:false};
      }
    }
  else
    {
    cacheObject = {inProgress:true, filterFlags:{}, updateTime:(new Date()), data:{dataStatus:0, transactionsData:false, transactionsFormObject:false, tradefileData:false, tradefileFormObject:false, fxData:false, fxFormObject:false, fxLookup:false}};
    document.dataCache[cacheKey] = cacheObject;
    }

  getSettlingTransactions({DIALOGID:dialogID, DATE_FROM:startDate, DATE_TO:endDate, DATE_FX:startDate, USE_SETTLEMENTDATE:useDateval, NO_C: 1}, setMoSettlingCashData);
  // getMoTradeFileData({DIALOGID:dialogID, DATE_FROM:startDate,
  // DATE_TO:endDate}, setMoSettlingCashData);
  getFxRates({DIALOGID:dialogID, DATE_FROM:startDate, DATE_TO:endDate}, setMoSettlingCashData);

  }

function getSettlingTransactions(parameters, customCallback, customParameters)
  {
  var folioID;
  var SICOVAM;
  var date_from;
  var date_from_format;
  var date_to;
  var date_to_format;
  var date_fx;
  var date_fx_format;
  var no_c;
  var no_e;
  var uniqueID;
  var dialogID;
  var use_Settlementdate;

  if (!parameters)
    {
    parameters = {};
    }

  uniqueID = 'GetData_' + getUniqueCounter();
  dialogID = (parameters['DIALOGID'] ? parameters['DIALOGID'] : '');
  folioID = (parameters['FOLIO'] ? parameters['FOLIO'] : 0); // 0 for Venice, 1 for Sophis.
  SICOVAM = (parameters['SICOVAM'] ? parameters['SICOVAM'] : 0);
  date_from = (parameters['DATE_FROM'] ? parameters['DATE_FROM'] : '1970-01-01');
  date_from_format = (parameters['DATE_FROM_FORMAT'] ? parameters['DATE_FROM_FORMAT'] : 'YYYY-MM-DD');
  date_to = (parameters['DATE_TO'] ? parameters['DATE_TO'] : '2199-07-09');
  date_to_format = (parameters['DATE_TO_FORMAT'] ? parameters['DATE_TO_FORMAT'] : 'YYYY-MM-DD');
  date_fx = (parameters['DATE_FX'] ? parameters['DATE_FX'] : (parameters['DATE_FROM'] ? parameters['DATE_FROM'] : (parameters['DATE_TO'] ? parameters['DATE_TO'] : '2199-07-09')));
  date_fx_format = (parameters['DATE_FX'] ? (parameters['DATE_FX_FORMAT'] ? parameters['DATE_FX_FORMAT'] : 'YYYY-MM-DD') : (parameters['DATE_FROM'] ? (parameters['DATE_FROM_FORMAT'] ? parameters['DATE_FROM_FORMAT'] : 'YYYY-MM-DD') : (parameters['DATE_TO'] ? (parameters['DATE_TO_FORMAT'] ? parameters['DATE_TO_FORMAT'] : 'YYYY-MM-DD') : '2199-07-09')));
  no_c = (parameters['NO_C'] ? parameters['NO_C'] : 1);
  no_e = (parameters['NO_E'] ? parameters['NO_C'] : 0);
  use_Settlementdate = (parameters['USE_SETTLEMENTDATE'] ? parameters['USE_SETTLEMENTDATE'] : 0);

  // Establish Query patameters object

  var formvar = {
    'FOLIO':folioID,
    'SICOVAM':SICOVAM,
    'DATE_FROM':date_from,
    'DATE_FROM_FORMAT':date_from_format,
    'DATE_TO':date_to,
    'DATE_TO_FORMAT':date_to_format,
    'DATE_FX':date_fx,
    'DATE_FX_FORMAT':date_fx_format,
    'NO_C':no_c,
    'NO_E':no_e,
    'USE_SETTLEMENTDATE':use_Settlementdate,
    'DIALOGID':dialogID,
    'ID':uniqueID,
    'TOKEN':document.dakotaSessionKey,
    'USERID':document.dakotaUserID
  };

  var cacheObject = document.dataCache.preloadURL;
  if (customCallback)
    {
    cacheObject[uniqueID] = {callback:customCallback, parameters:customParameters};
    }

  // Prepare parameters object for data fetch.


  // Get data.
  // Venice transactions data query.

  var url = "php/getTransactionSettlement.php";

  $.ajax({type:"POST",
    url:url,
    data:formvar,
    success:function (html)
      {
      var rVal = JSON.parse(html);
      var formObject;
      var indexVal;
      var thisUniqueID = uniqueID;
      var dialogID = '';
      var cacheObject;
      var cachekey;
      
      var formData = decodeURIComponent(this.data);
      formObject=QueryStringToJSON(formData);
      
      /*
      try
        {
        indexVal = formData.indexOf('=');
        if (indexVal >= 0)
          {
          formObject = JSON.parse(formData.substr(indexVal + 1));
          }
        else
          {
          formObject = JSON.parse(formData);
          }
        }
      catch (e)
        {
        formObject = {'ID':uniqueID};
        console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
        }
	*/
      
      if (formObject['ID'])
        {
        thisUniqueID = formObject['ID'];
        }

      if (formObject['DIALOGID'])
        {
        dialogID = formObject['DIALOGID'];
        }


      cachekey = MO_TRANSACTION_CACHE_PREFIX + dialogID;
      if (document.dataCache[cachekey])
        {
        cacheObject = document.dataCache[cachekey];
        cacheObject.data.transactionsData = rVal;
        cacheObject.data.transactionsFormObject = formObject;
        cacheObject.data.dataStatus = cacheObject.data.dataStatus | 1;
        }

      try
        {
        var cacheObject = document.dataCache.preloadURL;
        if (cacheObject[uniqueID])
          {
          var callbackObj = cacheObject[thisUniqueID];

          if (callbackObj && (callbackObj['callback']))
            {
            if (callbackObj['parameters'])
              {
              callbackObj['callback'](formObject, callbackObj['parameters']);
              }
            else
              {
              callbackObj['callback'](formObject);
              }
            }

          // Clean cache.
          delete cacheObject[uniqueID];

          }
        }
      catch (e)
        {
        console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
        }

      }
  });
  }

function getFxRates(parameters, customCallback, customParameters)
  {
  var folioID;
  var SICOVAM;
  var date_fx;
  var date_fx_format;
  var no_c;
  var no_e;
  var uniqueID;
  var dialogID;

  if (!parameters)
    {
    parameters = {};
    }

  uniqueID = 'GetData_' + getUniqueCounter();
  dialogID = (parameters['DIALOGID'] ? parameters['DIALOGID'] : '');
  folioID = (parameters['FOLIO'] ? parameters['FOLIO'] : 1);
  SICOVAM = (parameters['SICOVAM'] ? parameters['SICOVAM'] : 0);
  date_from = (parameters['DATE_FROM'] ? parameters['DATE_FROM'] : '1970-01-01');
  date_from_format = (parameters['DATE_FROM_FORMAT'] ? parameters['DATE_FROM_FORMAT'] : 'YYYY-MM-DD');
  date_to = (parameters['DATE_TO'] ? parameters['DATE_TO'] : '2199-07-09');
  date_to_format = (parameters['DATE_TO_FORMAT'] ? parameters['DATE_TO_FORMAT'] : 'YYYY-MM-DD');
  no_c = (parameters['NO_C'] ? parameters['NO_C'] : 1);
  no_e = (parameters['NO_E'] ? parameters['NO_C'] : 1);

  // Establish Query patameters object

  var formvar = {
    'FOLIO':folioID,
    'SICOVAM':SICOVAM,
    'DATE_FROM':date_from,
    'DATE_FROM_FORMAT':date_from_format,
    'DATE_TO':date_to,
    'DATE_TO_FORMAT':date_to_format,
    'NO_C':no_c,
    'NO_E':no_e,
    'DIALOGID':dialogID,
    'ID':uniqueID,
    'USERID':document.dakotaUserID,
    'TOKEN':document.dakotaSessionKey
  };

  var cacheObject = document.dataCache.preloadURL;
  if (customCallback)
    {
    cacheObject[uniqueID] = {callback:customCallback, parameters:customParameters};
    }

  // Prepare parameters object for data fetch.

  var formJSON = JSON.stringify(formvar);

  // Get data.
  // Venice FX data query.

  var url = "php/getFXQuery.php";

  $.ajax({type:"POST",
    url:url,
    data:formvar,
    success:function (html)
      {
      var rVal = JSON.parse(html);
      var formObject;
      var indexVal;
      var thisUniqueID = uniqueID;
      var dialogID = '';
      var cacheObject;
      var cachekey;
      var formData = decodeURIComponent(this.data);
      
      formObject=QueryStringToJSON(formData);
      
      /*
      try
        {
        indexVal = formData.indexOf('=');
        if (indexVal >= 0)
          {
          formObject = JSON.parse(formData.substr(indexVal + 1));
          }
        else
          {
          formObject = JSON.parse(formData);
          }
        }
      catch (e)
        {
        formObject = {'ID':uniqueID};
        console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
        }
        */

      if (formObject['ID'])
        {
        thisUniqueID = formObject['ID'];
        }

      if (formObject['DIALOGID'])
        {
        dialogID = formObject['DIALOGID'];
        }


      cachekey = MO_TRANSACTION_CACHE_PREFIX + dialogID;
      if (document.dataCache[cachekey])
        {
        cacheObject = document.dataCache[cachekey]
        cacheObject.data.fxData = rVal;
        cacheObject.data.fxFormObject = formObject;
        cacheObject.data.dataStatus = cacheObject.data.dataStatus | 4;
        }

      try
        {
        var cacheObject = document.dataCache.preloadURL;
        if (cacheObject[uniqueID])
          {
          var callbackObj = cacheObject[thisUniqueID];

          if (callbackObj && (callbackObj['callback']))
            {
            if (callbackObj['parameters'])
              {
              callbackObj['callback'](formObject, callbackObj['parameters']);
              }
            else
              {
              callbackObj['callback'](formObject);
              }
            }

          // Clean cache.
          delete cacheObject[uniqueID];

          }
        }
      catch (e)
        {
        console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
        }

      }
  });
  }

function refreshMoSettlingCashData(e)
  {
  if (e)
    {
    try
      {
      var dialogID = this.id;

      getMoSettlingCashData(dialogID);
      }
    catch (e)
      {
      console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
      }
    }
  }

function setMoSettlingCashData(formObject, customParams)
  {
  var cachekey;
  var cacheObject;
  var dialogID = '';

  try
    {
    if (formObject['DIALOGID'])
      {
      dialogID = formObject['DIALOGID'];

      cachekey = MO_TRANSACTION_CACHE_PREFIX + dialogID;
      if (document.dataCache[cachekey])
        {
        cacheObject = document.dataCache[cachekey];

        if (cacheObject.data.dataStatus < 5) // Transactions + FX Date, not
        // trade file data.
          {
          /* Not finished getting data.... */
          return;
          }

        cacheObject.inProgress = false;
        moFilterSettlingCashData(dialogID);
        }
      else
        {
        /* No control object. Abort, Abort.... */
        return;
        }
      }
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
    }
  }

function moFilterSettlingCashData(dialogID)
  {
    /*
    Having already retrieved the data, this Function will filter and display the Settling Cash grid
     */

  var cachekey;
  var cacheObject;
  var dialogDiv = $('#' + dialogID);
  var gridID = dialogID + '_grid';
  var grid = $("#" + gridID);
  var existingColSet;

  try
    {
    if (document.dataCache.folioListArray.folioToplevelLookup == undefined)
      {
      setTimeout("moFilterSettlingCashData('" + dialogID + "')", 200);
      return;
      }
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
    }

  try
    {
    var folioToplevelLookup = document.dataCache.folioListArray.folioToplevelLookup;  // Match
    // folio accounts to top-level
    // accounts, as used in the drop-down Fund list.
    var folioNameLookup = document.dataCache.folioListArray.folioNameLookup;  // Match
    // top-level accounts to names
    // var folioAccounts = document.dataCache.folioAccountsArray.accountsData;
    // // Match folio Accounts to BNP accounts (first 11 chars only).
    // var folioAccountsReverse =
    // document.dataCache.folioAccountsArray.accountsDataReverse; // Match BNP
    // accounts to folio Accounts (first 11 chars only).

    try
      {
      cachekey = MO_TRANSACTION_CACHE_PREFIX + dialogID;

      if (document.dataCache[cachekey])
        {
        cacheObject = document.dataCache[cachekey];
        }
      else
        {
        /* No control object. Abort, Abort.... */
        return;
        }
      }
    catch (e)
      {
      console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
      return;
      }

    /* Filter parameters */
    // $('#' + dialogID + ' .moUpdateButton').css("visibility", "hidden");
    var selectedFund = $('#' + dialogID + ' .moFundCombo').val();
    var selectedCurrency = getCcyFromSophisCode($('#' + dialogID + ' .moCurrencyCombo').val());
    var moDateType = $('#' + dialogID + ' .moDateCombo').val();
    var moDateFrom = $('#' + dialogID + ' .tradeDateFrom').val();
    var moDateTo = $('#' + dialogID + ' .tradeDateTo').val();
    var selectedFundFlag = false;
    var selectedCurrencyFlag = false;
    var useCcyColours = $('#' + dialogID + ' .checkboxCurrencyColours').is(':checked');

    cacheObject.filterFlags.currencyColours = useCcyColours;


    if (selectedFund != '1')
      {
      selectedFundFlag = true;
      }
    if (selectedCurrency != '')
      {
      selectedCurrencyFlag = true;
      }

    /*
     * Ok, looks good. We have the data , now what?
     */

    // Get Filter criteria.

    var veniceTransactions = cacheObject.data.transactionsData;
    var FxEntries = cacheObject.data.fxData;

    var params = cacheObject.data.transactionsFormObject;
    var filteredTransactionsArray = new Array();
    var gridArray = new Array();
    var workingCurrencyGridArray = new Array();
    var currencyGridArray = new Array();
    var gridRowTemplate = {ROW_TYPE:0, CCY_COLOUR:'', ISIN:'', BO_STATUS:'', NAME:'', DATE_ENTERED:'', DATE_PAID:''};
    var currencyGridRowTemplate = {ROW_TYPE:0, NAME:''};
    var thisGridRow;

    var filterDateFrom;
    var filterDateTo;

    // Establish date Range.
    try
      {
      filterDateFrom = new Date(moDateFrom);
      }
    catch (e)
      {
      console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');

      filterDateFrom = new Date();
      filterDateFrom.addBusDays(-5);
      }
    filterDateFrom.setHours(0, 0, 0, 0);

    try
      {
      filterDateTo = new Date(moDateTo);
      }
    catch (e)
      {
      console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');

      filterDateTo = new Date();
      }
    filterDateTo.setHours(23, 59, 59, 999);


    // If using DateEntered, we must first calculate what transactions apply and
    // then build the data array across all settlement dates.
    // If using Payment Date (DateVal) then we need only to show data for the
    // Payment Dates included.

    // OK, first pass, calculate required date range.
    // Collect array of admissible transactions.

    var colArray = new Array();
    var thisCol;
    var firstDisplayDate = filterDateFrom;
    var lastDisplayDate = filterDateFrom;
    var transactionCounter;
    var thisTransaction;
    var thisDateEntered;
    var thisDatePaid;
    var thisFilterDate;
    var thisProperty;

    if (veniceTransactions.length > 0)
      {
      // Note 'REFERENCE_DATE' will already refer to either Date Entered or
      // Date Paid depending on the value of 'USE_SETTLEMENTDATE'

      firstDisplayDate = new Date('2100-01-01');
      lastDisplayDate = new Date('1900-01-01');

      for (transactionCounter = 0; transactionCounter < veniceTransactions.length; transactionCounter++)
        {
        thisTransaction = veniceTransactions[transactionCounter];
        thisFilterDate = new Date(thisTransaction['REFERENCE_DATE']);
        thisDateEntered = new Date(thisTransaction['MIN_DATENEG']);
        thisDatePaid = new Date(thisTransaction['DATESETTLED']);

        if (selectedFundFlag)  // Selected Fund ....
          {
          if (selectedFund != folioToplevelLookup[thisTransaction['OPCVM']+":"+thisTransaction['INSTANCE']])
            {
            continue;
            }
          }

        // Transaction is in Date filter range ....
        if ((thisFilterDate.getTime() >= filterDateFrom.getTime()) && (thisFilterDate.getTime() <= filterDateTo.getTime()))
          {
          filteredTransactionsArray.push(thisTransaction);

          // selectedCurrency
          if (selectedCurrencyFlag)  // Selected Fund ....
            {
            if (selectedCurrency != thisTransaction['SETTLEMENT_CCY'])
              {
              continue;
              }
            }

          // if (firstDisplayDate.getTime() > thisDatePaid.getTime())
          if ((firstDisplayDate.getTime() > thisDatePaid.getTime()) && (thisDatePaid.getFullYear() < 2100) && (thisDatePaid.getFullYear() > 2000))
            {
            // firstDisplayDate = thisDateEntered;
            firstDisplayDate = thisDatePaid;
            }

          if ((lastDisplayDate.getTime() < thisDatePaid.getTime()) && (thisDatePaid.getFullYear() < 2100))
            {
            lastDisplayDate = thisDatePaid;
            }
          }
        }
      }

    // Update gridRowTemplate
    // Add value for each payment date.

    firstDisplayDate.setHours(0, 0, 0, 0);
    lastDisplayDate.setHours(23, 59, 59, 999);
    thisDateEntered = new Date(firstDisplayDate.getTime());

    if (!grid.flexExist())
      {
      for (thisColIndex in document.appDefaults.moSettlingCashGridDefaults.initialColumnOrder)
        {
        colArray.push(jQuery.extend({},document.appDefaults.moSettlingCashGridDefaults[document.appDefaults.moSettlingCashGridDefaults.initialColumnOrder[thisColIndex]]));
        }
      }
    else
      {
      existingColSet = grid.flexGet('colModel');

      colArray = existingColSet.slice(0, document.appDefaults.moSettlingCashGridDefaults.initialColumnOrder.length);
      }


    while (thisDateEntered.getTime() <= lastDisplayDate.getTime())
      {
      thisProperty = thisDateEntered.getFullYear() + '-' + ("00" + (thisDateEntered.getMonth() + 1)).slice(-2) + '-' + ("00" + thisDateEntered.getDate()).slice(-2);
      thisCol = jQuery.extend({}, document.appDefaults.moSettlingCashGridDefaults.DEFAULT_VALUE_CELL);
      thisCol['headerText'] = thisProperty;
      thisCol['dataKey'] = thisProperty;

      gridRowTemplate[thisProperty] = 0.0;
      currencyGridRowTemplate[thisProperty] = 0.0;
      thisDateEntered.addBusDays(1);
      colArray.push(thisCol);
      }

    // Initialise currency grid array
    var currencyGridTemplate = new Array();
    var currencyGridTotal;
    var settlingGridTotal;

    currencyGridTemplate['54875474'] = jQuery.extend(jQuery.extend({}, currencyGridRowTemplate), {NAME:'Euro', SETTLEMENT_CCY:'EUR', CCY_CODE:'54876474', ROW_TYPE:document.appDefaults.rowType.summary, noSortHighlight: true, CCY_COLOUR:(useCcyColours ? document.appDefaults.currencyColours['EUR'] : '')});
    currencyGridTemplate['55001680'] = jQuery.extend(jQuery.extend({}, currencyGridRowTemplate), {NAME:'British Pounds', SETTLEMENT_CCY:'GBP', CCY_CODE:'55001680', ROW_TYPE:document.appDefaults.rowType.summary, noSortHighlight: true,  CCY_COLOUR:(useCcyColours ? document.appDefaults.currencyColours['GBP'] : '')});
    currencyGridTemplate['55923524'] = jQuery.extend(jQuery.extend({}, currencyGridRowTemplate), {NAME:'US Dollars', SETTLEMENT_CCY:'USD', CCY_CODE:'55923524', ROW_TYPE:document.appDefaults.rowType.summary, noSortHighlight: true,  CCY_COLOUR:(useCcyColours ? document.appDefaults.currencyColours['USD'] : '')});
    currencyGridTemplate['55201881'] = jQuery.extend(jQuery.extend({}, currencyGridRowTemplate), {NAME:'Japanese Yen', SETTLEMENT_CCY:'JPY', CCY_CODE:'55201881', ROW_TYPE:document.appDefaults.rowType.summary, noSortHighlight: true,  CCY_COLOUR:(useCcyColours ? document.appDefaults.currencyColours['JPY'] : '')});
    currencyGridTemplate['54741062'] = jQuery.extend(jQuery.extend({}, currencyGridRowTemplate), {NAME:'Swiss Francs', SETTLEMENT_CCY:'CHF', CCY_CODE:'54741062', ROW_TYPE:document.appDefaults.rowType.summary, noSortHighlight: true,  CCY_COLOUR:(useCcyColours ? document.appDefaults.currencyColours['CHF'] : '')});
    currencyGridTemplate['54613316'] = jQuery.extend(jQuery.extend({}, currencyGridRowTemplate), {NAME:'Australian Dollars', SETTLEMENT_CCY:'AUD', CCY_CODE:'54613316', ROW_TYPE:document.appDefaults.rowType.summary, noSortHighlight: true,  CCY_COLOUR:(useCcyColours ? document.appDefaults.currencyColours['AUD'] : '')});
    currencyGridTemplate['55788875'] = jQuery.extend(jQuery.extend({}, currencyGridRowTemplate), {NAME:'Swedish Krona', SETTLEMENT_CCY:'SEK', CCY_CODE:'55788875', ROW_TYPE:document.appDefaults.rowType.summary, noSortHighlight: true,  CCY_COLOUR:(useCcyColours ? document.appDefaults.currencyColours['SEK'] : '')});

    currencyGridTemplate['EUR'] = jQuery.extend(jQuery.extend({}, currencyGridRowTemplate), {NAME:'Euro', SETTLEMENT_CCY:'EUR', CCY_CODE:'54876474', ROW_TYPE:document.appDefaults.rowType.summary, noSortHighlight: true, CCY_COLOUR:(useCcyColours ? document.appDefaults.currencyColours['EUR'] : '')});
    currencyGridTemplate['GBP'] = jQuery.extend(jQuery.extend({}, currencyGridRowTemplate), {NAME:'British Pounds', SETTLEMENT_CCY:'GBP', CCY_CODE:'55001680', ROW_TYPE:document.appDefaults.rowType.summary, noSortHighlight: true,  CCY_COLOUR:(useCcyColours ? document.appDefaults.currencyColours['GBP'] : '')});
    currencyGridTemplate['USD'] = jQuery.extend(jQuery.extend({}, currencyGridRowTemplate), {NAME:'US Dollars', SETTLEMENT_CCY:'USD', CCY_CODE:'55923524', ROW_TYPE:document.appDefaults.rowType.summary, noSortHighlight: true,  CCY_COLOUR:(useCcyColours ? document.appDefaults.currencyColours['USD'] : '')});
    currencyGridTemplate['JPY'] = jQuery.extend(jQuery.extend({}, currencyGridRowTemplate), {NAME:'Japanese Yen', SETTLEMENT_CCY:'JPY', CCY_CODE:'55201881', ROW_TYPE:document.appDefaults.rowType.summary, noSortHighlight: true,  CCY_COLOUR:(useCcyColours ? document.appDefaults.currencyColours['JPY'] : '')});
    currencyGridTemplate['CHF'] = jQuery.extend(jQuery.extend({}, currencyGridRowTemplate), {NAME:'Swiss Francs', SETTLEMENT_CCY:'CHF', CCY_CODE:'54741062', ROW_TYPE:document.appDefaults.rowType.summary, noSortHighlight: true,  CCY_COLOUR:(useCcyColours ? document.appDefaults.currencyColours['CHF'] : '')});
    currencyGridTemplate['AUD'] = jQuery.extend(jQuery.extend({}, currencyGridRowTemplate), {NAME:'Australian Dollars', SETTLEMENT_CCY:'AUD', CCY_CODE:'54613316', ROW_TYPE:document.appDefaults.rowType.summary, noSortHighlight: true,  CCY_COLOUR:(useCcyColours ? document.appDefaults.currencyColours['AUD'] : '')});
    currencyGridTemplate['SEK'] = jQuery.extend(jQuery.extend({}, currencyGridRowTemplate), {NAME:'Swedish Krona', SETTLEMENT_CCY:'SEK', CCY_CODE:'55788875', ROW_TYPE:document.appDefaults.rowType.summary, noSortHighlight: true,  CCY_COLOUR:(useCcyColours ? document.appDefaults.currencyColours['SEK'] : '')});

    currencyGridTemplate['0'] = jQuery.extend(jQuery.extend({}, currencyGridRowTemplate), {NAME:'Others', SETTLEMENT_CCY:'', CCY_CODE:'0', CCY_COLOUR:document.appDefaults.currencyColours.ccy_default, noSortHighlight: true});
    currencyGridTotal = jQuery.extend(jQuery.extend({}, currencyGridRowTemplate), {NAME:'Total :-', SETTLEMENT_CCY:'EUR', CCY_CODE:'54876474', ROW_TYPE:document.appDefaults.rowType.footer, noSortHighlight: true,  CCY_COLOUR:(useCcyColours ? document.appDefaults.currencyColours['EUR'] : '')});
    settlingGridTotal = jQuery.extend(jQuery.extend({}, currencyGridRowTemplate), {NAME:'Total :-', SETTLEMENT_CCY:'EUR', CCY_CODE:'54876474', ROW_TYPE:document.appDefaults.rowType.footer, noSortHighlight: true,  CCY_COLOUR:(useCcyColours ? document.appDefaults.currencyColours['EUR'] : '')});

    // document.appDefaults.moSettlingCashColumns = colArray;
    // Add transaction details.
    var FXIndex;
    var thisSettlementCurrency;

    if (filteredTransactionsArray.length > 0)
      {
      for (transactionCounter = 0; transactionCounter < filteredTransactionsArray.length; transactionCounter++)
        {
        thisGridRow = jQuery.extend({}, gridRowTemplate);
        thisTransaction = filteredTransactionsArray[transactionCounter];
        thisSettlementCurrency = thisTransaction['SETTLEMENT_CCY'];

        // thisDate = new Date(thisTransaction['DATESETTLED']);
        // thisProperty = thisDate.getFullYear() + '-' + ("00" +
        // (thisDate.getMonth() + 1)).slice(-2) + '-' + ("00" +
        // thisDate.getDate()).slice(-2);
        thisProperty = thisTransaction['DATESETTLED'];

        thisGridRow['RN'] = transactionCounter;
        thisGridRow['OPCVM'] = thisTransaction['OPCVM'];
        thisGridRow['INSTANCE'] = thisTransaction['INSTANCE'];
        thisGridRow['PAM_FUND_NAME'] = folioNameLookup[folioToplevelLookup[thisTransaction['OPCVM']+":"+thisTransaction['INSTANCE']]];
        thisGridRow['REFCON'] = thisTransaction['REFCON'];
        thisGridRow['ISIN'] = thisTransaction['ISIN'];
        thisGridRow['NAME'] = thisTransaction['LIBELLE'];
        thisGridRow['TYPE'] = thisTransaction['TYPE'];
        thisGridRow['PRICE'] = thisTransaction['PRICE'];
        thisGridRow['BO_STATUS'] = thisTransaction['BO_STATUS'];
        thisGridRow['DATE_ENTERED'] = thisTransaction['MIN_DATENEG'];
        thisGridRow['DATE_PAID'] = thisTransaction['DATESETTLED'];
        thisGridRow['DATE_PAID_MIN'] = thisTransaction['MIN_DATESETTLED'];
        thisGridRow['DATE_PAID_MAX'] = thisTransaction['MAX_DATESETTLED'];
        thisGridRow['INSTRUMENT_CCY'] = getCcyFromSophisCode(thisTransaction['INSTRUMENT_CCY']);
        thisGridRow['SETTLEMENT_CCY'] = getCcyFromSophisCode(thisSettlementCurrency);

        if (useCcyColours)
          {
          if (thisGridRow['SETTLEMENT_CCY'] in document.appDefaults.currencyColours)
            {
            thisGridRow['CCY_COLOUR'] = document.appDefaults.currencyColours[thisGridRow['SETTLEMENT_CCY']];
            }
          else
            {
            thisGridRow['CCY_COLOUR'] = document.appDefaults.currencyColours.ccy_default;
            }
          }

        // thisGridRow['FX_RATE'] = thisTransaction['FX_RATE'];

        if (thisGridRow['SETTLEMENT_CCY'] == 'EUR')
          {
          thisGridRow['FX_DATE'] = '';
          thisGridRow['FX_RATE'] = 1.0;
          thisGridRow[thisProperty] = parseFloat(thisTransaction['CONSIDERATION']);
          }
        else
          {
          FXIndex = findSettlingCashFXIndex(thisSettlementCurrency, thisTransaction['DATESETTLED'], FxEntries);

          if (FXIndex.length > 0)
            {
            thisGridRow['FX_DATE'] = FxEntries[FXIndex]['FX_DATE'];
            thisGridRow['FX_RATE'] = FxEntries[FXIndex]['FX_RATE'];
            }
          else
            {
            thisGridRow['FX_DATE'] = thisTransaction['FX_DATE']; // not in Venice Query ..... TODO ?
            thisGridRow['FX_RATE'] = thisTransaction['FX_RATE'];
            }

          // thisGridRow[thisProperty] = parseFloat(thisTransaction['CONSIDERATION']) /
          // parseFloat(thisGridRow['FX_RATE']);
          thisGridRow[thisProperty] = parseFloat(thisTransaction['CONSIDERATION']); // Show // Local CCY on grid.
          }

        // Currency Totals.

        if (!workingCurrencyGridArray[thisSettlementCurrency])
          {
          if (currencyGridTemplate[thisSettlementCurrency])
            {
            workingCurrencyGridArray[thisSettlementCurrency] = (currencyGridTemplate[thisSettlementCurrency]);
            }
          else
            {
            workingCurrencyGridArray[thisSettlementCurrency] = (jQuery.extend({NAME:getCcyFromSophisCode(thisTransaction['INSTRUMENT_CCY']), SETTLEMENT_CCY:getCcyFromSophisCode(thisTransaction['INSTRUMENT_CCY']), CCY_CODE:thisSettlementCurrency}, currencyGridTemplate['0']));
            }
          }

        workingCurrencyGridArray[thisSettlementCurrency][thisProperty] += parseFloat(thisTransaction['CONSIDERATION']);
        currencyGridTotal[thisProperty] += parseFloat(thisTransaction['CONSIDERATION']) * parseFloat(thisGridRow['FX_RATE']); // TODO : Change FX / to * for Venice ?

        // selectedCurrency
        if ((!selectedCurrencyFlag) || (selectedCurrency == thisSettlementCurrency))
          {
          settlingGridTotal[thisProperty] += parseFloat(thisTransaction['CONSIDERATION']) * parseFloat(thisGridRow['FX_RATE']); // TODO : Change FX / to * for Venice ?
          gridArray.push(thisGridRow);
          }
        }
      }

    var thisRow;
    for (thisRow in workingCurrencyGridArray)
      {
      currencyGridArray.push(workingCurrencyGridArray[thisRow]);
      }

    currencyGridArray.push(currencyGridTotal);

    // Show Grid(s)

    cacheObject.data.gridDataObject = gridArray;

    if (dialogID.length > 0)
      {
      var gridID2 = dialogID + '_grid2';
      // var splitterID = dialogID + '_splitter';
      // var splitterLeftID = dialogID + '_splitterLeft';
      var grid2 = $("#" + gridID2);

      // Option 2.

      gridArray.push(settlingGridTotal);


      //gridArray.push(jQuery.extend({}, currencyGridRowTemplate));
      gridArray.push({ROW_TYPE:document.appDefaults.rowType.spacer, noSortHighlight: true}); // Blank line

      for (thisRow in workingCurrencyGridArray)
        {
        gridArray.push(workingCurrencyGridArray[thisRow]);
        }
      gridArray.push(currencyGridTotal);

      for (thisRow in colArray)
        {
        if ('aggregate' in colArray[thisRow])
          {
          delete colArray[thisRow]['aggregate'];
          }
        if ('footerText' in colArray[thisRow])
          {
          delete colArray[thisRow]['footerText'];
          }
        }

      document.dialogArray[dialogID]['GRID2_HEIGHT'] = 1;
      document.buildingDialogID = dialogID;


      if (!grid.flexExist())
        {
        grid.flexigrid({
          jsArray:gridArray,
          colModel:colArray,
          rowStyleFormatter:moSettlingCashGridRowFormatter,
          cellStyleFormatter:moSettlingCashGridCellFormatter,
          sortall:true,
          gridSortFunction:settlingCashGridQuickSort,
          width:600,
          height:400})
        }
      else
        {
        // Need to re-build the grid if the columns change.
        // Optimise this.....
        // TODO
        existingColSet = grid.flexGet('colModel');
        var doWholeUpdate = false;

        try
          {
          if (existingColSet != undefined)
            {
            if (existingColSet.length == colArray.length)
              {
              for (thisIndex = 0; thisIndex < colArray.length; thisIndex++)
                {
                var thisProp;

                for (thisProp in colArray[thisIndex])
                  {
                  if (!((thisProp in existingColSet[thisIndex]) && (colArray[thisIndex][thisProp]==existingColSet[thisIndex][thisProp])))
                    {
                    doWholeUpdate = true;
                    break;
                    }
                  }
                if (doWholeUpdate) break;
                }
              }
            else {doWholeUpdate = true;}
            }
          else {doWholeUpdate = true;}
          }
        catch (e)
          {
          // Ignore these errors
          }

        if (doWholeUpdate)
          {
          grid.flexigrid({
            dataType:'json',
            jsArray:gridArray,
            colModel:colArray,
            rowStyleFormatter:moSettlingCashGridRowFormatter,
            cellStyleFormatter:moSettlingCashGridCellFormatter,
            sortall:true,
            gridSortFunction:settlingCashGridQuickSort,
            width:600,
            height:400})
          }
        else
          {
          grid.flexAddData({jsArray:gridArray});
          }
        }

      // JQuery Hover event

      // Mouse events configured once globally in initialisation.
      // $(document).on({mouseenter:moTransactionsHoverIn, mouseleave:moTransactionsHoverOut}, ".dialogDiv .gridPopupClass");

      // Resize Grid, if it exists. Fit to the right splitter panel.

      var splitterLeft = $("#" + dialogID + '_splitterLeft');
      var headingLeft = $("#" + dialogID + '_leftHeading');
      var topGap = 40;
      var grid2Height = (document.dialogArray[dialogID]['GRID2_HEIGHT'] ? document.dialogArray[dialogID]['GRID2_HEIGHT'] : 100);

      if (headingLeft[0])
        {
        topGap = headingLeft[0].clientHeight;
        }

      if ((splitterLeft[0]) && (grid[0]))
        {
        grid.flexSize({width:(dialogDiv[0].clientWidth), height:(dialogDiv[0].clientHeight - topGap - grid2Height)});
        }
      if ((splitterLeft[0]) && (grid2[0]))
        {
        grid2.flexSize({width:(dialogDiv[0].clientWidth), height:(grid2Height)});
        }
      }

    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
    }


  }

function findSettlingCashFXIndex(requiredFX, requiredDate, FxEntries)
  {
  // Function assumes that FXs are sorted by instrument and Date ascending.

  try
    {
    if (FxEntries.length <= 0)
      {
      return '';
      }

    var fxIndex;
    var bestIndexSoFar = '';
    var IndexCount = 0;
    var thisFX;
    var fxFound = false;

    for (fxIndex in FxEntries)
      {
      thisFX = FxEntries[fxIndex];

      if (thisFX['CURRENCYCODE'] == requiredFX)
        {
        if (!fxFound) bestIndexSoFar = fxIndex;

        if (thisFX['FX_DATE'] > requiredDate)
          {
          return bestIndexSoFar;
          }

        bestIndexSoFar = fxIndex;
        fxFound = true;
        }

      if ((IndexCount > 0) && (fxFound) && (thisFX['CURRENCYCODE'] != requiredFX))
        {
        return bestIndexSoFar;
        }
      }
    return bestIndexSoFar;
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
    }
  return '';
  }

function moSettlingCashGridRowFormatter(args)
  {
  var wg = $.addFlex;

  var compareFlags;
  try
    {
    var rowTag;
    var dataRow;

    if (args.state === wg.renderState.rendering)
      {
      if (args.type & wg.rowType.data)
        {

        if (args.data)
          {
          dataRow = args.data;

          // Real Data Row

          // args.$rows.dblclick(function (e)
          // {
          // // dblClickShowTransactionsGrid(e, rowTag);
          // });

          args.$rows.contextmenu(function (e)
          {
          e.preventDefault();
          // rtClickShowTransactionsContext(e, rowTag);
          return false;
          });

          if (dataRow['ROW_TYPE'] & document.appDefaults.rowType.footer)
            {
            args.$rows.removeClass('');

            args.$rows.css({background:document.appDefaults.gridColours.footerBarBackground});
            }
          }
        else
          {
          // Disable context menu for header and footer rows.
          args.$rows.contextmenu(function (e)
          {
          e.preventDefault();
          return false;
          });
          }
        }
      else if (args.type & wg.rowType.header)
        {
        rowTag = {
          DIALOGID:document.buildingDialogID};

        args.$rows.contextmenu(function (e)
        {
        e.preventDefault();
        rtClickShowMoSettlingCashColumnsContext(e, rowTag);
        return false;
        });

        }
      else
        {
        // Disable context menu for header and footer rows.
        args.$rows.contextmenu(function (e)
        {
        e.preventDefault();
        return false;
        });
        }
      }
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
    }
  }

function rtClickShowMoSettlingCashColumnsContext(e, rowTag)
  {
  try
    {
    document.currentContextData = rowTag;

    var index;

    // Only a single context menu is used for the Transactions Grids.

    var menu = $('#bodyDiv .fsContextMenu');
    var menuList = $('#bodyDiv .fsContextMenu ul');

    // Clear existing entries.

    menuList.empty();
    var menuText;

    var realGridCols = $("#" + rowTag.DIALOGID + '_grid')[0].p.colModel;
    var allCols = [];

    for (index = 0; index < realGridCols.length; index++)
      {
      allCols.push({colIndex:realGridCols[index].cid, headerText:realGridCols[index].headerText, dataKey:realGridCols[index].dataKey, visible:realGridCols[index].visible});
      }

    allCols.sort(
      function (a, b)
      {
      if (a.headerText < b.headerText) return -1;
      if (a.headerText > b.headerText) return 1;
      return 0;
      });

    if (allCols.length > 0)
      {
      var alwaysHidden = document.appDefaults.moSettlingCashGridDefaults.alwaysHidden;
      var initialColumnOrder = document.appDefaults.moSettlingCashGridDefaults.initialColumnOrder;

      for (index in allCols)
        {
        if (($.inArray(allCols[index].dataKey, initialColumnOrder) >= 0) && ($.inArray(allCols[index].dataKey, alwaysHidden) < 0)) // only
        // show
        // cols
        // in
        // 'initialColumnOrder[]',
        // but
        // not
        // always
        // hidden
          {
          menuText = allCols[index].headerText;
          if (menuText.length > 0)
            {
            menuList.append('<li colIndex="' + allCols[index].colIndex + '" colVisible="' + (allCols[index].visible ? '1' : '0') + '"><a style="margin-left:20px; position:relative;"><span class="ui-icon ui-icon-check" style="position:absolute; top: 0; left: -20px; display:' + (allCols[index].visible ? 'inline' : 'none') + ';"></span>' + menuText + '</a></li>');
            }

          }
        }

      $('li', menuList).click(function (e)
      {
      moSettlingCashHeaderContextClick(e, rowTag.DIALOGID);
      });

      }
    else
      {
      menuList.append('<li><a class="">There do not appear to be any columns.</a></li>');
      }

    menu.css({top:e.pageY - 10, left:e.pageX - 10, 'z-index':20000});
    menu.scrollTop(0);
    menu.show();

    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
    }
finally
    {
    RefreshTokenExpiry(document.dakotaUserID, document.dakotaSessionKey);
    }

  return false;
  }

function moSettlingCashHeaderContextClick(e, dialogID)
  {
  try
    {
    // $('#bodyDiv .fsContextMenu').hide();

    var gridID = dialogID + '_grid';
    var thisLI = $(e.target);
    var index = thisLI.attr('colIndex');
    var colVisible = thisLI.attr('colVisible');
    var newVisibleState = true;

    if (index == null)
      {
      thisLI = $(e.target).parents('li');
      index =  thisLI.attr('colIndex');
      colVisible = thisLI.attr('colVisible');
      }

    if (DEBUG_FLAG) console.log('Toggling col index=' + index);

    if (colVisible == '1')
      {
      newVisibleState = false;
      }

    thisLI.attr('colVisible', (newVisibleState ? "1" : "0"));
    $('span', thisLI).css("display", (newVisibleState ? "inline" : "none"));

    $("#" + gridID).flexToggleCol(index, newVisibleState);
    // moSettlingCashSaveColumnInfo(dialogID);
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
    }
  }

function moSettlingCashGridCellFormatter(args)
  {
  var wg = $.addFlex;
  var gridClasses = document.appDefaults.moSettlingCashGridClasses;
  var compareFlags;
  try
    {
    // var rowTag;
    var thisRowType;

    if (args.state === wg.renderState.rendering)
      {
      // rowTag = {DIALOGID:document.buildingDialogID};

      if (args.row.type & wg.rowType.data)
        {
        var dataRow = args.row.data; // this.jsArray[args.row.dataRowIndex];
        thisRowType = dataRow['ROW_TYPE'];

        // If this cell is the settlement value....
        if ((args.column.dataKey == dataRow['DATE_PAID']) || ((thisRowType > 0) && (args.column.valueCell)))
          {
          try
            {

            if (dataRow['CCY_COLOUR'].length == 0)
              {
              if (dataRow[args.column.dataKey] < 0)
                {
                args.$cell.css("color", 'red');
                }
              }
            else
              {
              args.$cell.css("color", dataRow['CCY_COLOUR']);
              }
            }
          catch (e)
            {
            console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
            }

          if (thisRowType == 0) // Ordinary Row
            {
            args.$cell.addClass(document.appDefaults.moSettlingCashGridClasses.SETTLING_VALUE_CELL);
            }
          }

        if ((args.column.dataKey == document.appDefaults.todaysDateString) && (thisRowType == 0))
          {
          args.$cell.css("background-color", MO_SETTLING_CASH_TODAY_ROW);
          }

        if ((thisRowType == 0) && (args.column.dataKey == 'DATE_PAID'))
          {
          if ((dataRow['DATE_PAID'] != dataRow['DATE_PAID_MIN']) || (dataRow['DATE_PAID'] != dataRow['DATE_PAID_MAX']))
            {
            args.$cell.css("color", MO_TRANSACTION_HIGHLIGHT_COLOUR);
            args.$cell.addClass(document.appDefaults.moSettlingCashGridClasses.MOVED_DATENEG_CELL);
            }
          }

        if ((thisRowType == 0) && (args.column.dataKey in gridClasses))
          {
          args.$cell.addClass(gridClasses[args.column.dataKey]);
          }
        }
      else if (args.row.type & wg.rowType.footer)
        {
        if ((!(args.column.dataKey in document.appDefaults.moSettlingCashGridDefaults)) && (args.column.dataFormatString == 'n0'))
          {
          if (parseFloat(args.$cell.text()) < 0)
            {
            args.$cell.css("color", 'red');
            }
          }
        }
      }
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
    }
  }

function moSettlingHoverIn(e)
  {
  try
    {
    var dialogID = $(this).parents('.dialogDiv').attr('id');
    var classes = $(this).attr('class');

    var dataArrayIndex = parseInt($(this).siblings('.rnClass').find('span').html());
    var cachekey = MO_TRANSACTION_CACHE_PREFIX + dialogID;
    var cacheObject = document.dataCache[cachekey];

    if (DEBUG_FLAG) console.log('Hover In : ' + dataArrayIndex);

    if (cacheObject.timeout)
      {
      clearTimeout(cacheObject.timeout);
      if (DEBUG_FLAG) console.log('  Clear Timeout : ' + cacheObject.dataArrayIndex);
      }

    $('div.fsTooltip').hide();
    if (document.dataCache[cachekey])
      {
      var contextVariant = classes;

      cacheObject.timeout = setTimeout("moSettlingTooltip('" + dialogID + "', " + dataArrayIndex + ", '" + contextVariant + "')", MO_TRANSACTION_TOOLTIP_DELAY);
      cacheObject.dataArrayIndex = dataArrayIndex;
      if (DEBUG_FLAG) console.log('  Set Timeout : ' + dataArrayIndex);

      }
    }
  catch (err)
    {
    var debug = 0;
    console.log(err.message + ', ' + err.fileName + '(line, '+ err.lineNumber+')');
    }

  }

function moSettlingTooltip(dialogID, dataArrayIndex, contextInfo)
  {
  // contextInfo will contain the classes to which the relevant element
  // belongs.
  var menu = $('#fsToolTip');
  var cachekey = MO_TRANSACTION_CACHE_PREFIX + dialogID;
  var cacheObject;
  var html;
  var folioToplevelLookup = document.dataCache.folioListArray.folioToplevelLookup;  // Match folio accounts to top-level accounts, as used in the drop-down Fund list.
  var folioNameLookup = document.dataCache.folioListArray.folioNameLookup;  // Match top-level accounts to names
  var dataArray = false;

  if (DEBUG_FLAG) console.log('moSettlingTooltip : ' + dataArrayIndex);

  try
    {

    if (document.dataCache[cachekey])
      {
      cacheObject = document.dataCache[cachekey];
      dataArray = cacheObject.data.gridDataObject;

      if (cacheObject.dataArrayIndex != dataArrayIndex)
        {
        if (DEBUG_FLAG) console.log('  Match IDs do not match, exit : ' + cacheObject.dataArrayIndex + ', ' + dataArrayIndex);
        return;
        }

      cacheObject.timeout = false;

      var thisItem;

      // Default tool tips text...

      html = folioNameLookup[folioToplevelLookup[dataArray[dataArrayIndex]['OPCVM']+":"+dataArray[dataArrayIndex]['INSTANCE']]] + '<br/>' +
        dataArray[dataArrayIndex]['NAME'] + '<br/>';

      // Deal with non-default tool tips :-

      if (nz(contextInfo, '').length > 0)
        {
        var classes = contextInfo.split(' ');
        var thisItem;

        if ((dataArray) && (classes.indexOf('gridSettlingValueClass') >= 0))
          {
          thisItem = dataArray[dataArrayIndex];

          html = '<div style="height: 18px;">' +
            '<a href="#" class="ui-corner-all print-link" role="button" printtable="#tooltipTable">' +
            '<span printtable="#tooltipTable" class="ui-icon ui-icon-document"></span></a></div>';

          html += '<table id="tooltipTable">';
          var indexText;

          for (thisIndex in thisItem)
            {
            html += '<tr>';
            indexText = thisIndex + '                    ';
            indexText = indexText.substr(0, Math.max(20, thisIndex.length));

            if (thisItem[thisIndex] == null)
              {
              html += '<td>' + indexText + '</td><td> : null</td>';
              }
            else
              {
              html += '<td>' + indexText + '</td><td> : ' + thisItem[thisIndex].toString() + '</td>';
              }
            html += '</tr>';
            } // for
          html += '</table>'
          html += '<div style="clear: both;"></div>';

          }
        } // if contextInfo
      } // if Datacache
    } // try
  catch (e)
    {
    html = 'An Error occurred : ' + e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')';
    console.log(html);
    }
  var test = $('#' + dialogID);

  menu.css({top:document.mouseY - 10, left:document.mouseX + 30, 'z-index':20000});
  menu.html(html);
  menu.find('.print-link').click(fnSimpleGridPrint);
  menu.show();

  if ((document.mouseX + 40 + menu[0].clientWidth) > window.innerWidth)
    {
    var lastWidth;

    do
      {
      lastWidth = menu[0].clientWidth;

      menu.css({left:(document.mouseX - (menu[0].clientWidth + 30))});
      menu.show();
      }
    while (lastWidth != menu[0].clientWidth)

    }

  if ((document.mouseY + menu[0].clientHeight) > window.innerHeight)
    {
    var lastHeight;

    do
      {
      lastHeight = menu[0].clientHeight;

      menu.css({top:(window.innerHeight - (menu[0].clientHeight + 20))});
      menu.show();
      }
    while (lastHeight != menu[0].clientHeight)

    }
  }

function moSettlingHoverOut(e)
  {
  try
    {
    var dialogID = $(this).parents('.dialogDiv').attr('id');
    var dataArrayIndex = parseInt($(this).siblings('.rnClass').find('span').html());
    var cachekey = MO_TRANSACTION_CACHE_PREFIX + dialogID;
    var cacheObject;
    var matchRow;

    if (DEBUG_FLAG) console.log('Hover Out : ' + dataArrayIndex);

    if (document.dataCache[cachekey])
      {
      cacheObject = document.dataCache[cachekey];

      try
        {
        // Only clear timeout if 1) The timeout exists and 2) it was set by this
        // row.
        if (cacheObject.dataArrayIndex == dataArrayIndex)
          {
          if (cacheObject.timeout)
            {
            if (DEBUG_FLAG) console.log('  Clear Timeout : ' + dataArrayIndex);
            clearTimeout(cacheObject.timeout);
            }

          $('div.fsTooltip').hide();
          }

        }
      catch (e)
        {
        console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
        }
      finally
        {
        cacheObject.timeout = false;
        }
      }
    }
  catch (err)
    {
    console.log(err.message + ', ' + err.fileName + '(line, '+ err.lineNumber+')');
    }
  }

// function moSettlingCashGridColumnEvent(e, args)
// {
// try
// {
// var dialogID = $(this).parents('.dialogDiv').attr('id');
//
// moveOrSizeGridColumnEvent(e, args, (dialogID + '_grid'),
// document.appDefaults.moSettlingCashColumns);
//
// moTransactionsSaveColumnInfo(dialogID);
// }
// catch (e)
// {
// console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
// }
//
// }
//
// function moSettlingCashSaveColumnInfo(dialogID)
// {
// try
// {
//
// }
// catch (e)
// {
// console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
// }
// }
