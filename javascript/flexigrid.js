/*
 * Flexigrid for jQuery -  v1.1
 *
 * Copyright (c) 2008 Paulo P. Marinas (code.google.com/p/flexigrid/)
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://jquery.org/license
 *
 * Extensively modified, Nicholas Pennington, July-October 2012.
 * No AJAX, no Paging. Takes data from JS Array. Grid is much faster. Tree view functionality.
 *
 * colDef properties :
 *     headerText       :
 *     dataKey          :
 *     width            :
 *     dataType         : 'string', 'number', 'numeric', 'boolean', 'bool', 'date'
 *     visible          : [true | false]
 *     col_class        : string - classes to added to cells in this column
 *     dataFormatString : format string to use.
 *     negativeColour   : Foreground Colour to use for negative numbers
 *     noCellFormatting : if true, cellFormatter is not called (for performance)
 *     sortable         : [true | false]
 *     footerText       : Text to appear in footer row.    {sum:'{0}', min:'{1}', max:'{2}', average:'{3}'}
 *     zeroIsBlank      : treat zero numeric values as blank.
 *     noSortHighlight  : Don't highlight this column when sorting on it.
 *
 * Update Log :
 *
 * 20 Dec 2012 - Fix Min / Max footer summation.
 * 20 Dec 2012 - Add flexScroll.
 * 20 Dec 2012 - Add negativeColour column property.
 * 02 Jan 2013 - Working on TreeView.
 * 03 Jan 2013 - Shallow copy grid data to stop sort etc affecting the original data.
 * 03 Jan 2013 - Grid sort column appended to treeView sort
 * 03 Jan 2013 - <tr> attributes 'dataindex' and 'displayindex' reflect data arrays held in the internal grid arrays : jsArray and displayData
 * 09 Jan 2013 - Add functions to Toggle or Set TreeView group line status (open or colapsed)
 * 09 Jan 2013 - fix to treeTotalsAtStart parameter.
 *
 * Missed a load...
 *
 * 24 May 2013 - fix Select-Text-while-drag-column problem. Used flexNoSelect CSS class.
 * 24 May 2013 - Gave column label higher z-index during drag so that it can be seen.
 *
 */
(function ($)
  {
//  jQuery.fn.removeAll = function() {
//  this.each(function() {
//  var newEl = this.cloneNode(false);
//  this.parentNode.replaceChild(newEl, this);
//
//  //Copy back events if they haven't been copied already by IE
//  //  if(jQuery.support.noCloneEvent) {
//  //  clonecopyevent($(this), $(newEl));
//  //  }
//  });
//  };

  if (document.is_chrome == undefined) document.is_chrome = navigator.userAgent.indexOf('Chrome') > -1;
  if (document.is_explorer == undefined) document.is_explorer = navigator.userAgent.indexOf('MSIE') > -1;
  if (document.is_firefox == undefined) document.is_firefox = navigator.userAgent.indexOf('Firefox') > -1;
  if (document.is_safari == undefined) document.is_safari = navigator.userAgent.indexOf("Safari") > -1;
  if (document.is_Opera == undefined) document.is_Opera = navigator.userAgent.indexOf("Presto") > -1;

  $.addFlex = function (t, p)
    {
    if (t.grid)
      {
      $(t).empty();
      $(t.grid.gDiv).before(t);
      $(t.grid.gDiv).remove();
      delete t.grid;
      delete t.p;
      $(t).flexigrid(p);
      return;
      } //return if already exist

    p = $.extend({ //apply default properties
      height:100, //default height
      width:100, //auto width
      striped:false, //apply odd even stripes
      novstripe:false,
      minwidth:1, //min width of columns
      minheight:20, //min height of columns
      resizable:false, //allow table resizing
      noRowSelect:false, // disable select of rows
      singleSelect:false, // Select Single row only.
      jsArray:false, // Javascript source array (NP)
      displayData:false, // Internal Use Only.
      colModel:false, // Array of columns.
      treeView:false, // Grouping Columns. Array of grouping fields, each element is an object giving Grouping field, Sort order and Display Field. i.e.  {groupby: 'XXXX', sortorder: [1: Asc | -1 Desc], display: 'YYYY', sortonly:[true|false], clearfield:[true:false]}
      treeTotalsAtStart:false,
      dataType:'json', //type of data : only 'json'
      nowrap:true,
      title:false,
      noScrollBars:false, // Do not allow scroll bars on the flexigrid.
      minColToggle:0, //minimum allowed column to be hidden - Use to create un-hideable columns to the left.
      showToggleBtn:false, //show or hide column toggle popup
      hideOnSubmit:true,
      blockOpacity:0.5,
      preProcess:false,
      gridSortFunction:defaultSort,
      rowStyleFormatter:false,
      cellStyleFormatter:false,
      sortname:'',
      sortorder:'',
      sortall:false, // default colDef 'sortable' value
      footerData:{}, // Not to be set by the user.
      noFooter:false, // Suppress automatic grid footer line (If you want grouping but no overall footer)
      onDragCol:false,
      onToggleCol:false,
      onChangeSort:false,
      onRowDoubleClick:false,
      onTreeViewClick:false,
      onSuccess:false,
      onError:false,
      _hasNegativeColour:false,
      _activeTreeCols:0
    }, p);

    $(t).show() //show if hidden
      .attr({
        cellPadding:0,
        cellSpacing:0,
        border:0
      })//remove padding and spacing

      .removeAttr('width'); //remove width properties

    //create grid class

    var g = {
      renderState:{none:0, rendering:1, current:2, hovered:4, selected:8},
      rowType:{header:1, data:2, dataAlt:4, filter:8, groupHeader:16, groupFooter:32, footer:32},
      footerFields:{sum:'{0}', min:'{1}', max:'{2}', average:'{3}'},
      hasFooter:false, // Internal flag, indicates the use of 'footerText' in colmodel.
      footerData:{},
      treeIcons:{base:'ui-icon', open_up:'ui-icon-carat-1-n', open_down:'ui-icon-carat-1-s', closed:'ui-icon-carat-1-e'},

      hset:{},

      rePosDrag:function ()
        {
        var cdleft = 0 - this.hDiv.scrollLeft;
        if (this.hDiv.scrollLeft > 0) cdleft -= Math.floor(p.cgwidth / 2);
        $(g.cDrag).css({
          top:g.hDiv.offsetTop + 1
        });
        var cdpad = this.cdpad;
        $('div', g.cDrag).hide();
        $('thead tr:first th:visible', this.hDiv).each(function ()
        {
        var n = $('thead tr:first th:visible', g.hDiv).index(this);
        var cdpos = parseInt($('div', this).width());
        if (cdleft == 0) cdleft -= Math.floor(p.cgwidth / 2);
        cdpos = cdpos + cdleft + cdpad;
        if (isNaN(cdpos))
          {
          cdpos = 0;
          }
        $('div:eq(' + n + ')', g.cDrag).css({
          'left':cdpos + 'px'
        }).show();
        cdleft = cdpos;
        });
        },
      fixHeight:function ()
        {
        var newH = $(g.bDiv).height();
        var hdHeight = $(this.hDiv).height();

        $('div', this.cDrag).height(newH + hdHeight);

        var nd = parseInt($(g.nDiv).height());
        if (nd > newH)
          {
          $(g.nDiv).height(newH).width(200);
          }
        else
          {
          $(g.nDiv).height('auto').width('auto');
          }
        $(g.block).css({
          height:newH,
          marginBottom:(newH * -1)
        });
        var hrH = g.bDiv.offsetTop + newH;
        if (p.height != 'auto' && p.resizable) hrH = g.vDiv.offsetTop;
        $(g.rDiv).css({
          height:hrH
        });
        },
      dragStart:function (dragtype, e, obj)
        { //default drag function start
        $(g.gDiv).addClass('flexNoSelect');

        if (dragtype == 'colresize')
          {//column resize
          $(g.nDiv).hide();
          $(g.nBtn).hide();
          var n = $('div', this.cDrag).index(obj);
          var ow = $('th:visible div:eq(' + n + ')', this.hDiv).width();
          $(obj).addClass('dragging').siblings().hide();
          $(obj).prev().addClass('dragging').show();
          this.colresize = {
            startX:e.pageX,
            ol:parseInt(obj.style.left),
            ow:ow,
            n:n
          };
          $('body').css('cursor', 'col-resize');
          }
        else
          {
          if (dragtype == 'vresize')
            {//table resize
            var hgo = false;
            $('body').css('cursor', 'row-resize');
            if (obj)
              {
              hgo = true;
              $('body').css('cursor', 'col-resize');
              }
            this.vresize = {
              h:p.height,
              sy:e.pageY,
              w:p.width,
              sx:e.pageX,
              hgo:hgo
            };
            }
          else
            {
            if (dragtype == 'colMove')
              {//column header drag
              $(g.nDiv).hide();
              $(g.nBtn).hide();
              this.hset = $(this.hDiv).offset();
              this.hset.right = this.hset.left + $('table', this.hDiv).width();
              this.hset.bottom = this.hset.top + $('table', this.hDiv).height();
              this.dcol = obj;
              this.dcoln = $('th', this.hDiv).index(obj);
              this.colCopy = document.createElement("div");
              this.colCopy.className = "colCopy";
              this.colCopy.innerHTML = obj.innerHTML;
              if ($.browser.msie)
                {
                this.colCopy.className = "colCopy ie";
                }
              $(this.colCopy).css({
                position:'absolute',
                float:'left',
                display:'none',
                'z-index':10000,
                textAlign:obj.align
              });
              $('body').append(this.colCopy);
              $(this.cDrag).hide();
              }
            }
          }
        // $('body').noSelect();
        },
      dragMove:function (e)
        {
        if (this.colresize)
          {//column resize
          var n = this.colresize.n;
          var diff = e.pageX - this.colresize.startX;
          var nleft = this.colresize.ol + diff;
          var nw = this.colresize.ow + diff;
          if (nw > p.minwidth)
            {
            $('div:eq(' + n + ')', this.cDrag).css('left', nleft);
            this.colresize.nw = nw;
            }
          }
        else
          {
          if (this.vresize)
            {//table resize
            var v = this.vresize;
            var y = e.pageY;
            var diff = y - v.sy;
            if (!p.defwidth) p.defwidth = p.width;
            if (p.width != 'auto' && !p.nohresize && v.hgo)
              {
              var x = e.pageX;
              var xdiff = x - v.sx;
              var newW = v.w + xdiff;
              if (newW > p.defwidth)
                {
                this.gDiv.style.width = newW + 'px';
                p.width = newW;
                }
              }
            var newH = v.h + diff;
            if ((newH > p.minheight || p.height < p.minheight) && !v.hgo)
              {
              this.bDiv.style.height = newH + 'px';
              p.height = newH;
              this.fixHeight();
              }
            v = null;
            }
          else
            {
            if (this.colCopy)
              {
              $(this.dcol).addClass('thMove').removeClass('thOver');
              if (e.pageX > this.hset.right || e.pageX < this.hset.left || e.pageY > this.hset.bottom || e.pageY < this.hset.top)
                {
                //this.dragEnd();
                $('body').css('cursor', 'move');
                }
              else
                {
                $('body').css('cursor', 'pointer');
                }
              $(this.colCopy).css({
                top:e.pageY + 10,
                left:e.pageX + 20,
                display:'block'
              });
              }
            }
          }
        },
      dragEnd:function ()
        {
        if (this.colresize)
          {
          var n = this.colresize.n;
          var nw = this.colresize.nw;
          var colIndex = $('th:visible:eq(' + n + ')', this.hDiv).attr('axis').substr(3);

          $('th:visible div:eq(' + n + ')', this.hDiv).css('width', nw);
          $('tr', this.bDiv).each(
            function ()
            {
            $('td:visible div:eq(' + n + ')', this).css('width', nw);
            }
          );

          p.colModel[colIndex].width = nw;

          this.hDiv.scrollLeft = this.bDiv.scrollLeft;
          $('div:eq(' + n + ')', this.cDrag).siblings().show();
          $('.dragging', this.cDrag).removeClass('dragging');
          this.rePosDrag();
          this.fixHeight();
          this.colresize = false;
          }
        else
          {
          if (this.vresize)
            {
            this.vresize = false;
            }
          else
            {
            if (this.colCopy)
              {
              $(this.colCopy).remove();
              if (this.dcolt != null)
                {
                if (this.dcoln > this.dcolt)
                  {
                  $('th:eq(' + this.dcolt + ')', this.hDiv).before(this.dcol);
                  }
                else
                  {
                  $('th:eq(' + this.dcolt + ')', this.hDiv).after(this.dcol);
                  }
                this.switchCol(this.dcoln, this.dcolt);
                $(this.cdropleft).remove();
                $(this.cdropright).remove();
                this.rePosDrag();
                if (p.onDragCol)
                  {
                  p.onDragCol(this.dcoln, this.dcolt);
                  }
                }
              this.dcol = null;
              this.hset = null;
              this.dcoln = null;
              this.dcolt = null;
              this.colCopy = null;
              $('.thMove', this.hDiv).removeClass('thMove');
              $(this.cDrag).show();
              }
            }
          }
        $('body').css('cursor', 'default');
        //$('body').noSelect(false);
        $(g.gDiv).removeClass('flexNoSelect');
        },
      toggleCol:function (cid, visible)
        {
        var ncol = $("th[axis='col" + cid + "']", this.hDiv)[0];
        var n = $('thead th', g.hDiv).index(ncol);
        var cb = $('input[value=' + cid + ']', g.nDiv)[0];
        var thisColModel; // = p.colModel[cid]; assumes colModel does not get re-ordered.

        for (var index in p.colModel)
          {
          if (p.colModel[index].cid == cid)
            {
            thisColModel = p.colModel[index];
            break;
            }
          }

        if (visible == null)
          {
          visible = ncol.hidden;
          }

        if ($('input:checked', g.nDiv).length < p.minColToggle && !visible)
          {
          return false;
          }
        if (visible)
          {
          ncol.hidden = false;
          $(ncol).show();
          if (cb) cb.checked = true;
          }
        else
          {
          ncol.hidden = true;
          $(ncol).hide();
          if (cb) cb.checked = false;
          }

        thisColModel.visible = visible;

        $('tbody tr', t).each(
          function ()
          {
          if (visible)
            {
            $('td:eq(' + n + ')', this).show();
            }
          else
            {
            $('td:eq(' + n + ')', this).hide();
            }
          }
        );
        this.rePosDrag();
        if (p.onToggleCol)
          {
          p.onToggleCol(cid, visible);
          }

        return visible;
        },

      expandGrid:function()
        {
        if (p.treeView )
          {
          var thisTable = $(t);
          thisTable.find('tr').css({display:''});

          var theseIcons = thisTable.find('.treeViewTotal');
          theseIcons.removeClass(g.treeIcons.closed);
          theseIcons.addClass(p.treeTotalsAtStart ? g.treeIcons.open_down : g.treeIcons.open_up);

          }
        },

      colapseGrid:function()
        {
        if (p.treeView )
          {
          var thisTable = $(t);

          var childCounter;
          var childRows = thisTable.find('._tv_top');
          for (childCounter = 0; childCounter < childRows.length; childCounter++)
            {
            g.toggleTreeViewRow(childRows.get(childCounter), 0);
            }
          }
        },

      toggleTreeViewRow:function(pElement, pState)
        {
        if (p.treeView )
          {
          var tableRow;
          var thisElement = $(pElement);

          if (thisElement.prop('tagName').toLowerCase() == 'tr')
            {
            tableRow = thisElement;
            }
          else
            {
            tableRow = $(thisElement.parents('tr')[0]);
            }

          var thisIcon = tableRow.find('.treeViewTotal');

          if (thisIcon.length > 0)
            {
            var thisTable = $(tableRow.parents('table')[0]);
            // var treeLevel = p.displayData.rows[tableRow.attr('displayindex')]['_treelevel']
            var treeCounter = tableRow.attr('treecounter');
            var state;

            if (pState == undefined)
              {
              state = thisIcon.hasClass(g.treeIcons.closed);
              }
            else
              {
              state = pState;
              }

            if (state)
              {
              // Show
              thisIcon.removeClass(g.treeIcons.closed);

              thisIcon.addClass(p.treeTotalsAtStart ? g.treeIcons.open_down : g.treeIcons.open_up);
              thisTable.find('._tv_' + treeCounter).css({display:''});
              }
            else
              {
              // Hide
              thisIcon.removeClass(p.treeTotalsAtStart ? g.treeIcons.open_down : g.treeIcons.open_up);
              thisIcon.addClass(g.treeIcons.closed);

              // Must Recursively hide sub-Groups
              var childCounter;
              var childRows = thisTable.find('._tv_' + treeCounter + '[treecounter]');
              for (childCounter = 0; childCounter < childRows.length; childCounter++)
                {
                g.toggleTreeViewRow(childRows.get(childCounter), 0);
                }
                //thisTable.find('._tv_' + treeCounter + '[treecounter]').each(g.toggleTreeViewRow(this, 0));


              thisTable.find('._tv_' + treeCounter).css({display:'none'});
              }
            }
          }
        },

      switchCol:function (cdrag, cdrop)
        { //switch columns
        $('tbody tr', t).each(
          function ()
          {
          if (cdrag > cdrop)
            {
            $('td:eq(' + cdrop + ')', this).before($('td:eq(' + cdrag + ')', this));
            }
          else
            {
            $('td:eq(' + cdrop + ')', this).after($('td:eq(' + cdrag + ')', this));
            }
          }
        );
        //switch order in nDiv
        if (cdrag > cdrop)
          {
          $('tr:eq(' + cdrop + ')', this.nDiv).before($('tr:eq(' + cdrag + ')', this.nDiv));
          }
        else
          {
          $('tr:eq(' + cdrop + ')', this.nDiv).after($('tr:eq(' + cdrag + ')', this.nDiv));
          }

        if ($.browser.msie && $.browser.version < 7.0)
          {
          $('tr:eq(' + cdrop + ') input', this.nDiv)[0].checked = true;
          }

        this.hDiv.scrollLeft = this.bDiv.scrollLeft;
        },

      scroll:function ()
        {
        this.hDiv.scrollLeft = this.bDiv.scrollLeft;
        this.rePosDrag();
        },

      addData:function (pData, repaintOnly)
        { //parse data
        var data = pData;
        var displayData;
        var colIndex;
        var tempCount;
        var thisFooterString;
        var thisFormat;
        var thisMultiplier;
        var formatLookup = {n0:'#,##0.', n1:'#,##0.01', n2:'#,##0.00', n3:'#,##0.000', n4:'#,##0.0000', p0:'#,##0.', p1:'#,##0.0', p2:'#,##0.00', p3:'#,##0.000', p4:'#,##0.0000'};
        var thisColDef;
        var activeTreeCols = 0;

        g.footerData = $.extend(true, {}, p.footerData);

        if (repaintOnly)
          {
          data = {rows:p.jsArray};
          }
        else
          {
          if (p.dataType == 'json')
            {
            if ((data != undefined) && ('jsArray' in data))
              {
              p.jsArray = data.jsArray;
              data = {rows:data.jsArray};
              }
            else
              {
              data = $.extend({rows:[]}, data);
              p.jsArray = data.rows;
              }
            }

          // Shallow copy data.rows to stop sorting etc affecting the original data
          if (p.jsArray.length > 0)
            {
            p.jsArray = p.jsArray.slice(0);
            data.rows = p.jsArray;
            }

          // pre-process if required.
          if (p.preProcess)
            {
            data = p.preProcess(data);
            }

          // Sort if necessary (not if this is TreeView, which sorts later...).

          if ((p.gridSortFunction) && (p.sortname.length > 0) && (!p.treeView))
            {
            if (p.sortorder != 'desc')
              {
              p.sortorder = 'asc';
              }

            for (colIndex in p.colModel)
              {
              if (p.colModel[colIndex].dataKey == p.sortname)
                {
                p.gridSortFunction($(t), p.colModel[colIndex], p.sortname, p.sortorder);
                if (p.onChangeSort) p.onChangeSort($(t), p.sortname, p.sortorder);
                break;
                }
              }
            }
          }

        displayData = data;

        if ((p.treeView) && (data.rows.length > 0)) // Treeview logic. Pretty much finished.
          {
          // treeView, // Grouping Columns. Array of grouping fields, each element is an object giving Grouping field, Sort order and Display Field.
          // e.g..  {groupby: 'XXXX', sortorder: [1: Asc | -1 Desc], display: 'YYYY', sortonly: [true:false], clearfield: [true:false]}

          var index;
          var rowIndex;
          var cmIndex;
          var clearIndex;
          var totalsArray = new Array();
          var viewCols = p.treeView;
          var treeCounter = 1;
          var thisTreeLevel;
          var treeClass;
          var sortCols = [];

          var displayRows = new Array();

          var OrderModifier = (p.treeTotalsAtStart ? -1 : 1);

          // Sort data for the treeView and start to initialise totalsArray[].

          thisTreeLevel = 1;
          for (index in viewCols)
            {
            /* Validate */
            viewCols[index] = $.extend(true, {groupby:'', display:viewCols[index]['groupby'], sortorder:1, sortonly:false, clearfield:false}, viewCols[index], {treelevel:thisTreeLevel});

            if (!viewCols[index]['sortonly'])
              {
              activeTreeCols++;
              thisTreeLevel++;
              }

            /* */
            if ((viewCols[index].sortorder == undefined) || (viewCols[index].sortorder == null) || (viewCols[index].sortorder >= 0))
              {
              viewCols[index].sortorder = 1; // OrderModifier; // 1
              }
            else
              {
              viewCols[index].sortorder = -1 // -OrderModifier; // -1
              }

            totalsArray[index] = [];

            /* Initialise sortCols Array */

            sortCols.push({groupby:viewCols[index].groupby, sortorder:(viewCols[index].sortorder * OrderModifier)});
            }
          p._activeTreeCols = activeTreeCols;

          /* Build field mask for the 'clearfield' property */

          var clearMask = {};

          for (clearIndex in viewCols)
            {
            if (viewCols[clearIndex]['clearfield'])
              {
              clearMask[viewCols[clearIndex]['display']] = '';
              }
            }

          // Initialise totalsArray[]

          for (colIndex in data.rows[0])
            {
            for (index in viewCols)
              {
              totalsArray[index][colIndex] = false;
              }
            }

          // Sort Data.

          if (p.sortname.length > 0)
            {
            sortCols.push({groupby:p.sortname, sortorder:(p.sortorder == 'asc' ? OrderModifier : -OrderModifier)});
            }


          data.rows.sort(
            function (a, b)
            {
            var sortIndex;

            for (sortIndex = 0 ; sortIndex < sortCols.length ; sortIndex++)
              {
              if (a[sortCols[sortIndex].groupby] < b[sortCols[sortIndex].groupby]) return (-sortCols[sortIndex].sortorder);
              if (a[sortCols[sortIndex].groupby] > b[sortCols[sortIndex].groupby]) return  sortCols[sortIndex].sortorder;
              }

            return 0;
            }
          );

          //
          treeClass = '';
          for (index in viewCols)
            {
            if (!viewCols[index]['sortonly'])
              {
              //treeClass = treeClass + ' _tv_' + treeCounter.toString(); //viewCols[viewCols.length-1]['treeCounter'];

              viewCols[index]['treeCounter'] = treeCounter++;
              }
            }
          treeClass = ' _tv_' + (treeCounter - 1); //viewCols[viewCols.length-1]['treeCounter'];

          // Build Tree Structure

          // must add _rowtype (of g.rowType) and _datarowindex (rowIndex) and _treelevel

          var first = true;
          var lastRow;
          var thisRow;
          var groupRow;

          for (rowIndex in data.rows)
            {
            thisRow = data.rows[rowIndex];

            if (first)
              {
              displayRows.push($.extend(true, {}, thisRow, {_rowtype:g.rowType.data, _datarowindex:rowIndex, _treelevel:-1, _treeClass:treeClass}, clearMask));
              first = false;
              }
            else
              {
              // TreeGrouping Changed?
              ParentTreelevel = 0;

              for (index in viewCols)
                {
                if ((thisRow[viewCols[index]['groupby']] != lastRow[viewCols[index]['groupby']]) && (!viewCols[index]['sortonly']))
                  {
                  for (colIndex = (viewCols.length - 1); colIndex >= index; colIndex--)
                    {
                    if (!viewCols[colIndex]['sortonly'])
                      {
                      groupRow = $.extend(true, {}, {_rowtype:g.rowType.groupFooter, _datarowindex:-1, _treelevel:viewCols[colIndex]['treelevel'], _reverse:OrderModifier, _treeClass:'', _treeCounter:viewCols[colIndex]['treeCounter'], noSortHighlight:true});
                      groupRow._treeClass = '_tv_top';

                      for (tempCount = (colIndex - 1); tempCount >= 0; tempCount--)
                        {
                        if ((tempCount >= 0) && (!viewCols[tempCount]['sortonly']))
                          {
                          groupRow._treeClass = '_tv_' + viewCols[tempCount]['treeCounter'];
                          break;
                          }
                        }

                      groupRow[viewCols[colIndex]['display']] = lastRow[viewCols[colIndex]['display']];

                      for (cmIndex in p.colModel)
                        {
                        if (p.colModel[cmIndex]['footerText'])
                          {
                          thisFooterString = p.colModel[cmIndex].footerText;

                          switch (p.colModel[cmIndex].dataType)
                          {
                            // totalsArray[colIndex][p.colModel[cmIndex]['dataKey']]

                            case 'bool':
                            case 'boolean':
                              //TODO

                              break;

                            case 'date':
                              // TODO

                              break;

                            case 'number':
                            case 'numeric':

                              //thisFormat = '###0.0#######';
                              thisFormat = (p.colModel[cmIndex].dataFormatString ? p.colModel[cmIndex].dataFormatString : 'n2');
                              thisMultiplier = 1.0;

                              // Adjust for percentage formats.

                              if (thisFormat.startsWith('p')) thisMultiplier = 100.0;
                              else if (thisFormat.endsWith('%')) thisMultiplier = 100.0;
                              if (thisFormat in formatLookup) thisFormat = formatLookup[thisFormat];

                              try
                                {
                                thisFooterString = thisFooterString.replace(g.footerFields.sum, format(thisFormat, (totalsArray[colIndex][p.colModel[cmIndex]['dataKey']]['sum']) * thisMultiplier));
                                thisFooterString = thisFooterString.replace(g.footerFields.min, format(thisFormat, (totalsArray[colIndex][p.colModel[cmIndex]['dataKey']]['min']) * thisMultiplier));
                                thisFooterString = thisFooterString.replace(g.footerFields.max, format(thisFormat, (totalsArray[colIndex][p.colModel[cmIndex]['dataKey']]['max']) * thisMultiplier));
                                thisFooterString = thisFooterString.replace(g.footerFields.average, format(thisFormat, ((totalsArray[colIndex][p.colModel[cmIndex]['dataKey']]['sum'] / totalsArray[index][p.colModel[cmIndex]['dataKey']]['sumcount'])) * thisMultiplier));
                                }
                              catch (e)
                                {
                                // Ignore any error here.
                                }

                              break;

                            case 'string':
                              //TODO

                              break;
                          }

                          groupRow[p.colModel[cmIndex]['dataKey']] = thisFooterString;
                          totalsArray[colIndex][p.colModel[cmIndex]['dataKey']] = false;
                          }
                        }

                      /* 'clearfield' ? */

                      if (colIndex > 0)
                        {
                        for (clearIndex = (colIndex - 1); clearIndex >= 0; clearIndex--)
                          {
                          if (viewCols[clearIndex]['clearfield'])
                            {
                            groupRow[viewCols[clearIndex]['display']] = '';
                            }
                          }
                        }

                      displayRows.push(groupRow);
                      }
                    }

                  // Update treeCounter
                  treeClass = '';
                  for (colIndex = (viewCols.length - 1); colIndex >= index; colIndex--)
                    {
                    if (!viewCols[colIndex]['sortonly'])
                      {
                      // treeClass = treeClass + ' _tv_' + treeCounter.toString();
                      viewCols[colIndex]['treeCounter'] = treeCounter++;
                      }
                    }

                  // Update treeClass
                  treeClass = ' _tv_' + viewCols[viewCols.length - 1]['treeCounter'];

                  break;
                  }
                }

              // Add Row

              displayRows.push($.extend(true, {}, thisRow, {_rowtype:g.rowType.data, _datarowindex:rowIndex, _treelevel:-1, _reverse:OrderModifier, _treeClass:treeClass}, clearMask));

              }

            // Update Aggregation Data

            for (cmIndex in p.colModel)
              {
              var thisDataKey = p.colModel[cmIndex].dataKey;

              if (p.colModel[cmIndex]['footerText'])
                {
                switch (p.colModel[cmIndex].dataType)
                {
                  case 'bool':
                  case 'boolean':

                    break;

                  case 'date':

                    break;

                  case 'number':
                  case 'numeric':
                    var thisNumber = thisRow[thisDataKey];

                    if ((thisNumber != undefined) && (thisNumber !== ''))
                      {
                      if (typeof (thisNumber) == 'string')
                        {
                        try
                          {
                          thisNumber = parseFloat(thisNumber);
                          thisRow[thisDataKey] = thisNumber;
                          }
                        catch (e)
                          {
                          thisNumber = row[thisProp.colDef.dataKey];
                          }
                        }

                      for (index in viewCols)
                        {
                        if (!viewCols[index]['sortonly'])
                          {
                          if (totalsArray[index][thisDataKey] === false)
                            {
                            totalsArray[index][thisDataKey] = {max:thisNumber, min:thisNumber, sum:thisNumber, sumcount:1};
                            }
                          else
                            {
                            totalsArray[index][thisDataKey] = {max:Math.max(thisNumber, totalsArray[index][thisDataKey].max), min:Math.min(thisNumber, totalsArray[index][thisDataKey].min), sum:thisNumber + totalsArray[index][thisDataKey].sum, sumcount:totalsArray[index][thisDataKey].sumcount + 1};
                            }
                          }
                        }

                      } // Number present.

                    break;

                  case 'string':

                    break;

                }

                }
              }

            //

            lastRow = thisRow;

            }

          // Add Final Group rows


          for (colIndex = (viewCols.length - 1); colIndex >= 0; colIndex--)
            {
            if (!viewCols[colIndex]['sortonly'])
              {

              groupRow = $.extend(true, {}, {_rowtype:g.rowType.groupFooter, _datarowindex:-1, _treelevel:viewCols[colIndex]['treelevel'], _reverse:OrderModifier, _treeClass:'', _treeCounter:viewCols[colIndex]['treeCounter'], noSortHighlight:true});
              groupRow._treeClass = '_tv_top';

              for (tempCount = (colIndex - 1); tempCount >= 0; tempCount--)
                {
                if ((tempCount >= 0) && (!viewCols[tempCount]['sortonly']))
                  {
                  groupRow._treeClass = '_tv_' + viewCols[tempCount]['treeCounter'];
                  break;
                  }
                }

              groupRow[viewCols[colIndex]['display']] = thisRow[viewCols[colIndex]['display']];

              for (cmIndex in p.colModel)
                {
                if (p.colModel[cmIndex]['footerText'])
                  {
                  thisFooterString = p.colModel[cmIndex].footerText;

                  switch (p.colModel[cmIndex].dataType)
                  {
                    // totalsArray[colIndex][p.colModel[cmIndex]['dataKey']]

                    case 'bool':
                    case 'boolean':
                      //TODO

                      break;

                    case 'date':
                      // TODO

                      break;

                    case 'number':
                    case 'numeric':

                      //thisFormat = '###0.0#######';
                      thisFormat = (p.colModel[cmIndex].dataFormatString ? p.colModel[cmIndex].dataFormatString : 'n2');
                      thisMultiplier = 1.0;

                      // Adjust for percentage formats.

                      if (thisFormat.startsWith('p')) thisMultiplier = 100.0;
                      else if (thisFormat.endsWith('%')) thisMultiplier = 100.0;
                      if (thisFormat in formatLookup) thisFormat = formatLookup[thisFormat];

                      try
                        {
                        thisFooterString = thisFooterString.replace(g.footerFields.sum, format(thisFormat, (totalsArray[colIndex][p.colModel[cmIndex]['dataKey']]['sum']) * thisMultiplier));
                        thisFooterString = thisFooterString.replace(g.footerFields.min, format(thisFormat, (totalsArray[colIndex][p.colModel[cmIndex]['dataKey']]['min']) * thisMultiplier));
                        thisFooterString = thisFooterString.replace(g.footerFields.max, format(thisFormat, (totalsArray[colIndex][p.colModel[cmIndex]['dataKey']]['max']) * thisMultiplier));
                        thisFooterString = thisFooterString.replace(g.footerFields.average, format(thisFormat, ((totalsArray[colIndex][p.colModel[cmIndex]['dataKey']]['sum'] / totalsArray[index][p.colModel[cmIndex]['dataKey']]['sumcount'])) * thisMultiplier));
                        }
                      catch (e)
                        {
                        // Ignore any error here.
                        }

                      break;

                    case 'string':
                      //TODO

                      break;
                  }

                  groupRow[p.colModel[cmIndex]['dataKey']] = thisFooterString;
                  totalsArray[colIndex][p.colModel[cmIndex]['dataKey']] = false;
                  }
                }

              /* 'clearfield' ? */
              if (colIndex > 0)
                {
                for (clearIndex = (colIndex - 1); clearIndex >= 0; clearIndex--)
                  {
                  if (viewCols[clearIndex]['clearfield'])
                    {
                    groupRow[viewCols[clearIndex]['display']] = '';
                    }
                  }
                }

              displayRows.push(groupRow);
              }
            }


          // Reverse results to put the totals lines at the top.
          if (p.treeTotalsAtStart)
            {
            displayRows.reverse();
            }

          // Save the index in DisplayRows for later.
          for (rowIndex in displayRows)
            {
            displayRows[rowIndex]['_displayindex'] = rowIndex;
            }

          displayData = {rows:displayRows};
          } // If TreeView

        p.displayData = displayData;

        if (!displayData)
          {
          return false;
          }

        if (Object.keys(displayData.rows).length == 0) // (displayData.rows.length == 0) // No Data Rows.
          {
          t.innerHTML = '';
          //$('tr, a, td, div', t).unbind('click');
          //$(t).empty();
          //$(t).removeAll();
          //$(t).html('');
          //$(t).children().detach().remove();

          return false;
          }
        // If there is only one row, ensure that it is always shown.
        if (Object.keys(displayData.rows).length == 1) // (displayData.rows.length == 1)
          {
          displayData.rows[0]['_treelevel'] = 1;
          }

        //build new body

        var tbody = document.createElement('tbody');
        var jqTbody = $(tbody);

        if (p.dataType == 'json')
          {
          var jqHeaders = $('thead tr:first th', g.hDiv);
          var headerProperties = new Array();
          var thisIndex;
          var thisProp;

          for (thisIndex = 0; thisIndex < jqHeaders.length; thisIndex++)
            {
            colIndex = $(jqHeaders[thisIndex]).attr('axis').substr(3);
            var cm = p.colModel[colIndex];

            headerProperties[thisIndex] =
            {
              width:('width: ' + $('div:first', jqHeaders[thisIndex])[0].style.width + '; '),
              hidden:(!cm.visible ? ' style="display: none;"' : ''),
              idx:colIndex,
              //abbr:$(jqHeaders[thisIndex]).attr('abbr'),
              alignStyle:('text-align:' + ((cm.align) ? cm.align : (('dataType' in cm) ? ((cm.dataType == 'string') ? 'left' : ((cm.dataType == 'boolean') || (cm.dataType == 'bool') ? 'center' : 'right')) : 'right')) + '; '),
              align:((cm.align) ? cm.align : (('dataType' in cm) ? ((cm.dataType == 'string') ? 'left' : ((cm.dataType == 'boolean') || (cm.dataType == 'bool') ? 'center' : 'right')) : 'right')),
              col_class:(((p.sortname == p.colModel[colIndex].dataKey) && (p.colModel[colIndex].noSortHighlight == false) ? 'sorted ' : '') + cm.col_class),
              col_class_noSort:cm.col_class,
              colDef:p.colModel[colIndex]
            };
            }

          for (thisIndex = 0; thisIndex < jqHeaders.length; thisIndex++)
            {
            thisProp = headerProperties[thisIndex];

            thisProp.tdHeader =
              '<td align="' + thisProp.align + '" ' +
                thisProp.hidden +
                (thisProp.col_class.length > 0 ? (' class="' + thisProp.col_class + '"') : '') +
                '>' +
                '<div style="' + ((p.treeView) ? 'position:relative; ' : '') +
                thisProp.alignStyle +
                (p.nowrap ? '' : 'white-space:normal; ') + thisProp.width + '">';

            thisProp.tdHeaderNoSorted =
              '<td align="' + thisProp.align + '" ' +
                thisProp.hidden +
                (thisProp.col_class_noSort.length > 0 ? (' class="' + thisProp.col_class_noSort + '"') : '') +
                '>' +
                '<div style="' + ((p.treeView) ? 'position:relative; ' : '') +
                thisProp.alignStyle +
                (p.nowrap ? '' : 'white-space:normal; ') + thisProp.width + '">';

            //  noSortHighlight: true,
            //  repeated later for the footer cells, (to exclude cell classes).
            }

          $.each(displayData.rows, function (i, row)
          {
          if (row == undefined) return;

          var iHtmlArray = new Array();
          var iCounter = 0;

          if (p.treeView)
            {
            iHtmlArray[iCounter++] = '<tr dataindex="';
            iHtmlArray[iCounter++] = row['_datarowindex'];
            iHtmlArray[iCounter++] = '" displayindex="';
            iHtmlArray[iCounter++] = row['_displayindex'];
            iHtmlArray[iCounter++] = '" ';
            }
          else
            {
            iHtmlArray[iCounter++] = '<tr dataindex="';
            iHtmlArray[iCounter++] = i;
            iHtmlArray[iCounter++] = '" displayindex="';
            iHtmlArray[iCounter++] = i;
            iHtmlArray[iCounter++] = '" ';
            }

          if ((p.treeView) && (row['_treelevel'] > 0))
            {
            iHtmlArray[iCounter++] = 'treeCounter="';
            iHtmlArray[iCounter++] = row['_treeCounter'];
            iHtmlArray[iCounter++] = '" ';
            }

          // Classes for striping and TreeView identification.
          if ((row) && (row['id']))
            {
            iHtmlArray[iCounter++] = 'id="row' + row.id;
            iHtmlArray[iCounter++] = '" ';
            }
          iHtmlArray[iCounter++] = 'class="';
          if (i % 2 && p.striped)
            {
            iHtmlArray[iCounter++] = 'erow ';
            }
          if ((p.treeView) && (row['_treeClass'].length > 0))
            {
            iHtmlArray[iCounter++] = row['_treeClass'];
            }
          iHtmlArray[iCounter++] = '">';


          for (thisIndex = 0; thisIndex < jqHeaders.length; thisIndex++)
            {
            thisProp = headerProperties[thisIndex];

            /* pre set in thisProp.tdHeader

             iHtml += '<td align="' + thisProp.align + '" ' +

             thisProp.hidden +
             thisProp.col_class + '>' +
             '<div style="' +
             thisProp.align  +
             (p.nowrap ? '' : 'white-space:normal; ') + thisProp.width + '">';
             */

            if (!row['noSortHighlight'])
              {
              iHtmlArray[iCounter++] = thisProp.tdHeader;
              }
            else
              {
              iHtmlArray[iCounter++] = thisProp.tdHeaderNoSorted;
              }

            // Add 'Expand' button.

            if ((p.treeView) && (row['_treelevel'] > 0) && (row['_treelevel'] == (thisIndex + 1)))
              {
              iHtmlArray[iCounter++] = '<span style="position:absolute; left:0px;" class="treeViewTotal ' + g.treeIcons.base + ' ' + g.treeIcons.closed + '"></span><span style=" padding-left:20px;">';
              }
            else if ((p.treeView) && (thisIndex < p._activeTreeCols)) // p.treeView.length))
              {
              iHtmlArray[iCounter++] = '<span style="padding-left:20px;">';
              }
            else
              {
              iHtmlArray[iCounter++] = '<span>';
              }

            if (('_rowtype' in row) && (row['_rowtype'] == g.rowType.groupFooter)) // _rowtype:g.rowType.groupFooter
              {
              iHtmlArray[iCounter++] = (nz(row[thisProp.colDef.dataKey], '').toString() == '' ? '&nbsp;' : nz(row[thisProp.colDef.dataKey], '').toString());
              }
            else
              {
              switch (thisProp.colDef.dataType)
              {
                case 'bool':
                case 'boolean':
                  // <input type="checkbox" disabled="disabled" checked="checked" class="wijgridinput">
                  if (nz(row[thisProp.colDef.dataKey], 'false') == 'true')
                    {
                    iHtmlArray[iCounter++] = '<input type="checkbox" disabled="disabled" checked="checked">';
                    }
                  else
                    {
                    iHtmlArray[iCounter++] = '<input type="checkbox" disabled="disabled">';
                    }

                  if ((g.hasFooter) && (thisProp.colDef.footerText !== false) && (!p.treeView || (row._rowtype = g.rowType.data)))
                    {
                    g.footerData[thisProp.colDef.dataKey]['max'] = (g.footerData[thisProp.colDef.dataKey]['max'] || nz(row[thisProp.colDef.dataKey], 'false'));
                    }

                  break;

                case 'date':
                  // TODO
                  iHtmlArray[iCounter++] = (nz(row[thisProp.colDef.dataKey], '').toString() == '' ? '&nbsp;' : nz(row[thisProp.colDef.dataKey], '').toString());

                  //                if ((g.hasFooter) && (thisProp.colDef.footerText !== false) && (!p.treeView || (row._rowtype = g.rowType.data)))
                  //                  {
                  //                  g.footerData[thisProp.colDef.dataKey]['max']=(g.footerData[thisProp.colDef.dataKey]['max'] || nz(row[thisProp.colDef.dataKey], 'false'));
                  //                  }
                  break;

                case 'number':
                case 'numeric':
                  var thisNumber = row[thisProp.colDef.dataKey];

                  // Values which are undefined or a blank string are shown as empty cells.

                  if ((thisNumber != undefined) && (thisNumber !== ''))
                    {
                    // Parse strings and save them to the grid data array for next time.

                    if (typeof (thisNumber) == 'string')
                      {
                      try
                        {
                        thisNumber = parseFloat(thisNumber);
                        row[thisProp.colDef.dataKey] = thisNumber;
                        }
                      catch (e)
                        {
                        thisNumber = row[thisProp.colDef.dataKey];
                        }
                      }

                    if ((!thisProp.colDef.zeroIsBlank) || (thisNumber != 0))
                      {

                      // Get format..

                      var thisFormat = (thisProp.colDef.dataFormatString ? thisProp.colDef.dataFormatString : 'n2');
                      var thisMultiplier = 1.0;

                      // Adjust for percentage formats.

                      if (thisFormat.startsWith('p')) thisMultiplier = 100.0;
                      else if (thisFormat.endsWith('%')) thisMultiplier = 100.0;
                      if (thisFormat in formatLookup) thisFormat = formatLookup[thisFormat];

                      // Display formatted number.

                      try
                        {
                        iHtmlArray[iCounter++] = format(thisFormat, (thisNumber * thisMultiplier));
                        }
                      catch (e)
                        {
                        // Ignore any error here.
                        }
                      }

                    // Collect footer data, if required.
                    if ((g.hasFooter) && (thisProp.colDef.footerText !== false) && (g.footerData[thisProp.colDef.dataKey]['lastIndex'] !== i) && (!p.treeView || (row._rowtype = g.rowType.data)))
                      {
                      if ((g.footerData[thisProp.colDef.dataKey]['max'] === false) || (g.footerData[thisProp.colDef.dataKey]['max'] < thisNumber))
                        {
                        g.footerData[thisProp.colDef.dataKey]['max'] = thisNumber;
                        }
                      if ((g.footerData[thisProp.colDef.dataKey]['min'] === false) || (g.footerData[thisProp.colDef.dataKey]['min'] > thisNumber))
                        {
                        g.footerData[thisProp.colDef.dataKey]['min'] = thisNumber;
                        }
                      g.footerData[thisProp.colDef.dataKey]['sum'] += thisNumber;
                      g.footerData[thisProp.colDef.dataKey]['sumcount'] += 1;
                      }
                    }

                  break;

                case 'string':
                  iHtmlArray[iCounter++] = (nz(row[thisProp.colDef.dataKey], '').toString() == '' ? '&nbsp;' : nz(row[thisProp.colDef.dataKey], '').toString());

                  if ((g.hasFooter) && (thisProp.colDef.footerText !== false) && (g.footerData[thisProp.colDef.dataKey]['lastIndex'] !== i) && (!p.treeView || (row._rowtype = g.rowType.data)))
                    {
                    if ((g.footerData[thisProp.colDef.dataKey]['max'] === false) || (g.footerData[thisProp.colDef.dataKey]['max'] < nz(row[thisProp.colDef.dataKey], '').toString()))
                      {
                      g.footerData[thisProp.colDef.dataKey]['max'] = nz(row[thisProp.colDef.dataKey], '').toString();
                      }
                    if ((g.footerData[thisProp.colDef.dataKey]['min'] === false) || (g.footerData[thisProp.colDef.dataKey]['min'] > nz(row[thisProp.colDef.dataKey], '').toString()))
                      {
                      g.footerData[thisProp.colDef.dataKey]['min'] = nz(row[thisProp.colDef.dataKey], '').toString();
                      }
                    }

                  break;

                default:
                  iHtmlArray[iCounter++] = (nz(row[thisProp.colDef.dataKey], '').toString() == '' ? '&nbsp;' : nz(row[thisProp.colDef.dataKey], '').toString());
                  break;

              }
              }
            iHtmlArray[iCounter++] = '</span></div></td>';

            if (g.hasFooter)
              {
              if (thisProp.colDef.footerText !== false)
                {
                g.footerData[thisProp.colDef.dataKey]['lastIndex'] = i; // reminder, 'i' is the index of this dataRow. Stop totals being distorted if the same dataKey appears more than once.
                }
              }
            }
          iHtmlArray[iCounter++] = '</tr>';

          //var jqRow = $(tr);
          var jqRow = $(iHtmlArray.join(''));
          if ((p.treeView) && (row['_treelevel'] != 1))
            {
            jqRow.hide();
            }

          jqTbody.append(jqRow);


          if (p.rowStyleFormatter)
            {
            p.rowStyleFormatter({data:row, state:g.renderState.rendering, type:((p.treeView) ? row._rowtype : g.rowType.data), $rows:jqRow, dataRowIndex:((p.treeView) ? row._datarowindex : i)});
            }

          if ((p.cellStyleFormatter) || (p._hasNegativeColour))
            {
            var aCells = $('td', jqRow);

            if (aCells.length > 0)
              {
              for (var indexCell = 0; indexCell < aCells.length; indexCell++)
                {
                thisColDef = headerProperties[indexCell].colDef;

                if (thisColDef.negativeColour != '')
                  {
                  if (row[thisColDef.dataKey] < 0)
                    {
                    $(aCells[indexCell]).css("color", thisColDef['negativeColour']);
                    }
                  }

                if ((p.cellStyleFormatter) && (!thisColDef.noCellFormatting))
                  {
                  p.cellStyleFormatter({$cell:$(aCells[indexCell]), column:thisColDef, state:g.renderState.rendering, row:{data:row, state:g.renderState.rendering, type:((p.treeView) ? row._rowtype : g.rowType.data), $rows:jqRow, dataRowIndex:((p.treeView) ? row._datarowindex : i)}});
                  }
                }
              }
            }
          tr = null;
          }); // each() row.

          // Add totals row if necessary.

          if (g.hasFooter)
            {
            var iHtmlArray = new Array();
            var iCounter = 0;

            iHtmlArray[iCounter++] = '<tr dataindex="-1" class="footerClass">';

            for (thisIndex = 0; thisIndex < jqHeaders.length; thisIndex++)
              {
              thisProp = headerProperties[thisIndex];
              thisFooterString = '';

              iHtmlArray[iCounter++] =
                '<td align="' + thisProp.align + '" ' +
                  thisProp.hidden +
                  '>' +
                  '<div style="' +
                  thisProp.alignStyle +
                  (p.nowrap ? '' : 'white-space:normal; ') + thisProp.width + '">';

              if (thisProp.colDef.dataKey in g.footerData)
                {
                thisFooterString = thisProp.colDef.footerText;

                switch (thisProp.colDef.dataType)
                {
                  case 'bool':
                  case 'boolean':
                    // <input type="checkbox" disabled="disabled" checked="checked" class="wijgridinput">

                    thisFooterString = thisFooterString.replace(g.footerFields.sum, '');
                    thisFooterString = thisFooterString.replace(g.footerFields.min, '');
                    thisFooterString = thisFooterString.replace(g.footerFields.max, '');
                    thisFooterString = thisFooterString.replace(g.footerFields.average, '');

                    break;

                  case 'date':
                    // TODO

                    thisFooterString = thisFooterString.replace(g.footerFields.sum, '');
                    thisFooterString = thisFooterString.replace(g.footerFields.min, '');
                    thisFooterString = thisFooterString.replace(g.footerFields.max, '');
                    thisFooterString = thisFooterString.replace(g.footerFields.average, '');

                    break;

                  case 'number':
                  case 'numeric':

                    // Get format..

                    thisFormat = (thisProp.colDef.dataFormatString ? thisProp.colDef.dataFormatString : 'n2');
                    thisMultiplier = 1.0;

                    // Adjust for percentage formats.

                    if (thisFormat.startsWith('p')) thisMultiplier = 100.0;
                    else if (thisFormat.endsWith('%')) thisMultiplier = 100.0;
                    if (thisFormat in formatLookup) thisFormat = formatLookup[thisFormat];

                    try
                      {
                      thisFooterString = thisFooterString.replace(g.footerFields.sum, format(thisFormat, (g.footerData[thisProp.colDef.dataKey]['sum'] * thisMultiplier)));
                      thisFooterString = thisFooterString.replace(g.footerFields.min, format(thisFormat, (g.footerData[thisProp.colDef.dataKey]['min'] * thisMultiplier)));
                      thisFooterString = thisFooterString.replace(g.footerFields.max, format(thisFormat, (g.footerData[thisProp.colDef.dataKey]['max'] * thisMultiplier)));
                      thisFooterString = thisFooterString.replace(g.footerFields.average, format(thisFormat, ((g.footerData[thisProp.colDef.dataKey]['sum'] / g.footerData[thisProp.colDef.dataKey]['sumcount']) * thisMultiplier)));
                      }
                    catch (e)
                      {
                      // Ignore any error here.
                      }


                    break;

                  case 'string':


                    break;

                  default:

                    break;

                }
                }

              iHtmlArray[iCounter++] = thisFooterString;
              iHtmlArray[iCounter++] = '</div></td>';

              }
            iHtmlArray[iCounter++] = '</tr>';

            //var jqRow = $(tr);
            var jqRow = $(iHtmlArray.join(''));
            jqTbody.append(jqRow);

            if (p.rowStyleFormatter)
              {
              p.rowStyleFormatter({data:false, state:g.renderState.rendering, type:g.rowType.footer, $rows:jqRow, dataRowIndex:(-1)});
              }

            if (p.cellStyleFormatter)
              {
              var aCells = $('td', jqRow);
              if (aCells.length > 0)
                {
                for (var indexCell = 0; indexCell < aCells.length; indexCell++)
                  {
                  thisColDef = headerProperties[indexCell].colDef;

                  if (!thisColDef.noCellFormatting)
                    {
                    p.cellStyleFormatter({$cell:$(aCells[indexCell]), column:thisColDef, state:g.renderState.rendering, row:{data:false, state:g.renderState.rendering, type:g.rowType.footer, $rows:jqRow, dataRowIndex:(-1)}});
                    }
                  }
                }
              }
            tr = null;

            } // Footer Row

          } // dataType = 'json' (what else)?

        $('tr', t).unbind();

        //$(t).empty();
        var element = t;
        for (var i = element.childNodes.length - 1; i >= 0; i--)
          {
          element.removeChild(element.childNodes[i]);
          }

        $(t).append(tbody);

        this.addRowProp();
        this.rePosDrag();
        tbody = null;
        data = null;
        displayData = null;

        i = null;
        if (p.onSuccess)
          {
          p.onSuccess(this);
          }
        if (p.hideOnSubmit)
          {
          $(g.block).remove();
          }
        this.hDiv.scrollLeft = this.bDiv.scrollLeft;
        if ($.browser.opera)
          {
          $(t).css('visibility', 'visible');
          }
        },

      changeSort:function (th)
        { //change sortorder

        $(g.nDiv).hide();
        $(g.nBtn).hide();
        if (p.sortname == $(th).attr('abbr'))
          {
          if (p.sortorder == 'asc')
            {
            p.sortorder = 'desc';
            }
          else
            {
            p.sortorder = 'asc';
            }
          }
        else
          {
          p.sortorder = 'asc';
          }

        $(th).addClass('sorted').siblings().removeClass('sorted');
        $('.sdesc', this.hDiv).removeClass('sdesc');
        $('.sasc', this.hDiv).removeClass('sasc');
        $('div', th).addClass('s' + p.sortorder);
        p.sortname = $(th).attr('abbr');

        if (p.treeView)
          {
          $(t).flexReload();
          if (p.onChangeSort) p.onChangeSort($(t), p.sortname, p.sortorder);
          }
        else if (p.gridSortFunction)
          {
          var colIndex = $(th).attr('axis').substr(3);
          var cm = p.colModel[colIndex];

          p.gridSortFunction($(t), cm, p.sortname, p.sortorder);
          if (p.onChangeSort) p.onChangeSort($(t), p.sortname, p.sortorder);
          }
        },

      addRowProp:function ()
        { // Adds mouse / hover events for the grid lines
        $('tbody tr', g.bDiv).each(function ()
        {
        $(this).click(function (e)
        {
        var obj = (e.target || e.srcElement);
        if (obj.href || obj.type) return true;
        if (!p.noRowSelect)
          {
          if (p.treeView)
            {
            if ($(this).attr('displayindex') != undefined)
              {
              if (p.displayData.rows[$(this).attr('displayindex')]['_treelevel'] < 0)
                {
                $(this).toggleClass('trSelected');
                if (p.singleSelect) $(this).siblings().removeClass('trSelected');
                }
              }
            }
          else // Not tree view, just normal.
            {
            $(this).toggleClass('trSelected');
            if (p.singleSelect) $(this).siblings().removeClass('trSelected');
            }
          }
        })
          .dblclick(function (e)
          {
          if (p.onRowDoubleClick)
            {
            p.onRowDoubleClick(e, $(this), t);
            }
          })
          .mousedown(function (e)
          {
          if (e.shiftKey)
            {
            $(this).toggleClass('trSelected');
            g.multisel = true;
            this.focus();
            $(g.gDiv).noSelect();
            }
          }).mouseup(function ()
          {
          if (g.multisel)
            {
            g.multisel = false;
            $(g.gDiv).noSelect(false);
            }
          }).hover(function (e)
          {
          if (g.multisel)
            {
            $(this).toggleClass('trSelected');
            }
          }, function ()
          {
          });

        $('.treeViewTotal', $(this)).click(
          function (e)
          {
          if ((!p.onTreeViewClick) || (p.onTreeViewClick(e, $(this), t)))
            {
            g.toggleTreeViewRow(this);
            }
          }
        );

        if ($.browser.msie && $.browser.version < 7.0)
          {
          $(this).hover(function ()
          {
          $(this).addClass('trOver');
          }, function ()
          {
          $(this).removeClass('trOver');
          });
          }
        });
        }

    };

    if (p.colModel)
      { //create model if any
      thead = document.createElement('thead');
      var tr = document.createElement('tr');
      var iHtmlArray = new Array();
      var iCounter = 0;
      var negativeColourUsed = false;
      var i;

      // Re-order colmodel so that grouped fields appear at the start.

      if ((p.treeView) && (p.treeView.length > 0))
        {
        var tempColModel = [];

        for (var treeIndex = 0; treeIndex < p.treeView.length; treeIndex++)
          {
          if (!p.treeView[treeIndex]['sortonly'])
            {
            for (i = 0; i < p.colModel.length; i++)
              {
              if ((p.colModel[i] !== false) && (p.treeView[treeIndex]['display'] == p.colModel[i]['dataKey']))
                {
                p.colModel[i]['visible'] = true;
                tempColModel.push(p.colModel[i]);
                p.colModel[i] = false;

                break;
                }
              }
            }
          }

        for (i = 0; i < p.colModel.length; i++)
          {
          if (p.colModel[i] !== false)
            {
            tempColModel.push(p.colModel[i]);
            p.colModel[i] = false;
            }
          }

        p.colModel = tempColModel;

        } // ((p.treeView) && (p.treeView.length > 0))

      for (i = 0; i < p.colModel.length; i++)
        {
        p.colModel[i] = $.extend({cid:i, width:100, dataType:'string', visible:true, col_class:'', dataFormatString:'', noCellFormatting:false, negativeColour:'', sortable:p.sortall, zeroIsBlank:false, footerText:false, noSortHighlight:false}, p.colModel[i]);
        if (p.colModel[i].negativeColour != '')
          {
          negativeColourUsed = true;
          }
        p._hasNegativeColour = negativeColourUsed;
        }

      for (i = 0; i < p.colModel.length; i++)
        {
        var cm = p.colModel[i];

        iHtmlArray[iCounter++] = '<th ';
        iHtmlArray[iCounter++] = 'axis="col' + i + '"';
        iHtmlArray[iCounter++] = ' align="' + ((cm.align) ? cm.align : (('dataType' in cm) ? ((cm.dataType == 'string') ? 'left' : ((cm.dataType == 'boolean') || (cm.dataType == 'bool') ? 'center' : 'right')) : 'right')) + '"';
        iHtmlArray[iCounter++] = ((cm.sortable) ? ' abbr="' + cm.dataKey + '"' : '');
        iHtmlArray[iCounter++] = ((!cm.visible) ? ' hidden="" style="display: none;"' : '');
        iHtmlArray[iCounter++] = '><div style="text-align: ';
        iHtmlArray[iCounter++] = ((cm.align) ? cm.align : (('dataType' in cm) ? ((cm.dataType == 'string') ? 'left' : ((cm.dataType == 'boolean') || (cm.dataType == 'bool') ? 'center' : 'right')) : 'right'));
        iHtmlArray[iCounter++] = ';width:' + cm.width + 'px;">';
        iHtmlArray[iCounter++] = cm.headerText + '</div></th>';

        if ((cm.footerText !== false) && (!g.hasFooter) && (!p.noFooter))
          {
          g.hasFooter = true;
          }

        if (cm.footerText !== false)
          {
          if (cm.footerText.length > 0)
            {
            // Set footerData template in the parameters object.
            p.footerData[cm.dataKey] = {sum:false, max:false, min:false, sumcount:0, lastIndex:''};
            }
          else
            {
            cm.footerText = false;
            }
          }

        } // for p.colModel

      //tr.innerHTML = iHtmlArray.join('');
      $(tr).append(iHtmlArray.join(''));

      $(thead).append(tr);
      $(t).prepend(thead);

      var jqRow = $(tr);
      if (p.rowStyleFormatter)
        {
        p.rowStyleFormatter({data:{}, state:g.renderState.rendering, type:g.rowType.header, $rows:jqRow, dataRowIndex:(-1)});
        }
      } // end if p.colmodel

    //init divs
    g.gDiv = document.createElement('div'); //create global container
    g.mDiv = document.createElement('div'); //create title container
    g.hDiv = document.createElement('div'); //create header container
    g.bDiv = document.createElement('div'); //create body container
    g.vDiv = document.createElement('div'); //create grip
    g.rDiv = document.createElement('div'); //create horizontal resizer
    g.cDrag = document.createElement('div'); //create column drag
    g.block = document.createElement('div'); //create blocker
    g.nDiv = document.createElement('div'); //create column show/hide popup
    g.nBtn = document.createElement('div'); //create column show/hide button
    g.iDiv = document.createElement('div'); //create editable layer
    g.tDiv = document.createElement('div'); //create toolbar
    g.sDiv = document.createElement('div');
    g.pDiv = document.createElement('div'); //create pager container

    g.pDiv.style.display = 'none';

    g.hTable = document.createElement('table');
    g.gDiv.className = 'flexigrid';
    if (p.width != 'auto')
      {
      g.gDiv.style.width = p.width + 'px';
      }
    //add conditional classes
    if ($.browser.msie)
      {
      $(g.gDiv).addClass('ie');
      }
    if (p.novstripe)
      {
      $(g.gDiv).addClass('novstripe');
      }
    $(t).before(g.gDiv);
    $(g.gDiv).append(t);

    //set toolbar
    if (p.buttons)
      {
      g.tDiv.className = 'tDiv';
      var tDiv2 = document.createElement('div');
      tDiv2.className = 'tDiv2';
      for (var i = 0; i < p.buttons.length; i++)
        {
        var btn = p.buttons[i];
        if (!btn.separator)
          {
          var btnDiv = document.createElement('div');
          btnDiv.className = 'fbutton';
          btnDiv.innerHTML = "<div><span>" + btn.name + "</span></div>";
          if (btn.bclass)
            {
            $('span', btnDiv).addClass(btn.bclass).css({
              paddingLeft:20
            });
            }
          btnDiv.onpress = btn.onpress;
          btnDiv.name = btn.name;
          if (btn.onpress)
            {
            $(btnDiv).click(function ()
            {
            this.onpress(this.name, g.gDiv);
            });
            }
          $(tDiv2).append(btnDiv);
          if ($.browser.msie && $.browser.version < 7.0)
            {
            $(btnDiv).hover(function ()
            {
            $(this).addClass('fbOver');
            }, function ()
            {
            $(this).removeClass('fbOver');
            });
            }
          }
        else
          {
          $(tDiv2).append("<div class='btnseparator'></div>");
          }
        }
      $(g.tDiv).append(tDiv2);
      $(g.tDiv).append("<div style='clear:both'></div>");
      $(g.gDiv).prepend(g.tDiv);
      }
    g.hDiv.className = 'hDiv';
    $(t).before(g.hDiv);
    g.hTable.cellPadding = 0;
    g.hTable.cellSpacing = 0;
    $(g.hDiv).append('<div class="hDivBox"></div>');
    $('div', g.hDiv).append(g.hTable);
    var thead = $("thead:first", t).get(0);
    if (thead) $(g.hTable).append(thead);
    thead = null;

    // Process Header Row. (Add Divs etc....)

    $('thead tr:first th', g.hDiv).mousedown(function (e)
    {

    if ($(this).attr('abbr'))
      {
      $(this).unbind('click').click(function (e)
      {

      if (!$(this).hasClass('thOver'))
        {
        return false;
        }
      var obj = (e.target || e.srcElement);
      if (obj.href || obj.type)
        {
        return true;
        }

      g.changeSort(this);
      });

      if ($(this).attr('abbr') == p.sortname)
        {
        $(this).addClass('sorted');
        $('div', this).addClass('s' + p.sortorder);  //thdiv.className = 's' + p.sortorder;
        }
      }

    g.dragStart('colMove', e, this);
    })
      .hover(function ()
      {
      if (!g.colresize && !$(this).hasClass('thMove') && !g.colCopy)
        {
        $(this).addClass('thOver');
        }
      if ($(this).attr('abbr') != p.sortname && !g.colCopy && !g.colresize && $(this).attr('abbr'))
        {
        $('div', this).addClass('s' + p.sortorder);
        }
      else
        {
        if ($(this).attr('abbr') == p.sortname && !g.colCopy && !g.colresize && $(this).attr('abbr'))
          {
          var no = (p.sortorder == 'asc') ? 'desc' : 'asc';
          $('div', this).removeClass('s' + p.sortorder).addClass('s' + no);
          }
        }
      if (g.colCopy)
        {
        var n = $('th', g.hDiv).index(this);
        if (n == g.dcoln)
          {
          return false;
          }
        if (n < g.dcoln)
          {
          $(this).append(g.cdropleft);
          }
        else
          {
          $(this).append(g.cdropright);
          }
        g.dcolt = n;
        }
      else
        {
        if (!g.colresize)
          {
          var nv = $('th:visible', g.hDiv).index(this);
          var onl = parseInt($('div:eq(' + nv + ')', g.cDrag).css('left'));
          var nw = jQuery(g.nBtn).outerWidth();
          var nl = onl - nw + Math.floor(p.cgwidth / 2);
          $(g.nDiv).hide();
          $(g.nBtn).hide();
          $(g.nBtn).css({
            'left':nl,
            top:g.hDiv.offsetTop
          }).show();
          var ndw = parseInt($(g.nDiv).width());
          $(g.nDiv).css({
            top:g.bDiv.offsetTop
          });
          if ((nl + ndw) > $(g.gDiv).width())
            {
            $(g.nDiv).css('left', onl - ndw + 1);
            }
          else
            {
            $(g.nDiv).css('left', nl);
            }
          if ($(this).hasClass('sorted'))
            {
            $(g.nBtn).addClass('srtd');
            }
          else
            {
            $(g.nBtn).removeClass('srtd');
            }
          }
        }
      }, function ()
      {
      $(this).removeClass('thOver');
      if ($(this).attr('abbr') != p.sortname)
        {
        $('div', this).removeClass('s' + p.sortorder);
        }
      else
        {
        if ($(this).attr('abbr') == p.sortname)
          {
          var no = (p.sortorder == 'asc') ? 'desc' : 'asc';
          $('div', this).addClass('s' + p.sortorder).removeClass('s' + no);
          }
        }
      if (g.colCopy)
        {
        $(g.cdropleft).remove();
        $(g.cdropright).remove();
        g.dcolt = null;
        }
      }); //wrap content

    //set bDiv
    g.bDiv.className = 'bDiv';
    $(t).before(g.bDiv);
    if (p.noScrollBars)
      {
      $(g.bDiv).css('overflow','hidden');
      }
    $(g.bDiv).css({
      height:(p.height == 'auto') ? 'auto' : p.height + "px"
    }).scroll(function (e)
      {
      g.scroll()
      }).append(t);
    if (p.height == 'auto')
      {
      $('table', g.bDiv).addClass('autoht');
      }

    //add td & row properties
    //g.addRowProp(); // Does nothing at this point ?

    //set cDrag
    var cdcol = $('thead tr:first th:first', g.hDiv).get(0);
    if (cdcol != null)
      {
      g.cDrag.className = 'cDrag';
      g.cdpad = 0;
      g.cdpad += (isNaN(parseInt($('div', cdcol).css('borderLeftWidth'))) ? 0 : parseInt($('div', cdcol).css('borderLeftWidth')));
      g.cdpad += (isNaN(parseInt($('div', cdcol).css('borderRightWidth'))) ? 0 : parseInt($('div', cdcol).css('borderRightWidth')));
      g.cdpad += (isNaN(parseInt($('div', cdcol).css('paddingLeft'))) ? 0 : parseInt($('div', cdcol).css('paddingLeft')));
      g.cdpad += (isNaN(parseInt($('div', cdcol).css('paddingRight'))) ? 0 : parseInt($('div', cdcol).css('paddingRight')));
      g.cdpad += (isNaN(parseInt($(cdcol).css('borderLeftWidth'))) ? 0 : parseInt($(cdcol).css('borderLeftWidth')));
      g.cdpad += (isNaN(parseInt($(cdcol).css('borderRightWidth'))) ? 0 : parseInt($(cdcol).css('borderRightWidth')));
      g.cdpad += (isNaN(parseInt($(cdcol).css('paddingLeft'))) ? 0 : parseInt($(cdcol).css('paddingLeft')));
      g.cdpad += (isNaN(parseInt($(cdcol).css('paddingRight'))) ? 0 : parseInt($(cdcol).css('paddingRight')));
      $(g.bDiv).before(g.cDrag);
      var cdheight = $(g.bDiv).height();
      var hdheight = $(g.hDiv).height();
      $(g.cDrag).css({
        top:-hdheight + 'px'
      });
      $('thead tr:first th', g.hDiv).each(function ()
      {
      var cgDiv = document.createElement('div');
      $(g.cDrag).append(cgDiv);
      if (!p.cgwidth)
        {
        p.cgwidth = $(cgDiv).width();
        }
      $(cgDiv).css({
        height:cdheight + hdheight
      }).mousedown(function (e)
        {
        g.dragStart('colresize', e, this);
        }).mouseup(function (e)
        {
        g.dragEnd()
        });
      if ($.browser.msie && $.browser.version < 7.0)
        {
        g.fixHeight();
        $(cgDiv).hover(function ()
        {
        g.fixHeight();
        $(this).addClass('dragging')
        }, function ()
        {
        if (!g.colresize) $(this).removeClass('dragging')
        });
        }
      });
      }

    //add strip
    if (p.striped)
      {
      $('tbody tr:odd', g.bDiv).addClass('erow');
      }
    if (p.resizable && p.height != 'auto')
      {
      g.vDiv.className = 'vGrip';
      $(g.vDiv).mousedown(function (e)
      {
      g.dragStart('vresize', e)
      }).html('<span></span>');
      $(g.bDiv).after(g.vDiv);
      }
    if (p.resizable && p.width != 'auto' && !p.nohresize)
      {
      g.rDiv.className = 'hGrip';
      $(g.rDiv).mousedown(function (e)
      {
      g.dragStart('vresize', e, true);
      }).html('<span></span>').css('height', $(g.gDiv).height());
      if ($.browser.msie && $.browser.version < 7.0)
        {
        $(g.rDiv).hover(function ()
        {
        $(this).addClass('hgOver');
        }, function ()
        {
        $(this).removeClass('hgOver');
        });
        }
      $(g.gDiv).append(g.rDiv);
      }

    // add pager (removed)

    $(g.pDiv, g.sDiv).append("<div style='clear:both'></div>");
    // add title
    if (p.title)
      {
      g.mDiv.className = 'mDiv';
      g.mDiv.innerHTML = '<div class="ftitle">' + p.title + '</div>';
      $(g.gDiv).prepend(g.mDiv);
      if (p.showTableToggleBtn)
        {
        $(g.mDiv).append('<div class="ptogtitle" title="Minimize/Maximize Table"><span></span></div>');
        $('div.ptogtitle', g.mDiv).click(function ()
        {
        $(g.gDiv).toggleClass('hideBody');
        $(this).toggleClass('vsble');
        });
        }
      }
    //setup cdrops
    g.cdropleft = document.createElement('span');
    g.cdropleft.className = 'cdropleft';
    g.cdropright = document.createElement('span');
    g.cdropright.className = 'cdropright';
    //add block
    g.block.className = 'gBlock';
    var gh = $(g.bDiv).height();
    var gtop = g.bDiv.offsetTop;
    $(g.block).css({
      width:g.bDiv.style.width,
      height:gh,
      background:'white',
      position:'relative',
      marginBottom:(gh * -1),
      zIndex:1,
      top:gtop,
      left:'0px'
    });
    $(g.block).fadeTo(0, p.blockOpacity);
    // add column control
    if ($('th', g.hDiv).length)
      {
      g.nDiv.className = 'nDiv';
      g.nDiv.innerHTML = "<table cellpadding='0' cellspacing='0'><tbody></tbody></table>";
      $(g.nDiv).css({
        marginBottom:(gh * -1),
        display:'none',
        top:gtop
      }).noSelect();
      var cn = 0;
      $('th div', g.hDiv).each(function ()
      {
      var kcol = $("th[axis='col" + cn + "']", g.hDiv)[0];
      var colDef = p.colModel[cn];
      var chk = 'checked="checked"';
      if (kcol.style.display == 'none')
        {
        chk = '';
        }
      if (!colDef['notInColumnList'])
        {
        $('tbody', g.nDiv).append('<tr><td class="ndcol1"><input type="checkbox" ' + chk + ' class="togCol" value="' + cn + '" /></td><td class="ndcol2">' + this.innerHTML + '</td></tr>');
        }
      cn++;
      });
      if ($.browser.msie && $.browser.version < 7.0)
        {
        $('tr', g.nDiv).hover(function ()
        {
        $(this).addClass('ndcolover');
        }, function ()
        {
        $(this).removeClass('ndcolover');
        });
        }
      $('td.ndcol2', g.nDiv).click(function ()
      {
      if ($('input:checked', g.nDiv).length <= p.minColToggle && $(this).prev().find('input')[0].checked) return false;
      return g.toggleCol($(this).prev().find('input').val());
      });
      $('input.togCol', g.nDiv).click(function ()
      {
      if ($('input:checked', g.nDiv).length < p.minColToggle && this.checked == false) return false;
      $(this).parent().next().trigger('click');
      });
      $(g.gDiv).prepend(g.nDiv);
      $(g.nBtn).addClass('nBtn')
        .html('<div></div>')
        .attr('title', 'Hide/Show Columns')
        .click(function ()
        {
        $(g.nDiv).toggle();
        return true;
        }
      );
      if (p.showToggleBtn)
        {
        $(g.gDiv).prepend(g.nBtn);
        }
      }
    // add date edit layer
    $(g.iDiv).addClass('iDiv').css({
      display:'none'
    });
    $(g.bDiv).append(g.iDiv);
    // add flexigrid events
    $(g.bDiv).hover(function ()
    {
    $(g.nDiv).hide();
    $(g.nBtn).hide();
    }, function ()
    {
    if (g.multisel)
      {
      g.multisel = false;
      }
    });
    $(g.gDiv).hover(function ()
    {
    }, function ()
    {
    $(g.nDiv).hide();
    $(g.nBtn).hide();
    });
    //add document events
    $(document).mousemove(function (e)
    {
    g.dragMove(e);
    return false;
    }).mouseup(function (e)
      {
      g.dragEnd()
      }).hover(function ()
      {
      }, function ()
      {
      g.dragEnd();
      });
    //browser adjustments
    if ($.browser.msie && $.browser.version < 7.0)
      {
      $('.hDiv,.bDiv,.mDiv,.pDiv,.vGrip,.tDiv, .sDiv', g.gDiv).css({
        width:'100%'
      });
      $(g.gDiv).addClass('ie6');
      if (p.width != 'auto')
        {
        $(g.gDiv).addClass('ie6fullwidthbug');
        }
      }
    g.rePosDrag();
    g.fixHeight();

    //make grid functions accessible
    t.p = p;
    t.grid = g;

    // load data

    if (p.jsArray && (p.dataType == 'json'))
      {
      g.addData({rows:p.jsArray}, false);
      }
    return t;
    };

  $.addFlex.renderState = {none:0, rendering:1, current:2, hovered:4, selected:8};
  $.addFlex.rowType = {header:1, data:2, dataAlt:4, filter:8, groupHeader:16, groupFooter:32, footer:32};

  var docloaded = false;
  $(document).ready(function ()
  {
  docloaded = true
  });

  function nz(pValue, pDefault)
    {
    if ((pValue == undefined) || (pValue == null))
      {
      if ((pDefault == undefined) || (pDefault == null))
        {
        return "";
        }
      else
        {
        return pDefault;
        }
      }

    return pValue;
    }

  function defaultSort(jFlexGrid, colModel, sortname, sortorder)
    {
    /*

     */
    try
      {
      var gridArray = jFlexGrid.flexGet('jsArray');
      var multiplier = 1;
      var thisDataType;
      var numericDataType = false;
      var dateDataType = false;
      var date1;
      var date2;

      if (!colModel.sortable)
        {
        return;
        }

      thisDataType = colModel.dataType;

      if ((thisDataType == "number") || (thisDataType == "numeric"))
        {
        numericDataType = true;
        }
      else if (thisDataType == "date")
        {
        dateDataType = true;
        }

      if (sortorder == 'desc')
        {
        multiplier = -1;
        }

      gridArray.sort(
        function (a, b)
        {
        if (numericDataType)
          {
          if (parseFloat(a[sortname]) < parseFloat(b[sortname])) return (-1 * multiplier);
          if (parseFloat(a[sortname]) > parseFloat(b[sortname])) return (multiplier);
          }
        else if (dateDataType)
          {
          try
            {
            date1 = Date(nz(a[sortname], ''));
            date2 = Date(nz(b[sortname], ''));

            if (date1.getTime() < date2.getTime()) return (-1 * multiplier);
            if (date1.getTime() > date2.getTime()) return (multiplier);

            }
          catch (e)
            {
            if (nz(a[sortname], '') < nz(b[sortname], '')) return (-1 * multiplier);
            if (nz(a[sortname], '') > nz(b[sortname], '')) return (multiplier);
            }
          }
        else
          {
          if (nz(a[sortname], '') < nz(b[sortname], '')) return (-1 * multiplier);
          if (nz(a[sortname], '') > nz(b[sortname], '')) return (multiplier);
          }

        return 0;
        }
      );

      jFlexGrid.flexReload();

      }
    catch (e)
      {
      console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
      }
    }

  $.fn.flexigrid = function (p)
    {
    return this.each(function ()
    {
    if (!docloaded)
      {
      $(this).hide();
      var t = this;
      $(document).ready(function ()
      {
      $.addFlex(t, p);
      });
      }
    else
      {
      $.addFlex(this, p);
      }
    });
    }; //end flexigrid

  $.fn.flexExist = function ()
    {
    if (this.length > 0)
      {
      if (this[0].grid)
        {
        return true;
        }
      }
    return false;
    }

  $.fn.flexScroll = function (pLeft, pTop)
    {
    return this.each(function ()
    {
    if (this.grid)
      {
      if (pLeft >= 0) this.grid.bDiv.scrollLeft = pLeft;
      if (pTop >= 0) this.grid.bDiv.scrollTop = pTop;
      }
    });
    };

  $.fn.flexSize = function (pSize)
    {
    return this.each(function ()
    {
    if (this.grid)
      {
      if ('width' in pSize)
        {
        this.p.width = pSize.width;
        if (this.p.width != 'auto')
          {
          this.grid.gDiv.style.width = pSize.width + 'px';
          }
        else
          {
          this.grid.gDiv.style.width = 'auto';
          }
        }
      if ('height' in pSize)  // switch to 'auto' is not tested. //TODO
        {
        if (pSize.height != 'auto')
          {
          var hdHeight = $(this.grid.hDiv).height();
          var bodyHeight = pSize.height - hdHeight - 2;

          $('table', this.grid.bDiv).removeClass('autoht');
          this.grid.bDiv.style.height = bodyHeight + 'px';
          this.p.height = bodyHeight;
          this.grid.fixHeight();
          }
        else
          {
          this.p.height = 'auto';
          this.grid.bDiv.style.height = 'auto';
          $('table', this.grid.bDiv).addClass('autoht');
          this.grid.fixHeight();
          }
        }
      }
    });
    };

  $.fn.flexReload = function ()
    { // function to reload grid
    return this.each(function ()
    {
    if (this.grid && (this.p.jsArray && (this.p.dataType == 'json')))
      {
      this.grid.addData(false, true);
      }
    });
    }; //end flexReload

  $.fn.flexOptions = function (p)
    { //function to update general options
    return this.each(function ()
    {
    if (this.grid)
      {
      $.extend(this.p, p);
      }

    if ('width' in p)
      {
      $(this).flexSize({width:p.width});
      }
    if ('height' in p)
      {
      $(this).flexSize({height:p.height});
      }
    });
    }; //end flexOptions

  $.fn.flexGet = function (pName)
    {
    if (this.length > 0)
      {
      if (this[0].p)
        {
        if (pName in this[0].p)
          {
          return this[0].p[pName];
          }
        else
          {
          return undefined;
          }
        }
      }
    return undefined;
    };

  $.fn.flexToggleCol = function (cid, visible)
    { // function to reload grid
    return this.each(function ()
    {
    if (this.grid) this.grid.toggleCol(cid, visible);
    });
    }; //end flexToggleCol

  $.fn.flexToggleGroupRow = function (pElement)
    { // function to toggle Open / Close of TreeView group.
    return this.each(function ()
    {
    if (this.grid) this.grid.toggleTreeViewRow(pElement);
    });
    }; //end flexToggleGroupRow

  $.fn.flexSetGroupRow = function (pElement, pState)
    { // function to set Open / Close of TreeView group.
    return this.each(function ()
    {
    if (this.grid) this.grid.toggleTreeViewRow(pElement, pState);
    });
    }; //end flexSetGroupRow

  $.fn.flexExpandGrid = function()
    {
    return this.each(function ()
    {
    if (this.grid)
      {
      this.grid.expandGrid();
      }
    });
    }

  $.fn.flexColapseGrid = function()
    {
    return this.each(function ()
    {
    if (this.grid)
      {
      this.grid.colapseGrid();
      }
    });
    }

  $.fn.flexAddData = function (data)
    { // function to add data to grid
    return this.each(function ()
    {
    if (this.grid) this.grid.addData(data, false);
    });
    };  // end flexAddData

  $.fn.flexSendToExcel = function ()
    { // function to write data to Excel
    return this.each(function ()
    {
    if (this.grid)
      {

      var flexrows = this.p.jsArray;

      // extract data from flexgrid and put into html table
      var html = "<table>";
      var i = 0;
      for (var index in flexrows)
        {
        i++;
        html += "<tr>";

        // if first row then insert headings
        if (i == 1)
          {
          for (var objectName in flexrows[index])
            {
            html += "<th>" + objectName + "</th>";
            }
          html += "</tr><tr>";
          }

        // loop through objects and insert into table row
        for (var objectName in flexrows[index])
          {
          html += "<td>" + flexrows[index][objectName] + "</td>";
          }
        html += "</tr>";
        }
      html += "</table>";

      // fire table at browser
      window.open('data:application/vnd.ms-excel;charset=utf8,' + encodeURIComponent(html), "returns");

      }
    });
    };  // end flexAddData

  $.fn.noSelect = function (p)
    { //no select plugin by me :-)
    var prevent = (p == null) ? true : p;

    console.log('noSelect(' + prevent.toString() + ')');

    if (prevent)
      {
      return this.each(function ()
      {
      if ($.browser.msie || $.browser.safari)
        {
        $(this).bind('selectstart', function ()
        {
        return false;
        });
        }
      else if ($.browser.mozilla)
        {
        $(this).css('MozUserSelect', 'none');
        $('body').trigger('focus');
        }
      else if ($.browser.opera)
        {
        $(this).bind('mousedown', function ()
        {
        return false;
        });
        }
      else
        {
        $(this).attr('unselectable', 'on');
        }
      });
      }
    else
      {
      return this.each(function ()
      {
      if ($.browser.msie || $.browser.safari)
        {
        $(this).unbind('selectstart');
        }
      else if ($.browser.mozilla)
        {
        $(this).css('MozUserSelect', 'inherit');
        }
      else if ($.browser.opera)
        {
        $(this).unbind('mousedown');
        }
      else
        {
        $(this).removeAttr('unselectable', 'on');
        }
      });
      }
    }; //end noSelect
  })(jQuery);



