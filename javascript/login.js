function showLogin()
  {
  try
    {
    var thisDialogType = document.dialogArray.dialogType.info;
    var dialogOptions =
    {
      title:"Connectez-vous",
      modal:true,
      closeOnEscape:false,
      captionButtons:{
        refresh:{visible:false},
        print:{visible:false},
        maximize:{visible:false},
        minimize:{visible:false}
      },
      height:'auto',
      width:'auto',
      resizable:false,
      position:'center'
    };

    var dialogObject = getDialog(thisDialogType, dialogOptions);
    var dialogID = dialogObject.ID;
    var dialogDiv = $('#' + dialogID);

    // Initialise Window.

    setHtmlFromFile(dialogDiv[0], 'html/loginForm.html', 'loginDiv', false, false, false, dialogID);

    // Show Dialog.

    dialogDiv.dialogExtend("open");
    $(".uibutton").button();
    $('.errorMessage').hide();
    
    // Read USERNAME cookie
    
    var username=$.cookie('USERNAME');
    
    if(typeof username != 'undefined'){
    	 //$(".Email").val(username);
    	document.UserName=username;
    }

    // Deal with forgotten password

    $("#forgotPassword").click(
      function ()
      {
      var email = $(this).parents(".simpleForm").find('.Email').val();
      var formerrortextPre = "<div class=\"ui-state-error ui-corner-all\" style=\"padding: 0 .7em;\">" +
        "<p class=\"messagetext\"><span class=\"ui-icon ui-icon-alert\"" +
        "style=\"float: left; margin-right: .3em;\"></span><strong>Alerte:</strong>";
      var formerrormessage = "";
      var formerrortextPost = "</p></div><br>";

      $(this).parents(".simpleForm").find('.loginErrorMessage').hide();

      if (!validEmail(email))
        {
        formerrormessage = " Pas une adresse email valide";
        $(this).parents(".simpleForm").find('.errorMessage').html(formerrortextPre + formerrormessage + formerrortextPost).show();
        }
      else
        {
        $(this).parents(".simpleForm").find('.errorMessage').hide();
        var formvar = {
          'Email':email
        };

        var url = "php/forgot_password.php";

        $.ajax({type:"POST",
          url:url,
          data:formvar,
          dataType: "json",
          context:this,
          success:function (json)
            {
            if (json.Status=='OK')
              {
              alert("Veuillez lire votre courriel pour obtenir des instructions");
              }
            else
              {
              formerrormessage = " " + json.Message + "</em>";
              $(this).parents(".simpleForm").find('.errorMessage').html(formerrortextPre + formerrormessage + formerrortextPost);
              $(this).parents(".simpleForm").find('.errorMessage').show();
              }
            }
        });

        }

      });

    $(".errorMessage").unbind().click(function ()
    {
    $(this).parents(".simpleForm").find('.errorMessage').hide();
    });

    $(".loginErrorMessage .ui-state-error").unbind().click(function ()
    {
    $(this).parents(".simpleForm").find('.loginErrorMessage').hide();
    });

    $(".Email").keydown(function(e) {
      if (e.keyCode == 13)
        {
          $('.okButton').click();
        }
    });

    $(".Password").keydown(function(e) {
    if (e.keyCode == 13)
      {
      $('.okButton').click();
      }
    });

    dialogDiv.find('.Email').val(document.UserName);

    return dialogID;
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }

  return '';

  }

function login(e)
  {
  try
    {
    if (DEBUG_FLAG) console.log('login()');

    e.stopPropagation();

    var loginDialog = $(e.target).parents('.dialogDiv');
    var dialogID = loginDialog.attr("id");
    var dialogDiv = $('#' + dialogID);

    var formerrortextPre = "<div class=\"ui-state-error ui-corner-all\" style=\"padding: 0 .7em;\">" +
      "<p class=\"messagetext\"><span class=\"ui-icon ui-icon-alert\"" +
      "style=\"float: left; margin-right: .3em;\"></span><strong>Alerte:</strong>";
    var formerrormessage = " Informations d'identification incorrectes";
    var formerrortextPost = "</p></div><br>";

    $(e.target).parents(".simpleForm").find('.errorMessage').html(formerrortextPre + formerrormessage + formerrortextPost);

    $(e.target).hide();

    var loginUser = dialogDiv.find('.Email').val(); //
    var loginPassword = dialogDiv.find('.Password').val(); //

    var SourceURL = "php/login_checkUser.php";

    document.UserName = loginUser;

    var formvar = {
      'USERNAME':loginUser,
      'PASSWORD':loginPassword,
      'DIALOGID':dialogID
    };


    jQuery.ajax({type:"GET",
      url:SourceURL,
      data:formvar,
      async:false,
      success:function (html)
        {
        try
          {
          var rVal = JSON.parse(html);

          if ((rVal) && (rVal.length > 0))
            {
        	  
        	loginDialog.find('.errorMessage').hide();
        	loginDialog.find('.ui-state-error').hide();
        	
            switch (rVal[0]['SessionKey'])
            {
              case 'fail':
            	
                loginDialog.find('.details').show();

                $('.loginErrorMessage').show();
                break;
                
              case 'Error':       	  
	           
	              loginDialog.find('.details').show();
	
	              $('.loginErrorMessage').show();
	              break;

              case 'expired':
            	
                loginDialog.find('.expired').show();

                $('.loginErrorMessage').show();

                document.dakotaUserID = rVal[0]['UserID'];

                loginDialog.dialog("close");

                changePasswordShow();

                break;

              case 'locked':

                loginDialog.find('.locked').show();

                //$('.loginErrorMessage').show();

                document.dakotaUserID = rVal[0]['UserID'];

                break;

              default:

                //   SELECT userID, sessionKey, userIsAdmin, userIsRegion, userIsAgency, userIsSalesman;
                document.dakotaUserID = rVal[0]['UserID'];
                document.dakotaSessionKey = rVal[0]['SessionKey'];
                document.dakotaIsAdmin = parseInt(rVal[0]['UserIsAdmin']);

                loginDialog.dialog("close");
                
                // set cookie
                
                $.cookie('USERNAME', formvar.USERNAME, { expires: 7 });

                // Customise Menu for user

                if (document.dakotaIsAdmin==1){
                	$("#menu").append(
                			'<li id="menuInformation" class="ui-widget menu-item">'+
                				'<a class="ui-corner-all">Admin</a>'+
                				'<ul class="sub-menu ui-widget-content ui-corner-all">'+
                					'<li id="users" class="sub-item"><a class="ui-corner-all">Users</a></li>'+
                					'<li id="adduser" class="sub-item"><a class="ui-corner-all">Add User</a></li>'+
                					'<div style="clear: both; height:7px;">&nbsp;</div>'+
                				'</ul>'+
                			'</li>');
                }

                
                // Initialise main menu.

            	$(".ui-menu-list a").mouseenter(function() {
            		$(this).addClass("ui-state-hover");
            		$(".sub-menu", $(this).parents(".ui-menu-list")).css('display', '');
            	}).mouseleave(function() {
            		$(this).removeClass("ui-state-hover");
            	}).click(function(e) {
            		$(".sub-menu", $(e.target).parents(".ui-menu-list")).css({
            			display : "none"
            		});
            		MenuSelect(e, this);
            	});

                $('#MenuDiv').show();
                
                // Set Folio list to cache
            	getFolioListArray(); // Set Folio list to cache
            	getFundValuationArray();

                // Handle re-login

                reloginOK();

                break;
            }

            }

          }
        catch (e)
          {
          console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
          }
        }
    });


    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }

  $(e.target).show();

  return false;
  }

function changePassword(e)
  {
  try
    {
    var loginDialog = $(e.target).parents('.dialogDiv');
    var dialogID = loginDialog.attr("id");
    var dialogDiv = $('#' + dialogID);

    $(e.target).hide();

    var loginUser = dialogDiv.find('.Email').val(); //
    var loginPassword = dialogDiv.find('.Password').val(); //
    var newPassword1 = dialogDiv.find('.PasswordNew1').val(); //
    var newPassword2 = dialogDiv.find('.PasswordNew2').val(); //

    if (newPassword1.length < 8)
      {
      dialogDiv.find('.wrongpassword').hide();//
      dialogDiv.find('.differentpasswords').hide();//
      dialogDiv.find('.passwordlength').show();//
      dialogDiv.find('.errorMessage').show('slow');//

      $(e.target).show();

      return;
      }

    if (newPassword1 != newPassword2)
      {
      dialogDiv.find('.wrongpassword').hide();//
      dialogDiv.find('.passwordlength').hide();//
      dialogDiv.find('.differentpasswords').show();//
      dialogDiv.find('.errorMessage').show('slow');//

      $(e.target).show();

      return;
      }

    var SourceURL = "php/changePassword.php";

    var formvar = {
      'USERNAME':loginUser,
      'PASSWORD':loginPassword,
      'NEWPASSWORD':newPassword1,
      'DIALOGID':dialogID
    };

    document.UserName = loginUser;

    var formJSON = JSON.stringify(formvar);

    jQuery.ajax({type:"GET",
      url:SourceURL,
      data:{formdata:formJSON},
      async:false,
      success:function (html)
        {
        try
          {
          var rVal = JSON.parse(html);

          if ((rVal) && (rVal.length > 0))
            {
            if (rVal[0]['sessionKey'] == 'fail')
              {
              dialogDiv.find('.differentpasswords').hide();//
              dialogDiv.find('.passwordlength').hide();//
              dialogDiv.find('.wrongpassword').show();//
              dialogDiv.find('.errorMessage').show('slow');

              $(e.target).show();
              return;
              }
            else
              {
              //   SELECT userID, sessionKey, userIsAdmin, userIsRegion, userIsAgency, userIsSalesman;
              document.dakotaUserID = rVal[0]['userID'];
              document.dakotaSessionKey = rVal[0]['sessionKey'];
              document.dakotaIsAdmin = parseInt(rVal[0]['userIsAdmin']);
              document.dakotaIsReadonly = parseInt(rVal[0]['userIsReadonly']);

              dialogDiv.dialog("close");

              // Customise Menu.


              }
            }

          }
        catch (e)
          {
          console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
          }
        }
    });

    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }
  }

function loginAgain()
  {
  try
    {
    /* Show login form */

    var dialogID = showLogin();

    // Set Login button action.

    var dialogDiv = $('#' + dialogID);
    dialogDiv.find("#okButton").unbind().click(login);

    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }
  }

function reloginOK()
  {
  try
    {
    var dialogID;
    var salesmanID;
    var clientID;

    switch (document.reloginCurrentState.FUNCTION_FROM)
    {
      case 'regionDrilldown_getData':
        dialogID = document.reloginCurrentState.DIALOGID;

        regionDrilldown_getData(dialogID);

        break;

      case 'salesmanDrilldown_getData':
        dialogID = document.reloginCurrentState.DIALOGID;

        salesmanDrilldown_getData(dialogID, document.reloginCurrentState.AGENCYID, document.reloginCurrentState.SALESMANID);

        break;

      case 'salesmanDrilldown_getClientData':
        dialogID = document.reloginCurrentState.DIALOGID;
        salesmanID = document.reloginCurrentState.SALESMANID;
        clientID = document.reloginCurrentState.CLIENTID;

        salesmanDrilldown_getClientData(dialogID, salesmanID, clientID);

        break;

      case 'salesmanDrilldown_progressSubmit':
        dialogID = document.reloginCurrentState.DIALOGID;

        salesmanDrilldown_progressSubmit(dialogID);

        break;

      default:


        break;
    }

    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }
  }

function changePasswordShow()
  {

  try
    {
    var thisDialogType = document.dialogArray.dialogType.info;
    var dialogOptions =
    {
      title:"Changer mot de passe",
      modal:true,
      closeOnEscape:false,
      captionButtons:{
        refresh:{visible:false},
        print:{visible:false},
        maximize:{visible:false},
        minimize:{visible:false}
      },
      height:'auto',
      width:'auto',
      resizable:false,
      position:'center'
    };

    var dialogObject = getDialog(thisDialogType, dialogOptions);
    var dialogID = dialogObject.ID;
    var dialogDiv = $('#' + dialogID);

    // Initialise Window.

    setHtmlFromFile(dialogDiv[0], 'html/changePassword.html', 'loginDiv', false, false, false, dialogID);

    dialogDiv.find('.Email').val(document.UserName);

    // Show Dialog.
    dialogDiv.dialogExtend("open");
    dialogDiv.find('#okButton').button();
    dialogDiv.find('.errorMessage').hide();
    dialogDiv.find("#okButton").unbind().click(changePassword);

    return dialogID;
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }

  return '';

  }
