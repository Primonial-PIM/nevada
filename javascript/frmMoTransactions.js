/**
 * User: Nicholas Pennington
 * Date:
 * Time:
 *
 * Updates :
 *
 */

/* Middle office Transaction */

function showMoTransactions()
  {
  try
    {
    var dialogID = (-1);
    var dialogDiv = undefined;
    var thisDialogType = undefined;
    var leftHeaderDiv = undefined;
    var dialogOptions = undefined;
    var dialogObject = undefined;

    thisDialogType = document.dialogArray.dialogType.MoTransactions;
    dialogOptions = {title: "Transaction Match", position: [5, 50], resizeStop: dialogMiddleOfficeTransactionsResizeStop, stateChanged: dialogMiddleOfficeTransactionsStateChange, open: dialogMiddleOfficeTransactionsOpen, close: dialogMiddleOfficeTransactionsClose, closeOnEscape: false, captionButtons: {pin: {visible: false}, refresh: {visible: true, click: refreshMoTransactionsData}, print: {visible: true, click: moTransactionsPrintContext}, export: {visible: true, click: writeFromDialogGridToExcel}}};

    dialogObject = getDialog(thisDialogType, dialogOptions, window.innerHeight - 60, window.innerWidth - 15);
    dialogID = dialogObject.ID;
    dialogDiv = $('#' + dialogID);

    var splitterDiv = $('<div id="' + dialogID + '_splitter" class="splitterDiv"><div id="' + dialogID + '_splitterLeft" class="leftPanel"><div id="' + dialogID + '_leftSuper"><div id="' + dialogID + '_leftHeading"></div><table id="' + dialogID + '_grid" class="gridTable"></table></div></div><div id="' + dialogID + '_splitterRight" class="rightPanel"></div></div>');
    dialogDiv.append(splitterDiv);

    // Initialise Splitter.

    leftHeaderDiv = splitterDiv.find('#' + dialogID + '_leftHeading');
    setHtmlFromFile(leftHeaderDiv[0], 'html/moTransactionElements.html?dummy=' + getUniqueCounter().toString(), 'moTransactionsFilter', false, false, setMoFilterEvents, dialogID); // setMoFilterEvents(dialogID));

    // Hide Filter

    $(".formTradeFilter").hide();
    $("button").button();

    // Initialise Transactions list.

    getMoTransactionsData(dialogID);

    // Refresh Fund Valuations.

    // getFundValuationArray();

    // Show Dialog.

    $(dialogDiv).dialogExtend("open");

    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }

  }

function setMoFilterEvents(targetObject, dialogID)
  {
  /* Clear Fund list options and populate */
  var fundCombo;
  var referenceStartDate;
  var referenceStartDateString;

  try
    {
    fundCombo = $('#' + dialogID + ' .moFundCombo');

    referenceStartDate = new Date;
    referenceStartDate.addBusDays(-5);
    referenceStartDate.setHours(0, 0, 0, 0);

    referenceStartDateString = referenceStartDate.getFullYear() + '-' + ("00" + (referenceStartDate.getMonth() + 1)).slice(-2) + '-' + ("00" + referenceStartDate.getDate()).slice(-2);

    fundCombo.empty();
    fundCombo.append("<option value='1'>All Funds</option>");

    var fundList = document.dataCache.folioListArray.data;

    for (thisIndex in fundList)
      {
      if (fundList[thisIndex]['value'] != '1')
        {
        fundCombo.append("<option value='" + fundList[thisIndex]['value'] + "'>" + fundList[thisIndex]['label'] + "</option>");
        }
      }
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }

  try
    {
    /* UI functionality */

    var pickers = $('#' + dialogID + ' .datePicker');
    pickers.datepicker({ dateFormat: "yy-mm-dd", gotoCurrent: true });

    // default values
    $('#' + dialogID + ' .tradeDateFrom').datepicker("setDate", referenceStartDateString);

    // Events
    $('#' + dialogID + ' input[type="checkbox"]').off('change', moTransactionFilterChanged);
    $('#' + dialogID + ' input[type="checkbox"]').on('change', moTransactionFilterChanged);

    $('#' + dialogID + ' .moTransactionsFilter span').off('click', moTransactionsSpanClick);
    $('#' + dialogID + ' .moTransactionsFilter span').on('click', moTransactionsSpanClick);

    // $('#' + dialogID + ' .checkboxDateFrom').change(moTransactionFilterChanged);
    // $('#' + dialogID + ' .checkboxDateTo').change(moTransactionFilterChanged);

    $('#' + dialogID + ' .jQueryUiCombobox').off('change', moTransactionFilterChanged);
    $('#' + dialogID + ' .jQueryUiCombobox').on('change', moTransactionFilterChanged);

    //$('#' + dialogID + ' .formItemDiv a').click(fnPrevNextDatepicker);

    $('#' + dialogID + ' .buttonSelectRecentDate').off('click', fnRecentDatepicker);
    $('#' + dialogID + ' .buttonSelectRecentDate').on('click', fnRecentDatepicker);

    // both dates down
    $('#' + dialogID + ' .datesdown').off('click');
    $('#' + dialogID + ' .datesdown').on('click', function ()
    {

    var dateObject;
    dateObject = $('#' + dialogID + ' .tradeDateFrom');

    var date1 = dateObject.datepicker('getDate');
    var newdate = new Date(Date.parse(date1));
    newdate.setDate(newdate.getDate() - 1);
    dateObject.datepicker('setDate', newdate);

    dateObject = $('#' + dialogID + ' .tradeDateTo');
    date1 = dateObject.datepicker('getDate');
    newdate = new Date(Date.parse(date1));
    newdate.setDate(newdate.getDate() - 1);
    dateObject.datepicker('setDate', newdate);
    dateObject.change();


    });

    // both dates up
    $('#' + dialogID + ' .datesup').off('click');
    $('#' + dialogID + ' .datesup').on('click', function ()
    {

    var date1 = $('#' + dialogID + ' .tradeDateFrom').datepicker('getDate');
    var newdate = new Date(Date.parse(date1));
    newdate.setDate(newdate.getDate() + 1);
    $('#' + dialogID + ' .tradeDateFrom').datepicker('setDate', newdate);

    date1 = $('#' + dialogID + ' .tradeDateTo').datepicker('getDate');
    newdate = new Date(Date.parse(date1));
    newdate.setDate(newdate.getDate() + 1);
    $('#' + dialogID + ' .tradeDateTo').datepicker('setDate', newdate);
    $('#' + dialogID + ' .tradeDateTo').change();


    });


    pickers.off('change', moTransactionFilterChanged);
    pickers.on('change', moTransactionFilterChanged);

    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }

  }

function moTransactionsSpanClick(e)
  {
  try
    {

    if ((e.target.nodeName == 'INPUT') && (e.target.type == 'checkbox')) return;

    var thisCheck = $(this).find('input[type="checkbox"]');

    if (thisCheck.length > 0)
      {
      thisCheck.prop("checked", !thisCheck.prop("checked"));
      thisCheck.change();
      }
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }
  }

function moTransactionFilterChanged(e)
  {
  /*
   Event function.
   When a filter criteria on the MO Transactions form is changed, this function determines whether or not to
   load more data or if the existing data is sufficient.
   */

  try
    {
    var dialogID = $(e.target).parents('.dialogDiv').attr('id');
    var cachekey = MO_TRANSACTION_CACHE_PREFIX + dialogID;
    var cacheObject = undefined;

    if (document.dataCache[cachekey])
      {
      cacheObject = document.dataCache[cachekey];

      if ((cacheObject.inProgress == true))
        {
        return;
        }

      if (cacheObject.data.dataStatus < 7)
        {
        getMoTransactionsData(dialogID);
        return;
        }

      if (cacheObject.data.transactionsFormObject == false)
        {
        getMoTransactionsData(dialogID);
        return;
        }
      }
    else
      {
      // Refresh all data.
      getMoTransactionsData(dialogID);
      return;
      }

    /* Reload Data, or just re-process existing data? */
    /* OK, if the data dates are reasonable, then just re-process */

    var thisFormObject = cacheObject.data.transactionsFormObject;
    var dataDateFrom = new Date(thisFormObject['DATE_FROM']);
    var dataDateTo = new Date(thisFormObject['DATE_TO']);

    var referenceStartDate = new Date();
    referenceStartDate.addBusDays(-5);
    referenceStartDate.setHours(0, 0, 0, 0);

    var moDateFrom = $('#' + dialogID + ' .tradeDateFrom').val();
    var filterDateFrom = new Date(((moDateFrom) && (moDateFrom.length > 0)) ? moDateFrom : referenceStartDate.getTime());
    var moDateTo = $('#' + dialogID + ' .tradeDateTo').val();
    var filterDateTo = new Date(((moDateTo) && (moDateTo.length > 0)) ? moDateTo : '01/01/2900');

    filterDateFrom.setDate(filterDateFrom.getDate() - MO_DATA_DAYS_UPDATE_MARGIN);
    filterDateTo.setDate(filterDateTo.getDate() + MO_DATA_DAYS_UPDATE_MARGIN);

    if ((filterDateFrom.getTime() >= dataDateFrom.getTime()) && (filterDateTo.getTime() <= dataDateTo.getTime()))
      {
      moFilterTransactionsData(dialogID, MO_TRANSACTION_CACHE_PREFIX + dialogID);
      return;
      }

    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }
  finally
    {
    RefreshTokenExpiry(document.dakotaUserID, document.dakotaSessionKey);
    }

  getMoTransactionsData(dialogID);
  }

function getMoTransactionsData(dialogID)
  {
  var today = new Date();
  today.setHours(0, 0, 0, 0);

  var referenceStartDate = new Date();
  referenceStartDate.setDate(today.getDate() - MO_DATA_DAYS_PADDING);
  referenceStartDate.setHours(0, 0, 0, 0);

  var referenceEndDate;
  var startDate;
  var endDate = '3000-01-01'; // Default End Date

  /* Filter parameters */

  // Date Selection

  try
    {
    var moDateType = $('#' + dialogID + ' .moDateCombo').val();
    var moDateFrom = $('#' + dialogID + ' .tradeDateFrom').val();
    var moDateTo = $('#' + dialogID + ' .tradeDateTo').val();

    if ((moDateFrom) && (moDateFrom.length > 0))  // Date From
      {
      referenceStartDate = new Date(moDateFrom);
      referenceStartDate.setDate(referenceStartDate.getDate() - MO_DATA_DAYS_PADDING);
      }

    startDate = referenceStartDate.getFullYear() + '-' + ("00" + (referenceStartDate.getMonth() + 1)).slice(-2) + '-' + ("00" + referenceStartDate.getDate()).slice(-2);

    if ((moDateTo) && (moDateTo.length > 0))  // Date To
      {
      referenceEndDate = new Date(moDateTo);
      referenceEndDate.setDate(referenceEndDate.getDate() + MO_DATA_DAYS_PADDING);
      endDate = referenceEndDate.getFullYear() + '-' + ("00" + (referenceEndDate.getMonth() + 1)).slice(-2) + '-' + ("00" + referenceEndDate.getDate()).slice(-2);
      }
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }

  /* Get Data */

  var cacheObject;
  var cacheKey = MO_TRANSACTION_CACHE_PREFIX + dialogID;

  if (cacheKey in document.dataCache)
    {
    cacheObject = document.dataCache[cacheKey];
    cacheObject['inProgress'] = true;
    cacheObject['updateTime'] = (new Date());

    if ('data' in cacheObject)
      {
      cacheObject.data['dataStatus'] = 0;
      }
    else
      {
      cacheObject['data'] = {dataStatus: 0, transactionsData: false, transactionsFormObject: false, tradefileData: false, tradefileFormObject: false, confirmationsData: false, confirmationsFormObject: false};
      }
    }
  else
    {
    cacheObject = {inProgress: true, filterFlags: {}, updateTime: (new Date()), data: {dataStatus: 0, transactionsData: false, transactionsFormObject: false, tradefileData: false, tradefileFormObject: false, confirmationsData: false, confirmationsFormObject: false}};
    document.dataCache[cacheKey] = cacheObject;
    }

  getMoTransactions({DIALOGID: dialogID, DATE_FROM: startDate, DATE_TO: endDate}, setMoTransactionsData);
  getMoTradeFileData({DIALOGID: dialogID, DATE_FROM: startDate, DATE_TO: endDate}, setMoTransactionsData);
  getMoConfirmationData({DIALOGID: dialogID, DATE_FROM: startDate, DATE_TO: endDate}, setMoTransactionsData);

  }

function refreshMoTransactionsData(e)
  {
  if (e)
    {
    var dialogID = this.id;

    getFundValuationArray();

    getMoTransactionsData(dialogID);
    }
  }

function setMoTransactionsData(formObject, customParams)
  {
  var cachekey;
  var cacheObject;
  var dialogID = '';

  if (formObject['DIALOGID'])
    {
    dialogID = formObject['DIALOGID'];
    }

  try
    {
    cachekey = MO_TRANSACTION_CACHE_PREFIX + dialogID;
    if (document.dataCache[cachekey])
      {
      cacheObject = document.dataCache[cachekey];

      if (cacheObject.data.dataStatus < 7)
        {
        /* Not finished getting data.... */
        return;
        }
      }
    else
      {
      /* No control object. Abort, Abort.... */
      return;
      }

    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    return;
    }

  if (DEBUG_FLAG) console.log("cacheObject.data.dataStatus = " + cacheObject.data.dataStatus);

  moMatchTransactionsData(dialogID, cachekey);
  moFilterTransactionsData(dialogID, cachekey);
  }

function moMatchTransactionsData(dialogID, cachekey)
  {
  var cacheObject;
  var debugUse;

  try
    {
    try
      {
      if (document.dataCache[cachekey])
        {
        cacheObject = document.dataCache[cachekey];
        }
      else
        {
        /* No control object. Abort, Abort.... */
        return;
        }
      }
    catch (e)
      {
      console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
      return;
      }

    if (DEBUG_FLAG) console.log("moMatchTransactionsData : cacheObject.data.dataStatus = " + cacheObject.data.dataStatus);

    /*
     * Ok, looks good. We have the data , now what?
     */

    /* Set to... */

    var matchArray = new Array();

    var transactionCounter;
    var tradefileCounter;
    var confirmationCounter;
    var matchArrayCounter;
    var possibleIndex;
    var matchCount;
    var thisEntryDate;

    var veniceTransactions = cacheObject.data.transactionsData;
    var tradeFileEntries = cacheObject.data.tradefileData;
    var tradeFileMatched = new Array();
    var neolinkConfirmations = cacheObject.data.confirmationsData;
    var confirmationMatched = new Array();


    // Initialise 'Matched' arrays. Used to keep track of what things have been
    // already matched.

    for (tradefileCounter = 0; tradefileCounter < tradeFileEntries.length; tradefileCounter++)
      {
      tradeFileMatched.push(false);
      }
    for (confirmationCounter = 0; confirmationCounter < neolinkConfirmations.length; confirmationCounter++)
      {
      confirmationMatched.push(false);
      }

    // First add element to match for each value in 'Transactions'

    var MATCH_COL_CERTAINTY = 0;
    var MATCH_COL_TRANSACTIONS = 1;
    var MATCH_COL_TRADEFILE = 2;
    var MATCH_COL_CONFIRMATIONS = 3;

    var MATCH_COL_DATE = 4;
    var MATCH_SOPHIS_DATENEG = 5;
    var MATCH_SOPHIS_DATENEG_PLUSONE = 6;
    var MATCH_SOPHIS_DATECOMPTABLE = 7;
    var MATCH_VENICE_VALUEDATE = 8;
    var MATCH_TRADEFILE_TRADEDATE = 9;
    var MATCH_BNP_TRADEDATE = 10;
    var MATCH_BNP_SETTLEDATE = 11;


    for (transactionCounter = 0; transactionCounter < veniceTransactions.length; transactionCounter++)
      {
      matchArray.push([-1, transactionCounter, false, false, new Date(veniceTransactions[transactionCounter]['DATENEG'].substr(0, 10)), false, false, false, false, false, false, false]);
      }

    // Now attempt to match trade file entries to Venice Transactions entries.

    matchCount = 0;

    // At this point, the matchArray matches the VeniceTransactions array.

    for (transactionCounter = 0; transactionCounter < veniceTransactions.length; transactionCounter++)
      {
      for (tradefileCounter = 0; tradefileCounter < tradeFileEntries.length; tradefileCounter++)
        {
        if ((!tradeFileMatched[tradefileCounter]) && (tradeFileEntries[tradefileCounter]['sophis_refcon'] == veniceTransactions[transactionCounter]['REFCON']))
          {
          matchArray[transactionCounter][MATCH_COL_TRADEFILE] = tradefileCounter;
          matchArray[transactionCounter][MATCH_COL_CERTAINTY] = 1;
          tradeFileMatched[tradefileCounter] = true;
          matchCount++;
          break;
          }
        }
      }

    // What about unmatched trade file entries
    if (matchCount < tradeFileEntries.length)
      {
      // OK, what we're going to do is to insert the unmatched tradeFile
      // entries into the matchArray, at the end of each entry day.

      try
        {
        possibleIndex = -1;

        for (tradefileCounter = 0; tradefileCounter < tradeFileEntries.length; tradefileCounter++)
          {
          if (!tradeFileMatched[tradefileCounter])
            {
            thisEntryDate = new Date(tradeFileEntries[tradefileCounter]['tradefiledate'].substr(0, 10));

            // Start from the bottom of the matchArray as Dates are descending
            // order
            for (matchArrayCounter = (matchArray.length - 1); matchArrayCounter >= 0; matchArrayCounter--)
              {
              if ((matchArray[matchArrayCounter][MATCH_COL_DATE].getTime() >= thisEntryDate.getTime()) && (possibleIndex < 0))
                {
                possibleIndex = matchArrayCounter;
                }

              if (matchArray[matchArrayCounter][MATCH_COL_DATE].getTime() == thisEntryDate.getTime())
                {
                if (veniceTransactions[matchArrayCounter[MATCH_COL_TRANSACTIONS]])
                  {
                  if (veniceTransactions[matchArrayCounter[MATCH_COL_TRANSACTIONS]]['EXTERNREF'] == tradeFileEntries[matchArrayCounter[MATCH_COL_TRADEFILE]]['instrumentcode'])
                    {
                    possibleIndex = matchArrayCounter;
                    }
                  }
                }

              if (matchArray[matchArrayCounter][MATCH_COL_DATE].getTime() > thisEntryDate.getTime())
                {
                // Insert matchArray entry.

                matchArray.splice(possibleIndex + 1, 0, [-1, false, tradefileCounter, false, new Date(tradeFileEntries[tradefileCounter]['tradefiledate'].substr(0, 10)), false, false, false, false, false, false, false]);

                break;
                }
              else if (matchArrayCounter == 0)
                {
                matchArray.splice(0, 0, [-1, false, tradefileCounter, false, new Date(tradeFileEntries[tradefileCounter]['tradefiledate'].substr(0, 10)), false, false, false, false, false, false, false]);
                break;
                }
              }

            tradeFileMatched[tradefileCounter] = true;
            }
          }
        }
      catch (e)
        {
        console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
        }

      }

    // Match neolink confirmations.
    // Hmmm, the tricky one?
    // Match Confirmations on clientReference first.

    var confRefcon;
    var confInstance;
    var thisInstance;
    var thisRefcon;
    var thisTicket;
    var thisMatchElement;
    var iterationCount;
    var thisTransaction;
    var thisTradefile;
    var thisConfirmation;

    matchCount = 0;

    // Match confirmations.
    // The matching is performed in two iterations, 'neo_t' <> 'D' and then
    // 'neo_t- == 'D'
    // This is because the 'D'elivery trades are (usually?) duplications of other
    // trades.

    for (iterationCount = 1; iterationCount <= 2; iterationCount++)
      {
      for (confirmationCounter = 0; confirmationCounter < neolinkConfirmations.length; confirmationCounter++)
        {
        thisConfirmation = neolinkConfirmations[confirmationCounter];

        if (!(confirmationMatched[confirmationCounter]) && (((iterationCount == 1) && (thisConfirmation['neo_t'] != 'D')) || ((iterationCount > 1) && (thisConfirmation['neo_t'] == 'D'))))
          {
          confRefcon = nz(thisConfirmation['neo_clientreference'], '');
          confInstance = nz(thisConfirmation['instance'], '');

          if (confRefcon.length > 0)
            {
            for (matchArrayCounter = 0; matchArrayCounter < matchArray.length; matchArrayCounter++)
              {
              // If this match item has not already matched
              if (!matchArray[matchArrayCounter][MATCH_COL_CONFIRMATIONS])
                {
                thisMatchElement = matchArray[matchArrayCounter];

                if (thisMatchElement[MATCH_COL_TRANSACTIONS] !== false)
                  {
                  thisTransaction = veniceTransactions[thisMatchElement[MATCH_COL_TRANSACTIONS]];

                  thisInstance = thisTransaction['INSTANCE'];
                  thisTicket = thisTransaction['TICKET'];
                  }
                else
                  {
                  thisInstance = '??';
                  thisTicket = 'Unmatched';
                  }

                if (thisMatchElement[MATCH_COL_TRANSACTIONS] !== false)
                  {
                  thisRefcon = veniceTransactions[thisMatchElement[MATCH_COL_TRANSACTIONS]]['REFCON'];
                  }
                else if (thisMatchElement[MATCH_COL_TRADEFILE] !== false)
                  {
                  thisRefcon = tradeFileEntries[thisMatchElement[MATCH_COL_TRADEFILE]]['sophis_refcon'];
                  }
                else
                  {
                  // Odd, this matchArray element has neither
                  // VeniceTransaction or TradeFile link?
                  continue;
                  }

                // if Instances and Refcons match...
                if (thisInstance == confInstance)
                  {
                  if (thisRefcon == confRefcon)
                    {
                    thisMatchElement[MATCH_COL_CONFIRMATIONS] = confirmationCounter;
                    thisMatchElement[MATCH_COL_CERTAINTY] = 1;

                    confirmationMatched[confirmationCounter] = true;
                    matchCount++;
                    break;  // Next confirmation please.
                    }
                  else if (thisInstance != '')
                    {
                    /* Additional matching checks for non-PAM instance (which have odd transaction refcons) */

                    switch (thisInstance)
                    {

                      case 'RB':
                        // Compare Venice Ticket to Administrator Reference.

                        if (confRefcon.indexOf(thisTicket) >= 0)
                          {
                          thisMatchElement[MATCH_COL_CONFIRMATIONS] = confirmationCounter;
                          thisMatchElement[MATCH_COL_CERTAINTY] = 1;

                          confirmationMatched[confirmationCounter] = true;
                          matchCount++;
                          break;  // Next confirmation please.
                          }

                      case 'AR':

                        // Compare Venice Reference to Administrator Reference. For AR, Admin reference is superset of Venice reference.

                        if ((thisRefcon.length > 7) && (confRefcon.indexOf(thisRefcon) >= 0))
                          {
                          thisMatchElement[MATCH_COL_CONFIRMATIONS] = confirmationCounter;
                          thisMatchElement[MATCH_COL_CERTAINTY] = 1;

                          confirmationMatched[confirmationCounter] = true;
                          matchCount++;
                          break;  // Next confirmation please.
                          }

                      default:

                    }


                    }
                  } // (thisInstance == confInstance)

                } // If not already matched.
              } // for matchArrayCounter
            } // if confRefcon.length > 0
          }
        } // for confirmationCounter
      }

    // OK, matched the refcons, now what shall we match on ?

    var confISIN;
    var confFlag;
    var confTRADE;
    var confDate;
    var confItem;

    var thisISIN;
    var thisTRADE;
    var thisDate;
    var thisString;
    var thisDatePlusOne;
    var plusOneDayMatch;
    var neo_multiplier;

    // Date.parse or Date.addBusDay is very slow. Turns out that the Date
    // function supports YYYY-MM-DD and adding a single Business Day is easy.
    // At this point we are only setting the MATCH_SOPHIS_DATENEG and
    // MATCH_SOPHIS_DATENEG_PLUSONE values, the rest are set later.

    try
      {
      for (matchArrayCounter = 0; matchArrayCounter < matchArray.length; matchArrayCounter++)
        {
        thisMatchElement = matchArray[matchArrayCounter];

        if (thisMatchElement[MATCH_COL_TRANSACTIONS] !== false)
          {
          thisTransaction = veniceTransactions[thisMatchElement[MATCH_COL_TRANSACTIONS]];

          thisDatePlusOne = new Date(thisTransaction['DATENEG'].substr(0, 10));
          thisDatePlusOne.setDate(thisDatePlusOne.getDate() + 1);
          if (thisDatePlusOne.getDay() == 6)
            {
            thisDatePlusOne.setDate(thisDatePlusOne.getDate() + 1);
            }
          if (thisDatePlusOne.getDay() == 0)
            {
            thisDatePlusOne.setDate(thisDatePlusOne.getDate() + 1);
            }

          thisMatchElement[MATCH_SOPHIS_DATENEG] = new Date(thisTransaction['DATENEG'].substr(0, 10));
          thisMatchElement[MATCH_SOPHIS_DATENEG_PLUSONE] = thisDatePlusOne;
          }

        }
      }
    catch (e)
      {
      console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
      }

    // Lets try tradeDate(+/-), ISIN and position.

    try
      {
      if (matchCount < neolinkConfirmations.length)
        {
        for (confirmationCounter = 0; confirmationCounter < neolinkConfirmations.length; confirmationCounter++)
          {
          if (!confirmationMatched[confirmationCounter])
            {
            confItem = neolinkConfirmations[confirmationCounter];
            confISIN = confItem['neo_isin'];
            confFlag = confItem['neo_t'];
            neo_multiplier = 1.0;

            switch (confItem['neo_cat'])
            {
              case 'R/D':
                neo_multiplier = (confItem['neo_t'].substr(0, 1) == 'D' ? (-1.0) : (1.0));
                break;

              default :
                neo_multiplier = (confItem['neo_t'].substr(0, 1) == 'R' ? (-1.0) : (1.0));
                break;

            }

            confTRADE = parseFloat(confItem['neo_quantity_value']) * neo_multiplier;

            if (confFlag == 'D')
              {
              // Ignore 'D' types ?
              confirmationMatched[confirmationCounter] = true;
              continue;
              }

            confDate = new Date(confItem['neo_trade_date'].substr(0, 10));
            plusOneDayMatch = (-1);

            for (matchArrayCounter = 0; matchArrayCounter < matchArray.length; matchArrayCounter++)
              {
              thisMatchElement = matchArray[matchArrayCounter];

              // If this match item has not already matched to a confirmation
              // and it does reference a Venice transaction....

              if ((thisMatchElement[MATCH_COL_CONFIRMATIONS] === false) && (thisMatchElement[MATCH_COL_TRANSACTIONS] !== false))
                {
                thisTransaction = veniceTransactions[thisMatchElement[MATCH_COL_TRANSACTIONS]];

                thisISIN = thisTransaction['EXTERNREF'];
                thisTRADE = parseFloat(thisTransaction['QUANTITE']);
                thisDate = matchArray[matchArrayCounter][MATCH_SOPHIS_DATENEG]; // Date.parse(thisTransaction['DATENEG'].substr(0,10));
                thisDatePlusOne = matchArray[matchArrayCounter][MATCH_SOPHIS_DATENEG_PLUSONE]; // Date.parse(thisTransaction['DATENEG'].substr(0,10));
                // thisDatePlusOne.addBusDays(1);

                if ((confISIN == thisISIN) && (confTRADE == thisTRADE))
                  {
                  if (thisDate.getTime() == confDate.getTime())
                    {
                    thisMatchElement[MATCH_COL_CONFIRMATIONS] = confirmationCounter;
                    thisMatchElement[MATCH_COL_CERTAINTY] = 2;

                    confirmationMatched[confirmationCounter] = true;
                    matchCount++;
                    break;
                    }
                  else if (thisDatePlusOne.getTime() == confDate.getTime())
                    {
                    plusOneDayMatch = matchArrayCounter;
                    }
                  }
                }
              } // for (matchArrayCounter ....

            // Was there a match for TradeDate plus one, as often is the case
            // when Confirmations are for the day following the entry date.

            if ((!confirmationMatched[confirmationCounter]) && (plusOneDayMatch >= 0))
              {
              thisMatchElement = matchArray[plusOneDayMatch];

              thisMatchElement[MATCH_COL_CONFIRMATIONS] = confirmationCounter;
              thisMatchElement[MATCH_COL_CERTAINTY] = 3;

              confirmationMatched[confirmationCounter] = true;
              matchCount++;
              }

            } // if (!confirmationMatched[confirmationCounter])
          } // for (confirmationCounter ....
        } // (matchCount < neolinkConfirmations.length)
      }
    catch (e)
      {
      console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
      }

    // Aaarrgghh, what can we match on now?

    if (matchCount < neolinkConfirmations.length)
      {
      // TODO Something Here.


      }

    // OK, can't find a match, just insert the remaining confirmations by date.

    try
      {
      if (matchCount < neolinkConfirmations.length)
        {

        for (confirmationCounter = 0; confirmationCounter < neolinkConfirmations.length; confirmationCounter++)
          {
          if (!confirmationMatched[confirmationCounter])
            {
            thisEntryDate = new Date(neolinkConfirmations[confirmationCounter]['neo_trade_date'].substr(0, 10));

            // Start from the bottom of the matchArray as DAtes are descending
            // order
            for (matchArrayCounter = (matchArray.length - 1); matchArrayCounter >= 0; matchArrayCounter--)
              {

              if (matchArray[matchArrayCounter][MATCH_COL_DATE] >= thisEntryDate)
                {
                // Insert matchArray entry.

                matchArray.splice(matchArrayCounter + 1, 0, [-1, false, false, confirmationCounter, new Date(neolinkConfirmations[confirmationCounter]['neo_trade_date'].substr(0, 10)), false, false, false, false, false, false, false]);

                break;
                }
              else if (matchArrayCounter == 0)
                {
                matchArray.splice(0, 0, [-1, false, false, confirmationCounter, new Date(neolinkConfirmations[confirmationCounter]['neo_trade_date'].substr(0, 10)), false, false, false, false, false, false, false]);
                break;
                }
              }

            confirmationMatched[confirmationCounter] = true;

            }
          } // for confirmationCounter
        } // if (matchCount < neolinkConfirmations.length)
      }
    catch (e)
      {
      console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
      }

    // Set other dates in the Match array for comparison purposes.

    // Date.parse or Date.addBusDay is very slow. Turns out that the Date
    // function supports YYYY-MM-DD and adding a single Business Day is easy.
    try
      {
      for (matchArrayCounter = 0; matchArrayCounter < matchArray.length; matchArrayCounter++)
        {
        thisMatchElement = matchArray[matchArrayCounter];

        if (thisMatchElement[MATCH_COL_TRANSACTIONS] !== false)
          {
          thisTransaction = veniceTransactions[thisMatchElement[MATCH_COL_TRANSACTIONS]];

          thisMatchElement[MATCH_SOPHIS_DATECOMPTABLE] = new Date(thisTransaction['DATECOMPTABLE'].substr(0, 10));
          thisMatchElement[MATCH_VENICE_VALUEDATE] = new Date(thisTransaction['DATEVAL'].substr(0, 10));
          }

        if (thisMatchElement[MATCH_COL_TRADEFILE] !== false)
          {
          thisTradefile = tradeFileEntries[thisMatchElement[MATCH_COL_TRADEFILE]];

          if (thisTradefile['tradefiledate'].length > 0)thisMatchElement[MATCH_TRADEFILE_TRADEDATE] = new Date(thisTradefile['tradefiledate'].substr(0, 10));
          }

        if (thisMatchElement[MATCH_COL_CONFIRMATIONS] !== false)
          {
          thisConfirmation = neolinkConfirmations[thisMatchElement[MATCH_COL_CONFIRMATIONS]];

          if (thisConfirmation['neo_trade'].length > 0)thisMatchElement[MATCH_BNP_TRADEDATE] = new Date(thisConfirmation['neo_trade_date'].substr(0, 10));
          if (thisConfirmation['neo_settlement'].length > 0)thisMatchElement[MATCH_BNP_SETTLEDATE] = new Date(thisConfirmation['neo_settlement_date'].substr(0, 10));
          }
        }
      }
    catch (e)
      {
      console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
      }

    cacheObject.data.matchArray = matchArray;
    cacheObject.inProgress = false;
    cacheObject['updateTime'] = (new Date());

    // Now Display the data grid......

    } // Encompasing 'try'
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }


  }

function moFilterTransactionsData(dialogID, cachekey)
  {
  /*
   After matching transactions between BNP & Venice in the 'moMatchTransactionsData()' function,
   this function selects that data to show and deals with presentation issues.
   */

  var cacheObject;
  var dialogDiv;
  var gridID = dialogID + '_grid';
  var grid = $("#" + gridID);

  dialogDiv = $('#' + dialogID);

  // Before we start, ensure that folioListArray has loaded.

  try
    {
    if (document.dataCache.folioListArray.folioToplevelLookup == undefined)
      {
      setTimeout("moFilterTransactionsData('" + dialogID + "','" + cachekey + "')", 200);
      return;
      }
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }

  // OK ......
  try
    {
    var fundValuationLookup = false;  // Match folio accounts to top-level accounts, as used in the drop-down Fund list.

    var folioToplevelLookup = document.dataCache.folioListArray.folioToplevelLookup;  // Match folio accounts to top-level accounts, as used in the drop-down Fund list.
    var folioNameLookup = document.dataCache.folioListArray.folioNameLookup;          // Match top-level accounts to names
    var folioAccounts = document.dataCache.folioAccountsArray.accountsData;           // Match folio Accounts to BNP accounts (first 11 chars only).
    var folioAccountsReverse = document.dataCache.folioAccountsArray.accountsDataReverse; // Match BNP accounts to folio Accounts (first 11 chars only).

    try
      {
      if (document.dataCache.fundValuationArray)
        {
        fundValuationLookup = document.dataCache.fundValuationArray.data;  // Match folio accounts to top-level accounts, as used in the drop-down Fund list.
        }
      }
    catch (e)
      {
      fundValuationLookup = false;
      }

    if (document.dataCache[cachekey])
      {
      cacheObject = document.dataCache[cachekey];
      }
    else
      {
      /* No control object. Abort, Abort.... */
      return;
      }

    if (DEBUG_FLAG) console.log("moFilterTransactionsData : cacheObject.data.dataStatus = " + cacheObject.data.dataStatus);

    var matchArray = cacheObject.data.matchArray;
    var matchArrayCounter;

    var veniceTransactions = cacheObject.data.transactionsData;
    var tradeFileEntries = cacheObject.data.tradefileData;
    var neolinkConfirmations = cacheObject.data.confirmationsData;
    //var formattingArray = new Array();

    var MATCH_COL_CERTAINTY = 0;
    var MATCH_COL_TRANSACTIONS = 1;
    var MATCH_COL_TRADEFILE = 2;
    var MATCH_COL_CONFIRMATIONS = 3;

    var MATCH_COL_DATE = 4;
    var MATCH_SOPHIS_DATENEG = 5;
    var MATCH_SOPHIS_DATENEG_PLUSONE = 6;
    var MATCH_SOPHIS_DATECOMPTABLE = 7;
    var MATCH_VENICE_VALUEDATE = 8;
    var MATCH_TRADEFILE_TRADEDATE = 9;
    var MATCH_BNP_TRADEDATE = 10;
    var MATCH_BNP_SETTLEDATE = 11;

    /* Filter parameters */

    var moDateType = $('#' + dialogID + ' .moDateCombo').val();
    var moDateFrom = $('#' + dialogID + ' .tradeDateFrom').val();

    var filterDateFrom;
    try
      {
      if ((moDateFrom) && (moDateFrom.length > 0))
        {
        filterDateFrom = new Date(moDateFrom);
        filterDateFrom.setHours(0, 0, 0, 0);
        }
      else
        {
        filterDateFrom = new Date();
        filterDateFrom.setDate(filterDateFrom.getDate() - 14);  // Limit dates
        // set in grid.
        filterDateFrom.setHours(0, 0, 0, 0);
        }
      }
    catch (e)
      {
      console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
      filterDateFrom = new Date();
      filterDateFrom.setDate(filterDateFrom.getDate() - 14);  // Limit dates
      // set in grid.
      filterDateFrom.setHours(0, 0, 0, 0);
      }

    var moDateTo = $('#' + dialogID + ' .tradeDateTo').val();
    var filterDateTo;

    try
      {
      filterDateTo = new Date(((moDateTo) && (moDateTo.length > 0)) ? moDateTo : '3000-01-01');
      filterDateTo.setHours(23, 59, 59, 0);
      }
    catch (e)
      {
      console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
      filterDateTo = new Date('3000-01-01');
      }


    var tradeSelectOK = $('#' + dialogID + ' .moTransactionShowOK').is(':checked');
    var tradeSelectPriceDiff = $('#' + dialogID + ' .moTransactionShowTradePriceDiff').is(':checked');
    var tradeSelectSettlementDateDiff = $('#' + dialogID + ' .moTransactionShowSettlementDateDiff').is(':checked');
    var tradeSelectTradeDateDiff = $('#' + dialogID + ' .moTransactionShowTradeDateDiff').is(':checked');
    var tradeSelectTradeAmountDiff = $('#' + dialogID + ' .moTransactionShowTradeAmountDiff').is(':checked');
    var tradeSelectNonFunds = $('#' + dialogID + ' .moTransactionShowNonFunds').is(':checked');
    var tradeSelectOthers = $('#' + dialogID + ' .moTransactionShowOthers').is(':checked');

    var selectedFund = $('#' + dialogID + ' .moFundCombo').val();
    var selectedFundFlag = false;
    var thisCustodianAccount = '';

    if (selectedFund != '1')
      {
      if (selectedFund in folioAccounts)
        {
        selectedFundFlag = true;
        thisCustodianAccount = folioAccounts[selectedFund]['account'];
        }
      }

    var dateFilterType = 1; // Default to Date Entered.
    if ((moDateType) && (moDateType.length > 0))
      {
      if (((moDateFrom) && (moDateFrom.length > 0)) || ((moDateTo) && (moDateTo.length > 0)))
        {
        switch (moDateType)
        {
          case 'DateEntered':
            dateFilterType = 1;
            break;

          case 'DateTraded':
            dateFilterType = 2;
            break;

          case 'DateSettled':
            dateFilterType = 3;
            break;
        }
        }
      }

    // Instance selection
    var selectedInstances = {};

    if ($('#' + dialogID + ' .moTransactionShowPAM').is(':checked'))
      {
      selectedInstances[''] = 1;
      selectedInstances['PAM'] = 1;
      }

    if ($('#' + dialogID + ' .moTransactionShowRB').is(':checked'))
      {
      selectedInstances['RB'] = 1;
      selectedInstances['ROCHEBRUNE'] = 1;
      }

    if ($('#' + dialogID + ' .moTransactionShowAR').is(':checked'))
      {
      selectedInstances['AR'] = 1;
      selectedInstances['ALTAROCCA'] = 1;
      }

    // Now Display the data grid......

    // Build data object for the Grid..

    var gridDataObject = new Array();
    var matchStatus;
    var matchStatusBrief;
    var thisVeniceIndex;
    var thisTradeFileIndex;
    var thisConfirmationsIndex;
    var thisMatchElement;
    var thisTransaction;
    var thisTradefile;
    var thisConfirmation;
    var neo_multiplier;
    var doContinue;

    try
      {
      var newGridDataObject;
      var gridDataObjectIndex = 0;
      // Initialise formatting Array
      var thisFormatObject;
      var templateFormatObject = {
        SOPHISMATCH: 0, TRADEFILEMATCH: 0, CONFIRMATIONMATCH: 0, INSTANCE: '', MATCHSTATUS: 0, MATCHSTATUSBRIEF: 0, SOPHISSTATUS_ID: 0, SOPHISSTATUS: '', BNPSTATUS: '',
        REFCON: 0, ISIN: 0, DEPOSITARY_NAME: 0, NAME: 0, SOPHISTRADE: 0,
        SOPHISSETTLE: 0, SOPHISAMOUNT: 0, SOPHISPRICE: 0, SOPHISCURRENCY: 0, SOPHISSETTLE_MIN: 0, SOPHISSETTLE_MAX: 0, SOPHIS_CONSIDERATION: 0,
        FILETRADE: 0, FILEAMOUNT: 0, FILECURRENCY: 0, CONFIRMTRADE: 0,
        CONFIRMSETTLE: 0, CONFIRMAMOUNT: 0, CONFIRMPRICE: 0, CONFIRMCURRENCY: 0, NEO_BANKREFERENCE: 0,
        SPACER: 0, RN: 0, PAM_FOLIO_ID: 0, BNP_FOLIO_ID: 0, PAM_FUND_NAME: 0, BNP_ACCOUNT: 0, EURO_VALUE: 0, FUND_NAV: 0, PERCENTAGE_OF_NAV: 0, SORT_DATE: 0
      };

      var EMSX_TradeStatus = new Array();

      EMSX_TradeStatus['-10'] = 'Error_Received';
      EMSX_TradeStatus['0'] = 'None';
      EMSX_TradeStatus['10'] = 'NotPlaced';
      EMSX_TradeStatus['20'] = 'Placed_AwaitingResponse';
      EMSX_TradeStatus['30'] = 'Placed_ResponseReceived';
      EMSX_TradeStatus['40'] = 'Routed_AwaitingResponse';
      EMSX_TradeStatus['50'] = 'Routed_ResponseReceived';
      EMSX_TradeStatus['60'] = 'Order_Working';
      EMSX_TradeStatus['70'] = 'Order_Filled';
      EMSX_TradeStatus['80'] = 'Completed';
      EMSX_TradeStatus['100'] = 'Cancelled_RoutesPendingResponse';
      EMSX_TradeStatus['110'] = 'Cancelled_RoutesPendingConfirmation';
      EMSX_TradeStatus['120'] = 'Cancelled_RoutesConfirmed';
      EMSX_TradeStatus['121'] = 'Cancelled_Route_Unexpected';
      EMSX_TradeStatus['130'] = 'Cancelled_OrderPendingConfirmation';
      EMSX_TradeStatus['140'] = 'Cancelled_OrderConfirmed';
      EMSX_TradeStatus['1000'] = 'Deleted';

      for (matchArrayCounter = 0; matchArrayCounter < matchArray.length; matchArrayCounter++)
        {
        thisMatchElement = matchArray[matchArrayCounter];
        thisVeniceIndex = thisMatchElement[MATCH_COL_TRANSACTIONS];
        thisTradeFileIndex = thisMatchElement[MATCH_COL_TRADEFILE];
        thisConfirmationsIndex = thisMatchElement[MATCH_COL_CONFIRMATIONS];
        thisTransaction = veniceTransactions[thisVeniceIndex];
        thisTradefile = tradeFileEntries[thisTradeFileIndex];
        thisConfirmation = neolinkConfirmations[thisConfirmationsIndex];

        matchStatus = '';
        matchStatusBrief = '';

        try //
          {
          neo_multiplier = 1.0;

          if (thisConfirmation)
            {
            switch (thisConfirmation['neo_cat'])
            {
              case 'R/D':
                neo_multiplier = (thisConfirmation['neo_t'].substr(0, 1) == 'D' ? (-1.0) : (1.0));
                break;

              default :
                neo_multiplier = (thisConfirmation['neo_t'].substr(0, 1) == 'R' ? (-1.0) : (1.0));
                break;

            }
            }
          }
        catch (e)
          {
          neo_multiplier = 1.0;
          console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
          }

        try
          {

          newGridDataObject = {
            RN: gridDataObjectIndex,
            MATCHARRAYINDEX: matchArrayCounter,
            SOPHISMATCH: (thisVeniceIndex !== false ? 'true' : 'false'),
            TRADEFILEMATCH: (thisTradeFileIndex !== false ? 'true' : 'false'),
            CONFIRMATIONMATCH: (thisConfirmationsIndex !== false ? 'true' : 'false'),
            INSTANCE: (thisVeniceIndex !== false ? thisTransaction['INSTANCE'] : (thisConfirmationsIndex !== false ? thisConfirmation['instance'] : '')),
            MATCHSTATUS: (thisVeniceIndex !== false ? thisTransaction['LIBELLE'] : (thisTradeFileIndex !== false ? thisTradefile['sophis_refcon'] : '')),
            MATCHSTATUSBRIEF: matchStatusBrief,
            REFCON: (thisVeniceIndex !== false ? thisTransaction['REFCON'] : (thisTradeFileIndex !== false ? thisTradefile['sophis_refcon'] : (thisConfirmationsIndex !== false ? thisConfirmation['neo_bankreference'] : ''))),
            ISIN: (thisConfirmationsIndex !== false ? thisConfirmation['neo_isin'] : (thisVeniceIndex !== false ? thisTransaction['EXTERNREF'] : thisTradefile['instrumentcode'])),
            NAME: (thisVeniceIndex !== false ? thisTransaction['LIBELLE'] : (thisConfirmationsIndex !== false ? thisConfirmation['neo_securitydescription'] : '')),
            DEPOSITARY_NAME: (thisConfirmationsIndex !== false ? thisConfirmation['neo_bank'] : ''), //(thisVeniceIndex !== false ? $.trim(thisTransaction['DEPOSITARY_NAME']) : ''),
            SOPHISTRADE: (thisVeniceIndex !== false ? thisTransaction['DATENEG'] : ''),
            SOPHISSETTLE: (thisVeniceIndex !== false ? thisTransaction['DATECOMPTABLE'] : ''),
            SOPHISAMOUNT: (thisVeniceIndex !== false ? parseFloat(thisTransaction['QUANTITE']) : 0),
            SOPHISPRICE: (thisVeniceIndex !== false ? parseFloat(thisTransaction['PRICE']) : ''), // ( thisTransaction['PRICE'].length > 0 ? parseFloat(thisTransaction['PRICE']) : '') : ''), // Venice 26 Mar 2013
            SOPHISSETTLE_MIN: (thisVeniceIndex !== false ? (thisTransaction['MIN_DATECOMPTABLE'] != undefined ? thisTransaction['MIN_DATECOMPTABLE'] : '') : ''),
            SOPHISSETTLE_MAX: (thisVeniceIndex !== false ? (thisTransaction['MAX_DATECOMPTABLE'] != undefined ? thisTransaction['MAX_DATECOMPTABLE'] : '') : ''),
            SOPHISCURRENCY: (thisVeniceIndex !== false ? getCcyFromSophisCode(thisTransaction['DEVISECTT']) : ''),
            SOPHISSETTLEMENTCURRENCY: (thisVeniceIndex !== false ? getCcyFromSophisCode(thisTransaction['DEVISEPAY']) : ''),
            SOPHISSTATUS: (thisVeniceIndex !== false ? thisTransaction['BO_STATUS'] : ''),
            SOPHISSTATUS_ID: (thisVeniceIndex !== false ? thisTransaction['BO_STATUS_ID'] : ''),
            SOPHIS_CONSIDERATION: (thisVeniceIndex !== false ? parseFloat(thisTransaction['CONSIDERATION']) : 0),
            FILETRADE: (thisTradeFileIndex !== false ? thisTradefile['tradefiledate'].substr(0, 10) : ''),
            FILEAMOUNT: (thisTradeFileIndex !== false ? parseFloat(thisTradefile['units']) * (thisTradefile['transactiontype'].substr(0, 1) == 'R' ? (-1) : (1)) : 0),
            FILECURRENCY: (thisTradeFileIndex !== false ? thisTradefile['currency'] : ''),
            CONFIRMTRADE: (thisConfirmationsIndex !== false ? thisConfirmation['neo_trade_date'].substr(0, 10) : ''),
            CONFIRMSETTLE: (thisConfirmationsIndex !== false ? thisConfirmation['neo_settlement_date'].substr(0, 10) : ''),
            CONFIRMAMOUNT: (thisConfirmationsIndex !== false ? parseFloat(thisConfirmation['neo_quantity_value']) * neo_multiplier : 0),
            CONFIRMPRICE: (thisConfirmationsIndex !== false ? (thisConfirmation['neo_price'].length > 0 ? parseFloat(thisConfirmation['neo_price_value']) : '') : ''),
            CONFIRMCURRENCY: (thisConfirmationsIndex !== false ? thisConfirmation['neo_settlementcurrency'] : ''),
            BNPSTATUS: (thisConfirmationsIndex !== false ? thisConfirmation['neo_st'] + ' ' + thisConfirmation['neo_status'] + ' ' + thisConfirmation['neo_cat'] + ' ' + thisConfirmation['neo_t'] : ''),
            NEO_BANKREFERENCE: (thisConfirmationsIndex !== false ? thisConfirmation['neo_bankreference'] : ''),
            // NEO_T:(thisConfirmationsIndex !== false ? thisConfirmation['neo_t'] : ''), // in BNPSTATUS.
            // NEO_BANK:(thisConfirmationsIndex !== false ? thisConfirmation['neo_bank'] : ''),
            // NEO_ST:(thisConfirmationsIndex !== false ? thisConfirmation['neo_st'] : ''),
            // NEO_STATUS:(thisConfirmationsIndex !== false ? thisConfirmation['neo_status'] : ''),
            EURO_VALUE: 0,
            PERCENTAGE_OF_NAV: 0,// fundValuationLookup
            FUND_NAV: 0,//
            AMOUNT_DIFFERENCE: 0,
            PAM_FOLIO_ID: '',
            BNP_FOLIO_ID: '',
            PAM_FUND_NAME: '',
            BNP_ACCOUNT: '',
            CCY_COLOUR: document.appDefaults.currencyColours.ccy_default,
            SPACER: '',
            FORMATTING: false,
            SORT_DATE: (thisVeniceIndex !== false ? thisTransaction['DATENEG'] : (thisConfirmationsIndex !== false ? thisConfirmation['neo_trade_date'].substr(0, 10) : (thisTradeFileIndex !== false ? thisTradefile['tradefiledate'].substr(0, 10) : '')))
          };
          if (newGridDataObject.SOPHISSETTLEMENTCURRENCY.length > 0)
            {
            newGridDataObject['CCY_COLOUR'] = document.appDefaults.currencyColours[newGridDataObject.SOPHISSETTLEMENTCURRENCY];
            }

          if (thisVeniceIndex !== false)
            {
            if (newGridDataObject.BNPSTATUS.length == 0 && thisTransaction['BROKERID'] != 0)
              {
              newGridDataObject['BNPSTATUS'] = EMSX_TradeStatus[thisTransaction['ORDERSTATUS']];
              }

            if (newGridDataObject['CONFIRMAMOUNT'] == 0 && thisTransaction['BROKERID'] != 0)
              {
              switch (thisTransaction['TRANSTYPE'])
              {
                case 5:
                  newGridDataObject['CONFIRMAMOUNT'] = -thisTransaction['FILLEDAMOUNT'];
                  break;

                case 4:
                  newGridDataObject['CONFIRMAMOUNT'] = thisTransaction['FILLEDAMOUNT'];
                  break;

              }

              }

            }
          newGridDataObject['AMOUNT_DIFFERENCE'] = Math.abs(newGridDataObject['SOPHISAMOUNT'] - newGridDataObject['CONFIRMAMOUNT']);
          }
        catch (e)
          {
          console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
          }

        if (thisVeniceIndex !== false)
          {
          newGridDataObject['PAM_FOLIO_ID'] = folioToplevelLookup[thisTransaction['OPCVM'] + ":" + thisTransaction['INSTANCE']];
          newGridDataObject['PAM_FUND_NAME'] = folioNameLookup[newGridDataObject['PAM_FOLIO_ID']];
          newGridDataObject['BNP_ACCOUNT'] = folioAccounts[newGridDataObject['PAM_FOLIO_ID']]['account'];

          newGridDataObject['EURO_VALUE'] = parseFloat(thisTransaction['CONSIDERATION']) * parseFloat(thisTransaction['FX_RATE']); //
          newGridDataObject['FUND_NAV'] = (fundValuationLookup ? fundValuationLookup[newGridDataObject['PAM_FOLIO_ID']]['EURO_VALUE'] : 0);
          newGridDataObject['PERCENTAGE_OF_NAV'] = Math.abs(newGridDataObject['EURO_VALUE']) / Math.max(1, newGridDataObject['FUND_NAV']);

          if (!(thisTransaction['INSTANCE'] in selectedInstances))
            {
            continue;
            }
          }
        else if (thisTradeFileIndex !== false)
          {

          if (thisTradefile['account'])
            {
            newGridDataObject['BNP_ACCOUNT'] = thisTradefile['account'].substr(thisTradefile['account'].length - 11, 11);
            }
          else
            {
            newGridDataObject['BNP_ACCOUNT'] = '';
            }

          if (newGridDataObject['BNP_ACCOUNT'] in folioAccountsReverse)
            {
            newGridDataObject['PAM_FOLIO_ID'] = folioAccountsReverse[newGridDataObject['BNP_ACCOUNT']]['folio'];
            }
          newGridDataObject['PAM_FUND_NAME'] = folioNameLookup[newGridDataObject['PAM_FOLIO_ID']];

          }
        else if (thisConfirmationsIndex !== false)
          {
          newGridDataObject['BNP_ACCOUNT'] = thisConfirmation['neo_securitiesaccount'].substr(thisConfirmation['neo_securitiesaccount'].length - 11, 11);
          if (newGridDataObject['BNP_ACCOUNT'] in folioAccountsReverse)
            {
            newGridDataObject['PAM_FOLIO_ID'] = folioAccountsReverse[newGridDataObject['BNP_ACCOUNT']]['folio'];
            }

          newGridDataObject['PAM_FUND_NAME'] = folioNameLookup[newGridDataObject['PAM_FOLIO_ID']];

          if (!(thisConfirmation['instance'] in selectedInstances))
            {
            continue;
            }
          }

        // Lookup BNP account specifically, so that it can be checked vs Venice.

        if (thisConfirmationsIndex !== false)
          {
          if (newGridDataObject['BNP_ACCOUNT'] in folioAccountsReverse)
            {
            newGridDataObject['BNP_FOLIO_ID'] = folioAccountsReverse[newGridDataObject['BNP_ACCOUNT']]['folio'];
            }
          }
        else
          {
          newGridDataObject['BNP_FOLIO_ID'] = newGridDataObject['PAM_FOLIO_ID'];
          }

        doContinue = false;

        if (selectedFundFlag)  //
          {
          if (thisVeniceIndex !== false)
            {
            if (selectedFund != folioToplevelLookup[thisTransaction['OPCVM'] + ":" + thisTransaction['INSTANCE']])
              {
              continue;
              }
            }
          else if (thisTradeFileIndex !== false)
            {
            if (thisTradefile['account'].substr(thisTradefile['account'].length - 11, 11) in folioAccountsReverse)
              {
              if (selectedFund != folioAccountsReverse[thisTradefile['account'].substr(thisTradefile['account'].length - 11, 11)]['folio'])
                {
                continue;
                }
              }

            }
          else if (thisConfirmationsIndex !== false)
            {
            if (thisConfirmation['neo_securitiesaccount'].substr(thisConfirmation['neo_securitiesaccount'].length - 11, 11) in folioAccountsReverse)
              {
              if (selectedFund != folioAccountsReverse[thisConfirmation['neo_securitiesaccount'].substr(thisConfirmation['neo_securitiesaccount'].length - 11, 11)]['folio'])
                {
                continue;
                }
              }
            }
          }

        if (doContinue)
          {
          continue;
          }

        if (dateFilterType > 0)
          {
          switch (dateFilterType)
          {
            case 1: // 'DateEntered':

              if (thisTradeFileIndex !== false)
                {
                if ((filterDateFrom.getTime() > thisMatchElement[MATCH_TRADEFILE_TRADEDATE].getTime()) || (filterDateTo.getTime() < thisMatchElement[MATCH_TRADEFILE_TRADEDATE].getTime()))
                  {
                  doContinue = true;
                  }
                }
              else if (thisVeniceIndex !== false)
                {
                if ((filterDateFrom.getTime() > thisMatchElement[MATCH_SOPHIS_DATENEG]) || (filterDateTo.getTime() < thisMatchElement[MATCH_SOPHIS_DATENEG]))
                  {
                  doContinue = true;
                  }
                }
              else if (thisConfirmationsIndex !== false)
                {
                if ((filterDateFrom.getTime() > thisMatchElement[MATCH_BNP_TRADEDATE]) || (filterDateTo.getTime() < thisMatchElement[MATCH_BNP_TRADEDATE]))
                  {
                  doContinue = true;
                  }
                }
              break;

            case 2: // 'DateTraded': (Traded, not ValueDate)

              if ((thisVeniceIndex !== false) && ((filterDateFrom.getTime() > thisMatchElement[MATCH_SOPHIS_DATENEG]) || (filterDateTo.getTime() < thisMatchElement[MATCH_SOPHIS_DATENEG])))
                {
                doContinue = true;
                }
              else if ((thisConfirmationsIndex !== false) && ((filterDateFrom.getTime() > thisMatchElement[MATCH_BNP_TRADEDATE]) || (filterDateTo.getTime() < thisMatchElement[MATCH_BNP_TRADEDATE])))
                {
                doContinue = true;
                }
              break;

            case 3: // 'DateSettled':
              if ((thisVeniceIndex !== false) && ((filterDateFrom.getTime() > thisMatchElement[MATCH_SOPHIS_DATECOMPTABLE]) || (filterDateTo.getTime() < thisMatchElement[MATCH_SOPHIS_DATECOMPTABLE])))
                {
                doContinue = true;
                }
              else if ((thisConfirmationsIndex !== false) && ((filterDateFrom.getTime() > thisMatchElement[MATCH_BNP_SETTLEDATE]) || (filterDateTo.getTime() < thisMatchElement[MATCH_BNP_SETTLEDATE])))
                {
                doContinue = true;
                }

              break;
          }


          }

        if (doContinue)
          {
          continue;
          }

        /* Calculate formatting */

        try
          {
          var thisGridDataObject;
          doContinue = true;

          // thisMatchElement = matchArray[matchArrayCounter]; // Already set.
          thisGridDataObject = newGridDataObject;

          matchStatus = thisGridDataObject['NAME'] + '<br/>' +
            thisGridDataObject['PAM_FUND_NAME'] + '<br/>';

          thisFormatObject = jQuery.extend({}, templateFormatObject);

          // Check 1) Instrument
          // Check 2) Trade Dates
          // Check 3) Settlement Dates
          // Check 4) Transaction Quantity / Amount
          // Check 5) Transaction Price
          // Check 6) Transaction Currency

          if (thisVeniceIndex !== false)
            {
            if ((thisGridDataObject['DEPOSITARY_NAME'].length == 0) && (false)) // TODO()  Venice does not have Depositary Information.
              {
              matchStatusBrief = (matchStatusBrief.length == 0 ? 'DEPOSITARY' : matchStatusBrief); //
              matchStatus = matchStatus + (matchStatus.length > 0 ? '<br/>' : '') + 'Depositary : Venice is missing a Depositary name. This trade will not be sent to BNP!';
              thisFormatObject['MATCHSTATUS'] = MO_TRANSACTION_SEVEREWARNING_FLAG;
              thisFormatObject['MATCHSTATUSBRIEF'] = MO_TRANSACTION_SEVEREWARNING_FLAG;
              thisFormatObject['DEPOSITARY_NAME'] = MO_TRANSACTION_SEVEREWARNING_FLAG;

              doContinue = false; // always show these ones.
              }

            if (thisGridDataObject['PERCENTAGE_OF_NAV'] >= 0.10)
              {
              matchStatusBrief = (matchStatusBrief.length == 0 ? 'Weighting' : matchStatusBrief); //
              matchStatus = matchStatus + (matchStatus.length > 0 ? '<br/>' : '') + 'This transaction represents more than 10% of Fund NAV.';
              thisFormatObject['MATCHSTATUS'] = MO_TRANSACTION_SEVEREWARNING_FLAG;
              thisFormatObject['MATCHSTATUSBRIEF'] = MO_TRANSACTION_SEVEREWARNING_FLAG;

              doContinue = false; // always show these ones.
              }

            if (thisTradeFileIndex !== false)
              {
              try
                {
                // Venice vs BNP Account
                if (thisGridDataObject['BNP_FOLIO_ID'] != thisGridDataObject['PAM_FOLIO_ID'])
                  {
                  matchStatusBrief = (matchStatusBrief.length == 0 ? 'ACCOUNT' : matchStatusBrief); //
                  matchStatus = matchStatus + (matchStatus.length > 0 ? '<br/>' : '') + 'Account : Venice (' + thisGridDataObject['PAM_FOLIO_ID'] + ') ' + thisGridDataObject['PAM_FUND_NAME'] + ' vs BNP (' + thisGridDataObject['BNP_FOLIO_ID'] + ') ' + folioNameLookup[newGridDataObject['BNP_FOLIO_ID']];
                  thisFormatObject['BNP_ACCOUNT'] = MO_TRANSACTION_WARNING_FLAG;
                  thisFormatObject['PAM_FOLIO_ID'] = MO_TRANSACTION_WARNING_FLAG;
                  thisFormatObject['PAM_FUND_NAME'] = MO_TRANSACTION_WARNING_FLAG;

                  if (tradeSelectOthers) doContinue = false;
                  }

                // Settlement Dates Changed, Important to Mansour

                if (((thisGridDataObject['SOPHISSETTLE'] != thisGridDataObject['SOPHISSETTLE_MIN']) && (thisGridDataObject['SOPHISSETTLE_MIN'] != '')) || (((thisGridDataObject['SOPHISSETTLE'] != thisGridDataObject['SOPHISSETTLE_MAX']) && (thisGridDataObject['SOPHISSETTLE_MAX'] != ''))))
                  {
                  matchStatusBrief = (matchStatusBrief.length == 0 ? 'S. Settle' : matchStatusBrief);
                  matchStatus = matchStatus + (matchStatus.length > 0 ? '<br/>' : '') + 'Venice Settlement Dates have changed : ' + thisGridDataObject['SOPHISSETTLE'] + ' vs ' + thisGridDataObject['SOPHISSETTLE_MIN'] + ' vs ' + thisGridDataObject['SOPHISSETTLE_MAX'];
                  thisFormatObject['SOPHISSETTLE'] = MO_TRANSACTION_SETTLEMENT_CHANGED_FLAG | MO_TRANSACTION_HIGHLIGHT_FLAG;
                  thisFormatObject['SOPHISSETTLE_MIN'] = MO_TRANSACTION_SETTLEMENT_CHANGED_FLAG | MO_TRANSACTION_HIGHLIGHT_FLAG;
                  thisFormatObject['SOPHISSETTLE_MAX'] = MO_TRANSACTION_SETTLEMENT_CHANGED_FLAG | MO_TRANSACTION_HIGHLIGHT_FLAG;
                  thisFormatObject['MATCHSTATUS'] = MO_TRANSACTION_WARNING_FLAG;

                  if (tradeSelectSettlementDateDiff) doContinue = false;
                  }

                // Instrument
                if (thisTransaction['EXTERNREF'] != thisTradefile['instrumentcode'])
                  {
                  matchStatusBrief = (matchStatusBrief.length == 0 ? 'ISIN' : matchStatusBrief);
                  matchStatus = matchStatus + (matchStatus.length > 0 ? '<br/>' : '') + 'ISIN : Venice ' + thisTransaction['EXTERNREF'].substr(0, 10) + ' vs Tradefile ' + thisTradefile['instrumentcode'].substr(0, 10);
                  thisFormatObject['ISIN'] = MO_TRANSACTION_WARNING_FLAG;

                  if (tradeSelectOthers) doContinue = false;
                  }

                // Trade Dates
                if (thisMatchElement[MATCH_SOPHIS_DATENEG].getTime() != thisMatchElement[MATCH_TRADEFILE_TRADEDATE].getTime())
                  {
                  matchStatusBrief = (matchStatusBrief.length == 0 ? 'T. Date' : matchStatusBrief);
                  matchStatus = matchStatus + (matchStatus.length > 0 ? '<br/>' : '') + 'Trade Date : ' + thisTransaction['DATENEG'].substr(0, 10) + ' vs ' + thisTradefile['tradefiledate'].substr(0, 10);
                  thisFormatObject['SOPHISTRADE'] = MO_TRANSACTION_HIGHLIGHT_FLAG;
                  thisFormatObject['FILETRADE'] = MO_TRANSACTION_HIGHLIGHT_FLAG;

                  if (tradeSelectTradeDateDiff) doContinue = false;
                  }

                // Settlement Dates - N/A for Trade file

                // Transaction Quantity / Amount

                if (thisGridDataObject.SOPHISAMOUNT != thisGridDataObject.FILEAMOUNT)
                  {
                  if ((thisGridDataObject.FILEAMOUNT != 0) || (thisTransaction['AMOUNT'] != thisTradefile['value']))
                    {
                    matchStatusBrief = (matchStatusBrief.length == 0 ? 'Amount' : matchStatusBrief);
                    matchStatus = matchStatus + (matchStatus.length > 0 ? '<br/>' : '') + 'Amount : Venice ' + (thisGridDataObject.SOPHISAMOUNT == '[Missing]', '', thisGridDataObject.SOPHISAMOUNT) + ' vs Tradefile ' + (thisGridDataObject.FILEAMOUNT == '' ? '[Missing]' : thisGridDataObject.FILEAMOUNT);
                    thisFormatObject['SOPHISAMOUNT'] = MO_TRANSACTION_WARNING_FLAG;
                    thisFormatObject['FILEAMOUNT'] = MO_TRANSACTION_WARNING_FLAG;

                    if (tradeSelectTradeAmountDiff) doContinue = false;
                    }
                  }

                // Transaction Price - N/A for Trade file

                }
              catch (e)
                {
                console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
                }
              }

            if (thisConfirmationsIndex !== false)
              {
              try
                {
                // Instrument
                if (thisTransaction['EXTERNREF'] != thisConfirmation['neo_isin']) // Instrument
                  {
                  matchStatusBrief = (matchStatusBrief.length == 0 ? 'ISIN' : matchStatusBrief);
                  matchStatus = matchStatus + (matchStatus.length > 0 ? '<br/>' : '') + 'ISIN : Venice ' + thisTransaction['EXTERNREF'] + ' vs BNP ' + thisConfirmation['neo_isin'];
                  thisFormatObject['ISIN'] = MO_TRANSACTION_WARNING_FLAG;

                  if (tradeSelectOthers) doContinue = false;
                  }

                // Trade Date
                if ((thisMatchElement[MATCH_VENICE_VALUEDATE].getTime() != thisMatchElement[MATCH_BNP_TRADEDATE].getTime()) && (thisMatchElement[MATCH_SOPHIS_DATENEG_PLUSONE].getTime() != thisMatchElement[MATCH_BNP_TRADEDATE].getTime()))
                  {
                  matchStatusBrief = (matchStatusBrief.length == 0 ? 'T. Date' : matchStatusBrief);
                  matchStatus = matchStatus + (matchStatus.length > 0 ? '<br/>' : '') + 'Trade Date : Venice ' + thisTransaction['DATENEG'].substr(0, 10) + ' vs BNP ' + thisConfirmation['neo_trade_date'].substr(0, 10);
                  thisFormatObject['SOPHISTRADE'] = MO_TRANSACTION_HIGHLIGHT_FLAG;
                  thisFormatObject['CONFIRMTRADE'] = MO_TRANSACTION_HIGHLIGHT_FLAG;

                  if (tradeSelectTradeDateDiff) doContinue = false;
                  }

                // Settlement Date
                if ((thisMatchElement[MATCH_BNP_SETTLEDATE]) && (thisMatchElement[MATCH_SOPHIS_DATECOMPTABLE].getTime() != thisMatchElement[MATCH_BNP_SETTLEDATE].getTime()))
                  {
                  matchStatusBrief = (matchStatusBrief.length == 0 ? 'S. Date' : matchStatusBrief);
                  matchStatus = matchStatus + (matchStatus.length > 0 ? '<br/>' : '') + 'Settlement Date : Venice ' + thisTransaction['DATECOMPTABLE'].substr(0, 10) + ' vs BNP ' + thisConfirmation['neo_settlement_date'].substr(0, 10);
                  thisFormatObject['SOPHISSETTLE'] = thisFormatObject['SOPHISSETTLE'] | MO_TRANSACTION_HIGHLIGHT_FLAG;
                  thisFormatObject['CONFIRMSETTLE'] = MO_TRANSACTION_HIGHLIGHT_FLAG;

                  if (tradeSelectSettlementDateDiff) doContinue = false;
                  }

                // Transaction Quantity / Amount
                if (thisGridDataObject.SOPHISAMOUNT != thisGridDataObject.CONFIRMAMOUNT)
                  {
                  matchStatusBrief = (matchStatusBrief.length == 0 ? 'Amount' : matchStatusBrief);
                  matchStatus = matchStatus + (matchStatus.length > 0 ? '<br/>' : '') + 'Amount : Venice ' + (thisGridDataObject.SOPHISAMOUNT == '[Missing]', '', thisGridDataObject.SOPHISAMOUNT) + ' vs BNP ' + (thisGridDataObject.CONFIRMAMOUNT == '' ? '[Missing]' : thisGridDataObject.CONFIRMAMOUNT);
                  thisFormatObject['SOPHISAMOUNT'] = MO_TRANSACTION_WARNING_FLAG;
                  thisFormatObject['CONFIRMAMOUNT'] = MO_TRANSACTION_WARNING_FLAG;

                  if (tradeSelectTradeAmountDiff) doContinue = false;
                  }

                // Transaction Price

                if (Math.abs(thisGridDataObject.SOPHISPRICE - thisGridDataObject.CONFIRMPRICE) >= MO_TRANSACTION_PRICE_THRESHOLD)
                  {
                  matchStatusBrief = (matchStatusBrief.length == 0 ? 'Price' : matchStatusBrief);
                  matchStatus = matchStatus + (matchStatus.length > 0 ? '<br/>' : '') + 'Price : Venice ' + (thisGridDataObject.SOPHISPRICE == '[Missing]', '', thisGridDataObject.SOPHISPRICE) + ' vs BNP ' + (thisGridDataObject.CONFIRMPRICE == '' ? '[Missing]' : thisGridDataObject.CONFIRMPRICE);
                  thisFormatObject['SOPHISPRICE'] = MO_TRANSACTION_WARNING_FLAG;
                  thisFormatObject['CONFIRMPRICE'] = MO_TRANSACTION_WARNING_FLAG;

                  if (tradeSelectPriceDiff) doContinue = false;
                  }

                // Check that BNP Status is Empty if PAM Status is cancelled

                if ((thisGridDataObject.SOPHISSTATUS_ID != undefined) && (document.appDefaults.cancelledByUs.indexOf(thisGridDataObject.SOPHISSTATUS_ID) >= 0))
                  {
                  if ((thisGridDataObject.BNPSTATUS != undefined) && (thisGridDataObject.BNPSTATUS.length > 0))
                    {
                    thisFormatObject['SOPHISSTATUS'] = MO_TRANSACTION_WARNING_FLAG;
                    thisFormatObject['BNPSTATUS'] = MO_TRANSACTION_WARNING_FLAG;
                    }
                  }

                }
              catch (e)
                {
                console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
                }
              }
            }
          else if (thisTradeFileIndex !== false)
            {
            if (thisConfirmationsIndex !== false)
              {
              try
                {
                // Instrument
                if (thisTradefile['instrumentcode'] != thisConfirmation['neo_isin']) // Instrument
                  {
                  matchStatusBrief = (matchStatusBrief.length == 0 ? 'ISIN' : matchStatusBrief);
                  matchStatus = matchStatus + (matchStatus.length > 0 ? '<br/>' : '') + 'ISIN : Tradefile ' + thisTradefile['instrumentcode'] + ' vs BNP ' + thisConfirmation['neo_isin'];
                  thisFormatObject['ISIN'] = MO_TRANSACTION_WARNING_FLAG;

                  if (tradeSelectOthers) doContinue = false;
                  }

                // Trade Date
                if (thisMatchElement[MATCH_TRADEFILE_TRADEDATE].getTime() != thisMatchElement[MATCH_BNP_TRADEDATE].getTime()) //
                  {
                  matchStatusBrief = (matchStatusBrief.length == 0 ? 'T. Date' : matchStatusBrief);
                  matchStatus = matchStatus + (matchStatus.length > 0 ? '<br/>' : '') + 'Trade Date : Tradefile ' + thisTradefile['tradefiledate'].substr(0, 10) + ' vs BNP ' + thisConfirmation['neo_trade_date'].substr(0, 10);
                  thisFormatObject['FILETRADE'] = MO_TRANSACTION_HIGHLIGHT_FLAG;
                  thisFormatObject['CONFIRMTRADE'] = MO_TRANSACTION_HIGHLIGHT_FLAG;

                  if (tradeSelectTradeDateDiff) doContinue = false;
                  }

                // Settlement Dates - N/A for Trade File

                // Transaction Quantity / Amount

                if (thisGridDataObject.FILEAMOUNT != thisGridDataObject.CONFIRMAMOUNT)
                  {
                  matchStatusBrief = (matchStatusBrief.length == 0 ? 'Amount' : matchStatusBrief);
                  matchStatus = matchStatus + (matchStatus.length > 0 ? '<br/>' : '') + 'Amount : Tradefile ' + (thisGridDataObject.FILEAMOUNT == '[Missing]', '', thisGridDataObject.FILEAMOUNT) + ' vs BNP ' + (thisGridDataObject.CONFIRMAMOUNT == '[Missing]', '', thisGridDataObject.CONFIRMAMOUNT);
                  thisFormatObject['FILEAMOUNT'] = MO_TRANSACTION_WARNING_FLAG;
                  thisFormatObject['CONFIRMAMOUNT'] = MO_TRANSACTION_WARNING_FLAG;

                  if (tradeSelectTradeAmountDiff) doContinue = false;
                  }

                // Transaction Price - N/A for Trade File

                }
              catch (e)
                {
                console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
                }
              }
            }

          thisGridDataObject.MATCHSTATUS = matchStatus;
          thisGridDataObject.MATCHSTATUSBRIEF = matchStatusBrief;
          thisGridDataObject.FORMATTING = thisFormatObject;
          //formattingArray.push(thisFormatObject);

          }
        catch (e)
          {
          console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
          }


        // tradeSelectOK

        if ((matchStatusBrief.length == 0) && (thisVeniceIndex !== false) && (thisTradeFileIndex !== false) && (thisConfirmationsIndex !== false))
          {
          // Complete line, no issues :-

          if (tradeSelectOK)
            {
            doContinue = false;
            }
          else
            {
            // Show trade if no problems and flag = true.
            doContinue = true;
            }
          }
        else if ((matchStatusBrief.length == 0) && ((thisVeniceIndex === false) || (thisTradeFileIndex === false) || (thisConfirmationsIndex === false)))
          {
          // Incomplete line, no specific issues :-

          if ((thisVeniceIndex !== false) && (thisTransaction['TYPE'] != 'Z') && (thisTransaction['TYPE'] != 'Fund')) // NPP 26/03/2013 - Allow for Venice Fund Type.
            {
            if (tradeSelectNonFunds)
              {
              doContinue = false;
              }
            }
          else if (tradeSelectOthers)
            {
            doContinue = false;
            }
          }

        if (doContinue)
          {
          continue;
          }

        gridDataObject.push(newGridDataObject);
        gridDataObjectIndex++;

        } // For

      //cacheObject.data.formattingArray = formattingArray;

      }
    catch (e)
      {
      console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
      }

    // Default Sort

    gridDataObject.sort(
      function (a, b)
      {
      // Sort_Date Desc, Name Asc
      if (a.SORT_DATE > b.SORT_DATE) return -1;
      if (a.SORT_DATE < b.SORT_DATE) return 1;
      if (a.NAME < b.NAME) return -1;
      if (a.NAME > b.NAME) return 1;
      return 0;
      });

    cacheObject.data.gridDataObject = gridDataObject;

    // Will now Calculate trade status, differences and formatting options.

    // Set Grid

    if (!document.appDefaults) document.appDefaults = {};

    // Get / Set Column definition array

    var colArray = new Array();
    var existingColSet = new Array();
    if ((grid.flexExist()) && (gridDataObject.length > 0))
      {
      existingColSet = grid.flexGet('colModel');
      }

    if ((gridDataObject.length == 0) || (!grid.flexExist()) || (existingColSet.length <= 2))
      {
      if ((gridDataObject.length > 0) && (document.appDefaults['moTransactionColumns']) && (document.appDefaults['moTransactionColumns'].length > 2))
        {
        // If I've done this before, use that....

        colArray = document.appDefaults.moTransactionColumns;
        }
      else
        {
        // Build Array to set Grid Colummn properties.

        // Ensure that default object exists and is OK.

        if (!document.appDefaults.moTransactionGridDefaults) document.appDefaults.moTransactionGridDefaults = {initialColumnOrder: false};
        if (!document.appDefaults.moTransactionGridDefaults.DEFAULT) document.appDefaults.moTransactionGridDefaults.DEFAULT = {visible: true};

        var knownColumnOrder = document.appDefaults.moTransactionGridDefaults.initialColumnOrder;
        var alwaysHidden = document.appDefaults.moTransactionGridDefaults.alwaysHidden;
        var colsAlreadySet = {};
        var thisColumnName;

        // OK Set the columns I know about, then sweep up the rest.
        if (gridDataObject.length == 0)
          {
          gridDataObject.push({Status: "There are no transactions to compare."});
          }

        if (knownColumnOrder)
          {
          for (var i = 0; i < knownColumnOrder.length; i++)
            {
            try
              {
              if (!knownColumnOrder[i]) continue;

              thisColumnName = knownColumnOrder[i];

              if (thisColumnName in gridDataObject[0])
                {
                if (thisColumnName in document.appDefaults.moTransactionGridDefaults)
                  {
                  colArray.push(document.appDefaults.moTransactionGridDefaults[thisColumnName]);
                  }
                else
                  {
                  colArray.push(document.appDefaults.moTransactionGridDefaults['DEFAULT']);
                  }

                colsAlreadySet[thisColumnName] = true;
                }
              }
            catch (e)
              {
              console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
              }
            }
          }

        // Sweep up columns I do not cater for specifically

        for (var gridDataItem in gridDataObject[0])
          {
          try
            {
            thisColumnName = gridDataItem;
            var thisValue = gridDataObject[0][gridDataItem];

            if (!colsAlreadySet[thisColumnName])
              {
              if (thisColumnName in document.appDefaults.moTransactionGridDefaults)
                {
                // Col has a template, but is not in the Ordered Cols set.
                colArray.push(document.appDefaults.moTransactionGridDefaults[thisColumnName]);
                }
              else
                {
                var thisColumnFormat = {headerText: gridDataItem, dataKey: gridDataItem, width: 100};

                if (isFinite(thisValue))
                  {
                  thisColumnFormat['dataType'] = "number";
                  }
                else
                  {
                  thisColumnFormat['dataType'] = "string";
                  }

                var thisCol = jQuery.extend(thisColumnFormat, document.appDefaults.moTransactionGridDefaults['DEFAULT']);

                colArray.push(thisCol);
                }

              colsAlreadySet[thisColumnName] = true;
              }
            }
          catch (e)
            {
            console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
            }

          }

        // Set Column details for next time.

        document.appDefaults.moTransactionColumns = colArray;
        } // ColArray not already exist.

      }
    else
      {
      existingColSet = grid.flexGet('colModel');
      colArray = existingColSet;
      }

    if (dialogID.length > 0)
      {
      //var gridID = dialogID + '_grid';
      // var splitterID = dialogID + '_splitter';
      // var splitterLeftID = dialogID + '_splitterLeft';
      //var grid = $("#" + gridID);

      document.buildingDialogID = dialogID;

      // The '(grid.flexGet('colModel').length <= 2)' bit is because if this
      // grid is drawn the first time with no data
      // then it only has one column, and unlike the Settling-Cash grid, I do
      // not check for column changes here
      // because they do not usually change.

      if ((!grid.flexExist()) || (grid.flexGet('colModel').length <= 2) || (colArray.length <= 2))
        {
        grid.flexigrid({
          dataType: 'json',
          jsArray: gridDataObject,
          colModel: colArray,
          rowStyleFormatter: moTransactionsGridRowFormatter,
          cellStyleFormatter: moTransactionsGridCellFormatter,
          sortall: true,
          width: 100,
          height: 100
          /*,treeView:[{groupby:'PAM_FUND_NAME', sortorder:1, display:'PAM_FUND_NAME'}]  /**/
        }) //
        }
      else
        {
        // grid.flexOptions({colModel:colArray});
        // grid.flexReload({jsArray:gridDataObject});
        grid.flexAddData({jsArray: gridDataObject});

        /* // Don't need to re-build, the columns don't change.... */
        }

      // flexSize
      // Resize Grid, if it exists. Fit to the right splitter panel.

      var splitterLeft = $("#" + dialogID + '_splitterLeft');
      var headingLeft = $("#" + dialogID + '_leftHeading');
      var topGap = 40;

      if (headingLeft[0])
        {
        topGap = headingLeft[0].clientHeight;
        }

      if ((splitterLeft[0]) && (grid[0]))
        {
        grid.flexSize({width: (dialogDiv[0].clientWidth), height: (dialogDiv[0].clientHeight - topGap)});
        }
      }
    } // Encompasing 'try'
  catch
    (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }
  finally
    {
    try
      {
      if ((dialogID != undefined) && (dialogID.length > 0))
        {
        // $(dialogDiv).activity(false);
        // $(dialogdiv).find('.loaderGif').css('visibility', 'hiden');
        }
      }
    catch (e)
      {
      console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
      }
    }

  }

function getMoTransactions(parameters, customCallback, customParameters)
  {
  var folioID;
  var SICOVAM;
  var date_from;
  var date_from_format;
  var date_to;
  var date_to_format;
  var no_c;
  var no_e;
  var uniqueID;
  var dialogID;

  if (!parameters)
    {
    parameters = {};
    }

  uniqueID = 'GetData_' + getUniqueCounter();
  dialogID = (parameters['DIALOGID'] ? parameters['DIALOGID'] : '');
  folioID = (parameters['FOLIO'] ? parameters['FOLIO'] : 0); // SQL Server requires 0 for 'All' Funds
  SICOVAM = (parameters['SICOVAM'] ? parameters['SICOVAM'] : 0);
  date_from = (parameters['DATE_FROM'] ? parameters['DATE_FROM'] : '1970-01-01');
  date_from_format = (parameters['DATE_FROM_FORMAT'] ? parameters['DATE_FROM_FORMAT'] : 'YYYY-MM-DD');
  date_to = (parameters['DATE_TO'] ? parameters['DATE_TO'] : '2199-07-09');
  date_to_format = (parameters['DATE_TO_FORMAT'] ? parameters['DATE_TO_FORMAT'] : 'YYYY-MM-DD');
  no_c = (parameters['NO_C'] ? parameters['NO_C'] : 1);
  no_e = (parameters['NO_E'] ? parameters['NO_C'] : 1);

  // Establish Query parameters object

  var formvar = {
    'FOLIO': folioID,
    'SICOVAM': SICOVAM,
    'DATE_FROM': date_from,
    'DATE_FROM_FORMAT': date_from_format,
    'DATE_TO': date_to,
    'DATE_TO_FORMAT': date_to_format,
    'NO_C': no_c,
    'NO_E': no_e,
    'DIALOGID': dialogID,
    'ID': uniqueID,
    'USERID': document.dakotaUserID,
    'TOKEN': document.dakotaSessionKey
  };

  var cacheObject = document.dataCache.preloadURL;
  if (customCallback)
    {
    cacheObject[uniqueID] = {callback: customCallback, parameters: customParameters};
    }

  // Prepare parameters object for data fetch.

  var formJSON = JSON.stringify(formvar);

  // Get data.
  // Venice transactions data query.

  var url = "php/getTransactionQuery.php";

  $.ajax({type: "POST",
    url: url,
    data: formvar,
    success: function (html)
      {
      var rVal = JSON.parse(html);
      var formObject;
      var indexVal;
      var thisUniqueID = uniqueID;
      var dialogID = '';
      var cacheObject;
      var cachekey;

      var formData = decodeURIComponent(this.data);
      formObject = QueryStringToJSON(formData);

      /*
       try
       {
       indexVal = formData.indexOf('=');
       if (indexVal >= 0)
       {
       formObject = JSON.parse(formData.substr(indexVal + 1));
       }
       else
       {
       formObject = JSON.parse(formData);
       }
       }
       catch (e)
       {
       formObject = {'ID':uniqueID};
       console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
       }

       */

      if (formObject['ID'])
        {
        thisUniqueID = formObject['ID'];
        }

      if (formObject['DIALOGID'])
        {
        dialogID = formObject['DIALOGID'];
        }


      cachekey = MO_TRANSACTION_CACHE_PREFIX + dialogID;
      if (document.dataCache[cachekey])
        {
        cacheObject = document.dataCache[cachekey]
        cacheObject.data.transactionsData = rVal;
        cacheObject.data.transactionsFormObject = formObject;
        cacheObject.data.dataStatus = cacheObject.data.dataStatus | 1;
        }

      try
        {
        cacheObject = document.dataCache.preloadURL;
        if (cacheObject[uniqueID])
          {
          var callbackObj = cacheObject[thisUniqueID];

          if (callbackObj && (callbackObj['callback']))
            {
            if (callbackObj['parameters'])
              {
              callbackObj['callback'](formObject, callbackObj['parameters']);
              }
            else
              {
              callbackObj['callback'](formObject);
              }
            }

          // Clean cache.
          delete cacheObject[uniqueID];

          }
        }
      catch (e)
        {
        console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
        }

      }
  });

  }

function getMoTradeFileData(parameters, customCallback, customParameters)
  {
  var REFCON;
  var date_from;
  var date_to;
  var ISIN;
  var uniqueID;
  var dialogID;

  if (!parameters)
    {
    parameters = {};
    }

  uniqueID = 'GetData_' + getUniqueCounter();
  dialogID = (parameters['DIALOGID'] ? parameters['DIALOGID'] : '');
  REFCON = (parameters['REFCON'] ? parameters['REFCON'] : '');
  date_from = (parameters['DATE_FROM'] ? parameters['DATE_FROM'] : '');
  /* Expects SQL date format 121 (YYYY-MM-DD), or '' */
  date_to = (parameters['DATE_TO'] ? parameters['DATE_TO'] : '');
  /* Expects SQL date format 121 (YYYY-MM-DD), or '' */
  ISIN = (parameters['ISIN'] ? parameters['ISIN'] : '');

  var formvar = {
    'REFCON': REFCON,
    'DATE_FROM': date_from,
    'DATE_TO': date_to,
    'ISIN': ISIN,
    'DIALOGID': dialogID,
    'ID': uniqueID,
    'USERID': document.dakotaUserID,
    'TOKEN': document.dakotaSessionKey
  };

  var cacheObject = document.dataCache.preloadURL;
  if (customCallback)
    {
    cacheObject[uniqueID] = {callback: customCallback, parameters: customParameters};
    }

  // Prepare parameters object for data fetch.

  var formJSON = JSON.stringify(formvar);

  // Get data.
  // Venice transactions data query.

  var url = "php/getTradeFilesQuery.php";

  $.ajax({type: "POST",
    url: url,
    data: formvar,
    success: function (html)
      {
      var rVal = JSON.parse(html);
      var formvar;
      var formObject;
      var indexVal;
      var thisUniqueID = uniqueID;
      var dialogID = '';
      var cacheObject;
      var cachekey;

      var formData = decodeURIComponent(this.data);
      formObject = QueryStringToJSON(formData);

      /*
       try
       {
       indexVal = formData.indexOf('=');
       if (indexVal >= 0)
       {
       formObject = JSON.parse(formData.substr(indexVal + 1));
       }
       else
       {
       formObject = JSON.parse(formData);
       }
       }
       catch (e)
       {
       formObject = {'ID':uniqueID};
       console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
       }
       */

      if (formObject['ID'])
        {
        thisUniqueID = formObject['ID'];
        }

      if (formObject['DIALOGID'])
        {
        dialogID = formObject['DIALOGID'];
        }

      cachekey = MO_TRANSACTION_CACHE_PREFIX + dialogID;
      if (document.dataCache[cachekey])
        {
        cacheObject = document.dataCache[cachekey]
        cacheObject.data.tradefileData = rVal;
        cacheObject.data.dataStatus = cacheObject.data.dataStatus | 2;
        cacheObject.data.tradefileFormObject = formObject;

        }

      try
        {
        var cacheObject = document.dataCache.preloadURL;
        if (cacheObject[uniqueID])
          {
          var callbackObj = cacheObject[thisUniqueID];

          if (callbackObj && (callbackObj['callback']))
            {
            if (callbackObj['parameters'])
              {
              callbackObj['callback'](formObject, callbackObj['parameters']);
              }
            else
              {
              callbackObj['callback'](formObject);
              }
            }

          // Clean cache.
          delete cacheObject[uniqueID];

          }
        }
      catch (e)
        {
        console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
        }

      }
  });

  }

function getMoConfirmationData(parameters, customCallback, customParameters)
  {
  var KnowledgeDate = '';
  var date_from;
  var date_to;
  var SecuritiesAccount;
  var MaxCaptureID;
  var uniqueID = 'MoT' + getUniqueCounter();
  var dialogID;

  if (!parameters)
    {
    parameters = {};
    }

  dialogID = (parameters['DIALOGID'] ? parameters['DIALOGID'] : '');
  KnowledgeDate = (parameters['KNOWLEDGEDATE'] ? parameters['KNOWLEDGEDATE'] : '');
  /* Expects SQL date format 121 (YYYY-MM-DD HH:mm:ss.sss), or '' */
  date_from = (parameters['DATE_FROM'] ? parameters['DATE_FROM'] : '');
  /* Expects SQL date format 103 (YYYY-MM-DD), or '' */
  date_to = (parameters['DATE_TO'] ? parameters['DATE_TO'] : '');
  /* Expects SQL date format 103, or '' */
  SecuritiesAccount = (parameters['SECURITIES_ACCOUNT'] ? parameters['SECURITIES_ACCOUNT'] : '');
  MaxCaptureID = (parameters['MAX_CAPTURE_ID'] ? parameters['MAX_CAPTURE_ID'] : '');

  var formvar = {
    'KNOWLEDGEDATE': KnowledgeDate,
    'DATE_FROM': date_from,
    'DATE_TO': date_to,
    'SECURITIES_ACCOUNT': SecuritiesAccount,
    'MAX_CAPTURE_ID': MaxCaptureID,
    'DIALOGID': dialogID,
    'ID': uniqueID,
    'USERID': document.dakotaUserID,
    'TOKEN': document.dakotaSessionKey
  };

  var cacheObject = document.dataCache.preloadURL;
  if (customCallback)
    {
    cacheObject[uniqueID] = {callback: customCallback, parameters: customParameters};
    }

  // Prepare parameters object for data fetch.

  var formJSON = JSON.stringify(formvar);

  // Get data.
  // Venice transactions data query.

  var url = "php/getConfirmationsQuery.php";

  $.ajax({type: "POST",
    url: url,
    data: formvar,
    success: function (html)
      {
      var rVal = JSON.parse(html);
      var formData;
      var formObject;
      var indexVal;
      var thisUniqueID = uniqueID;
      var dialogID = '';
      var cacheObject;
      var cachekey;

      var formData = decodeURIComponent(this.data);
      formObject = QueryStringToJSON(formData);

      /*
       try
       {
       indexVal = formData.indexOf('=');
       if (indexVal >= 0)
       {
       formObject = JSON.parse(formData.substr(indexVal + 1));
       }
       else
       {
       formObject = JSON.parse(formData);
       }
       }
       catch (e)
       {
       formObject = {'ID':uniqueID};
       console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
       }
       */

      if (formObject['ID'])
        {
        thisUniqueID = formObject['ID'];
        }

      if (formObject['DIALOGID'])
        {
        dialogID = formObject['DIALOGID'];
        }

      cachekey = MO_TRANSACTION_CACHE_PREFIX + dialogID;
      if (document.dataCache[cachekey])
        {
        cacheObject = document.dataCache[cachekey]
        cacheObject.data.confirmationsData = rVal;
        cacheObject.data.dataStatus = cacheObject.data.dataStatus | 4;
        cacheObject.data.confirmationsFormObject = formObject;
        }

      try
        {
        var cacheObject = document.dataCache.preloadURL;
        if (cacheObject[uniqueID])
          {
          var callbackObj = cacheObject[thisUniqueID];

          if (callbackObj && (callbackObj['callback']))
            {
            if (callbackObj['parameters'])
              {
              callbackObj['callback'](formObject, callbackObj['parameters']);
              }
            else
              {
              callbackObj['callback'](formObject);
              }
            }

          // Clean cache.
          delete cacheObject[uniqueID];

          }
        }
      catch (e)
        {
        console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
        }

      }
  });

  }

function moTransactionsGridRowFormatter(args)
  {
  var wg = $.addFlex;

  var compareFlags;
  try
    {
    var rowTag;
    var dataRow;

    if (args.state === wg.renderState.rendering)
      {
      if (args.type & wg.rowType.data)
        {
        // dataRow = this.data[args.dataRowIndex];
        // dataRow = this.jsArray[args.dataRowIndex];
        dataRow = args.data;

        // Highlight row if Trade / Trade file / Confirmation entries are missing. Don't worry about the Trade file entry for the non-PAM instances as they do not have trade files in the same way.
        if (((dataRow['INSTANCE'] == "") && (dataRow['TRADEFILEMATCH'] == "false")) || ((dataRow['SOPHISMATCH'] == "false") || (dataRow['CONFIRMATIONMATCH'] == "false")))
          {
          if ((ALWAYS_HIGHLIGHT_INCOMPLETE_ROWS) || (document.appDefaults.cancelledByUs.indexOf(thisGridDataObject.SOPHISSTATUS_ID) < 0))
            {
            args.$rows.css("background-color", MO_TRANSACTION_INCOMPLETE_ROW);
            }
          }

        if ((args.data) && (args.data['PAM_FOLIO_ID']))
          {
          // Real Data Row

          rowTag = {
            FOLIO: args.data['PAM_FOLIO_ID'],
            SICOVAM: args.data['SICOVAM'],
            ISIN: args.data['ISIN'],
            LIBELLE: args.data['LIBELLE']};

          // args.$rows.dblclick(function (e)
          // {
          // dblClickShowTransactionsGrid(e, rowTag);
          // });

          args.$rows.contextmenu(function (e)
          {
          e.preventDefault();
          rtClickShowTransactionsContext(e, rowTag);
          return false;
          });
          }
        else
          {
          // Disable context menu for header and footer rows.
          args.$rows.contextmenu(function (e)
          {
          e.preventDefault();
          return false;
          });
          }
        }
      else if (args.type & wg.rowType.header)
        {
        rowTag = {
          DIALOGID: document.buildingDialogID};

        args.$rows.contextmenu(function (e)
        {
        e.preventDefault();
        rtClickShowMoColumnsContext(e, rowTag);
        return false;
        });

        }
      else
        {
        // Disable context menu for header and footer rows.
        args.$rows.contextmenu(function (e)
        {
        e.preventDefault();
        return false;
        });
        }
      }
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }
  }

function moTransactionsGridCellFormatter(args)
  {
  var wg = $.addFlex;
  var compareFlags;
  try
    {
    // var rowTag;

    if (args.state & wg.renderState.rendering)
      {
      // rowTag = {DIALOGID:document.buildingDialogID};

      if (args.row.type & wg.rowType.data)
        {
        var dataRow = args.row.data; // this.jsArray[args.row.dataRowIndex];
        var FormattingInfo = false;

        if ((dataRow.FORMATTING) && (dataRow.FORMATTING[args.column.dataKey] > 0))// > 0
          {
          FormattingInfo = dataRow.FORMATTING[args.column.dataKey];

          if (FormattingInfo & MO_TRANSACTION_HIGHLIGHT_FLAG)
            {
            args.$cell.css({"font-weight": "bold", "color": MO_TRANSACTION_HIGHLIGHT_COLOUR});
            }
          if (FormattingInfo & MO_TRANSACTION_SEVEREWARNING_FLAG)
            {
            args.$cell.css("background-color", MO_TRANSACTION_SEVEREWARNING);
            }
          else if (FormattingInfo & MO_TRANSACTION_WARNING_FLAG)
            {
            args.$cell.css("background-color", MO_TRANSACTION_WARNNG);
            }

          if (FormattingInfo & MO_TRANSACTION_SETTLEMENT_CHANGED_FLAG)
            {
            args.$cell.addClass(document.appDefaults.moTransactionGridClasses.MOVED_DATENEG_CELL);
            }

          }
        else if ((args.column.ccy_colour) && (dataRow['CCY_COLOUR'].length > 0))
          {
          args.$cell.css("color", dataRow['CCY_COLOUR']);
          }

        // Popup for BNP Confirmations that have a price, to show price
        // evolution.
        if ((args.column.dataKey == 'CONFIRMPRICE') && (dataRow['CONFIRMPRICE'] != ''))
          {
          args.$cell.addClass(document.appDefaults.moTransactionGridClasses['HAS_VALUE_CONFIRMPRICE']);
          }

        // highlightifzero
        if ((args.column['highlightifzero'] != undefined) && (args.column['highlightifzero'] == true) && (dataRow[args.column.dataKey] == 0))
          {
          // Use direct css rather than class as the PDF report does not use a custom css file at present (it could if required).
          args.$cell.css({"font-weight": "bold", "background-color": "pink"});
          }

        }
      }
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }
  }

function moTransactionsHoverIn(e)
  {
  try
    {
    var MATCH_COL_TRANSACTIONS = 1;
    var MATCH_COL_TRADEFILE = 2;
    var MATCH_COL_CONFIRMATIONS = 3;

    var dialogID = $(this).parents('.dialogDiv').attr('id');
    var classes = $(this).attr('class');

    var gridDataIndex = parseInt($(this).parents('tr').attr('dataindex'));
    //var dataArrayIndex = parseInt($(this).siblings('.rnClass').find('span').html());
    var matchArrayIndex = parseInt($(this).siblings('.matchArrayClass').find('span').html());
    var cachekey = MO_TRANSACTION_CACHE_PREFIX + dialogID;
    var cacheObject = document.dataCache[cachekey];
    var matchArray = cacheObject.data.matchArray;

    if (DEBUG_FLAG) console.log('Hover In : ' + matchArrayIndex.toString() + ', ' + gridDataIndex.toString());

    if (cacheObject.timeout)
      {
      clearTimeout(cacheObject.timeout);
      if (DEBUG_FLAG) console.log('  Clear Timeout : ' + cacheObject.timeoutMatchIndex);
      }

    $('div.fsTooltip').hide();
    if (document.dataCache[cachekey])
      {
      var contextVariant = '';

      if (classes.length > 0)
        {
        var cArray = classes.split(' ');

        if (cArray.indexOf('sophisMatch') >= 0)
          {
          contextVariant = 'sophisMatch';
          if (matchArray[matchArrayIndex][MATCH_COL_TRANSACTIONS] === false)
            {
            return;
            } // No Context if no tick.
          }
        else if (cArray.indexOf('tradefileMatch') >= 0)
          {
          contextVariant = 'tradefileMatch';
          if (matchArray[matchArrayIndex][MATCH_COL_TRADEFILE] === false)
            {
            return;
            } // No Context if no tick.
          }
        else if (cArray.indexOf('confirmationMatch') >= 0)
          {
          contextVariant = 'confirmationMatch';
          if (matchArray[matchArrayIndex][MATCH_COL_CONFIRMATIONS] === false)
            {
            return;
            } // No Context if no tick.
          }
        else if (cArray.indexOf('gridMovedDatenegClass') >= 0)
          {
          contextVariant = 'gridMovedDatenegClass';
          }
        else if (cArray.indexOf('confirmationPrice') >= 0)
          {
          contextVariant = 'confirmationPrice';
          }
        }

      cacheObject.timeout = setTimeout("moTransactionsTooltip('" + dialogID + "', " + matchArrayIndex + ", " + gridDataIndex + ", '" + contextVariant + "')", MO_TRANSACTION_TOOLTIP_DELAY);
      cacheObject.timeoutMatchIndex = matchArrayIndex;
      if (DEBUG_FLAG) console.log('  Set Timeout : ' + matchArrayIndex);

      }
    }
  catch (err)
    {
    var debug = 0;
    console.log(err.message + ', ' + err.fileName + '(line, ' + err.lineNumber + ')');
    }

  }

function moTransactionsTooltip(dialogID, matchArrayIndex, gridDataIndex, variant)
  {
  var menu = $('#fsToolTip');
  var cachekey = MO_TRANSACTION_CACHE_PREFIX + dialogID;
  var cacheObject;
  var html;

  var MATCH_COL_TRANSACTIONS = 1;
  var MATCH_COL_TRADEFILE = 2;
  var MATCH_COL_CONFIRMATIONS = 3;

  // cacheObject.data.matchArray = matchArray
  // cacheObject.data.formattingArray = formattingArray
  // cacheObject.data.gridDataObject = gridDataObject

  if (DEBUG_FLAG) console.log('moTransactionsTooltip : ' + dialogID + ", " + matchArrayIndex + ", " + gridDataIndex + ", " + variant);

  try
    {

    if (document.dataCache[cachekey])
      {
      cacheObject = document.dataCache[cachekey];

      if (cacheObject.timeoutMatchIndex != matchArrayIndex)
        {
        if (DEBUG_FLAG) console.log('  Match IDs do not match, exit : ' + cacheObject.timeoutMatchIndex + ', ' + matchArrayIndex);
        return;
        }

      cacheObject.timeout = false;

      var matchArray = cacheObject.data.matchArray;
      var sophisTransactions = cacheObject.data.transactionsData;
      var thisItem;
      var tradeFileEntries = cacheObject.data.tradefileData;
      var neolinkConfirmations = cacheObject.data.confirmationsData;

      var sourceFlexgrid = $("#" + dialogID + '_grid');
      var gridDataObject = $(sourceFlexgrid).flexGet('jsArray');

      //      html = cacheObject.data.gridDataObject[dataArrayIndex]['MATCHSTATUS'];
      html = gridDataObject[gridDataIndex]['MATCHSTATUS'];

      // Deal with non-default tool tips :-

      if (variant.length > 0)
        {
        // populate tool tip as appropriate.

        if (variant.toLowerCase() == 'gridmoveddatenegclass')
          {
          html = gridDataObject[gridDataIndex]['PAM_FUND_NAME'] + '<br/>' +
            gridDataObject[gridDataIndex]['NAME'] + '<br/>' +
            'loading...<br/>';

          var formJSON = {
            'INSTANCE': gridDataObject[gridDataIndex]['INSTANCE'],
            'TRANSACTIONID': gridDataObject[gridDataIndex]['REFCON'],
            'DIALOGID': dialogID,
            'CACHEKEY': cachekey,
            'GRIDDATAINDEX': gridDataIndex,
            'MATCHARRAYINDEX': matchArrayIndex,
            'USERID': document.dakotaUserID,
            'TOKEN': document.dakotaSessionKey
          };

          if (DEBUG_FLAG) console.log('Getting transaction history (' + gridDataObject[gridDataIndex]['REFCON'] + ')');

          $.ajax({type: "POST",
            dataType:'json',
            url: "php/getTransactionHistory.php",
            data: formJSON,
            success: function (htmlData)
              {
              try
                {
                if (DEBUG_FLAG) console.log('  Got transaction history');

                var rVal = htmlData; // JSON.parse(htmlData);
                var formData = decodeURIComponent(this.data);
                var indexVal;
                var formObject;
                var tradeCount = 0;
                var tradeIndex;
                var lastTradeIndex = '0';
                var thisTrade;

                formObject = QueryStringToJSON(formData);

                /*
                 try
                 {
                 indexVal = formData.indexOf('=');
                 if (indexVal >= 0)
                 {
                 formObject = JSON.parse(formData.substr(indexVal + 1));
                 }
                 else
                 {
                 formObject = JSON.parse(formData);
                 }
                 }
                 catch (e)
                 {
                 console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
                 }

                 */

                cacheObject = document.dataCache[formObject['CACHEKEY']];

                if (cacheObject.timeoutMatchIndex == formObject['MATCHARRAYINDEX'])
                  {
                  menu = $('#fsToolTip');

                  html = gridDataObject[gridDataIndex]['PAM_FUND_NAME'] + '<br/>' +
                    gridDataObject[gridDataIndex]['NAME'] +
                    '<br/><br/>';

                  html += '<table border = "1"><thead><tr><td>Date Entered</td><td>Settlement Date</td><td>Amount</td><td>Status</td></tr></thead><tbody>';
                  html += '<tr><td></td><td></td><td></td><td></td></tr>';


                  for (tradeIndex in rVal)
                    {
                    thisTrade = rVal[tradeIndex];

                    if ((tradeCount == 0) || ((thisTrade['ENTRY_DATE'] != rVal[lastTradeIndex]['ENTRY_DATE']) || (thisTrade['SETTLEMENT_DATE'] != rVal[lastTradeIndex]['SETTLEMENT_DATE']) || (thisTrade['CONSIDERATION'] != rVal[lastTradeIndex]['CONSIDERATION'])))
                      {
                      html += '<tr><td>' + thisTrade['ENTRY_DATE'] + '</td><td>' + thisTrade['SETTLEMENT_DATE'] + '</td><td>' + parseInt(thisTrade['CONSIDERATION']).addCommas() + ' (' + getCcyFromSophisCode(thisTrade['SETTLEMENT_CCY']) + ')</td><td>' + thisTrade['TRADESTATUS'] + '</td></tr>';
                      tradeCount += 1;
                      }

                    lastTradeIndex = tradeIndex;
                    }

                  html += '</tbody></table>';

                  menu.html(html);
                  menu.show();

                  if ((document.mouseX + 40 + menu[0].clientWidth) > window.innerWidth)
                    {
                    var lastWidth;

                    do
                      {
                      lastWidth = menu[0].clientWidth;

                      menu.css({left: (document.mouseX - (menu[0].clientWidth + 30))});
                      menu.show();
                      }
                    while (lastWidth != menu[0].clientWidth)
                    }

                  if ((document.mouseY + menu[0].clientHeight) > window.innerHeight)
                    {
                    var lastHeight;

                    do
                      {
                      lastHeight = menu[0].clientHeight;

                      menu.css({top: (window.innerHeight - (menu[0].clientHeight + 20))});
                      menu.show();
                      }
                    while (lastHeight != menu[0].clientHeight)
                    }

                  }
                }
              catch (e)
                {
                console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
                }
              }
          });

          }

        else if (variant.toLowerCase() == 'confirmationprice') //
          {
          html = gridDataObject[gridDataIndex]['PAM_FUND_NAME'] + '<br/>' +
            gridDataObject[gridDataIndex]['NAME'] + '<br/>' +
            'loading...<br/>';

          var formJSON = {
            'NEO_BANKREFERENCE': gridDataObject[gridDataIndex]['NEO_BANKREFERENCE'],
            'DIALOGID': dialogID,
            'CACHEKEY': cachekey,
            'GRIDDATAINDEX': gridDataIndex,
            'MATCHARRAYINDEX': matchArrayIndex,
            'USERID': document.dakotaUserID,
            'TOKEN': document.dakotaSessionKey
          };

          $.ajax({type: "POST",
            url: "php/getConfirmationHistory.php",
            data: formJSON,
            success: function (htmlData)
              {
              try
                {
                var rVal = JSON.parse(htmlData);
                var formData = decodeURIComponent(this.data);
                var indexVal;
                var formObject;
                var tradeCount = 0;
                var tradeIndex;
                var lastTradeIndex = '0';
                var thisTrade;

                formObject = QueryStringToJSON(formData);

                /*
                 try
                 {
                 indexVal = formData.indexOf('=');
                 if (indexVal >= 0)
                 {
                 formObject = JSON.parse(formData.substr(indexVal + 1));
                 }
                 else
                 {
                 formObject = JSON.parse(formData);
                 }
                 }
                 catch (e)
                 {
                 console.log(e.message + ', ' + e.fileName + '(line, '+ e.lineNumber+')');
                 }
                 */

                cacheObject = document.dataCache[formObject['CACHEKEY']];

                if (cacheObject.timeoutMatchIndex == formObject['MATCHARRAYINDEX'])
                  {
                  menu = $('#fsToolTip');

                  html = gridDataObject[gridDataIndex]['PAM_FUND_NAME'] + '<br/>' +
                    gridDataObject[gridDataIndex]['NAME'] + '<br/>' +
                    'Bank Reference : ' + formObject['NEO_BANKREFERENCE'] + '<br/>' +
                    '<br/>';

                  html += '<table border = "1"><thead><tr><td>Date Entered</td><td>Price</td><td>Status</td></tr></thead><tbody>';

                  for (tradeIndex in rVal)
                    {
                    thisTrade = rVal[tradeIndex];

                    html += '<tr><td>' + thisTrade['DateEntered'] + '</td><td>' + thisTrade['neo_price'] + '</td><td>' + thisTrade['neo_st'] + ' ' + thisTrade['neo_status'] + ' ' + thisTrade['neo_cat'] + ' ' + thisTrade['neo_t'] + '</td></tr>';

                    lastTradeIndex = tradeIndex;
                    }

                  html += '</tbody></table>';

                  menu.html(html);
                  menu.show();

                  if ((document.mouseX + 40 + menu[0].clientWidth) > window.innerWidth)
                    {
                    var lastWidth;

                    do
                      {
                      lastWidth = menu[0].clientWidth;

                      menu.css({left: (document.mouseX - (menu[0].clientWidth + 30))});
                      menu.show();
                      }
                    while (lastWidth != menu[0].clientWidth)
                    }

                  if ((document.mouseY + menu[0].clientHeight) > window.innerHeight)
                    {
                    var lastHeight;

                    do
                      {
                      lastHeight = menu[0].clientHeight;

                      menu.css({top: (window.innerHeight - (menu[0].clientHeight + 20))});
                      menu.show();
                      }
                    while (lastHeight != menu[0].clientHeight)
                    }

                  }
                }
              catch (e)
                {
                console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
                }
              }
          });
          }

        else
          { // sophis / tradefile / confirmation match

          switch (variant.toLowerCase())
          {
            case 'sophismatch':
              thisItem = sophisTransactions[matchArray[matchArrayIndex][MATCH_COL_TRANSACTIONS]];
              break;

            case 'tradefilematch':
              thisItem = tradeFileEntries[matchArray[matchArrayIndex][MATCH_COL_TRADEFILE]];
              break;

            case 'confirmationmatch':
              thisItem = neolinkConfirmations[matchArray[matchArrayIndex][MATCH_COL_CONFIRMATIONS]];
              break;

          }

          html = '<div style="height: 18px;">' +
            '<a href="#" class="ui-corner-all print-link" role="button" printtable="#tooltipTable">' +
            '<span printtable="#tooltipTable" class="ui-icon ui-icon-document"></span></a></div>';

          html += '<table id="tooltipTable">';
          var indexText;

          for (thisIndex in thisItem)
            {
            html += '<tr>';
            indexText = thisIndex + '                    ';
            indexText = indexText.substr(0, Math.max(20, thisIndex.length));

            if (thisItem[thisIndex] == null)
              {
              html += '<td>' + indexText + '</td><td> : null</td>';
              }
            else
              {
              html += '<td>' + indexText + '</td><td> : ' + thisItem[thisIndex].toString() + '</td>';
              }
            html += '</tr>';
            } // for
          html += '</table>'
          html += '<div style="clear: both;"></div>';

          }
        }
      }
    }
  catch (e)
    {
    html = 'An Error occurred : ' + e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')';
    console.log(html);
    }

  menu.css({top: document.mouseY - 10, left: document.mouseX + 30, 'z-index': 20000});
  menu.html(html);
  menu.find('.print-link').click(fnSimpleGridPrint);
  menu.show();

  if ((document.mouseX + 40 + menu[0].clientWidth) > window.innerWidth)
    {
    var lastWidth;

    do
      {
      lastWidth = menu[0].clientWidth;

      menu.css({left: (document.mouseX - (menu[0].clientWidth + 30))});
      menu.show();
      }
    while (lastWidth != menu[0].clientWidth)
    }

  if ((document.mouseY + menu[0].clientHeight) > window.innerHeight)
    {
    var lastHeight;

    do
      {
      lastHeight = menu[0].clientHeight;

      menu.css({top: (window.innerHeight - (menu[0].clientHeight + 20))});
      menu.show();
      }
    while (lastHeight != menu[0].clientHeight)
    }

  }

function moTransactionsHoverOut(e)
  {
  try
    {
    var dialogID = $(this).parents('.dialogDiv').attr('id');
    var matchArrayIndex = parseInt($(this).siblings('.matchArrayClass').find('span').html());
    var cachekey = MO_TRANSACTION_CACHE_PREFIX + dialogID;
    var cacheObject;
    var matchRow;

    if (DEBUG_FLAG) console.log('Hover Out : ' + matchArrayIndex);

    if (document.dataCache[cachekey])
      {
      cacheObject = document.dataCache[cachekey];

      // cacheObject.data.matchArray = matchArray
      // cacheObject.data.formattingArray = formattingArray
      // cacheObject.data.gridDataObject = gridDataObject

      try
        {
        // Only clear timeout if 1) The timeout exists and 2) it was set by this
        // row.
        if (cacheObject.timeoutMatchIndex == matchArrayIndex)
          {
          if (cacheObject.timeout)
            {
            if (DEBUG_FLAG) console.log('  Clear Timeout : ' + matchArrayIndex);
            clearTimeout(cacheObject.timeout);
            }

          $('div.fsTooltip').hide();
          }

        }
      catch (e)
        {
        console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
        }
      finally
        {
        cacheObject.timeout = false;
        }
      }
    }
  catch (err)
    {
    console.log(err.message + ', ' + err.fileName + '(line, ' + err.lineNumber + ')');
    }
  }

function rtClickShowMoColumnsContext(e, rowTag)
  {
  try
    {
    document.currentContextData = rowTag;

    var index;

    // Only a single context menu is used for the Transactions Grids.

    var menu = $('#bodyDiv .fsContextMenu');
    var menuList = $('#bodyDiv .fsContextMenu ul');

    // Clear existing entries.

    menuList.empty();
    var menuText;

    var realGridCols = $("#" + rowTag.DIALOGID + '_grid')[0].p.colModel;
    var allCols = [];

    for (index = 0; index < realGridCols.length; index++)
      {
      allCols.push({colIndex: realGridCols[index].cid, headerText: realGridCols[index].headerText, dataKey: realGridCols[index].dataKey, visible: realGridCols[index].visible});
      }

    allCols.sort(
      function (a, b)
      {
      if (a.headerText < b.headerText) return -1;
      if (a.headerText > b.headerText) return 1;
      return 0;
      });

    if (allCols.length > 0)
      {
      var alwaysHidden = document.appDefaults.moTransactionGridDefaults.alwaysHidden;

      for (index in allCols)
        {
        if ($.inArray(allCols[index].dataKey, alwaysHidden) < 0)
          {
          menuText = allCols[index].headerText;
          if (menuText.length > 0)
            {
            // menuList.append('<li id="fsContextMenu_' + allCols[index].colIndex + '"><a
            // id="fsContextMenu_' + allCols[index].colIndex + '" style="margin-left:20px;
            // position:relative;"><span class="ui-icon ui-icon-check"
            // style="position:absolute; top: 0; left: -20px; display:' +
            // (visibleCols[allCols[index].dataKey] ? 'inline' : 'none') + ';"></span>' +
            // menuText + '</a></li>');
            menuList.append('<li colIndex="' + allCols[index].colIndex + '" colVisible="' + (allCols[index].visible ? '1' : '0') + '"><a style="margin-left:20px; position:relative;"><span class="ui-icon ui-icon-check" style="position:absolute; top: 0; left: -20px; display:' + (allCols[index].visible ? 'inline' : 'none') + ';"></span>' + menuText + '</a></li>');
            }

          }
        }

      $('li', menuList).click(function (e)
      {
      moTransactionsHeaderContextClick(e, rowTag.DIALOGID);
      });

      }
    else
      {
      menuList.append('<li><a class="">There do not appear to be any columns.</a></li>');
      }

    menu.css({top: e.pageY - 10, left: e.pageX - 10, 'z-index': 20000});
    menu.scrollTop(0);
    menu.show();

    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }
  finally
    {
    RefreshTokenExpiry(document.dakotaUserID, document.dakotaSessionKey);
    }
  return false;
  }

function moTransactionsHeaderContextClick(e, dialogID)
  {
  try
    {
    // $('#bodyDiv .fsContextMenu').hide();

    var gridID = dialogID + '_grid';
    var thisLI = $(e.target);
    var index = thisLI.attr('colIndex');
    var colVisible = thisLI.attr('colVisible');
    var newVisibleState = true;

    if (index == null)
      {
      thisLI = $(e.target).parents('li');
      index = thisLI.attr('colIndex');
      colVisible = thisLI.attr('colVisible');
      }

    if (DEBUG_FLAG) console.log('Toggling col index=' + index);

    if (colVisible == '1')
      {
      newVisibleState = false;
      }

    thisLI.attr('colVisible', (newVisibleState ? "1" : "0"));
    $('span', thisLI).css("display", (newVisibleState ? "inline" : "none"));
    document.appDefaults.moTransactionColumns[index].visible = newVisibleState;

    $("#" + gridID).flexToggleCol(index, newVisibleState);

    moTransactionsSaveColumnInfo(dialogID);
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }
  }

function moTransactionsGridColumnEvent(e, args)
  {
  try
    {
    var dialogID = $(this).parents('.dialogDiv').attr('id');

    moveOrSizeGridColumnEvent(e, args, (dialogID + '_grid'), document.appDefaults.moTransactionColumns);

    moTransactionsSaveColumnInfo(dialogID);
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }

  }

function moTransactionsSaveColumnInfo(dialogID)
  {
  try
    {

    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }
  }

function moTransactionsPrintContext(e)
  {
  try
    {
    var menu = $('#bodyDiv .fsContextDiv');

    menu.css({width: '250px'});

    //
    var dialog;

    if ($(e.target).hasClass('dialogDiv'))
      {
      dialog = e.target;
      }
    else
      {
      dialog = $(e.target).parents('.dialogDiv')[0];
      }

    var dialogID = dialog.id;

    // Clear existing entries.


    menu.empty();

    //

    menu.append('<span id=""><a id="" dialogid = "' + dialogID + '" reporttype="All" style="margin-left:10px; position:relative;">Full Details</a></span></br>');
    menu.append('<hr>');
    menu.append('<div><div style="float: left;"><input id="radio_SOPHIS" type="radio" name="group_rptSettleDate" value="SOPHIS" checked>Venice Dates&nbsp</div><div style="float: left;"><input id="radio_BNP" type="radio" name="group_rptSettleDate" value="BNP">BNP Dates</div></div>');
    menu.append('<div style="clear: both;"></div>');
    menu.append('<br/>');
    menu.append('<span id=""><a id="" dialogid = "' + dialogID + '" reporttype="SignoffReport" style="margin-left:10px; position:relative;">Trade Match report</a></span></br>');
    menu.append('<span id=""><a id="" dialogid = "' + dialogID + '" reporttype="SignoffReport_Selected" style="margin-left:10px; position:relative;">Trade Match report (Selected)</a></span></br>');

    $(menu).find('span').click(function (e)
    {
    moTransactionsReportClick(e);
    });
    //

    menu.css({top: document.mouseY - 10, left: document.mouseX - 30, 'z-index': 20000});
    menu.scrollTop(0);
    menu.show();

    // attempt to move the window on screen if it appears to go to the edge.

    if ((document.mouseX + menu[0].clientWidth) > window.innerWidth)
      {
      var lastWidth;

      do
        {
        lastWidth = menu[0].clientWidth;

        menu.css({left: (document.mouseX - (menu[0].clientWidth + 10))});
        menu.show();
        }
      while (lastWidth != menu[0].clientWidth)
      }

    if ((document.mouseY + menu[0].clientHeight) > window.innerHeight)
      {
      var lastHeight;

      do
        {
        lastHeight = menu[0].clientHeight;

        menu.css({top: (window.innerHeight - (menu[0].clientHeight + 10))});
        menu.show();
        }
      while (lastHeight != menu[0].clientHeight)
      }

    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }
  }

function moTransactionsPrintContext_originalAndUnused(e)
  {
  try
    {
    var menu = $('#bodyDiv .fsContextMenu');

    menu.css({width: '250px'});
    var menuList = $('#bodyDiv .fsContextMenu ul');

    //
    var dialog;

    if ($(e.target).hasClass('dialogDiv'))
      {
      dialog = e.target;
      }
    else
      {
      dialog = $(e.target).parents('.dialogDiv')[0];
      }

    var dialogID = dialog.id;

    // Clear existing entries.

    menuList.empty();

    //

    menuList.append('<li id=""><a id="" dialogid = "' + dialogID + '" reporttype="All" style="margin-left:10px; position:relative;">Full Details</a></li>');


    menuList.append('<li id=""><a id="" dialogid = "' + dialogID + '" reporttype="SignoffReport" style="margin-left:10px; position:relative;">Trade Match report (Venice Sett.)</a></li>');
    menuList.append('<li id=""><a id="" dialogid = "' + dialogID + '" reporttype="SignoffReport_Selected" style="margin-left:10px; position:relative;">Trade Match report (Selected, Venice Sett.)</a></li>');
    menuList.append('<li id=""><a id="" dialogid = "' + dialogID + '" reporttype="" style="margin-left:10px; position:relative;"></a></li>');
    menuList.append('<li id=""><a id="" dialogid = "' + dialogID + '" reporttype="SignoffReportBNPDates" style="margin-left:10px; position:relative;">Trade Match report (BNP Sett.)</a></li>');
    menuList.append('<li id=""><a id="" dialogid = "' + dialogID + '" reporttype="SignoffReport_SelectedBNPDates" style="margin-left:10px; position:relative;">Trade Match report (Selected, BNP Sett.)</a></li>');

    // $(menuList).find('a').click(function (e)
    // {
    // moTransactionsReportClick(e);
    // });

    $(menuList).find('li').click(function (e)
    {
    moTransactionsReportClick(e);
    });
    //

    menu.css({top: document.mouseY - 10, left: document.mouseX - 30, 'z-index': 20000});
    menu.scrollTop(0);
    menu.show();

    // attempt to move the window on screen if it appears to go to the edge.

    if ((document.mouseX + menu[0].clientWidth) > window.innerWidth)
      {
      var lastWidth;

      do
        {
        lastWidth = menu[0].clientWidth;

        menu.css({left: (document.mouseX - (menu[0].clientWidth + 10))});
        menu.show();
        }
      while (lastWidth != menu[0].clientWidth)
      }

    if ((document.mouseY + menu[0].clientHeight) > window.innerHeight)
      {
      var lastHeight;

      do
        {
        lastHeight = menu[0].clientHeight;

        menu.css({top: (window.innerHeight - (menu[0].clientHeight + 10))});
        menu.show();
        }
      while (lastHeight != menu[0].clientHeight)
      }

    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }
  }

function moTransactionsReportClick(e)
  {
  try
    {
    var dialogID;
    var reportType;
    var menu = $('#bodyDiv .fsContextDiv');
    var VeniceDates = document.getElementById('radio_SOPHIS').checked;

    e.preventDefault();

    var thisLink;
    if (e.target.nodeName == 'A')
      {
      dialogID = $(e.target).attr("dialogid");
      reportType = $(e.target).attr("reporttype");
      }
    else
      {
      dialogID = $(e.target).find('a').attr("dialogid");
      reportType = $(e.target).find('a').attr("reporttype");
      }


    var selectedFund = $('#' + dialogID + ' .moFundCombo option:selected').text();
    var selectedFundValue = $('#' + dialogID + ' .moFundCombo').val();

    var moDateType = $('#' + dialogID + ' .moDateCombo option:selected').val();

    var moDateFrom = $('#' + dialogID + ' .tradeDateFrom').datepicker("getDate");
    var moDateTo = $('#' + dialogID + ' .tradeDateTo').datepicker("getDate");
    // var xx = moDateFrom2.format('j-M-Y');
    var headerText;

    switch (reportType)
    {
      case 'All':
        headerText =
          '<table width="100%" style="border-bottom: 1px solid #000000; vertical-align: bottom; font-family:' +
            'serif; font-size: 12pt; font-weight: bold; color: #000088;">' +
            '<thead style="display:none"><tr width="25%"></tr><tr width="25%"></tr><tr width="25%"></tr><tr width="25%"></tr></thead>' +
            '<tbody>' +
            '<tr><td>Primonial Asset Management : Middle Office Transactions report.</td><td></td><td></td><td></td></tr>' +
            '<tr><td>' + (selectedFundValue > 0 ? selectedFund : 'All Funds.') + '</td><td></td><td></td><td align="right" style="font-weight: normal">' + moDateType + ' : ' + (moDateFrom ? (' ' + moDateFrom.format('j-M-Y')) : ' undefined') + (moDateTo ? ' to ' + moDateTo.format('j-M-Y') : ' onwards') + '</td></tr>' +
            '</tbody></table>';


        fnSimpleGridPrint({target: $('#' + dialogID)[0]}, {title: '', headerHTML: headerText});

        break;

      case 'SignoffReport':
      case 'SignoffReport_Selected':

        headerText =
          '<table width="100%" style="border-bottom: 1px solid #000000; vertical-align: bottom; font-family:' +
            'serif; font-size: 12pt; font-weight: bold; color: #000088;">' +
            '<thead style="display:none"><tr width="25%"></tr><tr width="25%"></tr><tr width="25%"></tr><tr width="25%"></tr></thead>' +
            '<tbody>' +
            '<tr><td>Primonial Asset Management : Middle Office Transactions Signoff report.</td><td></td><td></td><td></td></tr>' +
            '<tr><td>' + (selectedFundValue > 0 ? selectedFund : 'All Funds.') + '</td><td></td><td></td><td align="right" style="font-weight: normal">' + moDateType + ' : ' + (moDateFrom ? (' ' + moDateFrom.format('j-M-Y')) : ' undefined') + (moDateTo ? ' to ' + moDateTo.format('j-M-Y') : ' onwards') + '</td></tr>' +
            '</tbody></table>';

        sourceFlexgrid = $('#' + dialogID + ' .gridTable:first')
        workingFlexgrid = $('<div><table class="gridTable"></table></div>');

        var gridArray = $(sourceFlexgrid).flexGet('jsArray');
        var workingGridArray;
        var colModel = $(sourceFlexgrid).flexGet('colModel');
        var colArray = new Array();
        var rowStyleFormatter = $(sourceFlexgrid).flexGet('rowStyleFormatter');
        var cellStyleFormatter = $(sourceFlexgrid).flexGet('cellStyleFormatter');

        if (reportType == 'SignoffReport_Selected')
          {
          workingGridArray = new Array();
          var selectedRows = $('tr.trSelected', sourceFlexgrid).each(function (index, Element)
          {
          var thisDataIndex = $(Element).attr('dataindex');
          workingGridArray.push($.extend(true, {}, gridArray[thisDataIndex]));
          });
          }
        else
          {
          workingGridArray = $.extend(true, [], gridArray);
          }

        var reportColName;
        var SettlementDateField;

        if (VeniceDates)
          { // Venice Dates
          SettlementDateField = 'SOPHISSETTLE';

          reportColName = [
            'PAM_FUND_NAME',
            'REFCON',
            'SOPHISTRADE',
            'SOPHISSETTLE',
            'CONFIRMSETTLE',
            'ISIN',
            'SOPHISSETTLEMENTCURRENCY',
            'SOPHISAMOUNT',
            'CONFIRMAMOUNT',
            'AMOUNT_DIFFERENCE',
            // 'EURO_VALUE',
            'PERCENTAGE_OF_NAV',
            'NAME',
            'SOPHISSTATUS',
            'BNPSTATUS'];
          }
        else
          { // BNP Dates
          SettlementDateField = 'CONFIRMSETTLE';

          reportColName = [
            'PAM_FUND_NAME',
            'REFCON',
            'SOPHISTRADE',
            'SOPHISSETTLE',
            'CONFIRMSETTLE',
            'ISIN',
            'SOPHISSETTLEMENTCURRENCY',
            'SOPHISAMOUNT',
            'CONFIRMAMOUNT',
            'AMOUNT_DIFFERENCE',
            // 'EURO_VALUE',
            'PERCENTAGE_OF_NAV',
            'NAME',
            'SOPHISSTATUS',
            'BNPSTATUS'];
          }


        // Build Grid Column array based on reportColName array.

        for (var reportIndex in reportColName)
          {
          for (var index in colModel)
            {
            if (colModel[index].dataKey == reportColName[reportIndex])
              {
              /* Mansour would like 'BNP Qty' column to be highlighted if it is zero. */

              if (colModel[index].dataKey == 'CONFIRMAMOUNT')
                {
                colArray.push($.extend(true, {}, colModel[index], {visible: true, highlightifzero: true}));
                }
              else
                {
                colArray.push($.extend(true, {}, colModel[index], {visible: true, highlightifzero: false}));
                }
              break;
              }
            }
          }


        var uniqueFolioId = new Array();
        var uniqueSettlementDay = new Array();
        var settlementTemplate = {};
        var fundSettlement = {};

        for (var dataIndex in workingGridArray)
          {
          // Build Unique portfolio list for NAV display.

          if (!uniqueFolioId[workingGridArray[dataIndex]['PAM_FOLIO_ID']])
            {
            uniqueFolioId[workingGridArray[dataIndex]['PAM_FOLIO_ID']] =
            {PAM_FOLIO_ID: workingGridArray[dataIndex]['PAM_FOLIO_ID'],
              PAM_FUND_NAME: workingGridArray[dataIndex]['PAM_FUND_NAME'],
              FUND_NAV: workingGridArray[dataIndex]['FUND_NAV']
            };
            }

          // Build Settling cash tree, after ignoring dead trades.

          if ($.inArray(workingGridArray[dataIndex]['SOPHISSTATUS_ID'].toString(), document.appDefaults.deadSophisTrades) >= 0)
            {
            if (workingGridArray[dataIndex]['SOPHISSTATUS_ID'] == '')
              {
              workingGridArray[dataIndex]['SOPHISSETTLEMENTCURRENCY'] = '';
              }
            else
              {
              workingGridArray[dataIndex]['SOPHISSETTLEMENTCURRENCY'] = 'cxl';
              }
            }
          else
            {
            if (!fundSettlement[workingGridArray[dataIndex]['PAM_FUND_NAME']])
              {
              fundSettlement[workingGridArray[dataIndex]['PAM_FUND_NAME']] = {};
              }

            if (!fundSettlement[workingGridArray[dataIndex]['PAM_FUND_NAME']][workingGridArray[dataIndex]['SOPHISSETTLEMENTCURRENCY']])
              {
              fundSettlement[workingGridArray[dataIndex]['PAM_FUND_NAME']][workingGridArray[dataIndex]['SOPHISSETTLEMENTCURRENCY']] = {};
              }

            if (!fundSettlement[workingGridArray[dataIndex]['PAM_FUND_NAME']][workingGridArray[dataIndex]['SOPHISSETTLEMENTCURRENCY']][workingGridArray[dataIndex][SettlementDateField]])
              {
              fundSettlement[workingGridArray[dataIndex]['PAM_FUND_NAME']][workingGridArray[dataIndex]['SOPHISSETTLEMENTCURRENCY']][workingGridArray[dataIndex][SettlementDateField]] = (workingGridArray[dataIndex]['SOPHIS_CONSIDERATION']);
              }
            else
              {
              fundSettlement[workingGridArray[dataIndex]['PAM_FUND_NAME']][workingGridArray[dataIndex]['SOPHISSETTLEMENTCURRENCY']][workingGridArray[dataIndex][SettlementDateField]] += (workingGridArray[dataIndex]['SOPHIS_CONSIDERATION']);
              }

            // Build lists of Unique Settlement days.

            if (uniqueSettlementDay.indexOf(workingGridArray[dataIndex][SettlementDateField]) < 0)
              {
              uniqueSettlementDay.push(workingGridArray[dataIndex][SettlementDateField]);
              settlementTemplate[workingGridArray[dataIndex][SettlementDateField]] = 0;
              }
            }
          }

        // Sort.

        uniqueFolioId.sort(
          function (a, b)
          {
          if (a.PAM_FUND_NAME < b.PAM_FUND_NAME) return -1;
          if (a.PAM_FUND_NAME > b.PAM_FUND_NAME) return 1;
          return 0;
          });

        uniqueSettlementDay.sort(
          function (a, b)
          {
          if (a < b) return -1;
          if (a > b) return 1;
          return 0;
          });


        var CurrencyDiv = "<div>";
        var thisDate;
        var ccyTotal;
        var fundTable = '<table><thead><tr><td>Fund</td><td></td><td>NAV (EUR)</td></tr></thead><tbody>';

        if (VeniceDates)
          {
          CurrencyDiv += '<p>Using Venice Settlement dates :</p>';
          }
        else
          {
          CurrencyDiv += '<p>Using BNP Settlement dates :</p>';
          }


        for (var fundIndex in uniqueFolioId)
          {
          fundTable += '<tr><td>' + uniqueFolioId[fundIndex]['PAM_FUND_NAME'] + '</td><td>&nbsp;</td><td align="right">' + format('#,##0.', (uniqueFolioId[fundIndex]['FUND_NAV'])) + '</td></tr>';

          if (uniqueFolioId[fundIndex]['PAM_FUND_NAME'] in fundSettlement)
            {
            CurrencyDiv += "<div>" + uniqueFolioId[fundIndex]['PAM_FUND_NAME'] + "</div>";
            CurrencyDiv += '<table cellspacing="0" cellpadding="2" style="border-collapse: collapse; text-align: right; padding-bottom: 10px;"><thead><tr><td>&nbsp;</td>';

            // Header Row
            for (thisDate in uniqueSettlementDay)
              {
              CurrencyDiv += '<td style="border: 1pt solid #AAA;">&nbsp;' + uniqueSettlementDay[thisDate] + "&nbsp;</td>";
              }
            CurrencyDiv += '<td>&nbsp;Total</td></tr></thead><tbody>';

            // Currency table
            for (var thisCcy in fundSettlement[uniqueFolioId[fundIndex]['PAM_FUND_NAME']])
              {
              CurrencyDiv += '<tr style="border: 1pt solid #AAA;"><td style="border: 1pt solid #AAA; text-align: left;">' + thisCcy + '</td>';
              ccyTotal = 0;

              for (thisDate in uniqueSettlementDay)
                {
                if (uniqueSettlementDay[thisDate] in fundSettlement[uniqueFolioId[fundIndex]['PAM_FUND_NAME']][thisCcy])
                  {
                  CurrencyDiv += '<td style="border: 1pt solid #AAA;">' + format("#,##0.", fundSettlement[uniqueFolioId[fundIndex]['PAM_FUND_NAME']][thisCcy][uniqueSettlementDay[thisDate]]) + "</td>";

                  ccyTotal += fundSettlement[uniqueFolioId[fundIndex]['PAM_FUND_NAME']][thisCcy][uniqueSettlementDay[thisDate]];
                  }
                else
                  {
                  CurrencyDiv += "<td>&nbsp;</td>";
                  }
                }
              CurrencyDiv += '<td style="border: 1pt solid #aaaaaa;">&nbsp;' + format("#,##0.", ccyTotal) + "</td></tr>";

              }

            CurrencyDiv += '</tbody></table><div style="height: 3px;"></div>';
            }

          }

        fundTable += '</tbody></table>';
        CurrencyDiv += '</div>';


        workingFlexgrid.find('.gridTable').flexigrid({
          jsArray: workingGridArray,
          colModel: colArray,
          rowStyleFormatter: rowStyleFormatter,
          cellStyleFormatter: cellStyleFormatter
        });

        var appendHTML =
          '<div style="font-size: 8pt; border-bottom:0.1mm solid #220044; padding:2em 0em; ">Transaction Count : ' + workingGridArray.length + '</div>' +

            '<div style="height:20px;"></div>' +

            '<div style="float: right; width: 25%; padding-right: 0px; margin-right: 0px;">' + CurrencyDiv + '</div>' +
            '<div style="float: right; width: 25%; padding-right: 0px; margin-right: 0px;">' + fundTable + '</div>' +
            '<div style="float: left; width: 25%; padding-left: 0px; margin-left: 0px;">' +
            '<div >Signed :</div>' +
            '<div style="height:60px;"></div>' +
            '<div >Date :</div>' +
            '<div style="height:30px;"></div>' +
            '<div >Checked :</div>' +
            '<div style="height:60px;"></div>' +
            '<div >Date :</div>' +
            '</div>';

        fnSimpleGridPrint({target: $('#' + dialogID)[0]}, {flexigrid: workingFlexgrid.find('.flexigrid'), title: '', headerHTML: headerText, appendHTML: appendHTML, tableStyle: {'font-size': '6pt'}});

        break;

      default:

        return;
        break;
    }

    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }
  }
