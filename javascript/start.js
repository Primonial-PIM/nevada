$(function() {
	// Hide the menu
	$('#MenuDiv').hide();

	try {
		initialiseDefaults();
	} catch (e) {
	}

	// clear local storage

	try {
		localStorage.clear();
	} catch (e) {
	}

	// Get Folio details.
	
	

	

	// Make the context menu disappear when you exit it.
	$('#bodyDiv .fsContextMenu').mouseleave(function() {
		$(this).hide();
	});

	// Make the context div disappear when you exit it.
	$('#bodyDiv .fsContextDiv').mouseleave(function() {
		$(this).hide();
	});

	$(window).resize(function() {
		
	});
	
	setTimeout(checkPendingUrlCache, 5000);
	setTimeout(timedUpdates, TIMED_UPDATES_INTERVAL);

	/* Show login form */
	
	var dialogID;

	var resetToken = getQueryVariable('token');

	if ((resetToken !== false) && (resetToken.length > 15)) {
		/* reset Password */

		/* Show login form */

		document.reloginCurrentState = {
			DIALOGID : '',
			FUNCTION_FROM : ''
		};

		dialogID = showResetPassword(resetToken);

	} else {
		
		/* Normal Login */

		document.reloginCurrentState = {
			DIALOGID : '',
			FUNCTION_FROM : ''
		};

		dialogID = showLogin();

		// Set Login button action.

		var dialogDiv = $('#' + dialogID);
		dialogDiv.find("#okButton").unbind().click(login);

	}
	
});

function showResetInvalid() {
	try {
		var thisDialogType = document.dialogArray.dialogType.info;
		var dialogOptions = {
			title : "Requ\xEAte non valide",
			modal : true,
			closeOnEscape : false,
			captionButtons : {
				refresh : {
					visible : false
				},
				print : {
					visible : false
				},
				maximize : {
					visible : false
				},
				minimize : {
					visible : false
				}
			},
			height : 'auto',
			width : 'auto',
			resizable : false,
			position : 'center'
		};

		var dialogObject = getDialog(thisDialogType, dialogOptions);
		var dialogID = dialogObject.ID;
		var dialogDiv = $('#' + dialogID);

		// Initialise Window.

		setHtmlFromFile(dialogDiv[0], 'html/reset_invalid_dialog.html',
				'resetInvalidDiv', false, false, false, dialogID);

		// Show Dialog.
		dialogDiv.dialogExtend("open");

		$(".uibutton").button();
		// $('.errorMessage').hide();

		// Deal with forgotten password
		$("#okButton").click(function() {
			window.location="index.html";
		});
	} catch (e) {
		console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber
				+ ')');
	}

	return '';

}

function showResetPassword(resetToken) {
	try {

  var thisDialogType = document.dialogArray.dialogType.info;
		var dialogOptions = {
//			title : "R\351initialiser mot de passe",
			title : "Réinitialiser mot de passe",
			modal : true,
			closeOnEscape : false,
			captionButtons : {
				refresh : {
					visible : false
				},
				print : {
					visible : false
				},
				maximize : {
					visible : false
				},
				minimize : {
					visible : false
				}
			},
			height : 'auto',
			width : 'auto',
			resizable : false,
			position : 'center'
		};

		var dialogObject = getDialog(thisDialogType, dialogOptions);
		var dialogID = dialogObject.ID;
		var dialogDiv = $('#' + dialogID);

		// Initialise Window.

		setHtmlFromFile(dialogDiv[0], 'html/changePassword.html', 'loginDiv',
				false, false, false, dialogID);

		// Show Dialog.
		dialogDiv.dialogExtend("open");

    document.UserName = '';

		$(".uibutton").button();
		$('.hideable').hide();
		dialogDiv.find(".Email").val($("#resetEmail").html());
		dialogDiv.find(".Email").attr('disabled', true);
		dialogDiv.find(".Email").hide();

		$(".submitbutton").click(function() {
			var password1 = $(".PasswordNew1").val();
			var password2 = $(".PasswordNew2").val();
			var email = $("#resetEmail").html();
			var token = $("#resetToken").html();

			if (password1 != password2) {
				$(".errordiv").hide();
				$(".differentpasswords").show();
				return;
			}

			if (password1.length < 8) {
				$(".errordiv").hide();
				$(".passwordlength").show();
				return;
			}

			var formvar = {
				'password' : password1,
				'email' : email,
				'token' : token
			};

			var url = "php/forgot_password_reset.php";

			var formJSON = JSON.stringify(formvar);

			jQuery.ajax({
				type : "POST",
				url : url,
				data : {
					formdata : formJSON
				},
				async : false,
				success : function(html) {

					if (html.endsWith('OK'))
					{
						alert('Votre nouveau mot de passe a été enregistrée');
					}
					else
					{
						alert("Il n'a pas été possible de réinitialiser votre mot de passe");
					}

					window.location = "index.html";

				}
			});

		});

		$(".errordiv").click(function() {
			$(this).hide();
		});

	} catch (e) {
		console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber
				+ ')');
	}
}



