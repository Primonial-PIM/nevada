/*
 * User: npennin Date: 18/12/12
 *
 */
var savesearch;
var page;
var pagesize;
var savecurrentid;

function showUsers()
  {
  /*
    Initialise and show the System Status form.
   */
  try
    {
    var dialogID;
    var dialogDiv;
    var thisDialogType;
    var dialogOptions;
    var dialogObject;

    /* Create Dialog */

    thisDialogType = document.dialogArray.dialogType.holdings;
    dialogOptions = {title:"Users", width:"1000", height:"auto", resize:dialogHoldingsResizeStop, resizeStop:dialogHoldingsResizeStop, stateChanged:dialogHoldingsStateChange, open:dialogHoldingsOpen, close:dialogClose, closeOnEscape:false, captionButtons:{refresh:{visible:false}, print:{visible:true, click:fnSimpleGridPrint}, export:{visible:true, click:writeFromDialogGridToExcel}}};

    dialogObject = getDialog(thisDialogType, dialogOptions);
    dialogID = dialogObject.ID;
    dialogDiv = $('#' + dialogID);
    
    // Show Dialog
    
    $(dialogDiv).dialogExtend("open");
    
    usersFormShow(dialogID);
    
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }

  }


function usersFormShow(dialogID)
{

	setHtmlFromFile($('#'+dialogID)[0],'html/users.html','formDiv',false,false);
	
	$("#tabs").attr("id","tabs_"+dialogID);
	
	$("#tab-0").attr("id","tab-0_"+dialogID);
	
	$(".tab-0_tab").attr("href","#tab-0_"+dialogID);
	
	$("#search").attr("id","search_"+dialogID);
		
	$("#tabs_"+dialogID).tabs().css({
		'min-height':'400px',
		'overflow':"auto"
	});
		
	$('#'+dialogID+' .errorMessage').hide();
	
	// Remove any existing event handlers for the dialog
	
	$("#"+dialogID).off("click");
	$("#"+dialogID).off("keyup");
	$("#"+dialogID).off("change");
	
	// Deal with change in search box

    $("#"+dialogID).on("keyup",'#search_'+dialogID, function(){
        var dialogID=$(this).parents('div.dialogDiv').attr('id');
        checksearch(dialogID);
        //setTimeout("checksearch(dialogID)", 1000);
    });

    // Deal with tab changes


    // Deal with paging on tables

    $("#"+dialogID).on("click", ".pager", function() {
        var currentid = $(this).attr('id');

        var pageno=1;
        pageno=currentid;
        if (currentid=="First"){
            pageno=1;
        } 
        if (currentid=="Last"){
            pageno=999;
        }

        savesearch=$('#search_'+dialogID).val();
        var pagesize=10;
        
        pagesize=$('#tab-0_'+dialogID+' .recordspp').val();
        get_search(savesearch,pagesize,pageno, dialogID);
        
    });

    // Deal with a change in records per page on tables

    $("#"+dialogID).on("change", ".recordspp", function() {
       
        if (!regIsDigit($(this).val())){$(this).val(10);}

        savesearch=$('#search_'+dialogID).val();

        get_search(savesearch,$(this).val(),1,dialogID);
                    
        
    });

    
    // Deal with clicking on action icons in tables
    
    $("#"+dialogID).on("click",'.imgEdit', function(){
        var currentid = $(this).attr('id');
        var tabtype =$(this).parents('table').attr('id');
        //alert(tabtype);
        switch (tabtype)
                { 
            
                case  'user':
                	var myVar=setInterval(function(){showUser(currentid);clearInterval(myVar);},100);
                    break;
                             
                default:

                break;
                }
    });
    

    $("#"+dialogID).on("click", '.imgDelete', function() {
    	
        var currentid = $(this).attr('id');

        var formvar = {
                'user': currentid,
                'USERID':document.dakotaUserID,
        		'TOKEN':document.dakotaSessionKey
        };

        var url = "php/deleteUser.php";

        $.ajax({type: "POST", 
            url: url, 
            data: formvar,
            dataType: "json",
            success: function(json){ 
            	if (json.Status=='OK'){
            		savesearch="*";
            		checksearch(dialogID);
            	 } else {
            		 alert(json.Message);
            	 }
            }
        });
    });
    
    // initialise search
    
    savesearch="-";
    checksearch(dialogID);

}

function regIsDigit(fData){  
    var reg = new RegExp(/^\d+$/);  
    return (reg.test(fData));  
} 


function checksearch(dialogID){
    
    if (savesearch!=$('#search_'+dialogID).val()){
        savesearch=$('#search_'+dialogID).val();
        
        if ($("#tab-0_"+dialogID+' .recordspp').length>0){
            users_pagesize=$("#tab-0_"+dialogID+' .recordspp').val();
        } else {
            users_pagesize=10;
        }
        
        get_search(savesearch,users_pagesize,1,dialogID);
        
    }
}

function get_search(searchstring,pagesize,page, dialogID){
    
    
    var formvar = {
            'searchstring': searchstring,
            'pagesize': pagesize,
            'page': page,
            //'sort' : users_savesort,
            //'ascending':users_ascending,
            'USERID':document.dakotaUserID,
			'TOKEN':document.dakotaSessionKey
    };
        
    //var formJSON=JSON.encode(formvar);
    
    var url = "php/getUsers.php";
        
    $.ajax({type: "POST", 
         url: url,
         data: formvar,
         success: function(html){ 
             $("#tab-0_"+dialogID+" div.results").html(html);
             //$('.tab1link').html($('#searchresult1 div.info').html());
         }
    });    
}    



	
	
	
	
	
	
	




