/*
 * User: npennin Date: 18/12/12
 *
 */


function showFundBrowser()
  {
  /*
   Initialise and show the Fund Browser form.
   */
  try
    {
    var dialogID;
    var dialogDiv;
    var thisDialogType;
    var dialogOptions;
    var dialogObject;
    var rightHeaderDiv;

    /* Create Dialog */

    thisDialogType = document.dialogArray.dialogType.holdings;
    dialogOptions = {title: "Portfolio browse", resize: dialogHoldingsResizeStop, resizeStop: dialogHoldingsResizeStop, stateChanged: dialogHoldingsStateChange, open: dialogHoldingsOpen, close: dialogClose, closeOnEscape: false, captionButtons: {refresh: {visible: false}, print: {visible: true, click: fnSimpleGridPrint}, export: {visible: true, click: writeFromDialogGridToExcel}}};

    dialogObject = getDialog(thisDialogType, dialogOptions);
    dialogID = dialogObject.ID;
    dialogDiv = $('#' + dialogID);

    /* initialise cache item for this Dialog */

    var DialogCacheKey = MO_FUND_BROWSER_CACHE_PREFIX + dialogID;
    try
      {
      document.dataCache[DialogCacheKey] = {'folioID': -1};
      }
    catch (e)
      {
      }

    /* Initialise splitter dom object */

    var splitterDiv = $('<div id="' + dialogID + '_splitter" class="splitterDiv"><div id="' + dialogID + '_splitterLeft" class="leftPanel"><div id="' + dialogID + '_leftSuper"><div id="' + dialogID + '_list"></div></div></div><div id="' + dialogID + '_splitterRight" class="rightPanel"><div id="' + dialogID + '_rightHeading"></div><table id="' + dialogID + '_grid" class="gridTable"></table></div></div>');
    dialogDiv.append(splitterDiv);

    // Initialise Right Heading

    rightHeaderDiv = splitterDiv.find('#' + dialogID + '_rightHeading');
    setHtmlFromFile(rightHeaderDiv[0], 'html/moFundBrowserElements.html?dummy=' + getUniqueCounter().toString(), 'moCategoryFilter', false, false, setBrowserFilterEvents, dialogID); // setMoFilterEvents(dialogID));

    // Initialise Splitter control.

    splitterDiv.splitter({sizeLeft: 275, minRight: 150, dock: "dockLeft", sized: splitterHoldingsSized, panel2: {overflow: "hidden"}});

    // Initialise Folio (Fund) list.

    try
      {
      if (!document.dataCache.folioListArray)
        {
        getFolioListArray(setFolioList, (dialogID));
        }
      else if (document.dataCache.folioListArray.inProgress)
        {
        document.dataCache.folioListArray.callbacks.push({callback: setFolioList, parameters: (dialogID)});
        }
      else if (document.dataCache.folioListArray['data'].length <= 0)
        {
        getFolioListArray(setFolioList, (dialogID));
        }
      else
        {
        setFolioList(dialogID);
        }
      }
    catch (e)
      {
      console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
      }

    // Show Dialog.
    $(dialogDiv).dialogExtend("open");

    // Resize splitter, sizeLeft option does not appear to work. Fix it later?
    splitterDiv.trigger("resize", 260);

    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }

  }

function setBrowserFilterEvents(targetObject, dialogID)
  {
  /* having added the Risk Characteristic controls, initialise the control events as necessary
   *
   *
   */

  try
    {
    /* NAV Date */

    var pickers = $('#' + dialogID + ' .datePicker');
    pickers.datepicker({dateFormat: "yy-mm-dd", gotoCurrent: true });
    pickers.datepicker('setDate', new Date());
    pickers.change(fnDateChanged);

    $('#' + dialogID + ' a.PrevNext').click(fnPrevNextDatepicker);

    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }

  try
    {
    $('#' + dialogID + ' .CategoryGroup').hide();
    $('#' + dialogID + ' .CharacteristicGroup').hide();

    setCategoryCombo(dialogID, fbCategoryChanged);
    setCharacteristicsCombo(dialogID, undefined, fnCharacteristicChanged); // Clears Combo
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }

  try
    {
    /* Change event for 'Group by Category' check box. */

    $('#' + dialogID + ' .fbCategoryGroup').change(fnCategoryGroupCheckboxChanged);
    $('#' + dialogID + ' .fbTotalsAtTop').change(fnCategoryGroupCheckboxChanged);
    $('#' + dialogID + ' .btnExpand').on('click', fnExpandGrid);
    $('#' + dialogID + ' .btnColapse').on('click', fnColapseGrid);
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }
  }


function setCategoryCombo(dialogID, selectCallback)
  {
  /*

   Set the category combo(s) on the given dialog.
   Apply the give change callback.

   */


  try
    {
    var categoryCombo;
    var categoryList;
    var thisIndex;
    var DialogCacheKey = MO_FUND_BROWSER_CACHE_PREFIX + dialogID;
    var FundID = String(document.dataCache[DialogCacheKey]['folioID']);
    var sVal = FundID.split(":");
    var instance = "";
    if (sVal.length > 1)
      {
      instance = sVal[1];
      }

    categoryCombo = $('#' + dialogID + ' .fbCategoryCombo');
    categoryCombo.unbind();

    if (categoryCombo.length > 0)
      {
      categoryCombo.empty();

      categoryCombo.append("<option value='0'> </option>");

      var categoryCacheKey = 'tblRiskDataCategory_' + instance;

      if (categoryCacheKey in document.dataCache[DialogCacheKey])
        {
        categoryList = document.dataCache[DialogCacheKey][categoryCacheKey];
        }
      else
        {
        categoryList = getRenaissanceTable(document.dakotaUserID, document.dakotaSessionKey, instance, {'INSTANCE': instance}, 'tblRiskDataCategory', '', '', '', false, false);
        document.dataCache[DialogCacheKey][categoryCacheKey] = categoryList;
        }

      if (categoryList.length > 1)
        {
        categoryList.sort(function (a, b)
        {
        if (a['DataCategory'] < b['DataCategory']) return -1;
        if (a['DataCategory'] > b['DataCategory']) return 1;
        return 0
        });
        }

      for (thisIndex in categoryList)
        {
        categoryCombo.append("<option value='" + categoryList[thisIndex]['DataCategoryId'] + "'>" + categoryList[thisIndex]['DataCategory'] + "</option>");
        }

      if (selectCallback != undefined)
        {
        categoryCombo.change(selectCallback);
        }
      }
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }

  }

function fbCategoryChanged(e)
  {
  /*

   If a different Category is chosen, set the Characteristics combo appropriately and re-draw the grid.

   */
  try
    {
    var thisCombo = $(e.target);
    var mainCombo;
    var subCombo;
    var dialogID = thisCombo.parents('.dialogDiv').attr('id');

    mainCombo = $('#' + dialogID + ' .fbMainCategory');
    subCombo = $('#' + dialogID + ' .fbSubCategory');

    if (thisCombo.hasClass('fbMainCategory'))
      {
      if ((subCombo.val() == undefined) || (subCombo.val() <= 0))
        {
        setCharacteristicsCombo(dialogID, thisCombo, fnCharacteristicChanged);
        }
      }
    else if (thisCombo.hasClass('fbSubCategory'))
      {
      if ((subCombo.val() == undefined) || (subCombo.val() <= 0))
        {
        setCharacteristicsCombo(dialogID, mainCombo, fnCharacteristicChanged);
        }
      else
        {
        setCharacteristicsCombo(dialogID, thisCombo, fnCharacteristicChanged);
        }

      }

    drawFundBrowserGrid(dialogID);
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }
  }

function setCharacteristicsCombo(dialogID, categoryCombo, selectCallback)
  {
  /*

   Set the Characteristics combo(s) on the given dialog, select Characteristics only for the associated Category combo.
   Apply the give change callback.

   */

  try
    {
    // var categoryCombo;
    var characteristicCombo;
    var characteristicList;
    var thisIndex;
    var selectedCategory;
    var DialogCacheKey = MO_FUND_BROWSER_CACHE_PREFIX + dialogID;
    var FundID = String(document.dataCache[DialogCacheKey]['folioID']);
    var sVal = FundID.split(":");
    var instance = "";
    if (sVal.length > 1)
      {
      instance = sVal[1];
      }

    // categoryCombo = $('#' + dialogID + ' .fbCategoryCombo');

    if (categoryCombo != undefined)
      {
      selectedCategory = categoryCombo.val();
      }

    characteristicCombo = $('#' + dialogID + ' .fbCharacteristicCombo');
    characteristicCombo.unbind();

    characteristicCombo.empty();

    characteristicCombo.append("<option value='0'> </option>");

    var characteristicListCacheKey = 'tblRiskDataCharacteristic_' + instance;

    if (characteristicListCacheKey in document.dataCache[DialogCacheKey])
      {
      characteristicList = document.dataCache[DialogCacheKey][characteristicListCacheKey];
      }
    else
      {
      characteristicList = getRenaissanceTable(document.dakotaUserID, document.dakotaSessionKey, instance, {INSTANCE: instance}, 'tblRiskDataCharacteristic', '', '', '', false, false);
      document.dataCache[DialogCacheKey][characteristicListCacheKey] = characteristicList;
      }

    if (characteristicList.length > 1)
      {
      characteristicList.sort(function (a, b)
      {
      if (a['DataCharacteristic'] < b['DataCharacteristic']) return -1;
      if (a['DataCharacteristic'] > b['DataCharacteristic']) return 1;
      return 0
      });
      }

    for (thisIndex in characteristicList)
      {
      if (characteristicList[thisIndex]['DataCategoryId'] == selectedCategory)
        {
        characteristicCombo.append("<option value='" + characteristicList[thisIndex]['DataCharacteristicId'] + "'>" + characteristicList[thisIndex]['DataCharacteristic'] + "</option>");
        }
      }

    if (selectCallback != undefined)
      {
      characteristicCombo.change(selectCallback);
      }
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }

  }

function fnCharacteristicChanged(e)
  {
  /*
   If the chosen characteristic changes, re-draw the grid.
   */
  try
    {
    var dialogID = $(e.target).parents('.dialogDiv').attr('id');

    drawFundBrowserGrid(dialogID);
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }
  }

function setFolioList(dialogID)
  {

  var listID = dialogID + '_list';

  var list = $("#" + listID).nList({selectionMode: 'single', selected: function (e, data)
    {
    var dialogDiv = $('#' + dialogID);
    /* Establish Cache item for this Dialog and save the just-selected Folio ID in it. */

    var DialogCacheKey = MO_FUND_BROWSER_CACHE_PREFIX + dialogID;
    document.dataCache[DialogCacheKey]['folioID'] = data.item.value;

    /* Clear the Grid. */

    var grid = $("#" + dialogID + '_grid');
    if (grid.flexExist())
      {
      grid.flexAddData(); // Clear grid.
      }

    /* Set Dialog title. */

    dialogDiv.dialogExtend('option', 'title', 'Portfolio Browse : ' + data.item.label);

    /* Get the Folio holdings */

    getFolioAggregatedHoldings(data.item.value, drawFundBrowserGrid, dialogID);

    setCategoryCombo(dialogID, fbCategoryChanged);
    setCharacteristicsCombo(dialogID, undefined, fnCharacteristicChanged); // Clears Combo

    /* Reset Grid scroll position. */

    grid.flexScroll(0, 0);

    /* */

    RefreshTokenExpiry(document.dakotaUserID, document.dakotaSessionKey);

    }});

  // Pass the array list to the nList widget

  list.listSetItems(document.dataCache.folioListArray.data);

  }


function fnCategoryGroupCheckboxChanged(e)
  {
  /* When the 'Group by Category' check box is clicked... */
  try
    {
    var dialogID = $(this).parents('.dialogDiv').attr('id');
    var dialogDiv = $('#' + dialogID);

    var categorySelected = nz(dialogDiv.find('.fbCategoryCombo').val(), '0');
    var characteristicSelected = nz(dialogDiv.find('.fbCharacteristicCombo').val(), '0');

    if ($(e.target).hasClass('fbCategoryGroup'))
      {
      if ($(e.target).is(':checked'))
        {
        dialogDiv.find('.CategoryGroup').show();
        dialogDiv.find('.CharacteristicGroup').show();
        }
      else
        {
        dialogDiv.find('.CategoryGroup').hide();
        dialogDiv.find('.CharacteristicGroup').hide();
        }

      }

    // Redraw the grid if either combo box has a value.
    // If both are '0' then there is no difference to the grid if the check box is changed.

    if ((categorySelected != '0') || (characteristicSelected != '0'))
      {
      drawFundBrowserGrid(dialogID);
      }

    // Re-Size the grid to allow for the change in header group div visibility.

    fnResizeGrid(dialogID);

    RefreshTokenExpiry(document.dakotaUserID, document.dakotaSessionKey);

    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }
  }

function fnDateChanged(e)
  {
  /* Handle 'Date Changed' Event. */
  try
    {
    var dialogID = $(e.target).parents('.dialogDiv').attr('id');
    var DialogCacheKey = MO_FUND_BROWSER_CACHE_PREFIX + dialogID;
    var FundID = String(document.dataCache[DialogCacheKey]['folioID']);

    /* Clear the Grid. */

    var grid = $("#" + dialogID + '_grid');
    if (grid.flexExist())
      {
      grid.flexAddData(); // Clear grid.
      }

    if (FundID >= 0)
      {
      getFolioAggregatedHoldings(FundID, drawFundBrowserGrid, dialogID);
      }
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }
  }

function fnExpandGrid(e)
  {
  try
    {
    var dialogID = $(this).parents('.dialogDiv').attr('id');
    var gridID = dialogID + '_grid';
    var grid = $("#" + gridID);

    grid.flexExpandGrid();
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }
  }

function fnColapseGrid(e)
  {
  try
    {
    var dialogID = $(this).parents('.dialogDiv').attr('id');
    var gridID = dialogID + '_grid';
    var grid = $("#" + gridID);

    grid.flexColapseGrid();
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }
  }

/* Dialog Event functions.
 *
 *
 *
 *
 */

function dialogHoldingsOpen(e)
  {
  // Resize Dialog on Open. Ensures everything is the correct size.

  var data = {};

  if (!data.size) data.size = {height: e.target.clientHeight, width: e.target.clientWidth};
  if (!data.originalSize) data.originalSize = {height: 0, width: 0};

  dialogHoldingsResizeStop(e, data);

  }

function dialogClose(e)
  {

  }

function dialogHoldingsStateChange(e, data)
  {
  // Call resize code when the dialog is maximised or restored.
  var dialogID = e.target.id;

  switch (data.state)
  {

    case 'maximized':
      document.dialogMaximised[dialogID] = true;

      if (!data.size) data.size = {height: e.target.clientHeight, width: e.target.clientWidth};
      if (!data.originalSize) data.originalSize = {height: 0, width: 0};

      dialogHoldingsResizeStop(e, data);

      break;

    case 'normal':
      document.dialogMaximised[dialogID] = false;

      if (!data.size) data.size = {height: e.target.clientHeight, width: e.target.clientWidth};
      if (!data.originalSize) data.originalSize = {height: 0, width: 0};

      dialogHoldingsResizeStop(e, data);

      break;
  }

  }

function dialogHoldingsResizeStop(e, data)
  {

  // Exit if dialog has not resised.

  if ((data.size.height < 100) || ((data.originalSize.height == data.size.height) && (data.originalSize.width == data.size.width)))
    {
    return;
    }

  // Resize Splitter and refresh. // clientHeight

  var initialClientHeight = e.target.clientHeight;
  var initialClientWidth = e.target.clientWidth;
  var splitterDiv = $(e.target).find('.splitterDiv');
  var dialogID = e.target.id;

  if (DEBUG_FLAG)
    {
    console.log("dialogHoldingsResizeStop(" + dialogID + ")");
    console.log("  initialClientHeight = " + initialClientHeight + "");
    console.log("  initialClientWidth = " + initialClientWidth + "");
    }

  splitterDiv
    .css({height: e.target.clientHeight - 3, width: e.target.clientWidth - 3})
    .trigger("resize");


  // Do it again if scroll bars screwed it up..

  if ((initialClientHeight != e.target.clientHeight) || (initialClientWidth != e.target.clientWidth))
    {
    if (DEBUG_FLAG)
      {
      console.log("  retry:");
      console.log("  initialClientHeight = " + e.target.clientHeight + "");
      console.log("  initialClientWidth = " + e.target.clientWidth + "");
      }

    splitterDiv
      .css({height: e.target.clientHeight - 3, width: e.target.clientWidth - 3})
      .trigger("resize");
    }
  else
    {
    if (DEBUG_FLAG) console.log("  seems OK.");
    }

  // Resize Grid, if it exists. Fit to the right splitter panel.

  if (DEBUG_FLAG) console.log("  resize grid.");

  fnResizeGrid(dialogID);

  }


function splitterHoldingsSized(e)
  {
  // Resize the Grid control if the splitter div is moved.
  // Note, if the list is put in a super panel then that may need to be resized too.

  try
    {
    var dialogDiv = $(e.target).parents('.dialogDiv');

    if (dialogDiv[0])
      {
      var dialogID = dialogDiv[0].id;

      fnResizeGrid(dialogID);

      }
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }
  }

function fnResizeGrid(dialogID)
  {

  try
    {

    var splitterRight = $("#" + dialogID + '_splitterRight');
    var headingRight = $("#" + dialogID + ' #' + dialogID + '_rightHeading');
    var topGap = 0;

    if (splitterRight[0])
      {
      var fbGrid = $("#" + dialogID + '_grid');

      if (headingRight.length > 0)
        {
        topGap = headingRight.height();
        }

      if (fbGrid[0])
        {
        fbGrid.flexSize({width: (splitterRight[0].clientWidth), height: (splitterRight[0].clientHeight - topGap)});
        }

      }

    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }

  }

function holdingsGridRowFormatter(args)
  {

  var wg = $.addFlex;

  var compareFlags;
  try
    {
    if (args.state === wg.renderState.rendering)
      {
      if (args.type & wg.rowType.data)
        {
        if ((args.data) && (args.data['InstrumentISIN']))
          {
          // Real Data Row

          var rowTag = {
            FUND: args.data['Fund'],
            INSTRUMENT: args.data['Instrument'],
            ISIN: args.data['InstrumentISIN'],
            DESCRIPTION: args.data['InstrumentDescription']};

          args.$rows.dblclick(function (e)
          {
          dblClickShowTransactionsGrid(e, rowTag);
          });
          args.$rows.contextmenu(function (e)
          {
          e.preventDefault();
          rtClickShowTransactionsContext(e, rowTag);
          return false;
          });
          }
        else
          {
          // Disable context menu for header and footer rows.
          args.$rows.contextmenu(function (e)
          {
          e.preventDefault();
          return false;
          });
          }
        }
      else if (args.type & wg.rowType.header)
        {
        var rowTag = {
          DIALOGID: document.buildingDialogID};

        args.$rows.contextmenu(function (e)
        {
        e.preventDefault();
        rtClickShowColumnsContext(e, rowTag);
        return false;
        });

        }
      else
        {
        args.$rows.dblclick(function (e)
        {
        // Open / Close group line

        var grid = $(e.target).parents('.flexigrid table').eq(0);
        grid.flexToggleGroupRow(e.target);
        });

        // Disable context menu for header and footer rows.
        args.$rows.contextmenu(function (e)
        {
        e.preventDefault();
        return false;
        });

        if ((args.data) && (args.data['_treelevel']) && (args.data['_treelevel'] > 1))
          {
          args.$rows.css({background: document.appDefaults.gridColours.fundbrowser_characteristic_footerBarBackgroundSubGroup});
          }
        else
          {
          args.$rows.css({background: document.appDefaults.gridColours.fundbrowser_characteristic_footerBarBackground});
          }

        }
      }
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }

  }

function holdingsGridCellFormatter(args)
  {

  var wg = $.addFlex;
  var compareFlags;
  var dataRow;

  try
    {

    if (args.state & wg.renderState.rendering)
      {
      if (args.row.type & wg.rowType.data)
        {
        dataRow = args.row.data;

        if ((args['column'] != undefined))
          {
          if ((args['column']['dataKey'] == 'SignedUnits_Secondary') && (Math.abs(dataRow['SignedUnits'] - dataRow['SignedUnits_Secondary']) > 0.005))
            {
            args.$cell.css("background-color", MO_FUND_BROWSER_HIGHLIGHT_COLOUR);
            }
          }
        }
      }
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }

  }


function drawFundBrowserGrid(dialogID)
  {
  try
    {

    var DialogCacheKey = MO_FUND_BROWSER_CACHE_PREFIX + dialogID;
    var folioID = String(document.dataCache[DialogCacheKey]['folioID']);
    var navDate = $('#' + dialogID + ' .navDate').val();
    var dataCacheKey = 'FoAgHold_' + folioID + '_' + navDate;
    var dialogDiv = $('#' + dialogID);
    var firstPassFundData = [];
    var checkCategorySelected = dialogDiv.find('.fbCategoryGroup').is(':checked');
    var MainCategorySelected = nz(dialogDiv.find('.fbMainCategory').val(), '0');
    var SubCategorySelected = nz(dialogDiv.find('.fbSubCategory').val(), '0');

    var characteristicSelected = nz(dialogDiv.find('.fbCharacteristicCombo').val(), '0');
    var characteristicIndex;
    var positionIndex;

    if (MainCategorySelected.length > 0)
      {
      MainCategorySelected = parseInt(MainCategorySelected);
      }

    if (SubCategorySelected.length > 0)
      {
      SubCategorySelected = parseInt(SubCategorySelected);

      // If MainCategory is not selected, Substitute Sub category.

      if ((SubCategorySelected > 0) && (MainCategorySelected <= 0))
        {
        MainCategorySelected = SubCategorySelected;
        SubCategorySelected = 0;
        }
      }

    if (characteristicSelected.length > 0)
      {
      characteristicSelected = parseInt(characteristicSelected);
      }

    /* Clear grid and exit if no Folio has been selected. */

    var grid = $("#" + dialogID + '_grid');
    if (grid.flexExist())
      {
      grid.flexAddData(); // Clear grid.
      }

    /* Draw grid dependent on what options are selected */

    if (checkCategorySelected && ((MainCategorySelected != 0) || (characteristicSelected != 0)))
      {
      var fundData = document.dataCache[dataCacheKey]['data'];

      var mainCategoryData = getCategoryData(folioID, MainCategorySelected, true, false);
      var subCategoryData = undefined;
      var LastPositionIndex = -1;

      if (SubCategorySelected > 0)
        {
        /* Sub Category selected */
        subCategoryData = getCategoryData(folioID, SubCategorySelected, true, false);

        for (positionIndex = 0; positionIndex < fundData.length; positionIndex++)
          {
          for (characteristicIndex = 0; characteristicIndex < mainCategoryData.length; characteristicIndex++)
            {
            if ((fundData[positionIndex]['Instrument'] == mainCategoryData[characteristicIndex]['InstrumentID']) && (mainCategoryData[characteristicIndex]['DataWeighting'] != 0))
              {
              firstPassFundData.push($.extend(true, {}, fundData[positionIndex], {MainDataCategory: mainCategoryData[characteristicIndex]['DataCharacteristic'], MainDataCategoryId: mainCategoryData[characteristicIndex]['DataCharacteristicId'], MainDataWeighting: mainCategoryData[characteristicIndex]['DataWeighting']}));
              LastPositionIndex = positionIndex;
              }
            }

          if (positionIndex != LastPositionIndex)
            {
            // Line not added, no categoryvalues for this line...
            firstPassFundData.push($.extend(true, {}, fundData[positionIndex], {MainDataCategory: 'No Category', MainDataCategoryId: 0, MainDataWeighting: 1}));
            }
          }
        }
      else
        {
        /* No Sub Category selected */
        subCategoryData = mainCategoryData;
        firstPassFundData = fundData;
        }

      fundData = firstPassFundData
      var gridData = [];
      var instrumentsExist = {};

      /* What to do now ??

       OK, we have the folio data :
       and the Category / Characteristics data : mainCategoryData
       now create the appropriate data array and display the grid.

       */


      if (characteristicSelected != 0)
        {
        /* Group by Characteristic. */

        for (characteristicIndex = 0; characteristicIndex < subCategoryData.length; characteristicIndex++)
          {
          if (subCategoryData[characteristicIndex]['DataCharacteristicId'] == characteristicSelected)
            {
            for (positionIndex = 0; positionIndex < fundData.length; positionIndex++)
              {
              if (fundData[positionIndex]['Instrument'] == subCategoryData[characteristicIndex]['InstrumentID'])
                {
                gridData.push($.extend(true, {}, subCategoryData[characteristicIndex], fundData[positionIndex], {_catsort: 0, characteristicValue: (fundData[positionIndex]['EURValue'] * nz(fundData[positionIndex]['MainDataWeighting'], 1) * subCategoryData[characteristicIndex]['DataWeighting']), characteristicFundWeight: (fundData[positionIndex]['FUND_WEIGHT'] * subCategoryData[characteristicIndex]['DataWeighting'])}));
                instrumentsExist[fundData[positionIndex]['Instrument']] = true;
                }
              }
            }
          }

        /* Add Fund positions without the required characteristic to a final 'No Characteristics' characteristic. */

        for (positionIndex = 0; positionIndex < fundData.length; positionIndex++)
          {
          if (!(fundData[positionIndex]['Instrument'] in instrumentsExist))
            {
            gridData.push($.extend(true, {}, fundData[positionIndex], {DataCategory: 'No Category', DataCategoryId: 0, DataCharacteristic: 'Others', DataCharacteristicId: 0, DataWeighting: 0, InstrumentID: fundData[positionIndex]['Instrument'], RiskDataID: 0, isDefault: 0, isFixed: 0, _catsort: 1, characteristicValue: 0, characteristicFundWeight: 0}));
            }
          }

        }
      else
        {
        /* Group by Category. */

        if (subCategoryData !== false)
          {
          subCategoryData.sort(function (a, b)
          {
          if (a['DataCategory'] < b['DataCategory']) return -1;
          if (a['DataCategory'] > b['DataCategory']) return 1;
          return 0
          });

          for (characteristicIndex = 0; characteristicIndex < subCategoryData.length; characteristicIndex++)
            {
            for (positionIndex = 0; positionIndex < fundData.length; positionIndex++)
              {
              if (fundData[positionIndex]['Instrument'] == subCategoryData[characteristicIndex]['InstrumentID'])
                {
                gridData.push($.extend(true, {}, subCategoryData[characteristicIndex], fundData[positionIndex], {_catsort: 0, characteristicValue: (fundData[positionIndex]['EURValue'] * nz(fundData[positionIndex]['MainDataWeighting'], 1) * subCategoryData[characteristicIndex]['DataWeighting']), characteristicFundWeight: (fundData[positionIndex]['FUND_WEIGHT'] * subCategoryData[characteristicIndex]['DataWeighting'])}));
                instrumentsExist[fundData[positionIndex]['Instrument']] = true;
                }
              }
            }
          }

        /* Add Fund positions with no characteristics to a final 'No Characteristics' characteristic. */

        for (positionIndex = 0; positionIndex < fundData.length; positionIndex++)
          {
          if (!(fundData[positionIndex]['Instrument'] in instrumentsExist))
            {
            gridData.push($.extend(true, {}, fundData[positionIndex], {DataCategory: 'No Category', DataCategoryId: 0, DataCharacteristic: 'No Characteristic', DataCharacteristicId: 0, DataWeighting: 0, InstrumentID: fundData[positionIndex]['Instrument'], RiskDataID: 0, isDefault: 0, isFixed: 0, _catsort: 1, characteristicValue: 0, characteristicFundWeight: 0}));
            }
          }

        }

      setFolioHoldingsByCategoryGrid(dialogID, characteristicSelected, SubCategorySelected, gridData);

      }
    else
      {
      setFolioHoldingsGrid(dataCacheKey, dialogID);
      }

    RefreshTokenExpiry(document.dakotaUserID, document.dakotaSessionKey);

    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }

  }

function setFolioHoldingsByCategoryGrid(dialogID, characteristicSelected, SubCategorySelected, myData)
  {
  /* Function to draw the Holdings grid when the 'Show by Category' options are selected.
   *
   *  */

  var dialogDiv = $('#' + dialogID);
  var gridID = dialogID + '_grid';
  var splitterRightID = dialogID + '_splitterRight';
  var grid = $("#" + gridID);
  var defaultInitialColumnOrder;
  var defaultColumnDefinitions;
  var defaultColumnDefinition;
  var checkTotalsAtTopSelected = dialogDiv.find('.fbTotalsAtTop').is(':checked');

  try
    {
    if (!document.appDefaults) document.appDefaults = {};

    if (SubCategorySelected > 0)
      {
      defaultInitialColumnOrder = document.appDefaults.holdingGridDefaults.initialSubCharacteristicColumnOrder;
      defaultColumnDefinitions = document.appDefaults.holdingGridDefaults;
      defaultColumnDefinition = document.appDefaults.holdingGridDefaults.DEFAULT;
      }
    else
      {
      defaultInitialColumnOrder = document.appDefaults.holdingGridDefaults.initialCharacteristicColumnOrder;
      defaultColumnDefinitions = document.appDefaults.holdingGridDefaults;
      defaultColumnDefinition = document.appDefaults.holdingGridDefaults.DEFAULT;
      }

    // Get / Set Column definition array

    var colArray = new Array();

    var knownColumnOrder = defaultInitialColumnOrder;
    var colsAlreadySet = {};
    var thisColumnName;

    // OK Set the columns I know about, then sweep up the rest.
    if (myData.length == 0)
      {
      myData.push({Status: "There are no holdings in this fund."});
      }

    if (knownColumnOrder)
      {
      for (var i = 0; i < knownColumnOrder.length; i++)
        {
        try
          {
          if (!knownColumnOrder[i]) continue;

          thisColumnName = knownColumnOrder[i];

          if (thisColumnName in myData[0])
            {
            if (thisColumnName in defaultColumnDefinitions)
              {
              colArray.push(defaultColumnDefinitions[thisColumnName]);
              }

            colsAlreadySet[thisColumnName] = true;
            }
          }
        catch (e)
          {
          console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
          }
        }
      }

    // Sweep up columns I do not cater for specifically

    for (var i in myData[0])
      {
      try
        {
        thisColumnName = i;
        var thisValue = myData[0][i];

        if (!colsAlreadySet[thisColumnName])
          {
          if (thisColumnName in defaultColumnDefinitions)
            {
            // Col has a template, but is not in the Ordered Cols set.
            colArray.push(defaultColumnDefinitions[thisColumnName]);
            }
          else
            {
            var thisColumnFormat = {headerText: i, dataKey: i, width: 100};

            if (isFinite(thisValue))
              {
              thisColumnFormat['dataType'] = "number";
              }
            else
              {
              thisColumnFormat['dataType'] = "string";
              }

            var thisCol = jQuery.extend(thisColumnFormat, defaultColumnDefinition);

            colArray.push(thisCol);
            }

          colsAlreadySet[thisColumnName] = true;
          }
        }
      catch (e)
        {
        console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
        }
      }

    /* */

    document.buildingDialogID = dialogID;

    if (SubCategorySelected > 0)
      {
      grid.flexigrid({
        dataType: 'json',
        jsArray: myData,
        colModel: colArray,
        rowStyleFormatter: holdingsGridRowFormatter,
        cellStyleFormatter: holdingsGridCellFormatter,
        sortall: true,
        treeView: [
          {groupby: 'MainDataCategory', sortorder: 1, display: 'MainDataCategory', clearfield: true},
          {groupby: '_catsort', sortonly: true},
          {groupby: 'DataCharacteristic', sortorder: 1, display: 'DataCharacteristic', clearfield: true}
        ],
        treeTotalsAtStart: (checkTotalsAtTopSelected ? true : false),
        width: 100,
        height: 100});
      }
    else
      {
      grid.flexigrid({
        dataType: 'json',
        jsArray: myData,
        colModel: colArray,
        rowStyleFormatter: holdingsGridRowFormatter,
        cellStyleFormatter: holdingsGridCellFormatter,
        sortall: true,
        treeView: [
          {groupby: '_catsort', sortonly: true},
          {groupby: 'DataCharacteristic', sortorder: 1, display: 'DataCharacteristic', clearfield: true}
        ],
        treeTotalsAtStart: (checkTotalsAtTopSelected ? true : false),
        width: 100,
        height: 100});
      }


    var splitterRight = $("#" + splitterRightID);
    if (splitterRight[0])
      {
      fnResizeGrid(dialogID);
      }

    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }
  }


function setFolioHoldingsGrid(cacheKey, dialogID)
  {
  var gridID = dialogID + '_grid';
  var splitterRightID = dialogID + '_splitterRight';
  var cacheObject = document.dataCache[cacheKey];

  var grid = $("#" + gridID);

  if (!document.appDefaults) document.appDefaults = {};

  var myData = cacheObject['data'];

  // Get / Set Column definition array

  var colArray = new Array();

  if ((document.appDefaults['holdingGridColumns']) && (document.appDefaults['holdingGridColumns'].length > 2))
    {
    // If I've done this before, use that....

    colArray = document.appDefaults.holdingGridColumns;
    }
  else
    {
    // Build Array to set Grid Colummn properties.

    // Ensure that default object exists and is OK.

    if (!document.appDefaults.holdingGridDefaults) document.appDefaults.holdingGridDefaults = {initialColumnOrder: false};
    if (!document.appDefaults.holdingGridDefaults.DEFAULT) document.appDefaults.holdingGridDefaults.DEFAULT = {visible: true};

    var knownColumnOrder = document.appDefaults.holdingGridDefaults.initialColumnOrder;
    var colsAlreadySet = {};
    var thisColumnName;

    // OK Set the columns I know about, then sweep up the rest.
    if (myData.length == 0)
      {
      myData.push({Status: "There are no holdings in this fund."});
      }

    if (knownColumnOrder)
      {
      for (var i = 0; i < knownColumnOrder.length; i++)
        {
        try
          {
          if (!knownColumnOrder[i]) continue;

          thisColumnName = knownColumnOrder[i];

          if (thisColumnName in myData[0])
            {
            if (thisColumnName in document.appDefaults.holdingGridDefaults)
              {
              colArray.push(document.appDefaults.holdingGridDefaults[thisColumnName]);
              }
            else
              {
              colArray.push(document.appDefaults.holdingGridDefaults['DEFAULT']);
              }

            colsAlreadySet[thisColumnName] = true;
            }
          }
        catch (e)
          {
          console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
          }
        }
      }

    // Sweep up columns I do not cater for specifically

    for (var i in myData[0])
      {
      try
        {
        thisColumnName = i;
        var thisValue = myData[0][i];

        if (!colsAlreadySet[thisColumnName])
          {
          if (thisColumnName in document.appDefaults.holdingGridDefaults)
            {
            // Col has a template, but is not in the Ordered Cols set.
            colArray.push(document.appDefaults.holdingGridDefaults[thisColumnName]);
            }
          else
            {
            var thisColumnFormat = {headerText: i, dataKey: i, width: 100};

            if (isFinite(thisValue))
              {
              thisColumnFormat['dataType'] = "number";
              }
            else
              {
              thisColumnFormat['dataType'] = "string";
              }

            var thisCol = jQuery.extend(thisColumnFormat, document.appDefaults.holdingGridDefaults['DEFAULT']);

            colArray.push(thisCol);
            }

          colsAlreadySet[thisColumnName] = true;
          }
        }
      catch (e)
        {
        console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
        }
      }

    // Set Column details for next time.

    document.appDefaults.holdingGridColumns = colArray;
    }

  // Setup caching of data for instruments that could be of interest
  //
  try
    {
    var dataRow;
    var index;

    if (myData.length > 0)
      {
      // Clear pending URL cache.

      document.pendingUrlCache = [];

      // Aim to add all these instruments to the Page cache.

      for (index in myData)
        {
        dataRow = myData[index];

        if ((nz(dataRow['ISIN'], "").length > 5) && (Math.abs(nz(dataRow['EURO_VALUE'], 0)) > 10))
          {
          preloadMsSnapshotPageFromISIN(dataRow['ISIN'], "http://www.morningstar.fr/fr/snapshot/p_snapshot.aspx/?id=<MSCODE>", "msPrintSnapshot")
          }
        }
      }
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }

  document.buildingDialogID = dialogID;

  //  if ((!grid.flexExist()) || (grid.flexGet('colModel').length <= 2))
  //    {
  grid.flexigrid({
    dataType: 'json',
    jsArray: myData,
    colModel: colArray,
    rowStyleFormatter: holdingsGridRowFormatter,
    cellStyleFormatter: holdingsGridCellFormatter,
    sortall: true,
    treeView: false,
    width: 100,
    height: 100});
  //    }
  //  else
  //    {
  //    // grid.flexOptions({colModel:colArray});
  //    // grid.flexReload({jsArray:myData});
  //    grid.flexOptions({treeView:false});
  //    grid.flexAddData({jsArray:myData});
  //
  //    /* // Don't need to re-build, the columns don't change.... */
  //    }

  var splitterRight = $("#" + splitterRightID);
  if (splitterRight[0])
    {
    fnResizeGrid(dialogID);
    }

  }

function holdingGridColumnEvent(e, args)
  {
  try
    {
    var dialogID = $(this).parents('.dialogDiv').attr('id');

    moveOrSizeGridColumnEvent(e, args, (dialogID + '_grid'), document.appDefaults.holdingGridColumns);

    // folioTransactionsSaveColumnInfo(dialogID);
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }

  }

function setFolioTransactionsGrid(cacheKey, pParameters)
  {
  var dialogID = pParameters['dialogID'];
  var gridID = dialogID + '_grid';
  var cacheObject = document.dataCache[cacheKey];

  var grid = $("#" + gridID);

  if (!document.appDefaults) document.appDefaults = {};

  var myData = cacheObject['data'];

  // Get / Set Column definition array

  var colArray = new Array();

  if ((myData.length > 0) && (document.appDefaults['transactionsGridColumns']) && (document.appDefaults['transactionsGridColumns'].length > 2))
    {
    // If I've done this before, use that....

    colArray = document.appDefaults.transactionsGridColumns;
    }
  else
    {
    // Build Array to set Grid Column properties.

    // Ensure that default object exists and is OK.

    if (!document.appDefaults.transactionsGridDefaults) document.appDefaults.transactionsGridDefaults = {};
    if (!document.appDefaults.transactionsGridDefaults.DEFAULT) document.appDefaults.transactionsGridDefaults.DEFAULT = {visible: true};
    if (!document.appDefaults.transactionsGridDefaults.initialColumnOrder) document.appDefaults.transactionsGridDefaults.initialColumnOrder = [];

    var knownColumnOrder = document.appDefaults.transactionsGridDefaults.initialColumnOrder;
    var colsAlreadySet = {};
    var thisColumnName;

    // OK Set the columns I know about, then sweep up the rest.
    if (myData.length == 0)
      {
      myData.push({Status: "There are no transactions to show."});
      }

    if (knownColumnOrder)
      {
      for (var i = 0; i < knownColumnOrder.length; i++)
        {
        try
          {
          if (!knownColumnOrder[i]) continue;

          thisColumnName = knownColumnOrder[i];

          if (myData[0][thisColumnName])
            {
            if (document.appDefaults.transactionsGridDefaults[thisColumnName])
              {
              colArray.push(document.appDefaults.transactionsGridDefaults[thisColumnName]);
              }
            else
              {
              colArray.push(document.appDefaults.transactionsGridDefaults['DEFAULT']);
              }

            colsAlreadySet[thisColumnName] = true;
            }
          }
        catch (e)
          {
          console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
          }
        }
      }

    // Sweep up columns I do not cater for specifically

    for (var i in myData[0])
      {
      try
        {
        thisColumnName = i;
        var thisValue = myData[0][i];

        if (!colsAlreadySet[thisColumnName])
          {
          if (document.appDefaults.transactionsGridDefaults[thisColumnName])
            {
            // Col has a template, but is not in the Ordered Cols set.
            colArray.push(document.appDefaults.transactionsGridDefaults[thisColumnName]);
            }
          else
            {
            var thisColumnFormat = {headerText: i, dataKey: i, width: 100};

            if (isFinite(thisValue))
              {
              thisColumnFormat['dataType'] = "number";
              }
            else
              {
              thisColumnFormat['dataType'] = "string";
              }

            var thisCol = jQuery.extend(thisColumnFormat, document.appDefaults.transactionsGridDefaults['DEFAULT']);

            colArray.push(thisCol);
            }

          colsAlreadySet[thisColumnName] = true;
          }
        }
      catch (e)
        {
        console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
        }
      }

    // Set Column details for next time.

    document.appDefaults.transactionsGridColumns = colArray;
    }

  document.buildingDialogID = dialogID;

  if ((!grid.flexExist()) || (grid.flexGet('colModel').length <= 2))
    {
    grid.flexigrid({
      dataType: 'json',
      jsArray: myData,
      colModel: colArray,
      sortall: true,
      width: 100,
      height: 100})
    }
  else
    {
    // grid.flexOptions({colModel:colArray});
    // grid.flexReload({jsArray:myData});
    grid.flexAddData({jsArray: myData});

    /* // Don't need to re-build, the columns don't change.... */
    }

  var dialogObject = $("#" + dialogID);

  if (dialogObject.length > 0)
    {
    grid.flexSize({width: (dialogObject[0].clientWidth), height: (dialogObject[0].clientHeight)});
    }
  }

function folioTransactionsGridColumnEvent(e, args)
  {
  try
    {
    var dialogID = $(this).parents('.dialogDiv').attr('id');

    moveOrSizeGridColumnEvent(e, args, (dialogID + '_grid'), document.appDefaults.transactionsGridColumns);

    // folioTransactionsSaveColumnInfo(dialogID);
    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }

  }

function getFolioTransactions(rowTag, FundID, customCallback, customParameters)
  {
  /*

   */

  var folioID;
  var SICOVAM;
  var InstrumentDescription;
  var index;

  FundID = String(FundID);
  var sVal = FundID.split(":");
  var instance = "";
  if (sVal.length > 1)
    {
    instance = sVal[1];
    }

  if (!rowTag)
    {
    rowTag = {};
    }

  folioID = (rowTag['FUND'] ? rowTag['FUND'] : 1);
  SICOVAM = (rowTag['INSTRUMENT'] ? rowTag['INSTRUMENT'] : 1);
  InstrumentDescription = (rowTag['DESCRIPTION'] ? rowTag['DESCRIPTION'] : 'No Description');

  var cacheKey = 'FoTrans_' + folioID + '_' + SICOVAM;

  if (!document.dataCache[cacheKey])
    {
    document.dataCache[cacheKey] = {inProgress: false};
    }

  var cacheObject = document.dataCache[cacheKey];

  cacheObject['inProgress'] = true;
  cacheObject['ID'] = folioID + '_' + SICOVAM;

  if (!cacheObject['callbacks'])
    {
    cacheObject['callbacks'] = [];
    }

  if (customCallback)
    {
    cacheObject['callbacks'].push({callback: customCallback, parameters: customParameters});
    }

  // Establish Query patameters object

  var formvar =
  {
    'FUNDID': FundID
  };


  // Get data.
  // Venice (Funds) data query.

  var rVal = getRenaissanceTable(document.dakotaUserID, document.dakotaSessionKey, instance, formvar, 'tblTransaction', '', '((TransactionFund=' + folioID.toString() + ') AND (TransactionInstrument=' + SICOVAM.toString() + '))', '', false, false);

  var dataItem = rVal.slice();

  for (index in dataItem)
    {
    dataItem[index]['InstrumentDescription'] = InstrumentDescription;
    }


  cacheObject['updateTime'] = new Date();
  cacheObject['data'] = dataItem;
  cacheObject['inProgress'] = false;

  if (cacheObject['callbacks'])
    {
    if (cacheObject['callbacks'].length > 0)
      {
      for (var thisCallback in cacheObject['callbacks'])
        {
        try
          {
          if (thisCallback)
            {
            cacheObject['callbacks'][thisCallback].callback(cacheKey, cacheObject['callbacks'][thisCallback].parameters);
            }
          }
        catch (e)
          {
          console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
          }
        }

      // Clear callback array.
      cacheObject['callbacks'] = [];
      }
    }

  }

function getCategoryData(folioID, categorySelected, pOnlyActiveCategories, pAsync, customCallback, customParameters)
  {
  var cacheObject;

  try
    {
    var cacheKey = CACHE_CATEGORY_PREFIX + folioID + "_" + categorySelected + (pOnlyActiveCategories ? '_Active' : '');

    if (!document.dataCache[cacheKey])
      {
      document.dataCache[cacheKey] = {inProgress: false, data: false, callbacks: []};
      }

    cacheObject = document.dataCache[cacheKey];

    if (cacheObject.data !== false)
      {
      return cacheObject.data;
      }

    }
  catch (e)
    {
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }

  try
    {
    cacheObject.inProgress = true;

    if (customCallback)
      {
      cacheObject['callbacks'].push({callback: customCallback, parameters: customParameters});
      }

    var FundID = String(folioID);
    var sVal = FundID.split(":");
    var instance = "";
    if (sVal.length > 1)
      {
      instance = sVal[1];
      }

    // Establish Query parameters object

    var formvar = {
      'QUERYTYPE': 'characteristicdata',
      'USERID': document.dakotaUserID,
      'TOKEN': document.dakotaSessionKey,
      'DATACATEGORYID': categorySelected.toString(),
      'FUNDID': folioID,
      'INSTANCE': instance,
      'WHERE': (pOnlyActiveCategories ? '((RiskDataID > 0) AND (DataWeighting <> 0))' : '')
    };

    var url = "php/getRenaissanceData.php";

    $.ajax({type: "POST",
      url: url,
      data: formvar,
      async: pAsync,
      cacheKey: cacheKey,
      instance: instance,
      success: function (html)
        {
        var cacheObject = document.dataCache[this.cacheKey];

        try
          {
          var rVal = JSON.parse(html);

          /* Set Category Names */

          if (rVal.length > 0)
            {
            var categoryList = getRenaissanceTable(document.dakotaUserID, document.dakotaSessionKey, this['instance'], {INSTANCE: this['instance']}, 'tblRiskDataCategory', '', '', '', false, false);

            for (var rvalIndex in rVal)
              {
              for (var categoryIndex in categoryList)
                {
                if (rVal[rvalIndex]['DataCategoryId'] == categoryList[categoryIndex]['DataCategoryId'])
                  {
                  rVal[rvalIndex]['DataCategory'] = categoryList[categoryIndex]['DataCategory'];
                  break;
                  }
                }
              }
            }

          /* Save data to cache */

          cacheObject['updateTime'] = new Date();
          cacheObject['data'] = rVal;
          cacheObject['inProgress'] = false;

          if (cacheObject['callbacks'])
            {
            if (cacheObject.callbacks.length > 0)
              {
              for (var thisCallback in cacheObject.callbacks)
                {
                try
                  {
                  if (thisCallback)
                    {
                    cacheObject.callbacks[thisCallback].callback(cacheObject.callbacks[thisCallback].parameters);
                    }
                  }
                catch (e)
                  {
                  console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
                  }
                }

              // Clear callback array.
              cacheObject.callbacks = [];
              }
            }

          }
        catch (e)
          {
          cacheObject.data = false;
          console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
          }
        finally
          {
          cacheObject.inProgress = false;
          }

        }
    });

    }
  catch (e)
    {
    cacheObject.data = false;
    console.log(e.message + ', ' + e.fileName + '(line, ' + e.lineNumber + ')');
    }
  finally
    {
    if (cacheObject.inProgress == false)
      {
      return cacheObject.data;
      }
    }
  }


function dblClickShowTransactionsGrid(e, rowTag)
  {
  // Code for DoubleClick on the transactions grid.
  // Will bring up a window with Instrument transactions on it.

  var dialogID = $(e.target).parents('.dialogDiv').attr('id');
  var DialogCacheKey = MO_FUND_BROWSER_CACHE_PREFIX + dialogID;
  var FundID = String(document.dataCache[DialogCacheKey]['folioID']);

  document.currentContextData = rowTag;

  var dialogID = (-1);
  dialogOptions = {title: "Transactions : " + rowTag['DESCRIPTION'], resizeStop: dialogTransactionsResizeStop, stateChanged: dialogTransactionsStateChange, open: dialogTransactionsOpen, close: dialogClose, closeOnEscape: false, captionButtons: {print: {visible: true, click: fnSimpleGridPrint}, export: {visible: true, click: writeFromDialogGridToExcel}}};

  dialogObject = getDialog(document.dialogArray.dialogType.transactions, dialogOptions);
  dialogID = dialogObject.ID;
  dialogDiv = $('#' + dialogID);

  var gridDiv = $('<table id="' + dialogID + '_grid" class="gridTable"></table>');
  dialogDiv.append(gridDiv);

  getFolioTransactions(rowTag, FundID, setFolioTransactionsGrid, {'dialogID': dialogID, 'rowTag': rowTag, 'FundID': FundID});

  $(dialogDiv).dialogExtend("open");

  RefreshTokenExpiry(document.dakotaUserID, document.dakotaSessionKey);

  }

function rtClickShowTransactionsContext(e, rowTag)
  {
  // Code for right click event on the portfolio window.
  // Will bring up a context window giving various options to display furthere
  // data.

  document.currentContextData = rowTag;
  e.preventDefault();

  // Only a single context menu is used for the Transactions Grids.

  var menu = $('#bodyDiv .fsContextMenu');
  var menuList = $('#bodyDiv .fsContextMenu ul');

  // Clear existing entries.

  menuList.empty();

  // Build menu options.
  // At present there are not any options that do not require the ISIN code...

  var thisISIN = nz(rowTag['ISIN'], '');

  if (thisISIN.length > 0)
    {
    menuList.append('<li><a class="showMsPortfolioOverview">Portfolio Overview</a></li>');
    menuList.append('<li><a class="showMsChart">Morningstar Chart</a></li>');
    menuList.append('<li><a class="showMsSummary">Morningstar Summary</a></li>');
    menuList.append('<li><a class="showMsPerformance">Morningstar Performance</a></li>');
    menuList.append('<li><a class="showMsHoldings">Morningstar Top Holdings</a></li>');
    menuList.append('<li><a class="showMsContactDetails">Morningstar Contact Details</a></li>');
    menuList.append('<li><a class="showMsDocuments">Morningstar Documents</a></li>');
    }
  else
    {
    menuList.append('<li><a class="">Sorry, no ISIN is set.</a></li>');
    }

  // Set callbacks according to class.

  menuList.find('.showMsChart').click(function ()
  {
  transactionsContextClick(e, 1, rowTag)
  });

  menuList.find('.showMsSummary').click(function ()
  {
  transactionsContextClick(e, 2, rowTag)
  });

  menuList.find('.showMsPerformance').click(function ()
  {
  transactionsContextClick(e, 3, rowTag)
  });

  menuList.find('.showMsHoldings').click(function ()
  {
  transactionsContextClick(e, 4, rowTag)
  });

  menuList.find('.showMsContactDetails').click(function ()
  {
  transactionsContextClick(e, 5, rowTag)
  });

  menuList.find('.showMsDocuments').click(function ()
  {
  transactionsContextClick(e, 6, rowTag)
  });

  menuList.find('.showMsPortfolioOverview').click(function ()
  {
  transactionsContextClick(e, 7, rowTag)
  });

  // position and show window.

  menu.css({top: e.pageY - 10, left: e.pageX - 10, 'z-index': 20000});

  menu.show();

  return false;
  }

